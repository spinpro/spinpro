package de.fraunhofer.iosb.spinpro.service.ifelse;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.condlib.ConditionParser;
import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.condlib.IConditionParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.Collection;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A complex service, that recognizes an if-then-statement.
 * An IfAction is used as output.
 * The input has to have the form: "if|when|once [condition possibly with input variables] then|next|do [nested actions]"
 * The terms that are available in the condition are defined in {@link OperatorProvider}.
 * Note: Nested actions might need to be surrounded by a begin and end word.
 */
@Service(author = "Jochen Mueller", name = "IfServiceC - EN", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "39dcc5435f5c55990b3b67a4469a94583d79739291d3220cb03412132bf02db6")
public class ComplexIfService implements IComplexActionService {

  private static final String IF_THEN = "(?:if|when|once)\\s(.*)\\s(?:then|next|do)";
  private static final double TOLERANCE = 0.8;

  @Override
  public IAction execute(String[] input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    if (input.length != 2) {
      throw new ParsingException("Wrong match chosen");
    }
    Pattern pattern = Pattern.compile("^" + IF_THEN + "$");
    Matcher matcher = pattern.matcher(input[0]);
    if (!matcher.matches()) {
      throw new ParsingException("Wrong match chosen");
    }

    String condition = matcher.group(1).trim();
    IConditionParser parser = new ConditionParser(inputVarMan.getSortedAliasVariables(), new EnFixedValueParser(), OperatorProvider.createParser());
    IfAction action = new IfAction(parser.getExpression(condition, TOLERANCE));
    Collection<IAction> thenParsed = actionParser.parse(input[1]);
    action.setThenActions(thenParsed.toArray(IAction[]::new));
    StringJoiner joiner = new StringJoiner(" ");
    for (String s : input) {
      joiner.add(s);
    }
    action.setOriginalString(joiner.toString());
    return action;
  }

  @Override
  public String getRegex() {
    return "^(?:" + IF_THEN + "\\s(.*))$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{"if", "when", "do", "once", "then", "next"};
  }
}