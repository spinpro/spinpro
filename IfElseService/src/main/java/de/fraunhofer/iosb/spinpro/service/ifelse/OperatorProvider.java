package de.fraunhofer.iosb.spinpro.service.ifelse;

import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.BinaryBooleanToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.BinaryValueToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.BinaryValueToValueOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.ConcreteBinaryValueToValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.BinaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorsConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.UnaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.UnaryBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.UnaryValueOperators;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;

public class OperatorProvider {
  public static OperatorsConfig createParser() {
    BinaryOperatorConfig binary = new BinaryOperatorConfig();

    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.add, new ImmutableTriple<>("^.*$", "^(add|plus)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.sub, new ImmutableTriple<>("^.*$", "^(subtract|minus)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.mul, new ImmutableTriple<>("^.*$", "^(multiplies|times|multiplied by)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.div, new ImmutableTriple<>("^.*$", "^(divided by)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.mod, new ImmutableTriple<>("^.*$", "^(modulo)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(new ConcreteBinaryValueToValueOperator(null, null, "(%s \u00D8 %sm)"), new ImmutableTriple<>("^.*((?:the )?average of)$", "^(over)$", "^(minutes).*$")));

    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.lessThan, new ImmutableTriple<>("^.*$", "^((?:is )?less than)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.greaterThan, new ImmutableTriple<>("^.*$", "^((?:is )?greater than)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.lessEquals, new ImmutableTriple<>("^.*$", "^(less or equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.greaterEquals, new ImmutableTriple<>("^.*$", "^(greater or equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.equals, new ImmutableTriple<>("^.*$", "^(equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.unequals, new ImmutableTriple<>("^.*$", "^(unequal(?:s)?)$", "^.*$")));

    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.and, new ImmutableTriple<>("^.*$", "^(and)$", "^.*$")));
    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.or, new ImmutableTriple<>("^.*$", "^(or)$", "^.*$")));
    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.xor, new ImmutableTriple<>(".*(either)$", "^(or)$", "^.*$")));

    UnaryOperatorConfig unary = new UnaryOperatorConfig();

    unary.addVtV(new OperatorConfig<>(UnaryValueOperators.negate, new ImmutablePair<>("^.*\\s(minus)$", "^.*$")));
    unary.addBtB(new OperatorConfig<>(UnaryBooleanOperators.not, new ImmutablePair<>("^.*\\s(not)$", "^.*$")));


    return new OperatorsConfig(binary, unary);
  }
}
