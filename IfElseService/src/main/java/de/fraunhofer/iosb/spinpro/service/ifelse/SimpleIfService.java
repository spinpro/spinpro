package de.fraunhofer.iosb.spinpro.service.ifelse;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.condlib.ConditionParser;
import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.condlib.IConditionParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple service, that recognizes an if-then-statement.
 * An IfAction is used as output.
 * The input has to have the form: "if|when|once [condition possibly with input variables] then|next|do [nested action]"
 * The terms that are available in the condition are defined in {@link OperatorProvider}.
 */
@Service(author = "Jochen Mueller", name = "IfServiceS - EN", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "39dcc5435f5c55990b3b67a4469a94583d79739291d3220cb03412132bf02dbb")
public class SimpleIfService implements ISimpleActionService {

  private static final double TOLERANCE = 0.8;

  @Override
  public IAction execute(String input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    Pattern pattern = Pattern.compile(getRegex());
    Matcher matcher = pattern.matcher(input);
    if (!matcher.matches()) {
      throw new ParsingException("Wrong match chosen");
    }

    String condition = matcher.group(1).trim();
    String actions = matcher.group(2).trim();
    IConditionParser parser = new ConditionParser(inputVarMan.getSortedAliasVariables(), new EnFixedValueParser(), OperatorProvider.createParser());
    IfAction action = new IfAction(parser.getExpression(condition, TOLERANCE));
    Collection<IAction> parsedActions = actionParser.parse(actions);
    action.setThenActions(parsedActions.toArray(IAction[]::new));
    action.setOriginalString(input);
    return action;
  }

  @Override
  public String getRegex() {
    return "^(?:(?:if|when|once)\\s(.*)\\s(?:then|next|do)\\s(.*))$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{"if", "when", "do", "once", "then", "next"};
  }
}