package mockobjects;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.services.IActionParser;

import java.util.ArrayList;
import java.util.Collection;

public class ActionParserMock implements IActionParser {

  @Override
  public Collection<IAction> parse(String s) {
    return new ArrayList<>();
  }
}
