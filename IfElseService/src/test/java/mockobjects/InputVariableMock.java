package mockobjects;

import de.fraunhofer.iosb.spinpro.variables.IInputVariable;

import java.util.Collection;

public class InputVariableMock implements IInputVariable {

  private String name;
  private Collection<String> aliases;
  private Collection<String> devices;
  private String representation;

  public InputVariableMock(String name, Collection<String> aliases, Collection<String> devices, String representation) {
    this.name = name;
    this.aliases = aliases;
    this.devices = devices;
    this.representation = representation;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Collection<String> getAliases() {
    return aliases;
  }

  @Override
  public Collection<String> getDevices() {
    return devices;
  }

  @Override
  public String getRepresentation() {
    return representation;
  }
}
