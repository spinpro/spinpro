package mockobjects;

import de.fraunhofer.iosb.spinpro.variables.IVariable;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VariableManagerMock implements IVariableManager {

  private List<Pair<String, IVariable>> sortedAliasVariables;

  @Override
  public IVariable getVariable(String name) {
    return null;
  }

  @Override
  public boolean containsVariable(String name) {
    return false;
  }

  @Override
  public Collection<IVariable> getVariables() {
    return new ArrayList<>();
  }

  @Override
  public List<Pair<String, IVariable>> getVariablesInString(String s) {
    return new ArrayList<>();
  }

  @Override
  public List<Pair<String, IVariable>> getSortedAliasVariables() {
    return sortedAliasVariables;
  }

  public void setSortedAliasVariables(List<Pair<String, IVariable>> sortedAliasVariables) {
    this.sortedAliasVariables = sortedAliasVariables;
  }

  @Override
  public void setDevice(String device) {
  }
}
