package de.fraunhofer.iosb.spinpro.service.ifelse;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import mockobjects.ActionParserMock;
import mockobjects.InputVariableMock;
import mockobjects.VariableManagerMock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ComplexIfServiceTest {
  private ComplexIfService service;
  private VariableManagerMock manager = new VariableManagerMock();

  @BeforeEach
  void init() {
    service = new ComplexIfService();
  }

  @Test
  void executeTestInvalidInput1() {
    // invalid input
    manager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute(new String[]{"     a", "beh?   gnnnh!   "}, manager, manager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput2() {
    // invalid input
    manager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute(new String[]{"if gnah! gnaaah! do", "begin something end"}, manager, manager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInputEmpty() {
    // invalid input
    manager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute(new String[]{}, manager, manager, new ActionParserMock()));
  }

  @Test
  void executeTestX() throws ParsingException {
    // valid input
    String[] input = new String[]{"if x is greater than three do", "begin something end"};
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("x", new InputVariableMock("x", Arrays.asList("x", "X"), null, "x")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input[0] + " " + input[1], result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(x > 3.0)", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }

  @Test
  void executeTestA() throws ParsingException {
    // valid input
    String[] input = new String[]{"if a is greater than thirty-three do", "begin something end"};
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input[0] + " " + input[1], result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(a > 33.0)", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }

  @Test
  void executeTestB() throws ParsingException {
    // valid input
    String[] input = new String[]{"once BEE greater or equal minus temperature next", "begin something end"};
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasVariables.add(new ImmutablePair<>("BEE", new InputVariableMock("be", Arrays.asList("be", "BEE"), null, "be")));
    sortedAliasVariables.add(new ImmutablePair<>("temperature", new InputVariableMock("temperature", Arrays.asList("temperature", "TMP"), null, "temperature")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input[0] + " " + input[1], result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(be >= (-temperature))", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }
}