package de.fraunhofer.iosb.spinpro.service.ifelse;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import mockobjects.ActionParserMock;
import mockobjects.InputVariableMock;
import mockobjects.VariableManagerMock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleIfServiceTest {

  private SimpleIfService service;
  private VariableManagerMock manager = new VariableManagerMock();

  @BeforeEach
  void init() {
    service = new SimpleIfService();
  }

  @Test
  void executeTestInvalidInput1() {
    // invalid input
    manager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute(" abc  if    ", manager, manager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput2() {
    // invalid input
    manager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute("if gnah! gnaaah! do something", manager, manager, new ActionParserMock()));
  }

  @Test
  void executeTestX() throws ParsingException {
    // valid input
    String input = "if x is greater than three do something";
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("x", new InputVariableMock("x", Arrays.asList("x", "X"), null, "x")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(x > 3.0)", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }

  @Test
  void executeTestA() throws ParsingException {
    // valid input
    String input = "if a is greater than thirty-three do something";
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(a > 33.0)", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }

  @Test
  void executeTestB() throws ParsingException {
    // valid input
    String input = "once BEE greater or equal minus temperature next something";
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>(); // are given to condition parser
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasVariables.add(new ImmutablePair<>("BEE", new InputVariableMock("be", Arrays.asList("be", "BEE"), null, "be")));
    sortedAliasVariables.add(new ImmutablePair<>("temperature", new InputVariableMock("temperature", Arrays.asList("temperature", "TMP"), null, "temperature")));
    manager.setSortedAliasVariables(sortedAliasVariables);
    IAction result = service.execute(input, manager, null, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(IfAction.class, result.getClass());
    assertEquals("(be >= (-temperature))", ((IfAction) result).getCondition());
    assertEquals(0, ((IfAction) result).getThenActions().length);
    assertNull(((IfAction) result).getElseActions());
  }
}
