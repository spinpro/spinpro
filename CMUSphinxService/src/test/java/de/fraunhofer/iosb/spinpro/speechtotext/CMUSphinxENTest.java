package de.fraunhofer.iosb.spinpro.speechtotext;

import static org.junit.jupiter.api.Assertions.*;


import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

class CMUSphinxENTest {

  @Test
  void sphinx4With16k16bWavEn() {
    CMUSphinxEN cmu = null;
    try {
      cmu = new CMUSphinxEN();
    } catch (ServiceInitializingException e) {
      e.printStackTrace();
      fail();
    }
    String out = null;
    try {
      AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/" + "speechtotext/Anti-GravityWheel.wav"));
      out = cmu.speechToText(testWav);

    } catch (UnsupportedAudioFormat | IOException | UnsupportedAudioFileException e) {
      e.printStackTrace();
      fail();
    }

    String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
                  "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";

    double distance = StringUtils.getLevenshteinDistance(test, out);
    System.out.println(distance / test.length());
    assertTrue(distance / test.length() < 0.38d); //0.375 at the moment.
  }

  @Test
  void getSupportedAudioFormats() {
    CMUSphinxEN cmu = null;
    try {
      cmu = new CMUSphinxEN();
    } catch (ServiceInitializingException e) {
      e.printStackTrace();
      fail();
    }
    AudioFormat test = new AudioFormat(16000, 16, 1, false, false);
    assertTrue(test.matches(cmu.getSupportedAudioFormats()[0]));
  }
}