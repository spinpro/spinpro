package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 * Plugin Class for a speech-to-text plugin. ConfigurationProvider has to be in
 */
@Service(crypticId = "3cd645636fbffb53a28f4ade2b0a5dd1e8a53dfc2a1781fbb21c05ba1a219135", name = "CMUSphinxEN", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public class CMUSphinxEN implements ISpeechToTextService {
  private static final String CONFIGPATH = "CMUSphinxEN.properties";
  private StreamSpeechRecognizer recognizer = null;
  private Properties prop;

  public CMUSphinxEN() throws ServiceInitializingException {
    prop = new Properties();
    try {
      InputStream fis = this.getClass().getResourceAsStream(CONFIGPATH);
      prop.load(fis);
    } catch (IOException e) {
      e.printStackTrace();
      throw new ServiceInitializingException("Failed to load speech-to-text plugin.", e);
    }
    Configuration config = new Configuration();

    //making Paths absolut to load them as usual cause they are inside an external jar file
    String acousticModelPath = prop.getProperty("acousticModelPath", "en-us");
    String dictionaryPath = prop.getProperty("dictionaryPath", "cmudict-en-us.dict");
    String languageModelPath = prop.getProperty("languageModelPath", "en-us.lm.bin");

    String prefix = "";
    if (prop.getProperty("jarContainedModel", "true").equals("true")) {
      prefix = "jar:";
    }

    //to pack the Model into the same jar we need to hack us around the path that sphinx4 would usualy take.
    config.setAcousticModelPath(prefix + this.getClass().getResource(acousticModelPath).getPath());
    config.setDictionaryPath(prefix + this.getClass().getResource(dictionaryPath).getPath());
    config.setLanguageModelPath(prefix + this.getClass().getResource(languageModelPath).getPath());

    try {
      PrintStream stdout = System.out;
      PrintStream stderr = System.err;
      System.setOut(new PrintStream(new OutputStream() {
        public void write(int b) {
          //DO NOTHING
        }
      }));
      System.setErr(new PrintStream(new OutputStream() {
        @Override
        public void write(int i) throws IOException {
          //DO NOTHING
        }
      }));
      recognizer = new StreamSpeechRecognizer(config);
      System.setOut(stdout);
      System.setErr(stderr);
    } catch (IOException e) {
      e.printStackTrace();
      throw new ServiceInitializingException("Error in configuration or failed to load configuration for CMUSphinxEN.", e);
    }
  }

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
    PrintStream stdout = System.out;
    PrintStream stderr = System.err;
    System.setOut(new PrintStream(new OutputStream() {
      public void write(int b) {
        //DO NOTHING
      }
    }));
    System.setErr(new PrintStream(new OutputStream() {
      @Override
      public void write(int i) throws IOException {
        //DO NOTHING
      }
    }));
    recognizer.startRecognition(ais);
    System.setOut(stdout);
    System.setErr(stderr);
    SpeechResult result = null;
    StringBuilder out = new StringBuilder();
    while ((result = recognizer.getResult()) != null) {
      out.append(result.getHypothesis()).append(" ");
    }
    recognizer.stopRecognition();
    return out.substring(0, out.length() - 1);
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[] {new AudioFormat(16000, 16, 1, false, false)};
  }
}
