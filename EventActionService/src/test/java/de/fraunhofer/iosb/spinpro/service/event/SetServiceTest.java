package de.fraunhofer.iosb.spinpro.service.event;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import mockobjects.ActionParserMock;
import mockobjects.InputVariableMock;
import mockobjects.OutputEventMock;
import mockobjects.VariableManagerMock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetServiceTest {

  private SetService service;
  private VariableManagerMock variableManager = new VariableManagerMock();
  private VariableManagerMock eventManager = new VariableManagerMock();

  @BeforeEach
  void init() {
    service = new SetService();
  }

  @Test
  void executeTestInvalidInput1() {
    // invalid input
    variableManager.setSortedAliasVariables(new ArrayList<>());
    eventManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute(" abc  set   to ", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput2() {
    // invalid input
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("a", new OutputEventMock("a", Arrays.asList("a", "A"), null, "a")));
    eventManager.setSortedAliasVariables(sortedAliasVariables);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute("set a to bla", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput3() {
    // invalid input
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("a", new OutputEventMock("a", Arrays.asList("a", "A"), null, "a")));
    eventManager.setSortedAliasVariables(sortedAliasVariables);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute("set bla to a", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput4() {
    // invalid input
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("a", new OutputEventMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasVariables.add(new ImmutablePair<>("bl", new OutputEventMock("bl", Arrays.asList("bl", "BL"), null, "bl")));
    eventManager.setSortedAliasVariables(sortedAliasVariables);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute("set bl a to eight", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput5() {
    // invalid input
    eventManager.setSortedAliasVariables(new ArrayList<>());
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute(new String[]{"set x", "to y"}, variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestX() throws ParsingException {
    // valid input
    String[] input = new String[]{"set x to five"};
    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("x", new OutputEventMock("x", Arrays.asList("x", "X"), null, "x")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input[0], result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("x", ((EventAction) result).getOutput());
    assertEquals("5.0", ((EventAction) result).getValue());
  }

  @Test
  void executeTestA() throws ParsingException {
    // valid input
    String input = "set a to five modulo forty-two";
    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("a", new OutputEventMock("a", Arrays.asList("a", "A"), null, "a")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("a", ((EventAction) result).getOutput());
    assertEquals("(5.0 % 42.0)", ((EventAction) result).getValue());
  }

  @Test
  void executeTestB() throws ParsingException {
    // valid input
    String input = "set BEE to temperature";
    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("a", new OutputEventMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasEvents.add(new ImmutablePair<>("BEE", new OutputEventMock("be", Arrays.asList("be", "BEE"), null, "be")));
    sortedAliasEvents.add(new ImmutablePair<>("temperature", new OutputEventMock("temperature", Arrays.asList("temperature", "TMP"), null, "temperature")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);
    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasVariables.add(new ImmutablePair<>("BEE", new InputVariableMock("be", Arrays.asList("be", "BEE"), null, "be")));
    sortedAliasVariables.add(new ImmutablePair<>("temperature", new InputVariableMock("temperature", Arrays.asList("temperature", "TMP"), null, "temperature")));
    variableManager.setSortedAliasVariables(sortedAliasVariables);

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("be", ((EventAction) result).getOutput());
    assertEquals("temperature", ((EventAction) result).getValue());
  }
}
