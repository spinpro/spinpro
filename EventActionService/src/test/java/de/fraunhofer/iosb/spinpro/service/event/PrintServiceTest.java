package de.fraunhofer.iosb.spinpro.service.event;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import mockobjects.ActionParserMock;
import mockobjects.InputVariableMock;
import mockobjects.OutputEventMock;
import mockobjects.VariableManagerMock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PrintServiceTest {
  private PrintService service;
  private VariableManagerMock variableManager = new VariableManagerMock();
  private VariableManagerMock eventManager = new VariableManagerMock();

  @BeforeEach
  void init() {
    service = new PrintService();
  }

  @Test
  void executeTestInvalidInput1() {
    // invalid input
    variableManager.setSortedAliasVariables(new ArrayList<>());
    eventManager.setSortedAliasVariables(new ArrayList<>());
    assertThrows(ParsingException.class, () -> service.execute(" abc  print  hgn  ", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput2() {
    // invalid input
    eventManager.setSortedAliasVariables(new ArrayList<>());
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute("print five", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput3() {
    // invalid input
    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("stdout", new OutputEventMock("stdout", Arrays.asList("stdout", "STDOUT"), null, "stdout")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute("print thing", variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestInvalidInput4() {
    // invalid input
    eventManager.setSortedAliasVariables(new ArrayList<>());
    variableManager.setSortedAliasVariables(new ArrayList<>());

    assertThrows(ParsingException.class, () -> service.execute(new String[]{"print ", "x"}, variableManager, eventManager, new ActionParserMock()));
  }

  @Test
  void executeTestX() throws ParsingException {
    // valid input
    String[] input = new String[]{"print x"};

    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("x", new InputVariableMock("x", Arrays.asList("x", "X"), null, "x")));
    variableManager.setSortedAliasVariables(sortedAliasVariables);

    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("stdout", new OutputEventMock("stdout", Arrays.asList("stdout", "STDOUT"), null, "stdout")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input[0], result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("stdout", ((EventAction) result).getOutput());
    assertEquals("x", ((EventAction) result).getValue());
  }

  @Test
  void executeTestA() throws ParsingException {
    // valid input
    String input = "print five modulo forty-two";

    variableManager.setSortedAliasVariables(new ArrayList<>());

    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("stdout", new OutputEventMock("stdout", Arrays.asList("stdout", "STDOUT"), null, "stdout")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("stdout", ((EventAction) result).getOutput());
    assertEquals("(5.0 % 42.0)", ((EventAction) result).getValue());
  }

  @Test
  void executeTestB() throws ParsingException {
    // valid input
    String input = "print BEE divided by twenty-one";

    List<Pair<String, IVariable>> sortedAliasVariables = new ArrayList<>();
    sortedAliasVariables.add(new ImmutablePair<>("a", new InputVariableMock("a", Arrays.asList("a", "A"), null, "a")));
    sortedAliasVariables.add(new ImmutablePair<>("BEE", new InputVariableMock("be", Arrays.asList("be", "BEE"), null, "be")));
    sortedAliasVariables.add(new ImmutablePair<>("temperature", new InputVariableMock("temperature", Arrays.asList("temperature", "TMP"), null, "temperature")));
    variableManager.setSortedAliasVariables(sortedAliasVariables);

    List<Pair<String, IVariable>> sortedAliasEvents = new ArrayList<>();
    sortedAliasEvents.add(new ImmutablePair<>("stdout", new OutputEventMock("stdout", Arrays.asList("stdout", "STDOUT"), null, "stdout")));
    eventManager.setSortedAliasVariables(sortedAliasEvents);

    IAction result = service.execute(input, variableManager, eventManager, new ActionParserMock());
    assertEquals(input, result.getOriginalString());
    assertEquals(EventAction.class, result.getClass());
    assertEquals("stdout", ((EventAction) result).getOutput());
    assertEquals("(be / 21.0)", ((EventAction) result).getValue());
  }
}
