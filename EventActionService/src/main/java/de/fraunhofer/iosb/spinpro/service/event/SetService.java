package de.fraunhofer.iosb.spinpro.service.event;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.condlib.ArithmeticParser;
import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.condlib.VariableParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple and complex service, that sets an output event to an input variable or an arithmetic expression.
 * An EventAction is used as output.
 * The input has to have the form: "set [output event name] to [arithmetic expression, possibly containing input variables]"
 */
@Service(author = "Jochen Mueller", name = "EventActionService - EN", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "478d2dbd5bdc0de02cd75fa887af5e7b823eb72717c90e624e2ac37825894984")
public class SetService implements IComplexActionService, ISimpleActionService {

  private static final double TOLERANCE = 0.8;

  @Override
  public IAction execute(String input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    // match regex and extract value and output
    Pattern pattern = Pattern.compile(getRegex());
    Matcher matcher = pattern.matcher(input);
    if (!matcher.matches()) {
      throw new ParsingException("Wrong match chosen");
    }
    String output = matcher.group(1).trim();
    String value = matcher.group(2).trim();

    // parse output event
    VariableParser eventParser = new VariableParser(outputEventMan.getSortedAliasVariables());
    List<String> events = eventParser.getVariables(output);
    if (events.size() != 1) {
      throw new ParsingException(String.format("Invalid output event: %s", output));
    }
    output = events.get(0);
    // parse arithmetic expression
    ArithmeticParser arithmeticParser = new ArithmeticParser(inputVarMan.getSortedAliasVariables(), new EnFixedValueParser(), OperatorProvider.createParser());
    value = arithmeticParser.getExpression(value, TOLERANCE); // throws exception when not able to parse

    // create IAction
    IAction eventAction = new EventAction(output, value);
    eventAction.setOriginalString(input);
    return eventAction;
  }

  @Override
  public IAction execute(String[] input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    if (input.length != 1) {
      throw new ParsingException("Wrong match chosen"); //abort on fail
    }
    return execute(input[0], inputVarMan, outputEventMan, actionParser);
  }

  @Override
  public String getRegex() {
    return "^(?:set)(.*)(?:to)(.*)$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{"set", "to"};
  }

}
