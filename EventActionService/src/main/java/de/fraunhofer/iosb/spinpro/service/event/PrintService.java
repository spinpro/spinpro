package de.fraunhofer.iosb.spinpro.service.event;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.condlib.ArithmeticParser;
import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple and complex service, that prints an input variable or arithmetic expression to "stdio".
 * "stdio" needs to exists as output event.
 * An EventAction is used as output.
 * The input has to have the form: "print [arithmetic expression, possibly containing input variables]"
 */
@Service(author = "Jochen Mueller", name = "PrintService - EN", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "478d2dbd5bdc0de02cd75fa887af5e7b823eb72717c90e624e2ac37825894968")
public class PrintService implements IComplexActionService, ISimpleActionService {

  private static final String STDOUT = "stdout";
  private static final double TOLERANCE = 0.8;

  @Override
  public IAction execute(String input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    // match regex and extract value and output
    Pattern pattern = Pattern.compile(getRegex());
    Matcher matcher = pattern.matcher(input);
    if (!matcher.matches()) {
      throw new ParsingException("Wrong match chosen");
    }
    String value = matcher.group(1).trim();

    // check if stdout exists
    if (!outputEventMan.containsVariable(STDOUT)) {
      throw new ParsingException(String.format("'%s' not defined.", STDOUT));
    }
    // parse arithmetic expression
    ArithmeticParser arithmeticParser = new ArithmeticParser(inputVarMan.getSortedAliasVariables(), new EnFixedValueParser(), OperatorProvider.createParser());
    value = arithmeticParser.getExpression(value, TOLERANCE); // throws exception when not able to parse

    // create IAction
    IAction eventAction = new EventAction(STDOUT, value);
    eventAction.setOriginalString(input);
    return eventAction;
  }

  @Override
  public IAction execute(String[] input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    if (input.length != 1) {
      throw new ParsingException("Wrong match chosen");
    }
    return execute(input[0], inputVarMan, outputEventMan, actionParser);
  }

  @Override
  public String getRegex() {
    return "^(?:print)(.*)$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{"print"};
  }

}
