package de.fraunhofer.iosb.spinpro.speechtotext.condlib;

import de.fraunhofer.iosb.spinpro.condlib.NumberFixedValueParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class NumberFixedValueParserTest {
  @Test
  public void testValues() throws ParsingException {
    NumberFixedValueParser parser = new NumberFixedValueParser();
    List<Pair<String, Double>> a = new ArrayList<>();
    a.addAll(parser.findFixedValues("6"));
    a.addAll(parser.findFixedValues("1"));
    a.addAll(parser.findFixedValues("1.086468483"));
    a.addAll(parser.findFixedValues("651681681"));
    System.out.println(a.toString());
  }
}