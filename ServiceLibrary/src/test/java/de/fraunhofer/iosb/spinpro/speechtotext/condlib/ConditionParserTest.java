package de.fraunhofer.iosb.spinpro.speechtotext.condlib;

import de.fraunhofer.iosb.spinpro.condlib.ConditionParser;
import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.condlib.IConditionParser;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.BinaryBooleanToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.BinaryValueToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.BinaryValueToValueOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.ConcreteBinaryValueToValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.BinaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorsConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.UnaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.UnaryBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.UnaryValueOperators;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IInputVariable;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConditionParserTest {
  private static IConditionParser parser;
  private static Collection<IVariable> vars;
  private static OperatorsConfig ops;

  @BeforeAll
  public static void init() {
    BinaryOperatorConfig binary = new BinaryOperatorConfig();

    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.add, new ImmutableTriple<>("^.*$", "^(add|plus)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.sub, new ImmutableTriple<>("^.*$", "^(subtract|minus)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.mul, new ImmutableTriple<>("^.*$", "^(multiplies|times|multiplied by)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.div, new ImmutableTriple<>("^.*$", "^(divided by)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(BinaryValueToValueOperators.mod, new ImmutableTriple<>("^.*$", "^(modulo)$", "^.*$")));
    binary.addVtV(new OperatorConfig<>(new ConcreteBinaryValueToValueOperator(null, null, "(%s \u00D8 %sm)"), new ImmutableTriple<>("^.*((?:the )?average of)$", "^(over)$", "^(minutes).*$")));

    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.lessThan, new ImmutableTriple<>("^.*$", "^((?:is )?less than)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.greaterThan, new ImmutableTriple<>("^.*$", "^((?:is )?greater than)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.lessEquals, new ImmutableTriple<>("^.*$", "^(less or equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.greaterEquals, new ImmutableTriple<>("^.*$", "^(greater or equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.equals, new ImmutableTriple<>("^.*$", "^(equal(?:s)?)$", "^.*$")));
    binary.addVtB(new OperatorConfig<>(BinaryValueToBooleanOperators.unequals, new ImmutableTriple<>("^.*$", "^(unequal(?:s)?)$", "^.*$")));

    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.and, new ImmutableTriple<>("^.*$", "^(and)$", "^.*$")));
    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.or, new ImmutableTriple<>("^.*$", "^(or)$", "^.*$")));
    binary.addBtB(new OperatorConfig<>(BinaryBooleanToBooleanOperators.xor, new ImmutableTriple<>(".*(either)$", "^(or)$", "^.*$")));

    UnaryOperatorConfig unary = new UnaryOperatorConfig();

    unary.addVtV(new OperatorConfig<>(UnaryValueOperators.negate, new ImmutablePair<>("^.*\\s(minus)$", "^.*$")));
    unary.addBtB(new OperatorConfig<>(UnaryBooleanOperators.not, new ImmutablePair<>("^.*\\s(not)$", "^.*$")));


    Collection<String> alpha = new LinkedList<>();
    alpha.add("alpha");

    Collection<String> beta = new LinkedList<>();
    beta.add("beta");

    Collection<String> gamma = new LinkedList<>();
    gamma.add("gamma");

    Collection<String> x = new LinkedList<>();
    x.add("x");


    vars = new LinkedList<>();
    vars.add(new InputVar("$1", alpha));
    vars.add(new InputVar("$2", beta));
    vars.add(new InputVar("$3", gamma));
    vars.add(new InputVar("$x", x));

    ops = new OperatorsConfig(binary, unary);

    parser = new ConditionParser(vars, new EnFixedValueParser(), ops);
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/valuesForConditionParserTest.csv", numLinesToSkip = 1)
  public void testConditionWithDifferentConstructor(String expected, String input) throws ParsingException {
    List<Pair<String, IVariable>> variables = new LinkedList<>();
    vars.forEach(s -> s.getAliases().forEach(a -> variables.add(new ImmutablePair<>(a, s))));
    variables.sort(new Comparator<Pair<String, IVariable>>() {
      @Override
      public int compare(Pair<String, IVariable> o1, Pair<String, IVariable> o2) {
        int dist = o1.getLeft().length() - o2.getLeft().length();
        if (dist != 0) {
          return dist;
        }
        return (int) Math.signum(o1.getLeft().compareTo(o2.getLeft()));
      }
    });
    IConditionParser parser = new ConditionParser(variables, new EnFixedValueParser(), ops);
    String output = parser.getExpression(input);
    assertEquals(expected, output);
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/valuesForConditionParserTest.csv", numLinesToSkip = 1)
  public void testCondition(String expected, String input) throws ParsingException {
    String output = parser.getExpression(input);
    assertEquals(expected, output);
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/valuesForFailingConditionParserTest.csv", numLinesToSkip = 1)
  public void failing(String expected, String input) throws ParsingException {
    Exception exception = assertThrows(ParsingException.class, () -> {
      System.out.println(parser.getExpression(input));
    });
    assertTrue(exception.getMessage().contains(expected));
  }

  static class InputVar implements IInputVariable {
    Collection<String> aliases;
    private String name;


    public InputVar(String name, Collection<String> aliases) {
      this.aliases = aliases;
      this.name = name;
    }

    public InputVar(String name, String... aliases) {
      this.aliases = Arrays.asList(aliases);
      this.name = name;
    }

    @Override
    public String getRepresentation() {
      return name;
    }

    @Override
    @Deprecated
    public String getName() {
      return name;
    }

    @Override
    public Collection<String> getAliases() {
      return aliases;
    }

    @Override
    public Collection<String> getDevices() {
      return null;
    }
  }
}
