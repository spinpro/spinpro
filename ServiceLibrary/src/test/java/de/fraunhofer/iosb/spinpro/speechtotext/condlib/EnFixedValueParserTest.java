package de.fraunhofer.iosb.spinpro.speechtotext.condlib;

import de.fraunhofer.iosb.spinpro.condlib.EnFixedValueParser;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EnFixedValueParserTest {
  public static final double EPSILON = 0.00000001;

  @Test
  public void testValues() throws ParsingException {
    EnFixedValueParser parser = new EnFixedValueParser();
    List<Pair<String, Double>> a = new ArrayList<>();
    a.addAll(parser.findFixedValues("asdf one point five beatles two point nine mary"));
    return;
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/valuesForENFixedValueTest.csv", numLinesToSkip = 1)
  public void values(double expectedDouble, String expectedString, String input) throws ParsingException {
    EnFixedValueParser parser = new EnFixedValueParser();
    List<Pair<String, Double>> a = new ArrayList<>(parser.findFixedValues(input));
    assertEquals(1, a.size());
    assertEquals(expectedString, a.get(0).getLeft());
    assertEquals(expectedDouble, a.get(0).getRight(), EPSILON);
  }

  @Test
  public void failing1() throws ParsingException {
    EnFixedValueParser parser = new EnFixedValueParser();
    List<Pair<String, Double>> a = new ArrayList<>(parser.findFixedValues("fivea"));
    assertEquals(0, a.size());
  }
}