package de.fraunhofer.iosb.spinpro.speechtotext.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.BinaryBooleanToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.IBinaryBooleanToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.BinaryValueToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.IBinaryValueToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.BinaryValueToValueOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.IBinaryValueToValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.BinaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorsConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.UnaryOperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.ConcreteUnaryBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.IUnaryBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.UnaryBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.IUnaryValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.UnaryValueOperators;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigTest {
  @Test
  public void operatorConstructors() {
    Pair<String, String> p = new ImmutablePair<>("a", "b");
    Pair<String, String> p2 = new ImmutablePair<>("c", "d");

    List<Pair<String, String>> a = new LinkedList<>();
    a.add(p);
    OperatorConfig<IUnaryBooleanOperator, Pair<String, String>> op = new OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>(UnaryBooleanOperators.not, new LinkedList<>(a));
    OperatorConfig<IUnaryBooleanOperator, Pair<String, String>> op2 = new OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>(UnaryBooleanOperators.not);
    a.add(p2);
    op2.setRegexes(a);
    op.add(p2);

    assertEquals(2, op.getRegexes().size());
    assertTrue(op.getRegexes().contains(p));
    assertTrue(op.getRegexes().contains(p2));
    assertEquals(2, op2.getRegexes().size());
    assertTrue(op2.getRegexes().contains(p));
    assertTrue(op2.getRegexes().contains(p2));

    assertEquals(UnaryBooleanOperators.not, op.getInstance());
    ConcreteUnaryBooleanOperator operator = new ConcreteUnaryBooleanOperator(new BP(), "%s");
    op.setInstance(operator);
    assertEquals(operator, op.getInstance());
  }

  @Test
  public void unaryConfig() {
    Pair<String, String> p = new ImmutablePair<>("a", "b");
    Pair<String, String> p2 = new ImmutablePair<>("c", "d");

    List<Pair<String, String>> a = new LinkedList<>();
    a.add(p);
    OperatorConfig<IUnaryBooleanOperator, Pair<String, String>> op = new OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>(UnaryBooleanOperators.not, new LinkedList<>(a));
    OperatorConfig<IUnaryBooleanOperator, Pair<String, String>> op2 = new OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>(UnaryBooleanOperators.not);
    a.add(p2);
    op2.setRegexes(a);
    op.add(p2);

    Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> unaryBoolean = new LinkedList<>();
    unaryBoolean.add(op);
    unaryBoolean.add(op2);

    Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> unaryValue = new LinkedList<>();
    unaryValue.add(new OperatorConfig<IUnaryValueOperator, Pair<String, String>>(UnaryValueOperators.negate, p, p2));

    UnaryOperatorConfig unary = new UnaryOperatorConfig(new LinkedList<>(), new LinkedList<>());
    assertTrue(unary.getBooleanToBooleanOperators().isEmpty());
    assertTrue(unary.getValueToValueOperators().isEmpty());

    unary.setBooleanToBooleanOperators(unaryBoolean);
    unary.setValueToValueOperators(unaryValue);
    Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> temp = unary.getBooleanToBooleanOperators();
    temp.removeAll(unaryBoolean);
    assertTrue(temp.isEmpty());
    Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> temp2 = unary.getValueToValueOperators();
    temp2.removeAll(unaryValue);
    assertTrue(temp2.isEmpty());
  }

  @Test
  public void binaryConfig() {
    Triple<String, String, String> a = new ImmutableTriple<>("a", "b", "c");
    Collection<OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>> btb = new LinkedList<>();
    Collection<OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>> vtb = new LinkedList<>();
    Collection<OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>> vtv = new LinkedList<>();

    btb.add(new OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>(BinaryBooleanToBooleanOperators.xor, a));
    vtb.add(new OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>(BinaryValueToBooleanOperators.unequals, a));
    vtv.add(new OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>(BinaryValueToValueOperators.add, a));

    BinaryOperatorConfig binary = new BinaryOperatorConfig(new LinkedList<>(), new LinkedList<>(), new LinkedList<>());
    assertTrue(binary.getBooleanToBooleanOperators().isEmpty());
    assertTrue(binary.getValueToBooleanOperators().isEmpty());
    assertTrue(binary.getValueToValueOperators().isEmpty());

    binary.setBooleanToBooleanOperators(btb);
    binary.setValueToBooleanOperators(vtb);
    binary.setValueToValueOperators(vtv);

    Collection tbtb = binary.getBooleanToBooleanOperators();
    Collection tvtb = binary.getValueToBooleanOperators();
    Collection tvtv = binary.getValueToValueOperators();

    tbtb.removeAll(btb);
    tvtb.removeAll(vtb);
    tvtv.removeAll(vtv);

    assertTrue(tbtb.isEmpty());
    assertTrue(tvtb.isEmpty());
    assertTrue(tvtv.isEmpty());
  }

  @Test
  public void testOperatorsConfig() {
    OperatorsConfig config = new OperatorsConfig(null, null);
    assertNull(config.getBinaryOperators());
    assertNull(config.getUnaryOperators());

    BinaryOperatorConfig binary = new BinaryOperatorConfig();
    UnaryOperatorConfig unary = new UnaryOperatorConfig();

    config.setBinaryOperators(binary);
    config.setUnaryOperators(unary);

    assertEquals(binary, config.getBinaryOperators());
    assertEquals(unary, config.getUnaryOperators());
  }

  static class BP implements IBooleanPart {
    Collection<String> aliases;
    private String name;

    @Override
    public String getRepresentation() {
      return "b";
    }
  }
}
