package de.fraunhofer.iosb.spinpro.speechtotext.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.BinaryBooleanToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.ConcreteBinaryBooleanToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.BinaryValueToBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.ConcreteBinaryValueToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.BinaryValueToValueOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.ConcreteBinaryValueToValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.ConcreteUnaryBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.UnaryBooleanOperators;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.ConcreteUnaryValueOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.UnaryValueOperators;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OperatorTest {
  @ParameterizedTest
  @EnumSource(BinaryBooleanToBooleanOperators.class)
  public void bbtb(BinaryBooleanToBooleanOperators operators) {
    operators.setLeft(new BP());
    operators.setRight(new BP());
    assertTrue(operators.getRepresentation().matches("^\\(b . b\\)$"));
  }

  @ParameterizedTest
  @EnumSource(BinaryValueToBooleanOperators.class)
  public void bvtb(BinaryValueToBooleanOperators operators) {
    operators.setLeft(new VP());
    operators.setRight(new VP());
    assertTrue(operators.getRepresentation().matches("^\\(v ..? v\\)$"));
  }

  @ParameterizedTest
  @EnumSource(BinaryValueToValueOperators.class)
  public void bvtv(BinaryValueToValueOperators operators) {
    operators.setLeft(new VP());
    operators.setRight(new VP());
    assertTrue(operators.getRepresentation().matches("^\\(v . v\\)$"));
  }

  @ParameterizedTest
  @EnumSource(UnaryValueOperators.class)
  public void uvtv(UnaryValueOperators operators) {
    operators.setValue(new VP());
    assertTrue(operators.getRepresentation().matches("^\\(.v\\)$"));
  }

  @ParameterizedTest
  @EnumSource(UnaryBooleanOperators.class)
  public void ubtb(UnaryBooleanOperators operators) {
    operators.setValue(new BP());
    assertTrue(operators.getRepresentation().matches("^\\(.b\\)$"));
  }

  @Test
  public void cbbtb() {
    ConcreteBinaryBooleanToBooleanOperator a = new ConcreteBinaryBooleanToBooleanOperator(new BP(), new BP(), "%s + %s");
    a.setLeft(new BP2());
    a.setRight(new BP2());
    assertEquals("b2 + b2", a.getRepresentation());
    assertEquals("b + b", a.newInstance(new BP(), new BP()).getRepresentation());
  }

  @Test
  public void cbvtb() {
    ConcreteBinaryValueToBooleanOperator a = new ConcreteBinaryValueToBooleanOperator(new VP(), new VP(), "%s + %s");
    a.setLeft(new VP2());
    a.setRight(new VP2());
    assertEquals("v2 + v2", a.getRepresentation());
    assertEquals("v + v", a.newInstance(new VP(), new VP()).getRepresentation());
  }

  @Test
  public void cbvtv() {
    ConcreteBinaryValueToValueOperator a = new ConcreteBinaryValueToValueOperator(new VP(), new VP(), "%s + %s");
    a.setLeft(new VP2());
    a.setRight(new VP2());
    assertEquals("v2 + v2", a.getRepresentation());
    assertEquals("v + v", a.newInstance(new VP(), new VP()).getRepresentation());
  }

  @Test
  public void cuvtv() {
    ConcreteUnaryValueOperator a = new ConcreteUnaryValueOperator(new VP(), "9%s");
    a.setValue(new VP2());
    assertEquals("9v2", a.getRepresentation());
    assertEquals("9v", a.newInstance(new VP()).getRepresentation());
  }

  @Test
  public void cubtb() {
    ConcreteUnaryBooleanOperator a = new ConcreteUnaryBooleanOperator(new BP(), "9%s");
    a.setValue(new BP2());
    assertEquals("9b2", a.getRepresentation());
    assertEquals("9b", a.newInstance(new BP()).getRepresentation());
  }


  static class BP implements IBooleanPart {
    Collection<String> aliases;
    private String name;

    @Override
    public String getRepresentation() {
      return "b";
    }
  }

  static class BP2 implements IBooleanPart {
    Collection<String> aliases;
    private String name;

    @Override
    public String getRepresentation() {
      return "b2";
    }
  }

  static class VP implements IValuePart {
    Collection<String> aliases;
    private String name;

    @Override
    public String getRepresentation() {
      return "v";
    }
  }

  static class VP2 implements IValuePart {
    Collection<String> aliases;
    private String name;

    @Override
    public String getRepresentation() {
      return "v2";
    }
  }
}
