package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.IBinaryInstantiable;

/**
 * An binary operator representing a boolean.
 */
public interface IBinaryValueToBooleanOperator extends IBooleanPart, IBinaryInstantiable<IBinaryValueToBooleanOperator, IValuePart> {
  /**
   * Sets the inner left condition part.
   *
   * @param part The part to set.
   */
  void setLeft(IValuePart part);

  /**
   * Sets the inner right condition part.
   *
   * @param part The part to set.
   */
  void setRight(IValuePart part);
}
