package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * A concrete unary operator representing a boolean with child.
 */
public class ConcreteUnaryBooleanOperator extends UnaryBooleanOperator {
  private String representation;

  /**
   * Creates a new instance.
   *
   * @param left           The child to set.
   * @param representation The representation string used in the {@code String.format} call. May only contain one {@code %s}.
   */
  public ConcreteUnaryBooleanOperator(IBooleanPart left, String representation) {
    super(left);
    this.representation = representation;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getValue().getRepresentation());
  }

  @Override
  public IUnaryBooleanOperator newInstance(IBooleanPart left) {
    return new ConcreteUnaryBooleanOperator(left, representation);
  }
}
