package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary;

/**
 * Part of the prototype pattern. Used to be able to get a new instance from an operator, that has already a representation set.
 * The new instance must have the same representation.
 *
 * @param <T> The type of the operator to instantiate.
 * @param <G> The type that the operator accepts.
 */
public interface IUnaryInstantiable<T, G> {
  /**
   * Creates a new instance.
   *
   * @param left The left argument.
   * @return The new instance.
   */
  T newInstance(G left);
}
