package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;
import java.util.Objects;

/**
 * An interface representing an action that can be stored inside an {@code IRule}.
 */
public class IfAction extends AbstractAction {
  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "if";

  //How to implement condition? Especially things like "if a increased by 5 in the last 5 minutes"
  private String condition;
  private IAction[] thenActions;
  private IAction[] elseActions;

  /**
   * Creates an IfAction with a condition and actions that should be performed
   * if the condition is true, as well as conditions that should be performed
   * if the condition is false.
   *
   * @param condition   the condition
   * @param thenActions the actions on condition
   * @param elseActions the actions on !condition
   */
  public IfAction(String condition, IAction[] thenActions, IAction[] elseActions) {
    this.condition = condition;
    this.thenActions = thenActions;
    this.elseActions = elseActions;
  }

  /**
   * Creates an IfAction with a condition and actions that should be performed
   * if the condition is true, as well as conditions that should be performed
   * if the condition is false.
   *
   * @param condition the condition
   */
  public IfAction(String condition) {
    this.condition = condition;
  }

  /**
   * Returns the condition.
   *
   * @return the condition
   */
  public String getCondition() {
    return condition;
  }

  /**
   * Sets the condition.
   *
   * @param condition the condition
   */
  public void setCondition(String condition) {
    this.condition = condition;
  }

  /**
   * Returns the actions performed when the condition is true.
   *
   * @return the actions
   */
  public IAction[] getThenActions() {
    return thenActions;
  }

  /**
   * Sets the actions performed when the condition is true.
   *
   * @param thenActions the actions
   */
  public void setThenActions(IAction[] thenActions) {
    this.thenActions = thenActions;
  }

  /**
   * Returns the actions performed when the condition is false.
   *
   * @return the actions
   */
  public IAction[] getElseActions() {
    return elseActions;
  }

  /**
   * Sets the actions performed when the condition is false.
   *
   * @param elseActions the actions
   */
  public void setElseActions(IAction[] elseActions) {
    this.elseActions = elseActions;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      IfAction input = (IfAction) obj;
      return Arrays.equals(input.elseActions, this.elseActions) && Arrays.equals(input.thenActions, this.thenActions) && Objects.equals(input.condition, this.condition);
    }
    return false;
  }
}
