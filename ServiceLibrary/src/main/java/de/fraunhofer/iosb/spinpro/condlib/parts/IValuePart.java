package de.fraunhofer.iosb.spinpro.condlib.parts;

/**
 * A part of the condition representing a value.
 */
public interface IValuePart extends IConditionPart {
}
