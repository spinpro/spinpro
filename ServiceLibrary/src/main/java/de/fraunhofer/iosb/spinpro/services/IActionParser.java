package de.fraunhofer.iosb.spinpro.services;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;

import java.util.Collection;

/**
 * Manages the translation of a String to a Collection of {@link IAction}.
 */
public interface IActionParser {
  /**
   * Translates a String to a Collection of {@link IAction}.
   *
   * @param s the String that is translated
   * @return the translated Collection of {@link IAction}
   * @throws ParsingException if the input could not be parsed
   */
  Collection<IAction> parse(String s) throws ParsingException;

}

