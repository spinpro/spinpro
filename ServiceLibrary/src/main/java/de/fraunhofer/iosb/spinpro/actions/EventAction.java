package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;
import java.util.Objects;

/**
 * Defines an Event action that can be used in an {@code IRule}.
 */
public class EventAction extends AbstractAction {
  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "event";
  private String output;
  private String value;

  /**
   * Creates an event action with values that will be written into the output(variable).
   *
   * @param output the name of the output variable
   * @param values the values that should be stored in the variable
   */
  public EventAction(String output, String values) {
    this.output = output;
    this.value = values;
  }

  @Override
  public String getType() {
    return null;
  }

  /**
   * Returns the output-variable in which the values should be written.
   *
   * @return the output
   */
  public String getOutput() {
    return output;
  }

  /**
   * Sets the output-variable in which the values should be written.
   *
   * @param output the output
   */
  public void setOutput(String output) {
    this.output = output;
  }

  /**
   * Returns the value-array.
   *
   * @return the value-array
   */
  public String getValue() {
    return value;
  }

  /**
   * Sets the value-array that should be written to the output(variable).
   *
   * @param value the value-array
   */
  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      EventAction input = (EventAction) obj;
      return Objects.equals(input.value, this.value) && Objects.equals(input.output, this.output);
    }
    return false;
  }
}
