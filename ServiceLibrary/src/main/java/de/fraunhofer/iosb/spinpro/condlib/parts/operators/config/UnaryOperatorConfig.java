package de.fraunhofer.iosb.spinpro.condlib.parts.operators.config;

import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb.IUnaryBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv.IUnaryValueOperator;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Configuration containing all unary operators.
 */
public class UnaryOperatorConfig {
  Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> booleanToBooleanOperators;
  Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> valueToValueOperators;

  /**
   * Creates a new instance.
   *
   * @param booleanToBooleanOperators All boolean to boolean operator configs.
   * @param valueToValueOperators     All value to value operator configs.
   */
  public UnaryOperatorConfig(Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> booleanToBooleanOperators, Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> valueToValueOperators) {
    this.booleanToBooleanOperators = booleanToBooleanOperators;
    this.valueToValueOperators = valueToValueOperators;
  }

  /**
   * Creates a new instance.
   */
  public UnaryOperatorConfig() {
    booleanToBooleanOperators = new LinkedList<>();
    valueToValueOperators = new LinkedList<>();
  }

  /**
   * Adds a boolean to boolean operator config.
   *
   * @param btbOperator The config to add.
   */
  public void addBtB(OperatorConfig<IUnaryBooleanOperator, Pair<String, String>> btbOperator) {
    booleanToBooleanOperators.add(btbOperator);
  }

  /**
   * Adds a value to value operator config.
   *
   * @param vtvOperator The config to add.
   */
  public void addVtV(OperatorConfig<IUnaryValueOperator, Pair<String, String>> vtvOperator) {
    valueToValueOperators.add(vtvOperator);
  }

  /**
   * Getter for the boolean to boolean operator configs.
   *
   * @return All boolean to boolean operator configs.
   */
  public Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> getBooleanToBooleanOperators() {
    return booleanToBooleanOperators;
  }

  /**
   * Sets all boolean to boolean operator configs.
   *
   * @param booleanToBooleanOperators The configs to set.
   */
  public void setBooleanToBooleanOperators(Collection<OperatorConfig<IUnaryBooleanOperator, Pair<String, String>>> booleanToBooleanOperators) {
    this.booleanToBooleanOperators = booleanToBooleanOperators;
  }

  /**
   * Getter for the value to value operator configs.
   *
   * @return All value to value operator configs.
   */
  public Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> getValueToValueOperators() {
    return valueToValueOperators;
  }

  /**
   * Sets all value to value operator configs.
   *
   * @param valueToValueOperators The configs to set.
   */
  public void setValueToValueOperators(Collection<OperatorConfig<IUnaryValueOperator, Pair<String, String>>> valueToValueOperators) {
    this.valueToValueOperators = valueToValueOperators;
  }
}
