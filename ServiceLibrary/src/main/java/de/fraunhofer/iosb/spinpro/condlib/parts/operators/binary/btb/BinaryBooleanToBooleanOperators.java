package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * Possible binary operators representing a boolean with children.
 */
public enum BinaryBooleanToBooleanOperators implements IBinaryBooleanToBooleanOperator {
  /**
   * Logic and of two elements.
   */
  and("(%s & %s)"),
  /**
   * Logic or of two elements.
   */
  or("(%s | %s)"),
  /**
   * Logic exclusive or of two elements.
   */
  xor("(%s ^ %s)"),
  ;

  private String representation;
  private IBooleanPart left;
  private IBooleanPart right;

  BinaryBooleanToBooleanOperators(String representation) {
    this.representation = representation;
  }

  protected IBooleanPart getLeft() {
    return left;
  }

  @Override
  public void setLeft(IBooleanPart part) {
    left = part;
  }

  protected IBooleanPart getRight() {
    return right;
  }

  @Override
  public void setRight(IBooleanPart part) {
    right = part;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getLeft().getRepresentation(), getRight().getRepresentation());
  }

  @Override
  public IBinaryBooleanToBooleanOperator newInstance(IBooleanPart left, IBooleanPart right) {
    return new ConcreteBinaryBooleanToBooleanOperator(left, right, representation);
  }
}
