package de.fraunhofer.iosb.spinpro.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * An annotation to tag a service. Used to determinate the services entry point and provide some information on the
 * service to be able to identify the service.
 *
 * <h1>Writing a service</h1>
 * <p>A service is a class annotated with {@link Service} in a jar file. The service implements a interface that is
 * declared in the program. This interface provides the callable methods. For some use cases it is imported that a jar
 * file only contains one service class. In other cases multiple service classes can be packed into one jar. This
 * depends on the type of plugin loaded. For example a speech to text plugin may only contain one service class.</p>
 *
 * <p>To be able to identify each service version you may provide some extra metadata via the annotation {@link Service}.
 * This identification is used to be able encapsulate the version of the service set used to generate a object into the
 * object. Later you may check for the object to be compatible.</p>
 *
 * <p>In an service class you can load other classes and resources from inside the jar. To be able to load the resources
 * relatively you may use the {@code ClassLoader.getResource()} method of the classloader. If you need an absolute path
 * into the jar you may use an url with the prefix {@code jar:file://}.</p>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {
  /**
   * A cryptic identification code that may be unique for each service version.
   *
   * @return The cryptic identifier.
   */
  String crypticId();

  /**
   * The name of the service.
   *
   * @return The name of the service.
   */
  String name();

  /**
   * The author of the service.
   *
   * @return The author of the service.
   */
  String author();

  /**
   * The name of the organisation that the service was developed for.
   *
   * @return The organisation for that the service was developed.
   */
  String organisation();

  /**
   * The version of the service.
   *
   * @return The version of the service. May be unique for each change of the service.
   */
  String version();
}
