package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * An {@link IParser} that could be used as the start of a sequence of {@link IParser}s.
 */
public interface IParserStart extends IParser {
  /**
   * Simplification for getValues.
   *
   * @param string The original input string.
   * @return The association of variables and strings.
   * @throws ParsingException If it is not parsable.
   */
  List<Pair<String, IConditionPart>> getValues(String string) throws ParsingException;
}
