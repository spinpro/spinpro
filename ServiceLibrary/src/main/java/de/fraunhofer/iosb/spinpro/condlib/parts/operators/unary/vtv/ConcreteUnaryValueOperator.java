package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * A concrete unary operator representing a value with child.
 */
public class ConcreteUnaryValueOperator extends UnaryValueOperator {
  private String representation;

  /**
   * Creates a new instance.
   *
   * @param left           The child to set.
   * @param representation The representation string used in the {@code String.format} call. May only contain one {@code %s}.
   */
  public ConcreteUnaryValueOperator(IValuePart left, String representation) {
    super(left);
    this.representation = representation;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getValue().getRepresentation());
  }

  @Override
  public IUnaryValueOperator newInstance(IValuePart left) {
    return new ConcreteUnaryValueOperator(left, representation);
  }
}
