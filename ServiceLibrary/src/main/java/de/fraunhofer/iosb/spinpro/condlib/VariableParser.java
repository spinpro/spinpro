package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parses variables.
 */
public class VariableParser implements IParserStart {
  private List<Pair<String, IVariable>> variables;

  /**
   * Creates a new instance.
   *
   * @param variables The variables to parse.
   */
  public VariableParser(List<Pair<String, IVariable>> variables) {
    this.variables = variables;
  }

  @Override
  public List<Pair<String, IConditionPart>> getValues(List<Pair<String, IConditionPart>> input) {
    /* ***** Find variables ********/
    for (Pair<String, IVariable> var : variables) {
      for (int i = 0; i < input.size(); ) {
        Pair<String, IConditionPart> item = input.get(i);
        String left = var.getLeft();
        if (item.getRight() != null || !item.getLeft().contains(left)) {
          i++;
          continue;
        }
        int beginIndex = item.getLeft().indexOf(left);
        if (item.getLeft().substring(0, beginIndex).matches("^.+(?<!\\s)$") || item.getLeft().substring(beginIndex + left.trim().length()).matches("^(?!\\s).+$")) {
          i++;
          continue;
        }
        if (beginIndex != 0) {
          input.set(i, new ImmutablePair<>(item.getLeft().substring(0, beginIndex).trim(), null));
          i++;
          input.add(i, new ImmutablePair<>(left.trim(), var.getRight()));
        } else {
          input.set(i, new ImmutablePair<>(left.trim(), var.getRight()));
        }
        i++;
        String end = item.getLeft().substring(beginIndex + left.trim().length());
        if (end.length() != 0) {
          input.add(i, new ImmutablePair<>(item.getLeft().substring(beginIndex + left.length()).replaceAll("\\s+$", "").trim(), null));
        }
      }
    }
    return input;
  }

  /**
   * Simplification for getValues.
   *
   * @param string The original input string.
   * @return The association of variables and strings.
   */
  @Override
  public List<Pair<String, IConditionPart>> getValues(String string) {
    List<Pair<String, IConditionPart>> a = new LinkedList<>();
    a.add(new ImmutablePair<>(string, null));
    return getValues(a);
  }

  /**
   * Gets a list of all variables found.
   *
   * @param string The input string.
   * @return The list of all representations of variables found. Or an empty list if none was found.
   */
  public List<String> getVariables(String string) {
    List<Pair<String, IConditionPart>> a = getValues(string);
    return a.stream().filter(b -> b.getRight() != null).map(Pair::getRight).map(IConditionPart::getRepresentation).collect(Collectors.toList());
  }

}
