package de.fraunhofer.iosb.spinpro.condlib.parts;

/**
 * A part of a condition.
 */
public interface IConditionPart {
  /**
   * Recursively gets the representation of a condition part and all sub-parts.
   *
   * @return The representation.
   */
  String getRepresentation();
}
