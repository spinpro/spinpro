package de.fraunhofer.iosb.spinpro.actions;

public abstract class AbstractAction implements IAction {
  private transient String origin;

  @Override
  public String getOriginalString() {
    return origin;
  }

  @Override
  public void setOriginalString(String origin) {
    this.origin = origin;
  }
}
