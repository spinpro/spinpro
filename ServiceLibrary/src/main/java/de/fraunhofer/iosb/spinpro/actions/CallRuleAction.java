package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;

public class CallRuleAction extends AbstractAction {

  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "callRule";
  private int id;

  /**
   * Creates a CallRuleAction with the passed id.
   *
   * @param id the id
   */
  public CallRuleAction(int id) {
    this.id = id;
  }

  @Override
  public String getType() {
    return type;
  }

  /**
   * Returns the id.
   *
   * @return the id
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the id
   */
  public void setId(int id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      CallRuleAction input = (CallRuleAction) obj;
      return input.id == this.id;
    }
    return false;
  }
}
