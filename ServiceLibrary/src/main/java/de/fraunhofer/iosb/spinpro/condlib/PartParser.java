package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.IBinaryInstantiable;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.IUnaryInstantiable;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses operators as defined in package {@code part}.
 *
 * @param <B> The type of the binary operator.
 * @param <U> The type of the unary operator.
 * @param <R> The type that they accept.
 */
public class PartParser<B extends IConditionPart & IBinaryInstantiable<B, R>, U extends IConditionPart & IUnaryInstantiable<U, R>, R extends IConditionPart> implements IParser {
  private Collection<OperatorConfig<B, Triple<String, String, String>>> binary;
  private Collection<OperatorConfig<U, Pair<String, String>>> unary;
  private Class<R> partType;

  /**
   * Creates a new instance.
   *
   * @param binary   The binary operators.
   * @param unary    The unary operators.
   * @param partType The type the operators accept. Must be the same as R.
   */
  public PartParser(Collection<OperatorConfig<B, Triple<String, String, String>>> binary, Collection<OperatorConfig<U, Pair<String, String>>> unary, Class<R> partType) {
    this.binary = binary;
    this.unary = unary;
    this.partType = partType;
  }

  private static boolean matches(String regex, String text) {
    return Pattern.compile(regex).matcher(text).matches();
  }

  private static String replaceLast(String origin, String regex, String toSet) {
    return origin.replaceFirst(String.format("%s(?!.*%s)", regex, regex), toSet);
  }

  /**
   * Parses all entries of a list.
   *
   * <p><b>
   * This method must be run with the variables and the fixed values already known.
   * </b></p>
   *
   * @param input The list to parse. A list containing one or multiple strings with the already known association to IParts or {@code null} if there is no association.
   * @return The parsed list. A list containing the splitted strings with the new associations to IParts or {@code null} if there is no association.
   * @throws ParsingException If {@code input} is not parsable. Could be the wrong order of parsers.
   */
  @Override
  public List<Pair<String, IConditionPart>> getValues(List<Pair<String, IConditionPart>> input) throws ParsingException {
    for (int i = 0; i < input.size(); i++) {
      input = getUnaryOperator(i, input, unary, partType);
    }
    input = getBinaryOperator(input, binary, partType);
    return input;
  }

  /**
   * Finds all the binary operators at the moment visible.
   *
   * @param list          The list to work on.
   * @param operators     The operators to find.
   * @param partInterface The datatype interface needed by the operators. Has to be similar to S. Can either be {@link IValuePart} or {@link IBooleanPart}.
   * @return The changed list.
   */
  private List<Pair<String, IConditionPart>> getBinaryOperator(List<Pair<String, IConditionPart>> list, Collection<OperatorConfig<B, Triple<String, String, String>>> operators, Class<R> partInterface) {
    for (int index = 0; index < list.size() - 2; index++) {
      int argumentCount = 5;
      Pair<String, IConditionPart>[] temp = new Pair[argumentCount];
      int removeCount = argumentCount;
      int removeIndex = index - 1;
      int localIndex = index - 1;
      for (int i = 0; i < argumentCount; i++) {
        if (localIndex == -1) { //before string start
          temp[0] = new ImmutablePair<>("", null);
          removeIndex = 0;
          removeCount--;
        } else if (localIndex >= list.size()) { //at the end of string
          temp[i] = new ImmutablePair<>("", null);
          removeCount--;
        } else if ((i % 2) == 0 && list.get(localIndex).getRight() != null) { //in case two variables are standing beside each other.
          temp[i] = new ImmutablePair<>("", null);
          localIndex--;
          removeCount--;
        } else {
          temp[i] = list.get(localIndex);
        }
        localIndex++;
      }

      if (temp[1].getRight() == null || temp[3].getRight() == null) {
        continue;
      }
      if (!(partInterface.isAssignableFrom(temp[1].getRight().getClass()) && partInterface.isAssignableFrom(temp[3].getRight().getClass()))) {
        continue;
      }

      List<Pair<B, Triple<String, String, String>>> tempPars = new LinkedList<>();
      operators.forEach(item -> item.getRegexes().forEach(triple -> {
        if (matches(triple.getLeft(), temp[0].getLeft()) && matches(triple.getMiddle(), temp[2].getLeft()) && matches(triple.getRight(), temp[4].getLeft())) {
          tempPars.add(new ImmutablePair<>(item.getInstance(), triple));
        }
      }));

      if (tempPars.isEmpty()) {
        continue;
      }
      Pair<B, Triple<String, String, String>> hitMax = tempPars.get(0);
      String[] parsedMax = new String[]{"", "", ""};
      for (Pair<B, Triple<String, String, String>> s : tempPars) {
        String[] parsed = new String[3];
        parsed[0] = temp[0].getLeft();
        parsed[2] = temp[4].getLeft();

        parse(parsed, s.getRight().getLeft(), s.getRight().getRight());

        parsed[1] = String.format("%s %s %s %s %s", replaceLast(temp[0].getLeft(), parsed[0], ""), temp[1].getLeft(), temp[2].getLeft(), temp[3].getLeft(), replaceLast(temp[4].getLeft(), parsed[2], "")).replaceAll("\\s+$", "").trim();
        if (parsedMax[1].length() < parsed[1].length()) {
          System.arraycopy(parsed, 0, parsedMax, 0, 3);
          hitMax = s;
        }
      }

      for (int h = 0; h < removeCount; h++) {
        list.remove(removeIndex);
      }
      list.add(removeIndex, new ImmutablePair<String, IConditionPart>(parsedMax[1], hitMax.getLeft().newInstance((R) temp[1].getRight(), (R) temp[3].getRight()))); //save cast, was checked before.
      if (!parsedMax[0].isEmpty()) {
        list.add(removeIndex, new ImmutablePair<>(parsedMax[0], null)); //adding the prefix
        removeIndex++;
      }
      if (!parsedMax[2].isEmpty()) {
        list.add(removeIndex + 1, new ImmutablePair<>(parsedMax[2], null)); //adding the postfix
      }
      Pair<String, IConditionPart> entry = list.get(removeIndex);
      list = getUnaryOperator(removeIndex, list, unary, partType);
      index = -1; //reset for next go through.
    }
    return list;
  }

  /**
   * Finds all the binary operators at the moment visible.
   *
   * @param index         The start index.
   * @param list          The list to work on.
   * @param operators     The operators to find.
   * @param partInterface The datatype interface needed by the operators. Has to be similar to S. Can either be {@link IValuePart} or {@link IBooleanPart}.
   * @return The changed list.
   */
  private List<Pair<String, IConditionPart>> getUnaryOperator(int index, List<Pair<String, IConditionPart>> list, Collection<OperatorConfig<U, Pair<String, String>>> operators, Class<R> partInterface) {
    int argumentCount = 3;
    Pair<String, IConditionPart>[] temp = new Pair[argumentCount];
    int removeCount = argumentCount;
    int removeIndex = index - 1;
    int localIndex = index - 1;
    for (int i = 0; i < argumentCount; i++) {
      if (localIndex == -1) { //before string start
        temp[0] = new ImmutablePair<>("", null);
        removeIndex = 0;
        removeCount--;
      } else if (localIndex >= list.size()) { //at the end of string
        temp[i] = new ImmutablePair<>("", null);
        removeCount--;
      } else if (i % 2 == 0 && list.get(localIndex).getRight() != null) { //in case two variables are standing beside each other.
        temp[i] = new ImmutablePair<>("", null);
        localIndex--;
        removeCount--;
      } else {
        temp[i] = list.get(localIndex);
      }
      localIndex++;
    }

    if (temp[1].getRight() == null) {
      return list;
    }
    if (!(partInterface.isAssignableFrom(temp[1].getRight().getClass()))) {
      return list;
    }

    List<Pair<U, Pair<String, String>>> tempPars = new LinkedList<>();
    operators.forEach(item -> item.getRegexes().forEach(triple -> {
      if (matches(triple.getLeft(), temp[0].getLeft()) && matches(triple.getRight(), temp[2].getLeft())) {
        tempPars.add(new ImmutablePair<>(item.getInstance(), triple));
      }
    }));

    if (tempPars.isEmpty()) {
      return list;
    }

    Pair<U, Pair<String, String>> hitMax = tempPars.get(0);
    String[] parsedMax = new String[]{"", "", ""};
    for (Pair<U, Pair<String, String>> s : tempPars) {
      String[] parsed = new String[3];
      parsed[0] = temp[0].getLeft();
      parsed[2] = temp[2].getLeft();

      parse(parsed, s.getRight().getLeft(), s.getRight().getRight());

      parsed[1] = String.format("%s %s %s", replaceLast(temp[0].getLeft(), parsed[0], ""), temp[1].getLeft(), replaceLast(temp[2].getLeft(), parsed[2], "")).replaceAll("\\s+$", "").trim();
      if (parsedMax[1].length() < parsed[1].length()) {
        System.arraycopy(parsed, 0, parsedMax, 0, 3);
        hitMax = s;
      }
    }

    for (int h = 0; h < removeCount; h++) {
      list.remove(removeIndex);
    }
    list.add(removeIndex, new ImmutablePair<String, IConditionPart>(parsedMax[1], tempPars.iterator().next().getLeft().newInstance((R) temp[1].getRight()))); //save cast, was checked before.
    if (!parsedMax[0].isEmpty()) {
      list.add(removeIndex, new ImmutablePair<>(parsedMax[0], null)); //adding the prefix
      removeIndex++;
    }
    if (!parsedMax[2].isEmpty()) {
      list.add(removeIndex + 1, new ImmutablePair<>(parsedMax[2], null)); //adding the postfix
    }
    return list;
  }

  private void parse(String[] parsed, String left, String right) {
    String regex = left;
    for (int j = 0; j < 3; j += 2) {
      Matcher matcher = Pattern.compile(regex).matcher(parsed[j]);
      if (!matcher.matches()) {
        continue;
      }
      for (int i = 1; i <= matcher.groupCount(); i++) {
        parsed[j] = replaceLast(parsed[j], matcher.group(i), "");
      }
      parsed[j] = parsed[j].replaceAll("\\s+$", "").trim();
      regex = right;
    }
  }
}
