package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * Possible unary operators representing a value with children.
 */
public enum UnaryValueOperators implements IUnaryValueOperator {
  /**
   * Negates the value.
   */
  negate("(-%s)");

  private IValuePart part;
  private String representation;

  UnaryValueOperators(String s) {
    this.representation = s;
  }

  @Override
  public void setValue(IValuePart part) {
    this.part = part;
  }

  @Override
  public IUnaryValueOperator newInstance(IValuePart left) {
    return new ConcreteUnaryValueOperator(left, representation);
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, part.getRepresentation());
  }
}
