package de.fraunhofer.iosb.spinpro.variables;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;

import java.util.Collection;

/**
 * Represents a variable that can be used in a rule.
 */
public interface IVariable extends IConditionPart {
  /**
   * Returns the name of the variable.
   *
   * @return the name
   */
  String getName();

  /**
   * Returns the aliases of the variable.
   *
   * @return the aliases
   */
  Collection<String> getAliases();

  /**
   * Returns the devices that support the variable.
   *
   * @return the devices
   */
  Collection<String> getDevices();
}
