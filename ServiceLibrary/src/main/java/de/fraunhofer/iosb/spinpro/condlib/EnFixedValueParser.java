package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.FixedValue;
import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

/**
 * Implementation of a {@link IParser} for the english language with range from zero to one million.
 */
public class EnFixedValueParser implements IParserStart {
  StringHelper stringHelper;

  /**
   * Return fixed values as a list.
   *
   * @param input The input string.
   * @return A list of values found.
   * @throws ParsingException If a expression is not parsable.
   */
  public List<Pair<String, Double>> findFixedValues(String input) throws ParsingException {
    List<Pair<String, Double>> values = new ArrayList<>();
    List<String> words = new ArrayList<>();
    words.addAll(Arrays.stream(Numbers.values()).map(Enum::toString).collect(Collectors.toList()));
    words.addAll(Arrays.stream(TenNumbers.values()).map(Enum::toString).collect(Collectors.toList()));
    words.addAll(Arrays.stream(Factors.values()).map(Enum::toString).collect(Collectors.toList()));
    stringHelper = new StringHelper(input);

    while (true) {
      OptionalInt min = words.stream().filter(stringHelper.temp::contains).mapToInt(value -> stringHelper.temp.indexOf(value)).min();
      if (min.isEmpty()) {
        return values; //nothing more found
      }
      stringHelper.setTemp(stringHelper.getTemp().substring(min.getAsInt()));
      stringHelper.setSingleWord(stringHelper.getTemp().split("(\\s|-)")[0]);
      String old = stringHelper.temp;
      double value = stringHelper.getValue();
      int length = old.length() - stringHelper.temp.length();
      if (old.equals(stringHelper.temp)) {
        return values; //nothing happened...
      }
      values.add(new ImmutablePair<String, Double>(old.substring(0, length <= 0 ? old.length() : length).replaceAll("\\s+$", "").trim(), value));
    }
  }

  @Override
  public List<Pair<String, IConditionPart>> getValues(List<Pair<String, IConditionPart>> input) throws ParsingException {
    /* *************** find all fixed Values in every String *******************/
    for (int i = 0; i < input.size(); i++) {
      Pair<String, IConditionPart> element = input.get(i);
      if (element.getRight() != null) {
        continue;
      }
      List<Pair<String, Double>> values = findFixedValues(element.getLeft());
      for (Pair<String, Double> value : values) {
        element = input.get(i);
        int index = element.getLeft().indexOf(value.getLeft());
        if (index != 0) {
          input.set(i, new ImmutablePair<>(element.getLeft().substring(0, index).replaceAll("\\s+$", "").trim(), null));
          i++;
          input.add(i, new ImmutablePair<>(value.getLeft(), new FixedValue(value.getRight())));
        } else {
          input.set(i, new ImmutablePair<>(value.getLeft(), new FixedValue(value.getRight())));
        }
        i++;
        String end = element.getLeft().substring(index + value.getLeft().length());
        if (end.length() != 0) {
          input.add(i, new ImmutablePair<>(element.getLeft().substring(index + value.getLeft().length()).replaceAll("\\s+$", "").trim(), null));
        }
      }
    }
    return input;
  }

  @Override
  public List<Pair<String, IConditionPart>> getValues(String string) throws ParsingException {
    return getValues(List.of(new ImmutablePair<>(string, null)));
  }

  private enum Numbers {
    zero(0), one(1), two(2), three(3), four(4), five(5), six(6), seven(7), eight(8), nine(9), ten(10), elven(11), twelve(12), thirteen(13), fourteen(14), fifteen(15), sixteen(16), seventeen(17), eighteen(18), nineteen(19);
    private final int number;

    Numbers(int i) {
      this.number = i;
    }

    int getValue() {
      return number;
    }
  }

  private enum TenNumbers {
    twenty(20), thirty(30), forty(40), fifty(50), sixty(60), seventy(70), eighty(80), ninety(90);
    private final int number;

    TenNumbers(int i) {
      this.number = i;
    }

    int getValue() {
      return number;
    }
  }

  private enum Factors {
    hundred(100), thousand(1000), million(1000000);
    private final int number;

    Factors(int i) {
      this.number = i;
    }

    int getValue() {
      return number;
    }
  }

  private static class StringHelper {
    private String temp; //string to work on
    private String singleWord; //first word of the String

    public StringHelper(String temp) {
      this.temp = temp;
    }

    public String getTemp() {
      return temp;
    }

    public void setTemp(String temp) {
      this.temp = temp;
    }

    public void setSingleWord(String singleWord) {
      this.singleWord = singleWord;
    }

    private void nextWord() {
      temp = temp.substring(singleWord.length()).trim().replaceAll("^-", "");
      singleWord = temp.split("(\\s|-)")[0];
    }

    private boolean isInFactors() {
      return Arrays.stream(Factors.values()).map(Enum::toString).anyMatch(s -> s.equals(singleWord));
    }

    private boolean isInNumbers() {
      return Arrays.stream(Numbers.values()).map(Enum::toString).anyMatch(s -> s.equals(singleWord));
    }

    private boolean isInTenNumbers() {
      return Arrays.stream(TenNumbers.values()).map(Enum::toString).anyMatch(s -> s.equals(singleWord));
    }

    private int getDualDecimal() {
      if (isInNumbers()) {
        int tempVal = Numbers.valueOf(singleWord).getValue();
        nextWord();
        return tempVal;
      } else if (isInTenNumbers()) {
        int tempVal = TenNumbers.valueOf(singleWord).getValue();
        nextWord();
        if (isInNumbers()) {
          tempVal += Numbers.valueOf(singleWord).getValue();
          nextWord();
          return tempVal;
        } else {
          return tempVal;
        }
      } else {
        return 0; //no number there
      }
    }

    private int getHundred() throws ParsingException {
      //1-9 Hundred 1-9 0-9
      int temp = getDualDecimal();

      if (isInFactors() && Factors.valueOf(singleWord).equals(Factors.hundred)) {
        if (temp == 0) {
          throw new ParsingException("Zero not allowed in this place.");
        }
        if (temp > 9) {
          throw new ParsingException("Invalid value before hundred.");
        }
        temp *= Factors.hundred.getValue();
        nextWord();
        temp += getDualDecimal();
      }
      return temp;
    }

    private int getThousand() throws ParsingException {
      int temp = getHundred();
      if (isInFactors() && Factors.valueOf(singleWord).equals(Factors.thousand)) {
        temp *= Factors.thousand.getValue();
        nextWord();
        temp += getHundred();
      }
      return temp;
    }

    public double getValue() throws ParsingException {
      int temp = getHundred();
      if (isInFactors() && Factors.valueOf(singleWord).equals(Factors.million)) {
        temp *= Factors.million.getValue();
        nextWord();
        temp += getThousand();
      } else if (isInFactors() && Factors.valueOf(singleWord).equals(Factors.thousand)) {
        temp *= Factors.thousand.getValue();
        nextWord();
        temp += getHundred();
      }
      double value = 0;
      if (singleWord.equals("and")) {
        String old = this.temp;
        String singleWordOld = this.singleWord;
        nextWord();
        if (isInNumbers() || isInTenNumbers()) {
          temp += getDualDecimal();
        } else {
          this.temp = old;
          this.singleWord = singleWordOld;
        }
      }
      if (singleWord.equals("point")) {
        nextWord();
        int counter = 1;
        while (isInNumbers()) {
          value += Math.pow(10, -counter) * Numbers.valueOf(singleWord).getValue();
          nextWord();
          counter++;
        }
      }
      return temp + value;
    }
  }
}
