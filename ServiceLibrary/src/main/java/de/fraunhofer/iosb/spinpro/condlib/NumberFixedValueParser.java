package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.FixedValue;
import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

/**
 * Implementation of a {@link IParser} for digits.
 */
public class NumberFixedValueParser implements IParserStart {

  private static final int BASE = 10;
  private static final String HDIVIDER = ",";
  private static final String FDIVIDER = "\\.";
  private static final String[] DIGITS = new String[BASE];

  /**
   * Creates a new instance.
   */
  public NumberFixedValueParser() {
    for (int i = 0; i < BASE; i++) {
      DIGITS[i] = String.valueOf(i);
    }
  }

  /**
   * Return fixed values as a list.
   *
   * @param input The input string.
   * @return A list of values found, associated with the string representation.
   * @throws ParsingException If a expression is not parsable.
   */
  public List<Pair<String, Double>> findFixedValues(String input) throws ParsingException {
    List<Pair<String, Double>> output = new LinkedList<>();
    String inputCopy = input;
    while (true) {
      OptionalInt optMin = Arrays.stream(DIGITS).filter(inputCopy::contains).mapToInt(inputCopy::indexOf).min();
      int min = optMin.orElse(-1);
      int divider = inputCopy.indexOf(FDIVIDER);
      if (divider >= 0) {
        min = Math.min(min, divider);
      }
      if (min <= -1) {
        return output;
      }
      inputCopy = inputCopy.substring(min);
      double value = 0;
      boolean isInt = true;
      int factor = BASE;
      while (true) {
        String finalInputCopy = inputCopy;
        List<String> digit = Arrays.stream(DIGITS).filter(s -> finalInputCopy.matches("^" + s + ".*")).collect(Collectors.toList());
        if (digit.size() != 1) {
          if (inputCopy.matches("^" + FDIVIDER + ".*") && isInt) {
            isInt = false;
            inputCopy = inputCopy.replaceAll("^" + FDIVIDER, "");
          } else if (inputCopy.matches("^" + HDIVIDER + ".*")) {
            inputCopy = inputCopy.replaceAll("^" + HDIVIDER, "");
          } else {
            break;
          }
        } else {
          if (isInt) {
            value *= BASE;
            int temp = getDigitFromString(digit.get(0));
            if (temp == -1) {
              throw new ParsingException("Could not find digit.");
            }
            value += temp;
          } else {
            int temp = getDigitFromString(digit.get(0));
            if (temp == -1) {
              throw new ParsingException("Could not find digit.");
            }
            value += ((double) temp / factor);
            factor *= BASE;
          }
          inputCopy = inputCopy.replaceAll("^" + digit.get(0), "");
        }
      }
      output.add(new ImmutablePair<>(input.substring(min).replace(inputCopy, ""), value));
    }
  }

  private int getDigitFromString(String input) {
    for (int i = 0; i < DIGITS.length; i++) {
      if (DIGITS[i].equals(input)) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public List<Pair<String, IConditionPart>> getValues(List<Pair<String, IConditionPart>> input) throws ParsingException {
    /* *************** find all fixed Values in every String *******************/
    for (int i = 0; i < input.size(); i++) {
      Pair<String, IConditionPart> element = input.get(i);
      if (element.getRight() != null) {
        continue;
      }
      List<Pair<String, Double>> values = findFixedValues(element.getLeft());
      for (Pair<String, Double> value : values) {
        element = input.get(i);
        int index = element.getLeft().indexOf(value.getLeft());
        if (index != 0) {
          input.set(i, new ImmutablePair<>(element.getLeft().substring(0, index).replaceAll("\\s+$", "").trim(), null));
          i++;
          input.add(i, new ImmutablePair<>(value.getLeft(), new FixedValue(value.getRight())));
        } else {
          input.set(i, new ImmutablePair<>(value.getLeft(), new FixedValue(value.getRight())));
        }
        i++;
        String end = element.getLeft().substring(index + value.getLeft().length());
        if (end.length() != 0) {
          input.add(i, new ImmutablePair<>(element.getLeft().substring(index + value.getLeft().length()).replaceAll("\\s+$", "").trim(), null));
        }
      }
    }
    return input;
  }

  @Override
  public List<Pair<String, IConditionPart>> getValues(String string) throws ParsingException {
    return getValues(List.of(new ImmutablePair<>(string, null)));
  }
}
