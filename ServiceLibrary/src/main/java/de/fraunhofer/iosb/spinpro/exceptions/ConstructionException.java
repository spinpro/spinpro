package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * This {@link Exception} is whenever the construction of an Object jails due to an invalid configuration.
 */
public class ConstructionException extends Exception {
  static final long serialVersionUID = -1556364979673529117L;

  /**
   * Constructs a new {@link ConstructionException} exception.
   */
  public ConstructionException() {
  }

  /**
   * Constructs a new {@link ConstructionException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public ConstructionException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link ConstructionException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public ConstructionException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link ConstructionException} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public ConstructionException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link ConstructionException} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected ConstructionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
