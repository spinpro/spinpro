package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;
import java.util.Objects;

/**
 * An interface representing an action that can be stored inside an {@code IRule}.
 */
public class WhileAction extends AbstractAction {
  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "while";

  private String condition;
  private IAction[] thenActions;

  /**
   * Creates a WhileAction with a specified condition and actions that should be performed
   * while the condition is true.
   *
   * @param condition   the condition
   * @param thenActions the actions
   */
  public WhileAction(String condition, IAction[] thenActions) {
    this.condition = condition;
    this.thenActions = thenActions;
  }

  /**
   * Returns the condition of the while action.
   *
   * @return the condition
   */
  public String getCondition() {
    return condition;
  }

  /**
   * Sets the condition of the while action.
   *
   * @param condition the condition
   */
  public void setCondition(String condition) {
    this.condition = condition;
  }

  /**
   * Returns the actions that should be performed while the condition is true.
   *
   * @return the actions
   */
  public IAction[] getThenActions() {
    return thenActions;
  }

  /**
   * Sets the actions that should be performed while the condition is true.
   *
   * @param thenActions the actions
   */
  public void setThenActions(IAction[] thenActions) {
    this.thenActions = thenActions;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      WhileAction input = (WhileAction) obj;

      return Objects.equals(input.condition, this.condition) && Arrays.equals(input.thenActions, this.thenActions);
    }
    return false;
  }
}
