package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * Possible binary operators representing a boolean with children.
 */
public enum BinaryValueToBooleanOperators implements IBinaryValueToBooleanOperator {
  /**
   * The arithmetic comparison less than of two elements.
   */
  lessThan("(%s < %s)"),
  /**
   * The arithmetic comparison greater than of two elements.
   */
  greaterThan("(%s > %s)"),
  /**
   * The arithmetic comparison less or equals than of two elements.
   */
  lessEquals("(%s <= %s)"),
  /**
   * The arithmetic comparison greater or equals than of two elements.
   */
  greaterEquals("(%s >= %s)"),
  /**
   * The arithmetic comparison equals of two elements.
   */
  equals("(%s == %s)"),
  /**
   * The arithmetic comparison not equals of two elements.
   */
  unequals("(%s != %s)");

  private String representation;
  private IValuePart left;
  private IValuePart right;

  BinaryValueToBooleanOperators(String representation) {
    this.representation = representation;
  }

  protected IValuePart getLeft() {
    return left;
  }

  @Override
  public void setLeft(IValuePart part) {
    left = part;
  }

  protected IValuePart getRight() {
    return right;
  }

  @Override
  public void setRight(IValuePart part) {
    right = part;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getLeft().getRepresentation(), getRight().getRepresentation());
  }

  @Override
  public IBinaryValueToBooleanOperator newInstance(IValuePart left, IValuePart right) {
    return new ConcreteBinaryValueToBooleanOperator(left, right, representation);
  }
}
