package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 * The interface that a S2T-Plugin-Wrapper must implement. With this interface it is possible to recognize speech in an
 * audiofile and transfer the spoken into a {@code String}. This interface is part of an {@link java.util.ServiceLoader}
 * architecture.
 */
public interface ISpeechToTextService {
  /**
   * Recognizes speech in an audio file and returns the text. The {@link AudioFormat} of the
   * {@link AudioInputStream}is not guaranteed to be one of the supported {@link AudioFormat}s.
   * The plugin can either convert the {@link AudioInputStream} to have a fitting
   * {@link AudioFormat} or throw an {@code UnsupportedAudioFormat}-Exception.
   * If it is possible to convert the original input into one of the {@link AudioFormat}s returned by
   * {@code getSupportedAudioFormats}, the {@link AudioInputStream} will be provided in one of this formats.
   *
   * @param ais The audio data to recognize speech in.
   * @return The recognized speech as {@code String}
   * @throws UnsupportedAudioFormat       Whenever an {@link AudioFormat} is not supported by a plugin.
   * @throws ServiceInitializingException Whenever an Service cannot be initialized.
   */
  String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat, ServiceInitializingException;

  /**
   * Gives back an instance of the {@link AudioFormat} the plugin supports in {@code PSE.s2t} as the format of
   * {@link AudioInputStream}.
   *
   * @return The instance of the {@link AudioFormat} that is expected.
   */
  AudioFormat[] getSupportedAudioFormats();
}
