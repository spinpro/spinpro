package de.fraunhofer.iosb.spinpro.condlib.parts;

/**
 * A value, that does not change.
 */
interface IFixedValue extends IValuePart {
  /**
   * The fixed value.
   *
   * @return The value.
   */
  double getValue();
}
