package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * An concrete binary operator representing a boolean.
 */
public class ConcreteBinaryValueToBooleanOperator extends BinaryValueToBooleanOperator {
  private String representation;

  /**
   * Creates a new instance.
   *
   * @param left           The left child to set.
   * @param right          The right child to set.
   * @param representation The representation string used in the {@code String.format} call. May contain two {@code %s}.
   */
  public ConcreteBinaryValueToBooleanOperator(IValuePart left, IValuePart right, String representation) {
    super(left, right);
    this.representation = representation;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getLeft().getRepresentation(), getRight().getRepresentation());
  }

  @Override
  public IBinaryValueToBooleanOperator newInstance(IValuePart left, IValuePart right) {
    return new ConcreteBinaryValueToBooleanOperator(left, right, representation);
  }
}
