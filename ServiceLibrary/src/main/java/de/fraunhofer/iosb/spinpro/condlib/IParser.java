package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * A general parser. Takes a list of strings as an input and associates the strings with parts. Some parsers need a special format of the list as an input to be able to parse data in their step.
 *
 * <p>Usually multiple {@link IParser} will be connected in series to parse high level logic. A usual sequence of {@link IParser}s could be:
 * <ol>
 *   <li>{@link VariableParser} - For finding {@link de.fraunhofer.iosb.spinpro.variables.IInputVariable}s and/or {@link de.fraunhofer.iosb.spinpro.variables.IOutputEvent}s and associate them with the corresponding strings, by splitting of the
 *   string and set the corresponding {@link IConditionPart}</li>
 *   <li>{@link EnFixedValueParser} - For finding fixed values/numbers like "nine" (=9) in the string, split them of the string and associate them with their corresponding {@link IConditionPart}.</li>
 *   <li>{@link NumberFixedValueParser} - For finding fixed arabic values/numbers like 9 (=9) in the string, split them of the string and associate them with their corresponding {@link IConditionPart}.</li>
 *   <li>{@link PartParser} - For finding operators in the string, split them of the string and associate them with their corresponding {@link IConditionPart}.</li>
 * </ol>
 * As the start of the sequence you can use any {@link IParserStart}.
 *
 * <p>To be able to chain those {@link IParser}s effectively, two interfaces are providing a high level access to parsers for special use cases. If possible, parsing may be handled by these abstractions:
 * <ul>
 *   <li>{@link IArithmeticParser} - Parser for arithmetical expressions.</li>
 *   <li>{@link IConditionParser} - Parser for boolean arithmetical expressions.</li>
 * </ul>
 * These two will both use {@link IParser}s internally.
 */
public interface IParser {
  /**
   * Parses all entries of a list.
   *
   * @param input The list to parse. A list containing one or multiple strings with the already known association to IParts or {@code null} if there is no association.
   * @return The parsed list. A list containing the splitted strings with the new associations to IParts or {@code null} if there is no association.
   * @throws ParsingException If {@code input} is not parsable.
   */
  List<Pair<String, IConditionPart>> getValues(List<Pair<String, IConditionPart>> input) throws ParsingException;
}
