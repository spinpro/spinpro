package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorConfig;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.config.OperatorsConfig;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parses a natural spoken condition to an arithmetic expression.
 * Needs to be configured via a {@link OperatorConfig} object.
 */
public class ArithmeticParser implements IArithmeticParser {
  private static final String ESCAPE_CHAR = "°";
  private List<Pair<String, IVariable>> variables;
  private IParser fixedValueParser;
  private IParser vp;
  private IParser vtvP;

  /**
   * Creates a new instance.
   *
   * @param variables        The input-variables to use.
   * @param fixedValueParser The parser to use for fixed values.
   * @param ops              An object containing the configuration for the operators.
   */
  public ArithmeticParser(Collection<IVariable> variables, IParser fixedValueParser, OperatorsConfig ops) {
    this.variables = new LinkedList<>();
    variables.forEach(s -> s.getAliases().forEach(a -> this.variables.add(new ImmutablePair<>(a, s))));
    this.fixedValueParser = fixedValueParser;
    this.variables.sort(new Comparator<Pair<String, IVariable>>() {
      @Override
      public int compare(Pair<String, IVariable> o1, Pair<String, IVariable> o2) {
        int dist = o1.getLeft().length() - o2.getLeft().length();
        if (dist != 0) {
          return dist;
        }
        return (int) Math.signum(o1.getLeft().compareTo(o2.getLeft()));
      }
    });
    vtvP = new PartParser<>(ops.getBinaryOperators().getValueToValueOperators(), ops.getUnaryOperators().getValueToValueOperators(), IValuePart.class);
    vp = new VariableParser(this.variables);
  }

  /**
   * Creates a new instance.
   *
   * @param variables        The input-variables to use.
   * @param fixedValueParser The parser to use for fixed values.
   * @param ops              An object containing the configuration for the operators.
   */
  public ArithmeticParser(List<Pair<String, IVariable>> variables, IParser fixedValueParser, OperatorsConfig ops) {
    this.variables = variables;
    this.fixedValueParser = fixedValueParser;
    vtvP = new PartParser<>(ops.getBinaryOperators().getValueToValueOperators(), ops.getUnaryOperators().getValueToValueOperators(), IValuePart.class);
    vp = new VariableParser(this.variables);
  }

  @Override
  public String getExpression(String input) throws ParsingException {
    return getExpression(input, 10);
  }

  @Override
  public String getExpression(String input, double tolerance) throws ParsingException {
    List<Pair<String, IConditionPart>> list = new LinkedList<Pair<String, IConditionPart>>();
    list.add(new ImmutablePair<String, IConditionPart>(ESCAPE_CHAR + " " + input + " " + ESCAPE_CHAR, null));
    //At the begin of a string and at the end isn't implicitly an Variable. For that reason we are preventing the string to be over there, because one string path is defined to match between vars.
    //Usually a unary operator is captured, because it has at least one char before the operators name. The two chars will ensure that there is this char.

    list = fixedValueParser.getValues(list);
    list = vp.getValues(list);
    list = vtvP.getValues(list);

    list = list.stream().filter(a -> a.getRight() != null && IValuePart.class.isAssignableFrom(a.getRight().getClass())).collect(Collectors.toList());
    if (list.size() != 1) {
      throw new ParsingException("Could not identify the value part. Found " + list.size() + ".");
    }
    double percentage = list.get(0).getLeft().length() / (double) input.length();
    if (percentage <= tolerance) {
      throw new ParsingException("Could not successfully parse enough words to fulfill the tolerance.");
    }
    return list.get(0).getRight().getRepresentation();
  }
}
