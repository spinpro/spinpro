package de.fraunhofer.iosb.spinpro.variables;

/**
 * Defines a variable type that represents an output {@link IVariable} in a rule.
 */
public interface IOutputEvent extends IVariable {
}
