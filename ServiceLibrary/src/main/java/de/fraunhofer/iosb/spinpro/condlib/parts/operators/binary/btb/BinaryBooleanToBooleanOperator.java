package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * An binary operator representing a boolean.
 */
public abstract class BinaryBooleanToBooleanOperator implements IBinaryBooleanToBooleanOperator {
  /**
   * The left part.
   */
  private IBooleanPart left;
  /**
   * The right part.
   */
  private IBooleanPart right;

  /**
   * Creates a new instance.
   *
   * @param left  The left sub-part in the condition.
   * @param right The right sub-part in the condition.
   */
  public BinaryBooleanToBooleanOperator(IBooleanPart left, IBooleanPart right) {
    this.left = left;
    this.right = right;
  }

  /**
   * Getter for left part.
   *
   * @return The left part.
   */
  protected IBooleanPart getLeft() {
    return left;
  }

  @Override
  public void setLeft(IBooleanPart part) {
    left = part;
  }

  /**
   * Getter for right part.
   *
   * @return The right part.
   */
  protected IBooleanPart getRight() {
    return right;
  }

  @Override
  public void setRight(IBooleanPart part) {
    right = part;
  }
}
