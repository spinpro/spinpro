package de.fraunhofer.iosb.spinpro.condlib.parts;

public class FixedValue implements IFixedValue {
  private final double value;

  public FixedValue(double value) {
    this.value = value;
  }

  @Override
  public double getValue() {
    return value;
  }

  @Override
  public String getRepresentation() {
    return String.valueOf(getValue());
  }
}
