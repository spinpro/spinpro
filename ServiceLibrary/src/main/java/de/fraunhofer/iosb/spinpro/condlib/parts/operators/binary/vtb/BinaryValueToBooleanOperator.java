package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * An binary operator representing a boolean.
 */
public abstract class BinaryValueToBooleanOperator implements IBinaryValueToBooleanOperator {
  private IValuePart left;
  private IValuePart right;

  /**
   * Creates a new instance.
   *
   * @param left  The left sub-part in the condition.
   * @param right The right sub-part in the condition.
   */
  public BinaryValueToBooleanOperator(IValuePart left, IValuePart right) {
    this.left = left;
    this.right = right;
  }

  protected IValuePart getLeft() {
    return left;
  }

  @Override
  public void setLeft(IValuePart part) {
    left = part;
  }

  protected IValuePart getRight() {
    return right;
  }

  @Override
  public void setRight(IValuePart part) {
    right = part;
  }
}
