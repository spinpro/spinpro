package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * This {@link Exception} is thrown whenever a service can not be initialized. There can be various reasons
 * for this {@link Exception} like a wrong configuration or a missing file inside of the service jar.
 */
public class ServiceInitializingException extends Exception {
  static final long serialVersionUID = -3387516993124229947L;

  /**
   * Constructs a new {@link ServiceInitializingException} exception.
   */
  public ServiceInitializingException() {
  }

  /**
   * Constructs a new {@link ServiceInitializingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public ServiceInitializingException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link ServiceInitializingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public ServiceInitializingException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link ServiceInitializingException} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public ServiceInitializingException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link ServiceInitializingException} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected ServiceInitializingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
