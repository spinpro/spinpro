package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.IUnaryInstantiable;

/**
 * An unary operator representing a boolean with child.
 */
public interface IUnaryBooleanOperator extends IBooleanPart, IUnaryInstantiable<IUnaryBooleanOperator, IBooleanPart> {
  /**
   * Sets the inner condition part.
   *
   * @param part The part to set.
   */
  void setValue(IBooleanPart part);
}