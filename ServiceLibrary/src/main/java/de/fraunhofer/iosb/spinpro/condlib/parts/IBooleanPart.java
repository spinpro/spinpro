package de.fraunhofer.iosb.spinpro.condlib.parts;

/**
 * A part of the condition representing a boolean.
 */
public interface IBooleanPart extends IConditionPart {

}
