package de.fraunhofer.iosb.spinpro.actions;

/**
 * An interface representing an action that can be stored inside an {@code IRule}.
 */
public interface IAction {

  /**
   * Returns the type of the action.
   *
   * @return the type
   */
  String getType();

  /**
   * Returns the String that was parsed into this action.
   *
   * @return the original unparsed String
   */
  String getOriginalString();

  /**
   * Sets the String that was parsed into this action.
   *
   * @param origin the original unparsed String
   */
  void setOriginalString(String origin);
}
