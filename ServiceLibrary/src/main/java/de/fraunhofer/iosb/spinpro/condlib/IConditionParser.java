package de.fraunhofer.iosb.spinpro.condlib;

import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;

/**
 * Parses a natural spoken condition to a boolean arithmetic expression.
 */
public interface IConditionParser extends IArithmeticParser {
  /**
   * Parses a natural spoken condition to an boolean arithmetic expression.
   *
   * @param input The natural spoken.
   * @return The arithmetic expression.
   * @throws ParsingException If it is not parsable.
   */
  String getExpression(String input) throws ParsingException;

  /**
   * Parses a natural spoken condition to an value arithmetic expression.
   *
   * @param input The natural spoken.
   * @param tolerance The minimum of the string must be used for parsing.
   * @return The arithmetic expression.
   * @throws ParsingException If it is not parsable or not allowed to parse.
   */
  String getExpression(String input, double tolerance) throws ParsingException;
}
