package de.fraunhofer.iosb.spinpro.condlib.parts.operators.config;

import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb.IBinaryBooleanToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtb.IBinaryValueToBooleanOperator;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv.IBinaryValueToValueOperator;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Configuration containing all binary operators.
 */
@SuppressWarnings("checkstyle:LineLength")
public class BinaryOperatorConfig {
  Collection<OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>> booleanToBooleanOperators;
  Collection<OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>> valueToValueOperators;
  Collection<OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>> valueToBooleanOperators;

  /**
   * Creates a new instance.
   *
   * @param booleanToBooleanOperators All boolean to boolean operator configs.
   * @param valueToValueOperators     All value to value operator configs.
   * @param valueToBooleanOperators   All value to boolean operator configs.
   */
  public BinaryOperatorConfig(Collection<OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>> booleanToBooleanOperators,
                              Collection<OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>> valueToValueOperators,
                              Collection<OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>> valueToBooleanOperators) {
    this.booleanToBooleanOperators = booleanToBooleanOperators;
    this.valueToValueOperators = valueToValueOperators;
    this.valueToBooleanOperators = valueToBooleanOperators;
  }

  /**
   * Creates a new instance.
   */
  public BinaryOperatorConfig() {
    booleanToBooleanOperators = new LinkedList<>();
    valueToBooleanOperators = new LinkedList<>();
    valueToValueOperators = new LinkedList<>();
  }

  /**
   * Getter for boolean to boolean operators.
   *
   * @return All boolean to boolean operator configs.
   */
  public Collection<OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>> getBooleanToBooleanOperators() {
    return booleanToBooleanOperators;
  }

  /**
   * Sets all boolean to boolean operator configs.
   *
   * @param booleanToBooleanOperators The configs to set.
   */
  public void setBooleanToBooleanOperators(Collection<OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>>> booleanToBooleanOperators) {
    this.booleanToBooleanOperators = booleanToBooleanOperators;
  }

  /**
   * Getter for value to value operators.
   *
   * @return All value to value operator configs.
   */
  public Collection<OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>> getValueToValueOperators() {
    return valueToValueOperators;
  }

  /**
   * Sets all value to value operator configs.
   *
   * @param valueToValueOperators The configs to set.
   */
  public void setValueToValueOperators(Collection<OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>>> valueToValueOperators) {
    this.valueToValueOperators = valueToValueOperators;
  }

  /**
   * Getter for value to boolean operator configs.
   *
   * @return All value to boolean operator configs.
   */
  public Collection<OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>> getValueToBooleanOperators() {
    return valueToBooleanOperators;
  }

  /**
   * Sets all value to boolean operator configs.
   *
   * @param valueToBooleanOperators The configs to set.
   */
  public void setValueToBooleanOperators(Collection<OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>>> valueToBooleanOperators) {
    this.valueToBooleanOperators = valueToBooleanOperators;
  }

  /**
   * Adds a boolean to boolean operator config.
   *
   * @param btbOperator The config to add.
   */
  public void addBtB(OperatorConfig<IBinaryBooleanToBooleanOperator, Triple<String, String, String>> btbOperator) {
    booleanToBooleanOperators.add(btbOperator);
  }

  /**
   * Adds a value to value operator config.
   *
   * @param vtvOperator The config to add.
   */
  public void addVtV(OperatorConfig<IBinaryValueToValueOperator, Triple<String, String, String>> vtvOperator) {
    valueToValueOperators.add(vtvOperator);
  }

  /**
   * Adds a value to boolean operator config.
   *
   * @param vtbOperator The config to add.
   */
  public void addVtB(OperatorConfig<IBinaryValueToBooleanOperator, Triple<String, String, String>> vtbOperator) {
    valueToBooleanOperators.add(vtbOperator);
  }
}
