package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * An concrete binary operator representing a boolean.
 */
public class ConcreteBinaryBooleanToBooleanOperator extends BinaryBooleanToBooleanOperator {
  private String representation;

  /**
   * Creates a new instance.
   *
   * @param left           The left child to set.
   * @param right          The right child to set.
   * @param representation The representation string used in the {@code String.format} call. May contain two {@code %s}.
   */
  public ConcreteBinaryBooleanToBooleanOperator(IBooleanPart left, IBooleanPart right, String representation) {
    super(left, right);
    this.representation = representation;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getLeft().getRepresentation(), getRight().getRepresentation());
  }

  @Override
  public IBinaryBooleanToBooleanOperator newInstance(IBooleanPart left, IBooleanPart right) {
    return new ConcreteBinaryBooleanToBooleanOperator(left, right, representation);
  }
}
