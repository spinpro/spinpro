package de.fraunhofer.iosb.spinpro.services;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

/**
 * An {@link IActionService} that can disassemble complex sentence constructions.
 * A complex sentence construction can contain multiple nested or concatenated actions, that need further processing.
 * Special words are required, that describe the sentence construction e.g. for nesting or concatenating actions.
 * The service does not have to parse those special words.
 * But they might be contained in the nested actions of the given sentence, which are given back to {@link IActionParser}.
 */
public interface IComplexActionService extends IActionService {

  /**
   * Parses the given String to an {@link IAction}.
   * Uses input variable and output event manager
   * to find variables in the given rule.
   *
   * @param input          the input string, that is split before and after each nested block
   * @param inputVarMan    the provided input variable manager
   * @param outputEventMan the provided output event manager
   * @param actionParser   the action parser, that can be used for a callback
   * @return the created {@link IAction}
   * @throws ParsingException If an action cannot be parsed.
   */
  IAction execute(String[] input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException;

}
