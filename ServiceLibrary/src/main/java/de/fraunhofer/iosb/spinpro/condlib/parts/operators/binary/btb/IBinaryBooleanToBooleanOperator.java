package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.IBinaryInstantiable;

/**
 * An binary operator representing a boolean.
 */
public interface IBinaryBooleanToBooleanOperator extends IBooleanPart, IBinaryInstantiable<IBinaryBooleanToBooleanOperator, IBooleanPart> {
  /**
   * Sets the inner left condition part.
   *
   * @param part The part to set.
   */
  void setLeft(IBooleanPart part);

  /**
   * Sets the inner right condition part.
   *
   * @param part The part to set.
   */
  void setRight(IBooleanPart part);
}
