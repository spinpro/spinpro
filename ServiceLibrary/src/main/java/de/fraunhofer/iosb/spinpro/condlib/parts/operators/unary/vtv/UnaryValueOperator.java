package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * An unary operator representing a value.
 */
public abstract class UnaryValueOperator implements IUnaryValueOperator {
  private IValuePart value;

  /**
   * Creates a new instance.
   *
   * @param value The sub-part in the condition.
   */
  public UnaryValueOperator(IValuePart value) {
    this.value = value;
  }

  protected IValuePart getValue() {
    return value;
  }

  @Override
  public void setValue(IValuePart part) {
    this.value = part;
  }
}
