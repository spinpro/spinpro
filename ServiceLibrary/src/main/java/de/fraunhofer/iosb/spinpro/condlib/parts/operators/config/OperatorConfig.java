package de.fraunhofer.iosb.spinpro.condlib.parts.operators.config;

import de.fraunhofer.iosb.spinpro.condlib.parts.IConditionPart;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Configuration for a single operator.
 *
 * @param <T> The concrete operator type.
 * @param <S> The type of the collection used for the strings. Usually {@link org.apache.commons.lang3.tuple.Triple} for
 *            binary operators and {@link org.apache.commons.lang3.tuple.Pair} for unary operator.
 */
public class OperatorConfig<T extends IConditionPart, S> {
  private T instance;
  private Collection<S> regexes;

  /**
   * Creates a new instance.
   *
   * @param instance The operator.
   * @param regexes  The regexes to use for recognition. Each regex matches a full string between values.
   *                 The string matched into a capture group is the representation for the operator itself.
   *                 In case of a binary operator the full path between the values is removed. For the regexes
   *                 before the first or after the last operator only the strings in the capture groups are removed.
   */
  public OperatorConfig(T instance, Collection<S> regexes) {
    this.instance = instance;
    this.regexes = regexes;
  }

  /**
   * Creates a new instance.
   *
   * @param instance The operator.
   * @param strings  The regexes to use for recognition.
   */
  @SafeVarargs
  public OperatorConfig(T instance, S... strings) {
    this.instance = instance;
    this.regexes = new LinkedList<>();
    this.regexes.addAll(Arrays.asList(strings));
  }

  /**
   * Creates a new instance.
   *
   * @param instance The operator.
   */
  public OperatorConfig(T instance) {
    this.instance = instance;
    this.regexes = new LinkedList<>();
  }

  /**
   * Adds a new regex set to the collection.
   *
   * @param regexes The regexes to use for recognition.
   * @return This object for builder pattern.
   */
  public OperatorConfig<T, S> add(S regexes) {
    this.regexes.add(regexes);
    return this;
  }

  /**
   * Getter for the instance.
   *
   * @return The operator itself.
   */
  public T getInstance() {
    return instance;
  }

  /**
   * Sets the operator.
   *
   * @param instance The operator to set.
   */
  public void setInstance(T instance) {
    this.instance = instance;
  }

  /**
   * Getter for the regexes.
   *
   * @return The regexes to use for recognition.
   */
  public Collection<S> getRegexes() {
    return regexes;
  }

  /**
   * Sets the regexes used for recognition.
   *
   * @param regexes The regexes used for recognition.
   */
  public void setRegexes(Collection<S> regexes) {
    this.regexes = regexes;
  }
}
