package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * Possible unary operators representing a boolean with children.
 */
public enum UnaryBooleanOperators implements IUnaryBooleanOperator {
  /**
   * The boolean not.
   */
  not("(!%s)");

  private IBooleanPart part;
  private String representation;

  UnaryBooleanOperators(String s) {
    this.representation = s;
  }

  @Override
  public void setValue(IBooleanPart part) {
    this.part = part;
  }

  @Override
  public IUnaryBooleanOperator newInstance(IBooleanPart left) {
    return new ConcreteUnaryBooleanOperator(left, representation);
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, part.getRepresentation());
  }
}
