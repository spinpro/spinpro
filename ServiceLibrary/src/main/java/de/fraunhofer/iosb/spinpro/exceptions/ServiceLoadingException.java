package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * This {@link Exception} is thrown whenever a service can not be loaded.
 */
public class ServiceLoadingException extends Exception {
  static final long serialVersionUID = -3387516993124229947L;

  /**
   * Constructs a new {@link ServiceLoadingException} exception.
   */
  public ServiceLoadingException() {
  }

  /**
   * Constructs a new {@link ServiceLoadingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public ServiceLoadingException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link ServiceLoadingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public ServiceLoadingException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link ServiceLoadingException} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public ServiceLoadingException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link ServiceLoadingException} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected ServiceLoadingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
