package de.fraunhofer.iosb.spinpro.condlib.parts.operators.config;

/**
 * Configuration containing all operators.
 */
public class OperatorsConfig {
  private BinaryOperatorConfig binaryOperators;
  private UnaryOperatorConfig unaryOperators;

  /**
   * Creates a new instance.
   *
   * @param binaryOperators All the binary operators.
   * @param unaryOperators  All the unary operators.
   */
  public OperatorsConfig(BinaryOperatorConfig binaryOperators, UnaryOperatorConfig unaryOperators) {
    this.binaryOperators = binaryOperators;
    this.unaryOperators = unaryOperators;
  }

  /**
   * Getter for unary operators.
   *
   * @return Returns all unary operators.
   */
  public UnaryOperatorConfig getUnaryOperators() {
    return unaryOperators;
  }

  /**
   * Sets the unary operators.
   *
   * @param unaryOperators The unary operators to set.
   */
  public void setUnaryOperators(UnaryOperatorConfig unaryOperators) {
    this.unaryOperators = unaryOperators;
  }

  /**
   * Getter for binary operators.
   *
   * @return Returns all binary operators.
   */
  public BinaryOperatorConfig getBinaryOperators() {
    return binaryOperators;
  }

  /**
   * Sets the binary operators.
   *
   * @param binaryOperators The binary operators to set.
   */
  public void setBinaryOperators(BinaryOperatorConfig binaryOperators) {
    this.binaryOperators = binaryOperators;
  }
}
