package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * Possible binary operators representing a value with children.
 */
public enum BinaryValueToValueOperators implements IBinaryValueToValueOperator {
  /**
   * The sum of two elements.
   */
  add("(%s + %s)"),
  /**
   * The difference of two elements.
   */
  sub("(%s - %s)"),
  /**
   * The product of two elements.
   */
  mul("(%s * %s)"),
  /**
   * The quotient of two elements.
   */
  div("(%s / %s)"),
  /**
   * The modulo of two elements.
   */
  mod("(%s %% %s)");

  private String representation;
  private IValuePart left;
  private IValuePart right;

  BinaryValueToValueOperators(String representation) {
    this.representation = representation;
  }

  protected IValuePart getLeft() {
    return left;
  }

  @Override
  public void setLeft(IValuePart part) {
    left = part;
  }

  protected IValuePart getRight() {
    return right;
  }

  @Override
  public void setRight(IValuePart part) {
    right = part;
  }

  @Override
  public String getRepresentation() {
    return String.format(representation, getLeft().getRepresentation(), getRight().getRepresentation());
  }

  @Override
  public IBinaryValueToValueOperator newInstance(IValuePart left, IValuePart right) {
    return new ConcreteBinaryValueToValueOperator(left, right, representation);
  }
}
