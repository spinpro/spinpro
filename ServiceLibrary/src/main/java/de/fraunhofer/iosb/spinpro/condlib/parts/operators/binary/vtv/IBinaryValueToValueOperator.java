package de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.binary.IBinaryInstantiable;

/**
 * An binary operator representing a value with child.
 */
public interface IBinaryValueToValueOperator extends IValuePart, IBinaryInstantiable<IBinaryValueToValueOperator, IValuePart> {
  /**
   * Sets the inner left condition part.
   *
   * @param part The part to set.
   */
  void setLeft(IValuePart part);

  /**
   * Sets the inner right condition part.
   *
   * @param part The part to set.
   */
  void setRight(IValuePart part);
}
