package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.btb;

import de.fraunhofer.iosb.spinpro.condlib.parts.IBooleanPart;

/**
 * An unary operator representing a boolean.
 */
public abstract class UnaryBooleanOperator implements IUnaryBooleanOperator {
  private IBooleanPart value;

  /**
   * Creates a new instance.
   *
   * @param value The sub-part in the condition.
   */
  public UnaryBooleanOperator(IBooleanPart value) {
    this.value = value;
  }

  protected IBooleanPart getValue() {
    return value;
  }

  @Override
  public void setValue(IBooleanPart part) {
    this.value = part;
  }
}
