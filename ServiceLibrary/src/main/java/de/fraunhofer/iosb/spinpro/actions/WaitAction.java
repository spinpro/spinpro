package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;
import java.util.Objects;

/**
 * An interface representing an action that can be stored inside an {@code IRule}.
 */
public class WaitAction extends AbstractAction {
  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "wait";
  private String time;

  /**
   * Creates a WaitAction with the time this action should wait.
   *
   * @param time the time
   */
  public WaitAction(String time) {
    this.time = time;
  }

  /**
   * Returns the time this action should wait.
   *
   * @return the time
   */
  public String getTime() {
    return time;
  }

  /**
   * Sets the time this action should wait.
   *
   * @param time the time
   */
  public void setTime(String time) {
    this.time = time;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      WaitAction input = (WaitAction) obj;
      return Objects.equals(input.time, this.time);
    }
    return false;
  }
}
