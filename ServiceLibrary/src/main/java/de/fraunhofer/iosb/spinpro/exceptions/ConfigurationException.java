package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * This {@link Exception} is thrown whenever a configuration can not be loaded.
 */
public class ConfigurationException extends Exception {
  static final long serialVersionUID = -3387516993124229947L;

  /**
   * Constructs a new {@link ConfigurationException} exception.
   */
  public ConfigurationException() {
  }

  /**
   * Constructs a new {@link ConfigurationException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public ConfigurationException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link ConfigurationException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public ConfigurationException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link ConfigurationException} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public ConfigurationException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link ConfigurationException} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected ConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
