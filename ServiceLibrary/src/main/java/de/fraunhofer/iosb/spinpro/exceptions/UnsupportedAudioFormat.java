package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * Exception for an unsupported {@link javax.sound.sampled.AudioFormat}. This {@link Exception} is thrown whenever an
 * {@link javax.sound.sampled.AudioFormat} of a stream or an input could not be used.
 */
public class UnsupportedAudioFormat extends Exception {
  static final long serialVersionUID = -3387516993124229949L;

  /**
   * Constructs a new {@link UnsupportedAudioFormat} exception.
   */
  public UnsupportedAudioFormat() {
  }

  /**
   * Constructs a new {@link UnsupportedAudioFormat} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public UnsupportedAudioFormat(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link UnsupportedAudioFormat} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public UnsupportedAudioFormat(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link UnsupportedAudioFormat} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public UnsupportedAudioFormat(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link UnsupportedAudioFormat} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected UnsupportedAudioFormat(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}