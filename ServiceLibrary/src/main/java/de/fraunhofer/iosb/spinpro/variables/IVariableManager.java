package de.fraunhofer.iosb.spinpro.variables;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Manages {@link IVariable Variables} and provides them.
 */
public interface IVariableManager {
  /**
   * Gets a {@link IVariable} mapped to the given name.
   *
   * @param name the name of the {@link IVariable}
   * @return the specified {@link IVariable} or null if none was found
   */
  IVariable getVariable(String name);

  /**
   * Checks if a {@link IVariable} mapped to the given name exists.
   *
   * @param name the name of the {@link IVariable}
   * @return true if the the specified {@link IVariable} exists or false if none was found
   */
  boolean containsVariable(String name);

  /**
   * Returns all the variables available at the given folder location.
   *
   * @return all available variables
   */
  Collection<IVariable> getVariables();

  /**
   * Finds all the variables in the given string.
   *
   * @param s The string to search in.
   * @return The list of variables with the string parts they were found in.
   */
  List<Pair<String, IVariable>> getVariablesInString(String s);

  /**
   * Returns a sorted list with all the variables associated to their alias.
   *
   * @return The list.
   */
  List<Pair<String, IVariable>> getSortedAliasVariables();

  /**
   * Sets the device that must support the variables.
   *
   * @param device the device or if null all devices should support the variables
   */
  void setDevice(String device);
}
