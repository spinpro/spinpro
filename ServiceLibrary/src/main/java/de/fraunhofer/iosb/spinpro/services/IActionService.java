package de.fraunhofer.iosb.spinpro.services;

import de.fraunhofer.iosb.spinpro.actions.IAction;

/**
 * This Interface defines a Plugin that is able to process the input
 * and to disassemble it into an {@link IAction} Object.
 * Must be implemented by every plugin that wants to process speech input.
 */
public interface IActionService {

  /**
   * Returns the Regex that this plugin matches.
   *
   * @return the regex
   */
  String getRegex();

  /**
   * Returns all the hotword aliases that this plugin recognizes.
   * A hotword is the first word used in the rule.
   *
   * @return the array of hotwords
   */
  String[] getHotwords();

}
