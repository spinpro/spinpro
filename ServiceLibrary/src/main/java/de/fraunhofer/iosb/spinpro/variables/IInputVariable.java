package de.fraunhofer.iosb.spinpro.variables;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;

/**
 * Defines a variable type that represents an input {@link IVariable} in a rule.
 */
public interface IInputVariable extends IVariable, IValuePart {
}
