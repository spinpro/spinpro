package de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.vtv;

import de.fraunhofer.iosb.spinpro.condlib.parts.IValuePart;
import de.fraunhofer.iosb.spinpro.condlib.parts.operators.unary.IUnaryInstantiable;

/**
 * An unary operator representing a value with child.
 */
public interface IUnaryValueOperator extends IValuePart, IUnaryInstantiable<IUnaryValueOperator, IValuePart> {
  /**
   * Sets the inner condition part.
   *
   * @param part The part to set.
   */
  void setValue(IValuePart part);
}