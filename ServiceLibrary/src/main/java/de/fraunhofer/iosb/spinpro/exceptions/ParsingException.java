package de.fraunhofer.iosb.spinpro.exceptions;

/**
 * This {@link Exception} is thrown whenever the process of parsing an object fails.
 */
public class ParsingException extends Exception {
  static final long serialVersionUID = -3387516993124229950L;

  /**
   * Constructs a new {@link ParsingException} exception.
   */
  public ParsingException() {
  }

  /**
   * Constructs a new {@link ParsingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   */
  public ParsingException(String message) {
    super(message);
  }

  /**
   * Constructs a new {@link ParsingException} exception.
   *
   * @param message A description of the reason for the exception to be thrown.
   * @param cause   The exception causing the exception to be thrown.
   */
  public ParsingException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new {@link ParsingException} exception.
   *
   * @param cause The exception causing the exception to be thrown.
   */
  public ParsingException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructs a new {@link ParsingException} exception.
   *
   * @param message            A description of the reason for the exception to be thrown.
   * @param cause              The exception causing the exception to be thrown.
   * @param enableSuppression  Whether the suppression is enabled or not.
   * @param writableStackTrace Whether the stacktrace can be written.
   */
  protected ParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
