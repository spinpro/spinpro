package de.fraunhofer.iosb.spinpro.actions;

import java.util.Arrays;
import java.util.Objects;

/**
 * An interface representing an action that can be stored inside an {@code IRule}.
 */
public class RepeatAction extends AbstractAction {

  /**
   * Should not be used for type safety, only to be displayed in the final JSON.
   */
  private final String type = "repeat";

  private String number;
  private IAction[] thenActions;

  /**
   * Creates a RepeatAction with the number of times this loop should be repeated
   * and the {@link IAction IActions} that should be performed on each iteration.
   *
   * @param number      the number of times
   * @param thenActions the actions
   */
  public RepeatAction(String number, IAction[] thenActions) {
    this.number = number;
    this.thenActions = thenActions;
  }

  @Override
  public String getType() {
    return type;
  }

  /**
   * Returns the number of times this loop should be repeated.
   *
   * @return the number of times
   */
  public String getNumber() {
    return number;
  }

  /**
   * Sets the number of times this loop should be repeated.
   *
   * @param number the number of times
   */
  public void setNumber(String number) {
    this.number = number;
  }

  /**
   * Returns the {@link IAction IActions} that should be performed in each loop.
   *
   * @return the {@link IAction IActions}
   */
  public IAction[] getThenActions() {
    return thenActions;
  }

  /**
   * Set the {@link IAction IActions} that should be performed in each loop.
   *
   * @param thenActions the {@link IAction IActions}
   */
  public void setThenActions(IAction[] thenActions) {
    this.thenActions = thenActions;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      RepeatAction input = (RepeatAction) obj;
      return Objects.equals(input.number, this.number) && Arrays.equals(input.thenActions, this.thenActions);
    }
    return false;
  }

}
