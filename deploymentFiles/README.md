# SpinPro - Server

## Setup
1. Install Java version 13 or higher:
    ```shell script
    sudo add-apt-repository ppa:linuxuprising/java
    sudo apt-get update
    sudo apt-get install oracle-java13-installer
    sudo apt-get install oracle-java13-set-default
    ```
    
    Check Java version:
    ```shell script
    java --version
    ```
    Should return a version >= 13.

1. Get latest release:
    ```shell script
    wget "https://nextcloud.scc.kit.edu/s/bqt56apSjcQDAkQ/download"
    unnzip download
    cd Speechsupport-In-Production
    ```
    You should see a structure like this:
    ```
    .
    ├── README.md
    ├── bin
    │   ├── Speechsupport-In-Production
    │   └── Speechsupport-In-Production.bat
    ├── cmu-start.bash
    ├── cmu-start.sh
    ├── cmu-startconfig.properties
    ├── deepspeech-start.bash
    ├── deepspeech-start.sh
    ├── deepspeech-startconfig.properties
    ├── events
    │   └── event-example.json
    ├── lib
    │   ├── Configuration-0.1.jar
    │   ├── Core-0.1.jar
    │   ├── Main-0.1.jar
    │   ├── ServiceLibrary-0.1.jar
    │   ├── SpeechToText-0.1.jar
    │   ├── Speechsupport-In-Production.jar
    │   └── ...
    ├── logs
    │   └── logs will be here
    ├── results
    │   ├── example-rule.json
    │   └── generated rules will be here
    ├── services
    │   ├── EventActionService-0.1.jar
    │   └── IfElseService-0.1.jar
    ├── speech-to-text
    │   ├── CMUSphinxService-0.1-all.jar
    │   ├── DeepSpeechService-0.1-all.jar
    │   └── deepspeech
    │       ├── model
    │       ├── shell.sh
    │       └── virtual
    └── variables
        └── variable-example.json
    ```
1. Set permissions:
    ```shell script
    chmod 777 deepspeech-start.sh
    chmod 777 cmu-start.sh
    ```
### CMUSphinx
The server will use an english speech model for CMUSphinx as default.

To change the model you can change the `CMUSphinxEN.properties` inside of the `speech-to-text/CMUSphinxService-*-all.jar`.
### DeepSpeech
1. Install python and pip (for ubuntu): 
    ```shell script
    sudo apt-get install python python-pip virtualenv
    ```
1. Set permissions:
    ```shell script
    sudo chmod 777 speech-to-text/deepspeech/shell.sh
    sudo chmod -R 666 speech-to-text/deepspeech/virtual/
    ```
## Start the Server
To start the server using CMUSphinx just enter:
```shell script
./cmu-start.sh
```
To start the server using DeepSpeech just enter:
```shell script
./deepspeech-start.sh
```