#!/bin/bash
#--------------------------------------------------------------------------------------------------
# Author: Jochen Mueller
# Since:  1/17/20
#--------------------------------------------------------------------------------------------------
# Errorstatus (octal):
ERR_PYTHON=0001                                                       # 0001 - Python and or pip is not installed
ERR_ARGCNT=1000                                                       # 0002 - Wrong argument count
ERR_VRNUMB=0003                                                       # 0003 - VIRT_NUMB is out of range
ERR_CONNPS=0007                                                       # 0007 - Constants are not properly set
ERR_TMPPTH=0010                                                       # 0010 - TEMP_PATH is no directory, not readable or not writeable
ERR_VRTPTH=0020                                                       # 0020 - VIRT_PATH is no directory, not readable or not writeable
ERR_MDLPTH=0030                                                       # 0030 - MODL_PATH is no directory, not readable or not writeable
ERR_AUDIOF=0040                                                       # 0040 - ADIO_FILE could not be found
ERR_RNTIME=0100                                                       # 0100 - Runtime error
ERR_DSPREC=0200                                                       # 0200 - Runtime error while recognition
#--------------------------------------------------------------------------------------------------
# Constants:
CON_MXVIRT=20                                                                # Max count of virtualenvs to grant
CON_PRGNAM="./shell.sh"                                                      # Name of the programm
CON_DEFDWL="https://github.com/mozilla/DeepSpeech/releases/download/v0.6.1/" # Download url for deepspeech
CON_DEFMDL="deepspeech-0.6.1-models"                                         # Filename of deepspeech
#--------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------
# begin usage
#--------------------------------------------------------------------------------------------------
# Prints usage message
#--------------------------------------------------------------------------------------------------
function usage() {
  echo "Usage $CON_PRGNAM [options]"
  echo ""
  echo "Options:"
  echo "-h            Shows this help."
  echo ""
  echo "-r            Runs recognition on virtenv defined by i."
  echo "-a <file>     The audiofile for recognition."
  echo "-i <number>   The virtenv index to run recognition on."
  echo "-v <dir>      The path to the virtual environment dir."
  echo ""
  echo "-s            Runs setup."
  echo "-d            Get the default english model."
  echo "-m <dir>      The path to the directory with the model in it."
  echo "-n <number>   The number of virtual environments to have."
  echo "-t <dir>      The path to a temporary directory."
  echo "-v <dir>      The path to the virtual environment dir."
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end usage
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------------------------------
# begin check_permissions_on_dir
#--------------------------------------------------------------------------------------------------
# Checks the permissions for a dir
# $1 - dir to check
# $2 - the exit number to return on failure
#--------------------------------------------------------------------------------------------------
function check_permissions_on_dir() {
  USER=$(whoami)
  DIR=$1
  EXIT_NUM=$2

  if ! [ -d "$DIR" ]; then
    echo "$DIR is not a directory." >&2
    exit "$EXIT_NUM"
  fi

  if [ "$DIR" = "" ]; then
    echo "DIR is not set." >&2
    exit "$EXIT_NUM"
  fi

  # Use -L to get information about the target of a symlink,
  # not the link itself, as pointed out in the comments
  INFO=($(stat -L -c "%a %G %U" "$DIR"))
  PERM=${INFO[0]}
  GROUP=${INFO[1]}
  OWNER=${INFO[2]}

  ACCESS=no
  if ((($PERM & 0006) != 0)); then
    # Everyone has write and read access
    ACCESS=yes
  elif ((($PERM & 0060) != 0)); then
    # Some group has write and read access.
    # Is user in that group?
    gs=( $(groups "$USER") )
    for g in "${gs[@]}"; do
      if [[ $GROUP == $g ]]; then
        ACCESS=yes
        break
      fi
    done
  elif ((($PERM & 0600) != 0)); then
    # The owner has write and read access.
    # Does the user own the file?
    [[ $USER == $OWNER ]] && ACCESS=yes
  fi

  if ! [ "$ACCESS" = "yes" ]; then
    echo "$DIR is not a directory or has no read and write rights." >&2
    usage
    exit "$EXIT_NUM"
  fi
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end check_permissions_on_dir
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function failedModelFiles() {
    echo "Failed to find model files." >&2
    echo "Expected:            " >&2
    echo "$model               " >&2
    echo "├ lm.binary          " >&2
    echo "├ output_graph.pbmm  " >&2
    echo "└ trie               " >&2
    exit $ERR_MDLPTH
}
#--------------------------------------------------------------------------------------------------
# begin generate_virtualenv
#--------------------------------------------------------------------------------------------------
# Generates a virtual environment
# $1 - VIRT_PATH
# $2 - index of virtenv to generate
#--------------------------------------------------------------------------------------------------
function generate_virtualenv() {
  COLOR='\033[0;34m'
  NC='\033[0m' # No Color
  virtpath=$1
  index=$2

  echo ""
  echo -e "${COLOR}Generate virtualenv $virtpath/deepspeech-venv-$2${NC}"
  echo ""

    virtualenv -p python3 "$virtpath/deepspeech-venv-$2"
    if (($? != 0 )); then
      echo "Error while creating virtual environemnt $virtpath/deepspeech-venv-$2." >&2
      exit "$ERR_RUNTM"
    fi
    source "$virtpath/deepspeech-venv-$2/bin/activate"
    if (($? != 0 )); then
      echo "Error while entering virtual environemnt $virtpath/deepspeech-venv-$2." >&2
      exit "$ERR_RUNTM"
    fi

  pip3 install deepspeech

  deactivate
  echo ""
  echo -e "${COLOR}Created $virtpath/deepspeech-venv-$2${NC}"
  echo ""
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end generate_virtualenv
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------------------------------
# begin recognize
#--------------------------------------------------------------------------------------------------
# Generates a virtual environment
# $1 - VIRT_PATH
# $2 - index of virtenv to use
# $3 - MODL_PATH
# $4 - ADIO_FILE
#--------------------------------------------------------------------------------------------------
function recognize() {

  virtpath=$1
  index=$2
  model=$3
  audio=$4

  check_permissions_on_dir "$virtpath" $ERR_MDLPTH

  check_permissions_on_dir "$model" $ERR_MDLPTH

  if ! [ -r "$model/output_graph.pbmm" ]; then
    echo "missing $model/output_graph.pbmm"
    failedModelFiles
  fi

  if ! [ -r "$model/lm.binary" ]; then
    echo "missing $model/lm.binary"
    failedModelFiles
  fi

   if ! [ -r "$model/trie" ]; then
    echo "missing $model/trie"
    failedModelFiles
  fi

  if ! ([ -r "$audio" ]); then
    echo "No audiofile $audio found."
    exit $ERR_AUDIOF
  fi
  source "$virtpath/deepspeech-venv-$2/bin/activate"
  if (($? != 0)); then
    echo "Could not find virtualenv $virtpath/deepspeech-venv-$2" >&2
    exit $ERR_RNTIME
  fi

  deepspeech --model "$model/output_graph.pbmm" --lm "$model/lm.binary" --trie "$model/trie" --audio "$audio"
  if (($? != 0 )); then
    echo "Error while recognition" >&2
    exit $ERR_DSPREC
  fi

  deactivate
  exit 0
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end recognize
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------------------------------
# begin setup
#--------------------------------------------------------------------------------------------------
# setup of the programm
# $1 - download default model (0/1)
#--------------------------------------------------------------------------------------------------
function setup() {
  echo "******************************"
  echo "*** DeepSpeech setup scipt ***"
  echo "Path for temp paths:   $TEMP_PATH"
  echo "Path for virtualenvs:  $VIRT_PATH"
  echo "Path for model:        $MODL_PATH"
  echo "Number of virtualenvs: $VIRT_NUMB"
  echo "******************************"
  echo ""

  check_permissions_on_dir "$VIRT_PATH" $ERR_VRTPTH
  i=0
  while (($i < $VIRT_NUMB)); do
    generate_virtualenv "$VIRT_PATH" $i
    ((i++))
  done

  if (($1 == 1)); then
    check_permissions_on_dir "$MODL_PATH" $ERR_MDLPTH
    check_permissions_on_dir "$TEMP_PATH" $ERR_TMPPTH

    temp="$PWD"
    cd "$TEMP_PATH" || exit $ERR_TMPPTH
    curl -LO "$CON_DEFDWL$CON_DEFMDL.tar.gz"
    tar xvf "$CON_DEFMDL.tar.gz"
    for file in $CON_DEFMDL/*; do
      cp "$file" "$MODL_PATH"
    done
    rm -r "$CON_DEFMDL"
    rm "$CON_DEFMDL.tar.gz"
    cd "$temp" || exit $ERR_TMPPTH
  fi

  exit 0
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end setup
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------------------------------
# begin check_python
#--------------------------------------------------------------------------------------------------
# checks if python and pip is installed.
#--------------------------------------------------------------------------------------------------
function check_python() {
  # Check for python3 to be installed.
  CON_PYTHON="python3"
  if ! foobar_loc="$(type -p "$CON_PYTHON")" || [[ -z $foobar_loc ]]; then
    echo "python3 is not installed" >&2
    exit $ERR_PYTHON
  fi

  # Check if pip is installed
  VAR_PIP="pip"
  if ! foobar_loc="$(type -p "$VAR_PIP")" || [[ -z $foobar_loc ]]; then
    echo "pip is not installed" >&2
    exit $ERR_PYTHON
  fi
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end check_python
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------------------------------
# begin init
#--------------------------------------------------------------------------------------------------
# The init code of the program
#--------------------------------------------------------------------------------------------------
TEMP_PATH=""
VIRT_PATH=""
MODL_PATH=""
ADIO_FILE=""
VIRT_NUMB=1
VIRT_INDX=0
run_setup=0
run_rcgnt=0
def_model=0
while getopts ":hrsda:i:v:m:n:t:" o; do
  case "${o}" in
  t)
    TEMP_PATH=$(realpath "${OPTARG}")
    ;;
  v)
    VIRT_PATH=$(realpath "${OPTARG}")
    ;;
  m)
    MODL_PATH=$(realpath "${OPTARG}")
    ;;
  a)
    ADIO_FILE=$(realpath "${OPTARG}")
    ;;
  n)
    s=${OPTARG}
    if ! ((s > 0 && s <= $CON_MXVIRT)); then
      usage
      exit $ERR_VRNUMB
    fi
    VIRT_NUMB=$s
    ;;
  i)
    s=${OPTARG}
    if ! ((s >= 0 && s < $CON_MXVIRT)); then
      usage
      exit $ERR_VRNUMB
    fi
    VIRT_INDX=$s
    ;;
  h)
    usage
    exit $ERR_ARGCNT
    ;;
  s)
    run_setup=1
    ;;
  d)
    def_model=1
    ;;
  r)
    run_rcgnt=1
    ;;
  *)
    usage
    exit $ERR_ARGCNT
    ;;
  esac
done
check_python
if (($run_setup == 1)); then
  setup $def_model
fi
if (($run_rcgnt == 1)); then
  recognize "$VIRT_PATH" $VIRT_INDX "$MODL_PATH" "$ADIO_FILE"
fi
usage
echo ""
echo "You must use -r or -s."
exit $ERR_ARGCNT
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# end init
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
