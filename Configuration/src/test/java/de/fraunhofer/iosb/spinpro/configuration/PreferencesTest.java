package de.fraunhofer.iosb.spinpro.configuration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static de.fraunhofer.iosb.spinpro.configuration.PropertiesUtils.generateTempFromProp;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PreferencesTest {
    //TODO fix, not working with the logger using the parameters at creation time. (jm)
  void testPreferencesRead() throws IOException {
    // Generate file with contents of test.properties
    Path tempProps = generateTempFromProp("test");
    ConfigurationProvider.init(tempProps.toString());

    // Check that each property was overwritten correctly
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    for (PreferenceStrings pref : PreferenceStrings.values()) {
      switch (pref) {
        case LOCALE:
          assertEquals("de", prefs.getValue(pref));
          break;
        case LOGGING_LEVEL:
          assertEquals("FINE", prefs.getValue(pref));
          break;
        default:
          assertEquals("overwritten", prefs.getValue(pref));
          break;
      }
    }

    // TODO: figure out why this does not in fact delete the file (jm is the logger still open? The file is locked in this case!)
    tempProps.toFile().delete();
  }

  @Test
  void testInitWithEmptyPath() {
    assertThrows(RuntimeException.class, () -> ConfigurationProvider.init(""));
  }

  @Test
  void testPropertyOverwrite() throws IOException {
    final String OVERWRITE_STRING = "overwritten";

    // Generate file with german locale and otherwise default values
    Path path = generateTempFromProp("localeonly");
    ConfigurationProvider.init(path.toString());
    // init() currently fails to create a log file at the default path. This is ignored

    // Check that each preference (except locale) has its default value, overwrite it
    // and check if the new value is returned.
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    for (PreferenceStrings pref : PreferenceStrings.values()) {
      if (pref != PreferenceStrings.LOCALE) {
        assertEquals(pref.getDefault(), prefs.getValue(pref));
      } else {
        assertEquals("de", prefs.getValue(pref));
      }
      prefs.setValue(pref, OVERWRITE_STRING);
      assertEquals(OVERWRITE_STRING, prefs.getValue(pref));
    }
  }
}