package de.fraunhofer.iosb.spinpro.configuration;

import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static de.fraunhofer.iosb.spinpro.configuration.PropertiesUtils.generateTempFromProp;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageBundleTest {
    //TODO not usable with english systems. Pls change locale
  void testGermanSuccess() throws IOException {
    // Generate properties where locale is "de"
    Path p = generateTempFromProp("localeonly");
    ConfigurationProvider.init(p.toString());

    // Test that the correct bundle was loaded by comparing the success message
    // TODO: possibly test some other messages
    IMessageBundle mb = ConfigurationProvider.getMessageBundle(); //TODO change to defined locale
    assertEquals("Erfolg.", mb.getMessage(Messages.SUCCESS));
  }
}
