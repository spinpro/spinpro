package de.fraunhofer.iosb.spinpro.configuration;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class PropertiesUtils {
  // Writes the contents of /propName/.properties to a temporary file and returns its path
  public static Path generateTempFromProp(String propName) throws IOException {
    Path p = Files.createTempFile(propName, ".properties");
    InputStream inStream = PropertiesUtils.class.getResourceAsStream(String.format("/%s.properties", propName));
    FileOutputStream outStream = new FileOutputStream(p.toFile());
    inStream.transferTo(outStream);
    return p;
  }
}
