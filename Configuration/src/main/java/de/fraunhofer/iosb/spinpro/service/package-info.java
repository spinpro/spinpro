/**
 * <h1>Service structure.</h1>
 * <p>A package providing classes for loading services from jars at runtime.</p>
 *
 * @author Jochen M&uuml;ller
 * @version 1.0
 * @since 21/16/19
 */

package de.fraunhofer.iosb.spinpro.service;