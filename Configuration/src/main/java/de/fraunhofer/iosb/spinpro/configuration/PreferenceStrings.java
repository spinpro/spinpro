package de.fraunhofer.iosb.spinpro.configuration;

import java.util.Locale;
import javax.management.timer.Timer;

/**
 * Possible values for loading configuration from an config file.
 */
public enum PreferenceStrings implements IPreferenceStrings {
  SPEECH_TO_TEXT_JAR_PATH("speech-to-text-jar-path", ""), //SPEECH_CONSTRUCTION_PLUGINS_JAR_PATH("speech-construction-plugins-jar-path", ""),
  LOG_DIR("logging-dir", "log"),
  LOG_MAX_SIZE("log-max-size", "10485760"), //10 MiB
  LOG_MAX_COUNT("log-max-count", "10"), //Number of files to use.
  LOG_APPEND("log-append", "true"), //Number of files to use.
  CONSOLE_LOGGING("console-logging-enabled", "true"),
  RULE_DOMAIN("domain", ""),
  RULE_COMPANY("company", ""),
  RULE_COPYRIGHT("copyright", ""),
  RULE_OUTPUT_DIR("rule-output", "src/main/resources/RuleOutput"),
  INPUT_VARIABLE_DIR("input-variable-dir", "src/main/resources/variables/input_variables"),
  OUTPUT_EVENT_DIR("output-event-dir", "src/main/resources/variables/output_events"),
  SERVICE_DIR("service-dir", "../Core/src/test/resources/services"),
  MAX_ATTEMPT_COUNT_SIMPLE("max-attempt-count-simple", "5"),
  MAX_ATTEMPT_COUNT_COMPLEX("max-attempt-count-complex", "10"),
  MESSAGES_BASE_PATH("messages-base-path", "de/fraunhofer/iosb/spinpro/messages/messages"),
  LOCALE("locale", Locale.getDefault().toString()),
  SPEECH_TO_TEXT_WORKER_COUNT("speech-to-text-worker-count", "1"),
  CORE_WORKER_COUNT("core-worker-count", "2"),
  SPEECH_TO_TEXT_WORKER_BUFFER_SIZE("speech-to-text-worker-buffer-size", "10"),
  CORE_WORKER_BUFFER_SIZE("core-worker-buffer-size", "10"),
  PORT("port", "8080"),
  CLIENT_TIMEOUT("client-timeout", "120"),
  MAX_CONNECTIONS("max-connections", "100"),
  HOSTNAME("hostname", "localhost"),
  APPROVE_WORKER_COUNT("approve-worker-count", "2"),
  APPROVE_WORKER_BUFFER_SIZE("approve-worker-buffer-size", "10"),
  APPROVE_TIMEOUT("approve-timeout", String.format("%d", Timer.ONE_SECOND)),
  LOGGING_LEVEL("logging-level", "FINEST"),
  LOG_EXPIRATION_TIME("log-expiration-time", String.format("%d", Timer.ONE_HOUR)),
  MAINTENANCE_INTERVAL("maintenance-interval", String.format("%d", Timer.ONE_HOUR / 2)),
  CORE_SP_CON_BEGINNING_PHRASE("c-beginning-phrase", "begin"),
  CORE_SP_CON_ENDING_PHRASE("c-ending-phrase", "end"),
  CORE_SP_CON_CONCAT_PHRASE("c-concat-phrase", "also");

  private final String text;
  private String defaultValue;

  PreferenceStrings(String text, String defaultV) {
    this.text = text;
    this.defaultValue = defaultV;
  }

  @Override
  public String toString() {
    return text;
  }

  @Override
  public String getDefault() {
    return defaultValue;
  }
}