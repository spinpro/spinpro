package de.fraunhofer.iosb.spinpro.configuration;

import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.MessageBundle;
import org.apache.commons.lang3.LocaleUtils;

import java.io.IOException;

/**
 * Holds the program wide configuration.
 */
public class ConfigurationProvider {
  private static IPreferences pref = null;
  private static ILogger logger = null;
  private static IMessageBundle messageBundle = null;

  /**
   * Loads and initializes the parameters defined in the config file.
   *
   * @param path The path to the configuration file.
   */
  public static void init(String path) {
    try {
      pref = new LocalPreferences(path);
    } catch (IOException e) {
      throw new RuntimeException("Could not find main configuration.");
    }
    logger = new Logger("main");
    messageBundle = new MessageBundle(LocaleUtils.toLocale(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.LOCALE)));
  }

  /**
   * Gets the preferences for program configuration. In the program preferences is information regarding
   * plugins, such as paths to plugin jars and folders, and other configuration files, such as paths to specific
   * configuration files and folders.
   *
   * @return The programs main configuration.
   */
  public static IPreferences getMainPreferences() {
    return pref;
  }

  /**
   * Gets the standard logger for logging.
   *
   * @return The standard Logger to use.
   */
  public static ILogger getMainLogger() {
    return logger;
  }

  /**
   * Gets the message bundle with default locale.
   *
   * @return The message bundle to use.
   */
  public static IMessageBundle getMessageBundle() {
    return messageBundle;
  }
}
