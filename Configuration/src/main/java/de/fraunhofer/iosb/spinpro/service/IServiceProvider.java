package de.fraunhofer.iosb.spinpro.service;

import java.util.Collection;

/**
 * Provides the possibility to get a plugin to the interface {@link T} loaded from a Jarfile.
 */
public interface IServiceProvider<T> {
  /**
   * Gets the plugin as {@link T} to use.
   *
   * @return The {@link T} to use.
   */
  Collection<IServiceWrapper<T>> getServices();
}
