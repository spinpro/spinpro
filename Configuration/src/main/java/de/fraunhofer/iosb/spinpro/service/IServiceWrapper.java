package de.fraunhofer.iosb.spinpro.service;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;

/**
 * Used to pack metadata and the service into one structure.
 *
 * @param <T> The type of the service.
 */
public interface IServiceWrapper<T> {

  /**
   * The internal service.
   *
   * @return The internal service.
   */
  T getService();

  /**
   * The name of the service.
   *
   * @return The name of the service.
   */
  String getName();

  /**
   * The cryptic identification of the service. See {@link Service} for further information.
   *
   * @return The cryptic identification of the service. See {@link Service} for further information.
   */
  String getCrypticId();

  /**
   * The author of the service.
   *
   * @return The author of the service.
   */
  String getAuthor();

  /**
   * The organisation the service was written for.
   *
   * @return The organisation the service was written for.
   */
  String getOrganisation();

  /**
   * The version of the service.
   *
   * @return The version of the service.
   */
  String getVersion();

  /**
   * The sha256 hash of all metadata provided by the plugin.
   *
   * @return The sha256 hash of the service.
   * @throws ConfigurationException If a hash could not be generated.
   */
  String getHash() throws ConfigurationException;
}
