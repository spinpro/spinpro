package de.fraunhofer.iosb.spinpro.messages;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * Uses a {@link java.util.ResourceBundle ResourceBundle} to manage messages that can be accessed with a {@link Messages}.
 */
public class MessageBundle implements IMessageBundle {

  private static final String BUNDLE_NOT_LOADED = "The ResourceBundle could not be loaded from the base path '%s'.";
  private static final String BUNDLE_NOT_AVAILABLE = "The ResourceBundle was not loaded properly. Messages might be empty.";
  private static final String MESSAGE_NOT_AVAILABLE = "A message for the key '%s' could not be found.";
  private ResourceBundle messageBundle;
  private ILogger logger;

  /**
   * Creates a message bundle by loading the {@link java.util.ResourceBundle ResourceBundle} from a path configured in {@link ConfigurationProvider}.
   * Initializes a logger.
   *
   * @param locale the locale used for the {@link java.util.ResourceBundle ResourceBundle}
   */
  public MessageBundle(Locale locale) {
    logger = ConfigurationProvider.getMainLogger();
    String bundlePath = "";
    try {
      bundlePath = ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.MESSAGES_BASE_PATH);
      messageBundle = ResourceBundle.getBundle(bundlePath, locale);
    } catch (MissingResourceException e) {
      messageBundle = null;
      logger.log(Level.SEVERE, String.format(BUNDLE_NOT_LOADED, bundlePath));
    }
  }

  /**
   * Creates a message bundle by loading the {@link java.util.ResourceBundle ResourceBundle} from a path configured in {@link ConfigurationProvider}.
   * Initializes a logger.
   *
   * @param locale     the locale used for the {@link java.util.ResourceBundle ResourceBundle}
   * @param bundlePath the path of the bundle
   */
  public MessageBundle(String bundlePath, Locale locale) {
    logger = ConfigurationProvider.getMainLogger();
    try {
      messageBundle = ResourceBundle.getBundle(bundlePath, locale);
    } catch (MissingResourceException e) {
      messageBundle = null;
    }
  }

  @Override
  public String getMessage(Messages msg) {
    String foundMsg;
    if (messageBundle == null) {
      return "";
    }
    if (msg == null) {
      logger.log(Level.WARNING, BUNDLE_NOT_AVAILABLE);
      return "";
    }
    try {
      foundMsg = messageBundle.getString(msg.toString());
    } catch (MissingResourceException | ClassCastException e) {
      logger.log(Level.WARNING, String.format(MESSAGE_NOT_AVAILABLE, msg.toString()));
      return "";
    }
    return foundMsg;
  }

}
