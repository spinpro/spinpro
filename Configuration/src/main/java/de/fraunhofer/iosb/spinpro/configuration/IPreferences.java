package de.fraunhofer.iosb.spinpro.configuration;

/**
 * An Interface, that contains all the preferences, that are used in the program. The preferences will be saved
 * even if the program stopped or the computer was shutdown. It is possible to exchange the preferences between
 * different instances of the program.
 */
public interface IPreferences {

  /**
   * Used to get the value of a property.
   *
   * @param key The key of the property.
   * @return The value that is associated to {@code key} or in case of failure the default value.
   */
  String getValue(IPreferenceStrings key);

  /**
   * Used to set the value of a property.
   *
   * @param key   The key of the property.
   * @param value The value that the property should get.
   */
  void setValue(IPreferenceStrings key, String value);
}
