package de.fraunhofer.iosb.spinpro.service;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * A wrapper for a service with metadata.
 *
 * @param <T> The type of the internal service.
 */
public class ServiceWrapper<T> implements IServiceWrapper<T> {
  private T service;
  private String name;
  private String crypticId;
  private String author;
  private String organisation;
  private String version;

  /**
   * Constructs a new {@link ServiceWrapper}.
   *
   * @param service    The internal service.
   * @param annotation The annotation with the parameters used.
   */
  public ServiceWrapper(T service, Service annotation) {
    this(service, annotation.name(), annotation.crypticId(), annotation.author(), annotation.organisation(), annotation.version());

  }

  /**
   * Constructs a new {@link ServiceWrapper}.
   *
   * @param service      The internal service.
   * @param name         The name of the service.
   * @param crypticId    The cryptic identification of the service.
   *                     See {@link Service} for further information.
   * @param author       The author of the service.
   * @param organisation The organisation the service was written for.
   * @param version      The version of the se
   *                     include ':Console'
   *                     project(":Console").projectDir = file("../Console")rvice.
   **/
  public ServiceWrapper(T service, String name, String crypticId, String author, String organisation, String version) {
    this.service = service;
    this.name = name;
    this.crypticId = crypticId;
    this.author = author;
    this.organisation = organisation;
    this.version = version;
  }

  @Override
  public T getService() {
    return service;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getCrypticId() {
    return crypticId;
  }

  @Override
  public String getAuthor() {
    return author;
  }

  @Override
  public String getOrganisation() {
    return organisation;
  }

  @Override
  public String getVersion() {
    return version;
  }

  @Override
  public String getHash() throws ConfigurationException {
    MessageDigest digest = null;
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new ConfigurationException("Can not find SHA-256 algorithm.", e);
    }
    return bytesToHex(digest.digest(toString().getBytes(StandardCharsets.UTF_8)));
  }

  @Override
  public String toString() {
    return String.format("%s:%s:%s:%s:%s", name, version, author, organisation, crypticId);
  }

  private String bytesToHex(byte[] hash) {
    StringBuffer hexString = new StringBuffer();
    for (byte b : hash) {
      String hex = Integer.toHexString(0xff & b);
      if (hex.length() == 1) {
        hexString.append('0');
      }
      hexString.append(hex);
    }
    return hexString.toString();
  }
}
