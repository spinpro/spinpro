package de.fraunhofer.iosb.spinpro.configuration;

/**
 * A preference loaded from a config file.
 */
public interface IPreferenceStrings {
  /**
   * The default value of a preference.
   *
   * @return The default value of the preference.
   */
  String getDefault();

  /**
   * The {@code String} key of a preference.
   *
   * @return The {@code String} key of a preference to use.
   */
  String toString();
}
