package de.fraunhofer.iosb.spinpro.messages;

/**
 * Contains all messages that can be used to communicate with the user.
 */
public enum Messages {

  SUCCESS("success"),
  EXCEPTION_WHILE_EXECUTION("exception-while-execution"),
  SHUTDOWN_THROUGH_USER_INTERRUPT("shutdown-through-user-interrupt"),
  EXCEPTION_WHILE_CREATION("exception-while-creation"),
  EXCEPTION_WHILE_TERMINATION("exception-while-termination"),
  WRONG_ARGUMENT_COUNT("wrong-argument-count"),
  CONFIG_FILE_NOT_FOUND("config-file-not-found"),
  INITIALISATION_COMPLETE("initialisation-complete"),
  REQUEST_LISTENER("request-listener"),
  JOB("job"),
  ERROR_STOPPING_CLIENT_COMMUNICATOR("error-stopping-client-communicator"),
  ERROR_TOO_MANY_CONNECTIONS("error-too-many-connections"),
  ERROR_TOO_MANY_REQUESTS("error-too-many-requests"),
  ERROR_BAD_REQUEST("error-bad-request"),
  SERVER_STOPPED("server-stopped"),
  ERROR_PARSING_CONFIGURED_PORT("error-parsing-configured-port"),
  ERROR_PARSING_CONFIGURED_LISTENER_COUNT("error-parsing-configured-listener-count"),
  ERROR_PARSING_CONFIGURED_CLIENT_TIMEOUT("error-parsing-configured-client-timeout"),
  ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT("error-parsing-configured-speech-to-text-worker-count"),
  ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT("error-parsing-configured-core-worker-count"),
  ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE("error-parsing-configured-speech-to-text-worker-buffer-size"),
  ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE("error-parsing-configured-core-worker-buffer-size"),
  ERROR_PARSING_DEFAULT_PORT("error-parsing-default-port"),
  ERROR_PARSING_DEFAULT_LISTENER_COUNT("error-parsing-default-listener-count"),
  PIPELINE_STEP_ERROR_ZERO_CAPACITY("pipeline-step-error-zero-capacity"),
  PIPELINE_STEP_ERROR_ZERO_THREADS("pipeline-step-error-zero-threads"),
  PIPELINE_STEP_ERROR_NO_PIPELINE("pipeline-step-error-no-pipeline"),
  PIPELINE_STEP_INTERRUPT_WHILE_CLOSING("pipeline-step-interrupt-while-closing"),
  ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT("error-initializing-speech-to-text-component"),
  ERROR_PROCESSING_AUDIO("error-processing-audio"),
  ERROR_CREATING_JOB("error-creating-job"),
  ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE("error-parsing-configured-approve-worker-buffer-size"),
  ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT("error-parsing-configured-approve-worker-buffer-size"),
  SERVER_STARTED("server-started"),
  CLIENT_CONNECTED("clients-connected"),
  CLIENT_DISCONNECTED("client-disconnected"),
  GOT_AUDIO_REQUEST("got-audio-request"),
  GOT_TEXT_REQUEST("got-text-request"),
  ERROR_PARSING_AUDIO_REQUEST("error-parsing-audio-request"),
  ERROR_PARSING_TEXT_REQUEST("error-parsing-text-request"),
  ERROR_PARSING_MESSAGE("error-parsing-message"),
  LOADING_SERVICE_FAILED("loading-service-failed"),
  JAR_COUNT("jar-count"),
  EXCEPTION_WHILE_SERVICE_LOADING("exception-while-service-loading"),
  SERVICE_NOT_LOADED("service-not-loaded"),
  MORE_THAN_ONE_SERVICE_FOUND("more-than-one-service-found"),
  UNSUPPORTED_AUDIO_FORMAT("unsupported-audio-format"),
  AUDIO_FORMAT_CONVERTED("audio-format-converted"),
  SPEECH_TO_TEXT_RECOGNIZED_SENTENCE("speech-to-text-recognized-sentence"),
  ERROR_GETTING_RESULT("error-getting-result"),
  PIPELINE_STEP_RETURNED_NULL("pipeline-step-returned-null"),
  ENTERED_APPROVE_STEP("entered-approve-step"),
  ENTERED_SPEECH_TO_TEXT_STEP("entered-speech-to-text-step"),
  ENTERED_RULE_CREATION_STEP("entered-rule-creation-step"),
  JOB_GOT_NOT_APPROVED("job-got-not-approved"),
  JOB_GOT_APPROVED("job-got-approved"),
  JOB_GENERATED("job-generated"),
  MACHINE("machine"),
  CLIENT_ID("client-id"),
  PERFORMED_MAINTENANCE("performed-maintenance"),
  CORE_ERROR_PIPELINE_FAILED("c-error-pipeline-failed"),
  CORE_ERROR_INVALID_CHAR_INPUT("c-error-invalid-char-input"),
  CORE_LOG_DELETED_CHARS_INPUT_SENTENCE("c-log-deleted-chars-input-sentence"),
  CORE_ERROR_CREATE_OUTPUT_FOLDER("c-error-create-output-folder"),
  CORE_ERROR_NO_RULE_NAME("c-error-no-rule-name"),
  CORE_ERROR_RULE_ALREADY_EXISTS("c-error-rule-already-exists"),
  CORE_ERROR_CREATE_OUTPUT_FILE("c-error-create-output-file"),
  CORE_ERROR_LOAD_SERVICES("c-error-load-services"),
  CORE_ERROR_NON_COMPLEX_RULE_PARSED_COMPLEX("c-error-non-complex-rule"),
  CORE_ERROR_MALFORMED_COMPLEX_RULE("c-error-malformed-complex-rule"),
  CORE_CANNOT_GENERATE_HASH("c-cannot-generate-hash"),
  CORE_ERROR_MAX_PARS_ATTEMPTS_REACHED("c-error-max-parsing-attempts-reached"),
  CORE_ERROR_NO_FURTHER_SERVICES("c-error-no-further-services"),
  CORE_ERROR_SPEECH_CONSTR_SERVICE_EXCEPTION("c-error-service-exception"),
  CORE_ERROR_SPEECH_CONSTR_SERVICE_FAILED("c-error-service-failed"),
  CORE_PROCESSING_FINISHED_IN("c-core-finished-in"),
  CORE_ERROR_STRUCTURE_END_BEFORE_BEGIN("c-error-struct-end-before-begin"),
  CORE_ERROR_STRUCTURE_INCORRECT("c-error-struct-incorrect"),
  CORE_ERROR_STRUCTURE_START_EQ_CLOSE("c-error-struct-starting-equals-closing"),
  CORE_VARIABLES_INVALID_PATH("c-variables-invalid-path"),
  CLIENT_COUNT("client-count"),
  LOADING_VARIABLES_FROM_PATH("loading-variables-from-path"),
  LOADING_EVENTS_FROM_PATH("loading-events-from-path");

  private final String key;

  /**
   * Initializes the key for an entry.
   *
   * @param key the key of an entry
   */
  Messages(String key) {
    this.key = key;
  }

  @Override
  public String toString() {
    return key;
  }
}
