package de.fraunhofer.iosb.spinpro.configuration;

import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

/**
 * A interface for a logger to log application data.
 */
public interface ILogger {

  /**
   * Used to log a message with a specified level of importance.
   *
   * @param ll The level of importance.
   * @param s  The message to log.
   */
  void log(Level ll, String s);

  /**
   * Used to log a message with a specified level of importance.
   *
   * @param ll The level of importance.
   * @param s  The message to log.
   */
  void log(Level ll, Messages s);

  /**
   * Used to log a message with a specified level of importance.
   *
   * @param ll      The level of importance.
   * @param message The message to log.
   * @param e       The exception to print.
   */
  void log(Level ll, String message, Exception e);

  /**
   * Used to log a message with a specified level of importance.
   *
   * @param ll      The level of importance.
   * @param message The message to log.
   * @param e       The exception to print.
   */
  void log(Level ll, Messages message, Exception e);
}
