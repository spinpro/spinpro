package de.fraunhofer.iosb.spinpro.messages;

/**
 * Manages messages that can be accessed with a {@link Messages}.
 */
public interface IMessageBundle {

  /**
   * Gets the message stored in the bundle for the given {@link Messages}.
   *
   * @param msg the {@link Messages} containing the key for the bundle
   * @return the message or an empty String if the message could not be loaded or msg is null
   */
  String getMessage(Messages msg);
}
