package de.fraunhofer.iosb.spinpro.service;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceLoadingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarFile;
import java.util.logging.Level;

/**
 * Loads and provides a service of type {@code T}.
 *
 * @param <T> The type of the service to load.
 * @author Jochen Mueller
 * @version 1.0
 * @since 12/16/19
 */
public class GenericServiceProvider<T> implements IServiceProvider<T> {
  private Collection<IServiceWrapper<T>> service;
  private Class<T> type;

  /**
   * Creates a new {@link GenericServiceProvider}.
   * Searches all subdirectories of the given folder for services, that can be loaded.
   *
   * @param classpath The classpath to the class or folder to load.
   * @param type      The Interface to use. (Must be equals to T)
   * @throws ServiceLoadingException If a plugin could not be loaded properly.
   */
  public GenericServiceProvider(URL classpath, Class<T> type) throws ServiceLoadingException {
    this(classpath, type, ConfigurationProvider.getMainLogger(), ConfigurationProvider.getMessageBundle());
  }

  /**
   * Creates a new {@link GenericServiceProvider}.
   * Searches all subdirectories of the given folder for services, that can be loaded.
   *
   * @param classpath The classpath to the class or folder to load.
   * @param type      The Interface to use. (Must be equals to T)
   * @param logger    The logger to use.
   * @param message   The message bundle containing error and logging messages
   * @throws ServiceLoadingException If a plugin could not be loaded properly.
   */
  @SuppressWarnings("unchecked")
  public GenericServiceProvider(URL classpath, Class<T> type, ILogger logger, IMessageBundle message) throws ServiceLoadingException {
    File home = null;
    try {
      home = Paths.get(classpath.toURI()).toFile();
    } catch (URISyntaxException e) {
      logger.log(Level.SEVERE, message.getMessage(Messages.LOADING_SERVICE_FAILED) + classpath.getPath());
      throw new ServiceLoadingException(message.getMessage(Messages.LOADING_SERVICE_FAILED) + classpath.getPath());
    }
    JarFile[] jfs = null;
    File[] files = null;
    List<URL> urls = new ArrayList<>();
    if (home.isDirectory()) {
      try {
        files = Files.walk(home.toPath()).map(Path::toFile).filter(File::isFile).filter(file -> file.getName().endsWith(".jar")).toArray(File[]::new);
      } catch (IOException e) {
        throw new ServiceLoadingException(message.getMessage(Messages.LOADING_SERVICE_FAILED) + classpath.getPath());
      }
    } else {
      files = new File[]{home};
    }

    if (files != null) {
      jfs = Arrays.stream(files).map(file -> {
        try {
          urls.add(file.toURI().toURL());
          return new JarFile(file);
        } catch (IOException e) {
          return null;
        }
      }).filter(Objects::nonNull).toArray(JarFile[]::new);
    }

    if (jfs == null || jfs.length <= 0) {
      logger.log(Level.SEVERE, message.getMessage(Messages.LOADING_SERVICE_FAILED) + home.getAbsolutePath());
      throw new ServiceLoadingException(message.getMessage(Messages.LOADING_SERVICE_FAILED) + home.getAbsolutePath());
    }
    logger.log(Level.FINE, message.getMessage(Messages.JAR_COUNT) + ": " + jfs.length);

    ClassLoader classLoader = new URLClassLoader(urls.toArray(URL[]::new), this.getClass().getClassLoader());
    Thread.currentThread().setContextClassLoader(classLoader);
    service = new ArrayList<>();

    Arrays.stream(jfs).forEach(jarFile -> {
      jarFile.stream().filter(jarEntry -> jarEntry.getName().endsWith(".class")).map(jarEntry -> {
        try {
          return classLoader.loadClass(jarEntry.getName().replaceAll("/", ".").replace(".class", ""));
        } catch (ClassNotFoundException | NoClassDefFoundError e) {
          return null; //skip failing classes, filter them afterwards
        }
      }).filter(Objects::nonNull).filter(var -> var.isAnnotationPresent(Service.class) && Arrays.asList(var.getInterfaces()).contains(type)).map(cl -> {
        try {
          Class<T> tempClass = (Class<T>) cl;
          T temp = tempClass.getDeclaredConstructor().newInstance();
          Service annotation = tempClass.getAnnotation(Service.class);
          return new ServiceWrapper<T>(temp, annotation);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
          logger.log(Level.WARNING, Messages.EXCEPTION_WHILE_SERVICE_LOADING, e);
          return null;
        }
      }).filter(Objects::nonNull).forEach(service::add);
    });

    if (service.size() <= 0) {
      logger.log(Level.SEVERE, Messages.SERVICE_NOT_LOADED);
      throw new ServiceLoadingException(message.getMessage(Messages.SERVICE_NOT_LOADED));
    }
  }

  @Override
  public Collection<IServiceWrapper<T>> getServices() {
    return service;
  }
}
