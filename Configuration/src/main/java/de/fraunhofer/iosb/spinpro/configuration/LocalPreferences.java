package de.fraunhofer.iosb.spinpro.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Implementation of {@link IPreferences}. Uses {@link java.util.prefs.Preferences}.
 */
public class LocalPreferences implements IPreferences {

  private Properties prop;

  /**
   * Creates a new {@link IPreferences} object. The configuration is loaded from the {@code .properties} file
   * specified by the path in {@code path}.
   *
   * @param path The path to the {@code .properties} file to use.
   * @throws IOException If the {@code .properties} file could not get loaded.
   */
  public LocalPreferences(String path) throws IOException {
    prop = new Properties();
    FileInputStream fip = new FileInputStream(path);
    prop.load(fip);
    fip.close();
  }

  @Override
  public String getValue(IPreferenceStrings key) {
    return prop.getProperty(key.toString(), key.getDefault());
  }

  @Override
  public void setValue(IPreferenceStrings key, String value) {
    prop.setProperty(key.toString(), value);
  }
}
