package de.fraunhofer.iosb.spinpro.configuration;

import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.CONSOLE_LOGGING;
import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.LOG_APPEND;
import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.LOG_DIR;
import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.LOG_MAX_COUNT;
import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.LOG_MAX_SIZE;

/**
 * An implementation of an {@link ILogger} using the Java-Logging-API. The standard configuration file for logging with
 * the Java-Logging-API is in {@code java.util.logging.config.file} and may be changed via jvm start arguments.
 */
public class Logger implements ILogger {
  private java.util.logging.Logger logger;

  /**
   * Creating a standard logger writing to a file and if not in production mode ({@code production = false}).
   *
   * @param name The name of the logger.
   */
  public Logger(String name) {
    String path = ConfigurationProvider.getMainPreferences().getValue(LOG_DIR);
    Level level = Level.parse(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.LOGGING_LEVEL));
    logger = java.util.logging.Logger.getLogger("de.fraunhofer.iosb.spinpro." + name);
    logger.setUseParentHandlers(false);
    logger.setLevel(level);

    if (ConfigurationProvider.getMainPreferences().getValue(CONSOLE_LOGGING).equals("true")) {
      ConsoleHandler handler = new ConsoleHandler();
      handler.setLevel(level);
      handler.setFormatter(new CustomFormatter());
      logger.addHandler(handler);
    }

    FileHandler fileHandler = null;
    try {
      fileHandler = new FileHandler(path + "/" + logger.getName() + ".%g.log", Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(LOG_MAX_SIZE)),
          Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(LOG_MAX_COUNT)), Boolean.parseBoolean(ConfigurationProvider.getMainPreferences().getValue(LOG_APPEND)));
      fileHandler.setLevel(level);
      fileHandler.setFormatter(new CustomFormatter());
      logger.addHandler(fileHandler);
    } catch (IOException e) {
      logger.log(Level.SEVERE, String.format("Logger %s can not be written to file %s.", logger.getName(), path + "/" + logger.getName() + ".log"));
    }
  }

  @Override
  public void log(Level ll, String s) {
    logger.log(ll, s);
  }

  @Override
  public void log(Level ll, Messages s) {
    logger.log(ll, ConfigurationProvider.getMessageBundle().getMessage(s));
  }


  @Override
  public void log(Level ll, String message, Exception e) {
    StringWriter s = new StringWriter();
    PrintWriter pw = new PrintWriter(s);
    e.printStackTrace(pw);
    logger.log(ll, String.format("%s: %s", message, s.toString()));
  }

  @Override
  public void log(Level ll, Messages message, Exception e) {
    log(ll, ConfigurationProvider.getMessageBundle().getMessage(message), e);
  }

  public java.util.logging.Logger getNative() {
    return logger;
  }

  private static class CustomFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
      String s = String.format("%1$tF %1$tT [%2$s] (%3$d)", new Date(record.getMillis()), record.getLevel().toString(), record.getThreadID());
      return String.format("%-40s %s\n", s, record.getMessage());
    }
  }
}
