#!/bin/bash
./gradlew --version
./gradlew clean || exit
cd IfElseService || exit
../gradlew deploy || exit
cd ../EventActionService || exit
../gradlew deploy || exit
cd ../DeepSpeechService || exit
../gradlew deploy || exit
cd ../CMUSphinxService || exit
../gradlew deploy || exit
cd .. || exit
./gradlew build || exit
./gradlew test || exit
rm deploymentFiles/speech-to-text/CMUSphinxService*
rm deploymentFiles/speech-to-text/DeepSpeechService*
rm deploymentFiles/services/IfElseService*
rm deploymentFiles/services/EventActionService*
