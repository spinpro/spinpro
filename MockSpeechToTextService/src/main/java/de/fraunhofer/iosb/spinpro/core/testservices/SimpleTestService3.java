package de.fraunhofer.iosb.spinpro.core.testservices;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

@Service(author = "Tim Windecker", name = "SimpleTestService3", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f836")

/*
  A simple service for testing purposes.
  Uses startWord as content of the parsed construct and creates event actions.
  A compiled version should be in test/resources/services.
 */
public class SimpleTestService3 implements ISimpleActionService {

  private static final String OUTPUT = "out";
  private static final String VALUE = "valuable";
  private final static String START_WORD = "stop";

  @Override
  public IAction execute(String input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    EventAction action = new EventAction(OUTPUT, VALUE);
    if (!input.equals(START_WORD)) {
      throw new ParsingException(String.format("Failed to parse '%s'", input));
    }
    action.setOriginalString(input);
    return action;
  }

  @Override
  public String getRegex() {
    return "^" + START_WORD + "$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{START_WORD};
  }

}
