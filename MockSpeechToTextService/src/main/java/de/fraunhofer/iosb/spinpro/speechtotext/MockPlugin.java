package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

@Service(crypticId = "9bc9e34875ddde9e66b0803016f99d538b7c6e1472b12f5d74d405e9953f779a", name = "MockPlugin", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public class MockPlugin implements ISpeechToTextService {

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
    return "if this teststring is right then pass the test.";
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[]{new AudioFormat(16000, 16, 1, false, false)};
  }
}
