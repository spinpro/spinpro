package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

@Service(crypticId = "d517a340ad4e88d3133837e7d6bbc43d3ed3b3660f5fd48070f85946ed4f9b51", name = "FailingPlugin3", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public abstract class FailingPlugin3 implements ISpeechToTextService {

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
    return "if this teststring is right then pass the test.";
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[]{new AudioFormat(16000, 16, 1, false, false)};
  }
}
