package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

@Service(crypticId = "d337c4bbd454eb85e1a2996edaeca85daf20c78c29b3440ef89db25ac978fd14", name = "FailingPlugin1", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public class FailingPlugin1 implements ISpeechToTextService {

  //no standard constructor --> failing
  public FailingPlugin1(String a) {
  }

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
    return "if this teststring is right then pass the test.";
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[]{new AudioFormat(16000, 16, 1, false, false)};
  }
}
