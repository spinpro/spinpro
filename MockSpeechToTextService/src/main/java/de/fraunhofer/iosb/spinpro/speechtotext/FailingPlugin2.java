package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

@Service(crypticId = "d248ed024218896ab23a8efd05315be86f781dde2f1310d1e5fb689efd3ea68a", name = "FailingPlugin2", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public class FailingPlugin2 implements ISpeechToTextService {

  //throws exception --> failing
  public FailingPlugin2() throws ServiceInitializingException {
    throw new ServiceInitializingException();
  }

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
    return "if this teststring is right then pass the test.";
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[]{new AudioFormat(16000, 16, 1, false, false)};
  }
}
