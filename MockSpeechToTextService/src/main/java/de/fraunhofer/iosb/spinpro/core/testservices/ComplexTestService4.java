package de.fraunhofer.iosb.spinpro.core.testservices;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.Collection;

@Service(author = "Tim Windecker", name = "ComplexTestService4", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34faf0")

/*
  A complex service for testing purposes.
  Uses startWord as beginning of the parsed construct and creates if actions.
  A compiled version should be in test/resources/services.
 */
public class ComplexTestService4 implements IComplexActionService {

  private static final String CONDITION = "true";
  private static final String CONDITION_WORD = "check";
  private final static String START_WORD = "do";
  private final static String SEPARATION_WORD = "furthermore";

  @Override
  public IAction execute(String[] input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    IfAction action = new IfAction(CONDITION);
    if ((input.length == 2)) {
      if (input[0].equals(START_WORD + " " + CONDITION_WORD + " " + SEPARATION_WORD)) {
        Collection<IAction> parsedAction = actionParser.parse(input[1]);
        action.setThenActions(parsedAction.toArray(IAction[]::new));
        action.setOriginalString(input[0] + " " + input[1]);
        return action;
      }
    }
    throw new ParsingException(String.format("Failed to parse '%s' | '%s'", input[0], input[1]));
  }

  @Override
  public String getRegex() {
    return "^" + START_WORD + "\\s(.*)\\s" + SEPARATION_WORD + "\\s(.*)$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{START_WORD, SEPARATION_WORD};
  }

}
