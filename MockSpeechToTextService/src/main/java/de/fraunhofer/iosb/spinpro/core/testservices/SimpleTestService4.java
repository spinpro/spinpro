package de.fraunhofer.iosb.spinpro.core.testservices;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;

import java.util.Collection;

@Service(author = "Tim Windecker", name = "SimpleTestService4", organisation = "Fraunhofer IOSB", version = "0.1", crypticId = "b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f842")

/*
  A simple service for testing purposes.
  Uses startWord as beginning of the parsed construct and creates if actions.
  A compiled version should be in test/resources/services.
 */
public class SimpleTestService4 implements ISimpleActionService {

  private static final String CONDITION = "true";
  private static final String CONDITION_WORD = "check";
  private final static String START_WORD = "do";
  private final static String SEPARATION_WORD = "furthermore";

  @Override
  public IAction execute(String input, IVariableManager inputVarMan, IVariableManager outputEventMan, IActionParser actionParser) throws ParsingException {
    String unparsedWord;
    IfAction action = new IfAction(CONDITION);
    if (input.startsWith(START_WORD)) {
      unparsedWord = input.substring(START_WORD.length()).trim();
      if (unparsedWord.startsWith(CONDITION_WORD)) {
        unparsedWord = unparsedWord.substring(CONDITION_WORD.length()).trim();
        Collection<IAction> parsedAction = actionParser.parse(unparsedWord);
        action.setThenActions(parsedAction.toArray(IAction[]::new));
        action.setOriginalString(input);
        return action;
      }
    }
    throw new ParsingException(String.format("Failed to parse '%s'", input));
  }

  @Override
  public String getRegex() {
    return "^" + START_WORD + "\\s(.*)\\s" + SEPARATION_WORD + "\\s(.*)$";
  }

  @Override
  public String[] getHotwords() {
    return new String[]{START_WORD, SEPARATION_WORD};
  }

}
