package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceLoadingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.service.GenericServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;

/**
 * This abstract class loads all available {@link IActionService} using a {@link GenericServiceProvider} and is able to reload
 * the services at runtime.
 */
public abstract class AbstractActionServiceSelector<S> implements IActionServiceSelector<S> {

  protected GenericServiceProvider<S> provider;
  protected Set<IServiceWrapper<S>> services;
  protected IMessageBundle messageBundle = ConfigurationProvider.getMessageBundle();

  /**
   * Constructs an AbstractActionServiceSelector.
   * The source path of the services is loaded from the given {@link IPreferences}.
   * This constructor exists mainly for debug purposes.
   *
   * @param prefs containing the source path
   * @param s     the type of class that is loaded and selected from
   * @throws ConstructionException if the {@link GenericServiceProvider} failed to load the services
   */
  public AbstractActionServiceSelector(IPreferences prefs, Class<S> s) throws ConstructionException {
    // Use GenericServiceProvider to load service .jar's
    try {
      provider = new GenericServiceProvider<>(new File(prefs.getValue(PreferenceStrings.SERVICE_DIR)).toURI().toURL(), s);
    } catch (ServiceLoadingException | MalformedURLException e) {
      throw new ConstructionException(messageBundle.getMessage(Messages.CORE_ERROR_LOAD_SERVICES), e);
    }
    // Load services
    reloadServices();
  }

  /**
   * Reloads all the loaded services that this class holds at runtime.
   * Should be used rarely, as it may be very expensive to reload a huge number of services.
   */
  @Override
  public void reloadServices() {
    services = new HashSet<>(provider.getServices());
  }

}

