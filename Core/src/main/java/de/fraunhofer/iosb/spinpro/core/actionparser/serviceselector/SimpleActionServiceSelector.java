package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.service.GenericServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link IActionService} selector, that matches hot words and regular expressions of the available services
 * to determine which are most likely to parse the input text.
 */
public class SimpleActionServiceSelector extends AbstractActionServiceSelector<ISimpleActionService> {

  /**
   * Creates a new SimpleActionServiceSelector using the main preferences.
   *
   * @throws ConstructionException if the {@link GenericServiceProvider} failed to load the services
   */
  public SimpleActionServiceSelector() throws ConstructionException {
    this(ConfigurationProvider.getMainPreferences());
  }

  /**
   * Creates an new SimpleActionServiceSelector using specified preferences.
   *
   * @param prefs the specified preferences
   * @throws ConstructionException if the {@link GenericServiceProvider} failed to load the services
   */
  public SimpleActionServiceSelector(IPreferences prefs) throws ConstructionException {
    super(prefs, ISimpleActionService.class);
  }

  /**
   * Returns an {@link List} of {@link IActionService} depending on the input text.
   * Every available {@link IActionService} is examined whether it can possibly parse the text.
   * The text has to start with one hot word.
   * Then the regex is matched with the text,
   * The {@link List} is ordered in a way, that the ones, which are most likely to parse the text, are at the beginning.
   *
   * @param textInput The input text, that is not split in parts
   * @return the {@link IActionService services} inside their {@link IServiceWrapper} that can possibly parse the text
   */
  @Override
  public List<IServiceWrapper<ISimpleActionService>> getServices(String[] textInput) {
    // Filter all services that could parse textInput
    List<IServiceWrapper<ISimpleActionService>> possibleServices;
    possibleServices = services.stream()
        .filter(s -> Arrays.stream(s.getService().getHotwords()).anyMatch(textInput[0]::startsWith))
        .filter(s -> textInput[0].matches(s.getService().getRegex()))
        .collect(Collectors.toList());
    // Sort by sum of position, if equal by sum of length, of matched hotwords in ascending order
    possibleServices.sort((s1, s2) -> {
      double indexDiff = averageIndex(s1, textInput[0]) - averageIndex(s2, textInput[0]);
      double lengthDiff = averageLength(s1, textInput[0]) - averageLength(s2, textInput[0]);
      return (int) ((indexDiff != 0) ? indexDiff : lengthDiff);
    });
    return possibleServices;
  }

  /**
   * Calculates average of the index where all hotwords occur.
   *
   * @param service which hotwords are used
   * @param text    that is searched
   * @return average index
   */
  private double averageIndex(IServiceWrapper<ISimpleActionService> service, String text) {
    double indexSum = 0.0;
    double amount = 0.0;
    for (String word : service.getService().getHotwords()) {
      indexSum += text.indexOf(word);
      amount += (text.contains(word)) ? 1 : 0;
    }
    return (amount != 0) ? indexSum / amount : -1.0;
  }

  /**
   * Calculates average of the length of all matching hotwords.
   *
   * @param service which hotwords are used
   * @param text    that is searched
   * @return average length
   */
  private double averageLength(IServiceWrapper<ISimpleActionService> service, String text) {
    double lengthSum = 0.0;
    double amount = 0.0;
    for (String word : service.getService().getHotwords()) {
      lengthSum += word.length();
      amount += (text.contains(word)) ? 1 : 0;
    }
    return (amount != 0) ? lengthSum / amount : -1.0;
  }

}
