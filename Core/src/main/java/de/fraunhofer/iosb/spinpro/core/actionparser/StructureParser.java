package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * A Parser that can recognize information about the structure of a given sentence.
 * Needed for the {@link ComplexActionParser} to split complex speech construction sequences in the input string.
 */
public class StructureParser implements IStructureParser {

  private IMessageBundle messageBundle;

  public StructureParser() {
    messageBundle = ConfigurationProvider.getMessageBundle();
  }

  /**
   * This method checks if a word is contained in a string.
   * It differs from .contains(), because it does not simply check if the word is contained, but if the word is contained and surrounded by spaces
   * (only leading space if word is at the end/ending space if word is at start of sentence is needed).
   * In that way, it cannot happen that the word that is checked may be part of another word in the sentence.
   *
   * @param sentence The input sentence
   * @param word     the word that may be contained in the sentence
   * @return true if it is contained, false if not
   */
  public boolean checkWordInString(String sentence, String word) {
    return getWordIndex(sentence, word) != -1;
  }

  /**
   * As in {@link #checkWordInString(String, String)}, this method returns the next position of the word contained in the sentence.
   * It differs from .indexOf(), because it does not simply check the index, but the index of the word surrounded by spaces
   * (only leading space if word is at the end/ending space if word is at start of sentence is needed).
   * In that way, it cannot happen that the word that is checked may be part of another word in the sentence.
   *
   * @param sentence the input sentence
   * @param word     the word to get the index for
   * @return the index of the word if contained, -1 if not contained
   */
  private int getWordIndex(String sentence, String word) {
    if ((sentence.contains(word + " ") && sentence.indexOf(word) == 0) || sentence.equals(word)) {
      return 0;
    }
    if (sentence.contains(" " + word + " ")) {
      return sentence.indexOf(" " + word + " ") + 1;
    }
    int endIndex = sentence.length() - word.length();
    if (sentence.contains(" " + word) && sentence.indexOf(word) == endIndex) {
      return endIndex;
    }
    return -1;
  }

  /**
   * Checks if the given sentence structure is legal.
   *
   * @param sentence    the sentence
   * @param beginPhrase the starting phrase
   * @param endPhrase   the ending phrase
   * @throws ParsingException         if the sentence does not match structure criteria
   * @throws IllegalArgumentException if the starting phrase equals ending phrase or if no starting and ending phrase is contained
   */
  private void checkStartEnd(String sentence, String beginPhrase, String endPhrase) throws ParsingException, IllegalArgumentException {
    if (beginPhrase.equals(endPhrase)) {
      throw new IllegalArgumentException(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_START_EQ_CLOSE));
    }
    if ((sentence.indexOf(beginPhrase) > sentence.indexOf(endPhrase)) && checkWordInString(sentence, endPhrase)) {
      throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_END_BEFORE_BEGIN), endPhrase, beginPhrase));
    }
    boolean stop = false;
    String localCopyStart = sentence;
    while (!stop) {
      if (checkWordInString(localCopyStart, beginPhrase)) {
        localCopyStart = localCopyStart.replaceFirst(beginPhrase, "");
        if (checkWordInString(localCopyStart, endPhrase)) {
          localCopyStart = localCopyStart.replaceFirst(endPhrase, "");
        } else {
          throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
        }
      } else if (checkWordInString(localCopyStart, endPhrase)) {
        throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
      } else {
        stop = true;
      }
    }
  }

  private String getNestedSection(final String sentence, String beginPhrase, String endPhrase) throws ParsingException {
    String localCopy = sentence;


    if (!checkWordInString(localCopy, beginPhrase)) {
      return null;
    }
    localCopy = localCopy.substring(getWordIndex(localCopy, beginPhrase) + beginPhrase.length());
    int startCount = 1;
    int endCount = 0;


    StringBuilder builder = new StringBuilder();
    // Loop as long as we are not in top-level. When amount(start_phrase) = amount(end_phrase), top level is reached again
    while (startCount != endCount) {
      if (getWordIndex(localCopy, beginPhrase) < getWordIndex(localCopy, endPhrase) && checkWordInString(localCopy, beginPhrase)) {
        String beginning = localCopy.substring(0, localCopy.indexOf(beginPhrase) + beginPhrase.length());
        builder.append(beginning);
        localCopy = localCopy.replaceFirst(beginning, "");
        startCount++;
      } else {
        if (checkWordInString(localCopy, endPhrase)) {

          // Remember the last end, once the loop is over, this will be used as ending position for the string
          String beginning = localCopy.substring(0, localCopy.indexOf(endPhrase) + endPhrase.length());
          builder.append(beginning);
          localCopy = localCopy.replaceFirst(beginning, "");
          endCount++;
        } else {
          throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
        }
      }
    }

    return beginPhrase + builder.toString();
  }

  @Override
  public List<String> findToplevelSections(final String sentence, String beginPhrase, String endPhrase, String concatPhrase) throws ParsingException, IllegalArgumentException {

    checkStartEnd(sentence, beginPhrase, endPhrase);


    // Copy the input sentence. This copy will get smaller during loop-iterations, losing its suffix if it is a sequence.
    String copy = sentence;

    // Stop is the signal that there is no more splitting to do.
    boolean stop = false;

    // Counter counts the amount of sequences found, the sequences are saved into allSections
    int counter = -1;
    List<String> allSections = new ArrayList<>();


    while (!stop) {

      counter++;


      String copyBeginning = "";

      while (allSections.size() < counter + 1) {

        // Checks if the next nested structure is valid. this is done if either:
        // - The sentence contains a beginning phrase that is index before the next concat phrase
        // OR
        // - The sentence contains a beginning phrase but does not contain a concat phrase.

        while ((getWordIndex(copy, beginPhrase) < getWordIndex(copy, concatPhrase) && checkWordInString(copy, beginPhrase)) || (checkWordInString(copy, beginPhrase) && !checkWordInString(copy, concatPhrase))) {
          String nestedSection = getNestedSection(copy, beginPhrase, endPhrase);
          if (nestedSection != null) {
            String replacement = copy.substring(0, copy.indexOf(nestedSection)) + nestedSection;
            copyBeginning += replacement;
            copy = copy.replaceFirst(replacement, "");
          }
        }

        // After reaching top-level again, check for a concatenation phrase
        if (checkWordInString(copy, concatPhrase)) {
          if (((getWordIndex(copy, concatPhrase) < getWordIndex(copy, endPhrase)) && checkWordInString(copy, endPhrase)) || (!checkWordInString(copy, endPhrase))) {
            String placeholder = copy.substring(0, getWordIndex(copy, concatPhrase));
            copyBeginning += placeholder;
            allSections.add(copyBeginning.trim());
            copy = copy.replaceFirst(placeholder + concatPhrase, "").trim();
            copyBeginning = "";
          } else {
            throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
          }
        } else {
          if (!checkWordInString(copy, beginPhrase)) {
            if (checkWordInString(copy, endPhrase)) {
              throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
            }
            if (!copyBeginning.equals("")) {
              allSections.add(copyBeginning.trim());
            }
            if (!copy.equals("")) {
              allSections.add(copy.trim());
            }
            stop = true;
          }
        }

      }

    }
    return allSections;


  }

  @Override
  public String[] splitSections(final String sentence, String beginPhrase, String endPhrase) throws ParsingException, IllegalArgumentException {
    checkStartEnd(sentence, beginPhrase, endPhrase);

    String remainingString = sentence;
    List<String> returnString = new ArrayList<>();
    while (checkWordInString(remainingString, beginPhrase)) {
      if (getWordIndex(remainingString, beginPhrase) > getWordIndex(remainingString, endPhrase)) {
        throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_END_BEFORE_BEGIN), endPhrase, beginPhrase));
      }
      String outerSequence = remainingString.substring(0, getWordIndex(remainingString, beginPhrase));
      if (!outerSequence.equals("")) {
        returnString.add(remainingString.substring(0, getWordIndex(remainingString, beginPhrase)).trim());
      }
      remainingString = remainingString.replaceFirst(remainingString.substring(0, getWordIndex(remainingString, beginPhrase) + beginPhrase.length()), "");
      int neededBegins = 1;

      String beginEndSection = beginPhrase;
      StringBuilder builder = new StringBuilder();
      while (neededBegins != 0) {
        String toDelete;
        if (getWordIndex(remainingString, beginPhrase) < getWordIndex(remainingString, endPhrase) && checkWordInString(remainingString, beginPhrase)) {
          neededBegins++;
          toDelete = remainingString.substring(0, getWordIndex(remainingString, beginPhrase) + beginPhrase.length());

        } else {
          neededBegins--;
          toDelete = remainingString.substring(0, getWordIndex(remainingString, endPhrase) + endPhrase.length());

        }

        builder.append(toDelete);
        remainingString = remainingString.substring(toDelete.length());
      }

      beginEndSection += builder.toString();
      returnString.add(beginEndSection.trim());
    }

    if (checkWordInString(remainingString, endPhrase)) {
      throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_STRUCTURE_INCORRECT), beginPhrase, endPhrase));
    }

    if (!remainingString.equals("")) {
      returnString.add(remainingString.trim());
    }

    return returnString.toArray(new String[0]);
  }
}
