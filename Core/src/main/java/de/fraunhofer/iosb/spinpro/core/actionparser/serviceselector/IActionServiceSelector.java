package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;

import java.util.List;

/**
 * Describes a selector, that can use a text input to find a {@link List} of {@link IActionService} that can possibly parse the text.
 */
public interface IActionServiceSelector<S> {
  /**
   * Returns an {@link List} of {@link IActionService} depending on the input text.
   * The returned {@link IActionService} should be able to parse the text.
   *
   * @param textInput used to determine the {@link IActionService services}, that is split in parts to represent the sentence structure
   * @return the {@link IActionService services} inside their {@link IServiceWrapper} that can possibly parse the text
   */
  List<IServiceWrapper<S>> getServices(String[] textInput);

  /**
   * Reloads all the loaded services that this class holds at runtime.
   * Should be used rarely, as it may be very expensive to reload a huge number of services.
   */
  void reloadServices();

}
