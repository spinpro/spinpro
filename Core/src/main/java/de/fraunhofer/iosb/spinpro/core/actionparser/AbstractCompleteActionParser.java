package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector.IActionServiceSelector;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.variables.InputVariableManager;
import de.fraunhofer.iosb.spinpro.core.variables.OutputEventManager;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;

import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;

/**
 * An abstract Parser that defines a core-structure for both {@link SimpleActionParser} and {@link ComplexActionParser}.
 *
 * @param <T> the Type of Service that the actual parser will use (i.e. {@link ISimpleActionService ISimpleActionService} for {@link SimpleActionParser}).
 */
public abstract class AbstractCompleteActionParser<T> implements ICompleteActionParser {
  protected ILogger logger;
  protected IActionServiceSelector<T> actionServiceSelector;
  protected InputVariableManager inputVariableManager;
  protected OutputEventManager outputEventManager;
  protected Collection<IServiceWrapper<T>> usedServices;
  protected IMessageBundle messageBundle;
  protected IPreferences prefs;

  /**
   * Creates a new AbstractCompleteActionParser.
   */
  AbstractCompleteActionParser() {
    prefs = ConfigurationProvider.getMainPreferences();
    messageBundle = ConfigurationProvider.getMessageBundle();
    inputVariableManager = new InputVariableManager();
    outputEventManager = new OutputEventManager();
    usedServices = new HashSet<>();
  }

  /**
   * Parses a complete sentence.
   * This is the body-/core-method of the parser.
   * The overridden version of this method of each extending class can modify the input first,
   * and then use this method to parse the modified input.
   *
   * @param rule       the rule that will be filled with information
   * @param toBeParsed the input that will be parsed
   * @throws ParsingException if there are 1 or more substructures in the given input that cannot be parsed by any loaded service.
   */
  protected void parseCompleteBody(IRule rule, String toBeParsed) throws ParsingException {
    actionServiceSelector.reloadServices();
    usedServices = new HashSet<>();
    inputVariableManager.setDevice(rule.getMachine());
    outputEventManager.setDevice(rule.getMachine());
    Collection<IAction> actions = parse(toBeParsed);
    rule.setActions(actions.toArray(IAction[]::new));
    rule.setPluginSet(usedServices.stream().map(IServiceWrapper::getName).sorted().toArray(String[]::new));
    rule.setPluginHash(usedServices.stream()
        .map(s -> {
          try {
            return s.getHash();
          } catch (ConfigurationException e) {
            logger.log(Level.FINE, messageBundle.getMessage(Messages.CORE_CANNOT_GENERATE_HASH));
            return "";
          }
        }).sorted()
        .toArray(String[]::new));
  }
}
