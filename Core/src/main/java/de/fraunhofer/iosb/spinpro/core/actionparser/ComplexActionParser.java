package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector.ComplexActionServiceSelector;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

/**
 * Manages the translation of a String, containing a complex action pattern, to a Collection of {@link IAction}.
 */
public class ComplexActionParser extends AbstractCompleteActionParser<IComplexActionService> {

  private final String beginPhrase;
  private final String endPhrase;
  private final String concatPhrase;
  private final int maxAttemptCount;

  /**
   * Creates a ComplexActionParser.
   *
   * @throws ConstructionException if the {@link ComplexActionServiceSelector} could not be created
   */
  public ComplexActionParser() throws ConstructionException {
    super();
    beginPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_BEGINNING_PHRASE);
    endPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_ENDING_PHRASE);
    concatPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_CONCAT_PHRASE);
    maxAttemptCount = Integer.parseInt(prefs.getValue(PreferenceStrings.MAX_ATTEMPT_COUNT_COMPLEX));
    actionServiceSelector = new ComplexActionServiceSelector();
  }

  /**
   * Decides if a given text contains a complex action pattern.
   *
   * @param text the text to be checked for complexity
   * @return true if it is complex, false if not
   */
  public static Boolean isComplex(String text) {
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    // Check if begin or end phrase occur in the given text
    String beginPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_BEGINNING_PHRASE);
    String endPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_ENDING_PHRASE);
    return (text.contains(beginPhrase) && text.contains(endPhrase) && text.indexOf(beginPhrase) < text.indexOf(endPhrase));
  }

  /**
   * Translates a String to a Collection of {@link IAction}.
   * The String must start with the specified beginning phrase, and end with the specified ending phrase.
   * Uses a {@link ComplexActionServiceSelector} to determine which {@link IActionService} is used for the translation.
   * Can be called recursively by the selected {@link IActionService} with a callback.
   * Guarantees that the whole given String is parsed, otherwise an exception is thrown.
   *
   * @param s the String that is translated
   * @return the translated Collection of {@link IAction}
   * @throws ParsingException if the input could not be parsed
   */
  @Override
  public Collection<IAction> parse(String s) throws ParsingException {
    if (!s.startsWith(beginPhrase) || !s.endsWith(endPhrase)) {
      throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_MALFORMED_COMPLEX_RULE), beginPhrase, endPhrase));
    }

    // Removing the begin and the end phrase
    String editedSentence = s.substring(beginPhrase.length(), s.length() - endPhrase.length());
    editedSentence = editedSentence.trim();

    Iterator<IServiceWrapper<IComplexActionService>> serviceIterator;
    IServiceWrapper<IComplexActionService> service;
    StructureParser structureParser = new StructureParser();
    List<IAction> actions = new ArrayList<>();

    List<String> allSections = structureParser.findToplevelSections(editedSentence, beginPhrase, endPhrase, concatPhrase);
    // Outer loop, for all Actions that are parallel on the same stage
    for (String section : allSections) {

      String[] splitNestedSections = structureParser.splitSections(section, beginPhrase, endPhrase);

      // Match next section.
      IAction newAction = null;

      // Inner loop, repeat to try for every single action until maximum amount of tries is reached
      serviceIterator = actionServiceSelector.getServices(splitNestedSections).iterator();
      int attempt = 1;
      boolean successful;
      do {
        if (attempt > maxAttemptCount) {
          throw new ParsingException(messageBundle.getMessage(Messages.CORE_ERROR_MAX_PARS_ATTEMPTS_REACHED));
        }
        if (!serviceIterator.hasNext()) {
          throw new ParsingException(messageBundle.getMessage(Messages.CORE_ERROR_NO_FURTHER_SERVICES));
        }
        service = serviceIterator.next();
        try {
          newAction = service.getService().execute(splitNestedSections, inputVariableManager, outputEventManager, this);
          successful = true;
        } catch (Exception e) {
          if (logger != null) {
            logger.log(Level.FINE, String.format(messageBundle.getMessage(Messages.CORE_ERROR_SPEECH_CONSTR_SERVICE_EXCEPTION), e.getMessage()));
          }
          successful = false;
        }
        // Check if it was actually successful
        if (successful) {
          if ((newAction == null)) {
            if (logger != null) {
              logger.log(Level.FINE, messageBundle.getMessage(Messages.CORE_ERROR_SPEECH_CONSTR_SERVICE_FAILED));
            }
            successful = false;
          } else if (!newAction.getOriginalString().equals(section.trim())) {
            successful = false;
          }
        }
        attempt++;
      } while (!successful);

      actions.add(newAction);
      usedServices.add(service);
    }
    return actions;
  }

  /**
   * Parses a text to multiple {@link IAction}.
   * Gets the needed information from the given{@link IRule}.
   * Writes the {@link IAction actions}, the used {@link IActionService} names and their hashes to the {@link IRule}.
   *
   * @param rule   that contains the parsed text and machine name
   * @param logger the logger that is used
   * @throws ParsingException if the input could not be parsed completely
   */
  @Override
  public void parseComplete(IRule rule, ILogger logger) throws ParsingException {

    this.logger = logger;

    if (!isComplex(rule.getOriginalSentence())) {
      throw new IllegalArgumentException(messageBundle.getMessage(Messages.CORE_ERROR_NON_COMPLEX_RULE_PARSED_COMPLEX));
    }

    String toBeParsed = rule.getOriginalSentence();
    if (!toBeParsed.startsWith(beginPhrase)) {
      toBeParsed = beginPhrase + " " + toBeParsed + " " + endPhrase;
    }


    this.parseCompleteBody(rule, toBeParsed);
  }
}
