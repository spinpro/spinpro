package de.fraunhofer.iosb.spinpro.core.variables;

import de.fraunhofer.iosb.spinpro.variables.IOutputEvent;

import java.util.Collection;

/**
 * Defines a variable type that represents an output event in a rule.
 */
public class OutputEvent extends Variable implements IOutputEvent {

  private static final long serialVersionUID = -9116813155863650299L;

  /**
   * Creates an OutputEvent by calling the parents constructor.
   *
   * @param name of the OutputEvent
   */
  public OutputEvent(String name) {
    super(name);
  }

  /**
   * Creates an OutputEvent by calling the parents constructor.
   *
   * @param name    of the OutputEvent
   * @param aliases of the OutputEvent
   */
  public OutputEvent(String name, Collection<String> aliases) {
    super(name, aliases);
  }

  /**
   * Creates an OutputEvent by calling the parents constructor.
   *
   * @param name    of the OutputEvent
   * @param aliases of the OutputEvent
   * @param devices that support the OutputEvent
   */
  public OutputEvent(String name, Collection<String> aliases, Collection<String> devices) {
    super(name, aliases, devices);
  }
}
