package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.actionparser.ComplexActionParser;
import de.fraunhofer.iosb.spinpro.core.actionparser.ICompleteActionParser;
import de.fraunhofer.iosb.spinpro.core.actionparser.SimpleActionParser;
import de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector.AbstractActionServiceSelector;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionService;

/**
 * A Component that uses the plain input text stored inside of the passed {@link IRule} to receive a matching
 * {@link IActionService} from a {@link AbstractActionServiceSelector}.
 * It then passes the rule to the service. After processing, the configured rule is returned.
 */
public class SpeechConstructionStep extends AbstractCorePipelineStep {

  private ICompleteActionParser simpleParser;
  private ICompleteActionParser complexParser;

  /**
   * Creates a new SpeechConstructionStep.
   *
   * @throws ConstructionException if the construction of the action parsers failed
   */
  public SpeechConstructionStep() throws ConstructionException {
    super();
    simpleParser = new SimpleActionParser();
    complexParser = new ComplexActionParser();
  }

  /**
   * Parses the original sentence in rule to a collection of {@link de.fraunhofer.iosb.spinpro.actions.IAction actions}.
   * Saves which {@link IActionService services} were used.
   *
   * @param rule   with the input text saved inside of the rule
   * @param logger used for logging errors and status information
   * @return the rule with the parsed {@link de.fraunhofer.iosb.spinpro.actions.IAction actions}.
   * @throws ParsingException is thrown when the Input could not be parsed
   */
  @Override
  public IRule execute(IRule rule, ILogger logger) throws ParsingException {
    if (ComplexActionParser.isComplex(rule.getOriginalSentence())) {
      complexParser.parseComplete(rule, logger);
    } else {
      simpleParser.parseComplete(rule, logger);
    }
    return rule;
  }

}
