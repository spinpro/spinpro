package de.fraunhofer.iosb.spinpro.core.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.ICorePipelineStep;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Provides an interface that can manage a pipeline based on {@link ICorePipelineStep} classes.
 */
public interface ICorePipeline {

  /**
   * Starts a pipeline on the given rule.
   *
   * @param rule   the input rule
   * @param logger used for logging errors and status information
   * @return the rule that was processed by the pipeline
   * @throws ParsingException if the Pipeline could not successfully run with the given {@link IRule}
   * @throws IOException      if the Pipeline could not successfully run with the given {@link IRule}
   */
  IRule startPipeline(IRule rule, ILogger logger) throws ParsingException, IOException;

  /**
   * Adds a step to the steps of the pipeline.
   *
   * @param step the added step.
   */
  void addStep(ICorePipelineStep step);

  /**
   * Adds steps to the steps of the pipeline.
   *
   * @param steps the added steps.
   */
  void addSteps(Collection<ICorePipelineStep> steps);

  /**
   * Clears all the steps used by the pipeline.
   */
  void clearSteps();

  /**
   * Returns the steps the pipeline holds.
   *
   * @return the steps in order.
   */
  List<ICorePipelineStep> getSteps();
}
