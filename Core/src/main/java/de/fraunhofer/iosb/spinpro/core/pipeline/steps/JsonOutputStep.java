package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import com.google.gson.Gson;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * A pipeline step that outputs the given rule as a JSON file in a specified folder.
 */
public class JsonOutputStep extends AbstractCorePipelineStep {

  private static final String OUTPUT_FORMAT = "%s/%s_%s.json";
  private File outputFolder;

  /**
   * Creates a {@link JsonOutputStep} by loading the preferences for the output folder.
   */
  public JsonOutputStep() {
    super();
    IPreferences pref = ConfigurationProvider.getMainPreferences();
    outputFolder = new File(pref.getValue(PreferenceStrings.RULE_OUTPUT_DIR));
  }

  /**
   * Creates a {@link JsonOutputStep} by loading the preferences for the output folder.
   * Also passing the folder where the {@link IRule} should be saved.
   *
   * @param outputFolder folder where the {@link IRule} is saved
   */
  public JsonOutputStep(File outputFolder) {
    super();
    this.outputFolder = outputFolder;
  }

  /**
   * Converts the rule to a JSON and saves it to a file.
   * In order to create a valid file name, the {@link IRule} must have a name and id.
   * Uses the {@link Gson} library.
   * Fills the OutputFile-Field in the {@link IRule} with information.
   *
   * @param rule   the rule that will be converted and saved
   * @param logger used for logging errors and status information
   * @return the unmodified rule
   */
  @Override
  public IRule execute(IRule rule, ILogger logger) throws IOException {
    if (outputFolder.isFile()) {
      throw new IOException(messageBundle.getMessage(Messages.CORE_ERROR_CREATE_OUTPUT_FOLDER));
    }
    if (!outputFolder.exists()) {
      if (!outputFolder.mkdirs()) {
        throw new IOException(messageBundle.getMessage(Messages.CORE_ERROR_CREATE_OUTPUT_FOLDER));
      }
    }
    if (rule.getName() == null) {
      throw new IOException(messageBundle.getMessage(Messages.CORE_ERROR_NO_RULE_NAME));
    }
    File file = new File(String.format(OUTPUT_FORMAT, outputFolder.getPath(), rule.getName(), rule.getID()));
    if (file.exists()) {
      throw new IOException(messageBundle.getMessage(Messages.CORE_ERROR_RULE_ALREADY_EXISTS));
    }
    if (!file.createNewFile()) {
      throw new IOException(messageBundle.getMessage(Messages.CORE_ERROR_CREATE_OUTPUT_FILE));
    }
    FileWriter writer = new FileWriter(file);
    Gson gson = new Gson();
    gson.toJson(rule, writer);
    writer.flush();
    writer.close();
    rule.setOutputFile(file);
    return rule;
  }
}
