package de.fraunhofer.iosb.spinpro.core.variables;

import com.google.gson.Gson;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.IPreferenceStrings;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import de.fraunhofer.iosb.spinpro.variables.IVariableManager;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Manages the registered {@link Variable Variables} and provides them, using a factory-method pattern.
 */
public abstract class AbstractVariableManager implements IVariableManager {
  protected final IPreferences prefs;

  /**
   * The path of the folder in which the {@link Variable Variables} are stored.
   */
  protected String storageFolder;
  protected ILogger logger;
  protected Gson gson;
  protected IMessageBundle messageBundle;
  /**
   * The device that is currently checked for.
   * If null all variables are allowed.
   */
  private String device;
  private List<IVariable> variables;
  private Set<Pair<String, IVariable>> aliasVars;

  /**
   * Creates a new AbstractVariableManager, loading variables from the preferences with the given preference name.
   * If two variables have a common alias only one will be available.
   * Uses the main logger.
   *
   * @param prefString the preference in which the path to the storage folder is saved
   */
  public AbstractVariableManager(IPreferenceStrings prefString) {
    this(ConfigurationProvider.getMainPreferences().getValue(prefString));
  }

  /**
   * Creates a new AbstractVariableManager, loading the given folder where the variables are stored.
   * If two variables have a common alias only one will be available.
   * Uses the main logger.
   *
   * @param storageFolder folder where variables are stored
   */
  public AbstractVariableManager(String storageFolder) {
    prefs = ConfigurationProvider.getMainPreferences();
    this.logger = ConfigurationProvider.getMainLogger();
    this.storageFolder = storageFolder;
    this.gson = new Gson();
    this.messageBundle = ConfigurationProvider.getMessageBundle();
    variables = new LinkedList<>();
    aliasVars = new TreeSet<>((o1, o2) -> {
      int dist = o1.getLeft().length() - o2.getLeft().length();
      if (dist != 0) {
        return dist;
      }
      return (int) Math.signum(o1.getLeft().compareTo(o2.getLeft()));
    });
    File folder = new File(storageFolder);
    if (!(folder.isDirectory())) {
      this.logger.log(Level.WARNING, messageBundle.getMessage(Messages.CORE_VARIABLES_INVALID_PATH));
    } else {
      Arrays.stream(Objects.requireNonNull(folder.list())).forEach(s -> addVariable(loadVariable(folder.getPath() + "/" + s)));
    }
  }

  /**
   * Adds a {@link IVariable} to variables and aliasVars.
   *
   * @param vars variable that is added, if null nothing is added
   */
  private void addVariable(IVariable vars) {
    if (vars != null) {
      variables.add(vars);
      Collection<String> aliases = vars.getAliases();
      if (aliases != null) {
        aliases.forEach(s -> aliasVars.add(new ImmutablePair<>(s, vars)));
      }
    }
  }

  /**
   * Loads a {@link IVariable} with the specified name from folder.
   *
   * @param path the path of the {@link IVariable} that should be loaded
   * @return the {@link IVariable}
   */
  protected abstract IVariable loadVariable(String path);

  /**
   * Returns a {@link IVariable} mapped to the given alias.
   * A found variable has to be supported by the selected device.
   * If two variables have a common alias only one will be available.
   *
   * @param name the name or an alias of the {@link IVariable}
   * @return the specified {@link IVariable} or null if none was found
   */
  @Override
  public IVariable getVariable(String name) {
    return aliasVars.stream().filter(p -> (device == null) || (p.getRight().getDevices() == null) || (p.getRight().getDevices().contains(device))).filter(p -> p.getLeft().equals(name)).findFirst().orElse(new ImmutablePair<>(null, null)).getRight();
  }

  /**
   * Returns if a {@link Variable} mapped to the given alias exists.
   * A found variable has to be supported by the selected device.
   * If two variables have a common alias only one will be available.
   *
   * @param name the name of the {@link Variable}
   * @return true if the the specified {@link Variable} exists or false if none was found
   */
  @Override
  public boolean containsVariable(String name) {
    return aliasVars.stream().filter(p -> (device == null) || (p.getRight().getDevices() == null) || (p.getRight().getDevices().contains(device))).anyMatch((p -> p.getLeft().equals(name)));
  }

  /**
   * Returns all the variables available at the given folder location that are supported by the selected device.
   *
   * @return all available variables supported by the device
   */
  @Override
  public Collection<IVariable> getVariables() {
    return variables.stream().filter(v -> (device == null) || (v.getDevices() == null) || (v.getDevices().contains(device))).collect(Collectors.toList());
  }

  /**
   * Finds all the variables in the given string.
   * A found variable has to be supported by the selected device.
   * If two variables have a common alias only one will be available.
   *
   * @param s The string to search in.
   * @return The list of variables with the string parts they were found in.
   */
  @Override
  public List<Pair<String, IVariable>> getVariablesInString(String s) {
    return this.aliasVars.stream().filter(p -> (device == null) || (p.getRight().getDevices() == null) || (p.getRight().getDevices().contains(device))).filter(a -> s.contains(a.getLeft())).collect(Collectors.toList());
  }

  /**
   * Returns a sorted list with all the variables associated to their alias.
   * A found variable has to be supported by the selected device.
   * If two variables have a common alias only one will be available.
   *
   * @return The list.
   */
  @Override
  public List<Pair<String, IVariable>> getSortedAliasVariables() {
    return aliasVars.stream().filter(p -> (device == null) || (p.getRight().getDevices() == null) || (p.getRight().getDevices().contains(device))).collect(Collectors.toList());
  }

  /**
   * Sets the device that is checked for when giving out variables.
   *
   * @param device the device or if null all variables are allowed
   */
  @Override
  public void setDevice(String device) {
    this.device = device;
  }


}
