package de.fraunhofer.iosb.spinpro.core.rule;

import de.fraunhofer.iosb.spinpro.actions.IAction;

import java.io.File;

/**
 * A rule, used for pipelining. JSON will be created from this Rule
 */
public class Rule implements IRule {

  private String name;
  private String originalSentence;
  private long id;
  private String creator;
  private String version;
  private String lang;
  private String domain;
  private String machine;
  private String company;
  private String copyright;
  private IAction[] actions;
  private String[] pluginSet;
  private String[] pluginHash;
  private transient File outputFile;


  /**
   * Creates a new Rule, passing the original sentence that was entered.
   *
   * @param originalSentence the original sentence entered by the user
   */
  public Rule(String originalSentence) {
    this.originalSentence = originalSentence;
  }


  /**
   * Creates a new Rule, passing the original sentence that was entered and the machine that is used.
   *
   * @param originalSentence the original sentence entered by the user
   * @param machine          the machine this Rule should belong to
   */
  public Rule(String originalSentence, String machine) {
    this.originalSentence = originalSentence;
    this.machine = machine;
  }

  /**
   * Creates a new Rule, passing the original sentence that was entered, the machine that is used and the id of the person that created the rule.
   *
   * @param originalSentence the original sentence entered by the user
   * @param machine          the machine this Rule should belong to
   * @param creator          the id of the person that created the rule
   */
  public Rule(String originalSentence, String machine, String creator) {
    this.originalSentence = originalSentence;
    this.machine = machine;
    this.creator = creator;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getOriginalSentence() {
    return originalSentence;
  }

  @Override
  public void setOriginalSentence(String originalSentence) {
    this.originalSentence = originalSentence;
  }

  @Override
  public long getID() {
    return id;
  }

  @Override
  public void setID(long id) {
    this.id = id;
  }

  @Override
  public String getVersion() {
    return version;
  }

  @Override
  public void setVersion(String version) {
    this.version = version;
  }

  @Override
  public String getLang() {
    return lang;
  }

  @Override
  public void setLang(String lang) {
    this.lang = lang;
  }

  @Override
  public String getDomain() {
    return domain;
  }

  @Override
  public void setDomain(String domain) {
    this.domain = domain;
  }

  @Override
  public String getMachine() {
    return machine;
  }

  @Override
  public void setMachine(String machine) {
    this.machine = machine;
  }

  @Override
  public String getCompany() {
    return company;
  }

  @Override
  public void setCompany(String company) {
    this.company = company;
  }

  @Override
  public String getCopyright() {
    return copyright;
  }

  @Override
  public void setCopyright(String copyright) {
    this.copyright = copyright;
  }

  @Override
  public IAction[] getActions() {
    return actions;
  }

  @Override
  public void setActions(IAction[] actions) {
    this.actions = actions;
  }

  @Override
  public String[] getPluginSet() {
    return pluginSet;
  }

  @Override
  public void setPluginSet(String[] names) {
    this.pluginSet = names;
  }

  @Override
  public String[] getPluginHash() {
    return pluginHash;
  }

  @Override
  public void setPluginHash(String[] hashes) {
    this.pluginHash = hashes;
  }

  @Override
  public void addAction(IAction action) {
    if (action != null) {
      this.actions = expandArray(this.actions, new IAction[]{action});
    }
  }

  @Override
  public void addActions(IAction[] actions) {
    this.actions = expandArray(this.actions, actions);
  }

  @Override
  public File getOutputFile() {
    return outputFile;
  }

  @Override
  public void setOutputFile(File file) {
    this.outputFile = file;
  }

  @Override
  public String getCreator() {
    return creator;
  }

  @Override
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Creates a bigger Array using the smallArray as first Objects, and addedArray as last Objects in the array.
   *
   * @param smallArray the first array that will be copied
   * @param addedArray the last array that will be copied
   * @return the expanded array or null if both array are null
   */
  private IAction[] expandArray(IAction[] smallArray, IAction[] addedArray) {

    // If second array is null, just return first array (can be null).
    if (addedArray == null) {
      return smallArray;
    }
    // If first array is null, just return the second array
    if (smallArray == null) {
      return addedArray;
    }
    // Copy both arrays into the larger one
    IAction[] largerActionsArray = new IAction[smallArray.length + addedArray.length];
    System.arraycopy(smallArray, 0, largerActionsArray, 0, smallArray.length);
    System.arraycopy(addedArray, 0, largerActionsArray, smallArray.length, addedArray.length);
    return largerActionsArray;
  }


}
