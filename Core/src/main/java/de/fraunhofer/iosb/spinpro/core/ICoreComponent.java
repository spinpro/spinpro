package de.fraunhofer.iosb.spinpro.core;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;

import java.io.File;

/**
 * The interface to communicate with the core.
 */
public interface ICoreComponent {

  /**
   * Creates a rule from the given input and stores it.
   * The return String is null on success, or contains the error message on failure.
   *
   * @param input   the input text
   * @param machine the machine the Rule should be created for
   * @param creator the name of the creator of the rule
   * @param logger  used for logging errors and status information
   * @return the file in which the created rule is saved, null on error
   */
  File createRule(String input, String machine, String creator, ILogger logger);

}
