package de.fraunhofer.iosb.spinpro.core.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.GeneralInformationStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.ICorePipelineStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.InputStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.JsonOutputStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.SpeechConstructionStep;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

/**
 * A class that arranges the pipeline structure, executing all registered {@link ICorePipelineStep pipeline steps}.
 */
public class CorePipeline implements ICorePipeline {
  private List<ICorePipelineStep> steps;
  private IMessageBundle messageBundle;

  /**
   * Creates a new pipeline.
   * The steps that should be executed can be passed.
   * If null is passed instead, the pipeline executes the following steps in order:
   *    <ul>
   *       <li>{@link InputStep}</li>
   *       <li>{@link GeneralInformationStep}</li>
   *       <li>{@link SpeechConstructionStep}</li>
   *       <li>{@link JsonOutputStep}</li>
   *     </ul>
   *
   * <p>(null therefore initializes a standard-pipeline)
   *
   * @param steps pipeline steps, that are executed, if null default steps are used
   * @throws ConstructionException if the construction of the action parsers failed
   */
  public CorePipeline(List<ICorePipelineStep> steps) throws ConstructionException {
    messageBundle = ConfigurationProvider.getMessageBundle();

    // Set all steps
    if (steps == null) {
      this.steps = new ArrayList<>();
      this.steps.add(new InputStep());
      this.steps.add(new GeneralInformationStep());
      this.steps.add(new SpeechConstructionStep());
      this.steps.add(new JsonOutputStep());
    } else {
      this.steps = new ArrayList<>(steps);
    }
  }

  /**
   * Starts a pipeline on the given rule.
   *
   * @param rule   the input rule
   * @param logger used for logging errors and status information
   * @throws ParsingException if a pipeline step could not execute properly with the given rule
   * @throws IOException      if a pipeline step could not execute properly with the given rule
   */
  @Override
  public IRule startPipeline(IRule rule, ILogger logger) throws ParsingException, IOException {
    long millis = System.currentTimeMillis();
    IRule processedRule = rule;
    for (ICorePipelineStep step : steps) {
      processedRule = step.execute(processedRule, logger);
    }
    float interval = ((float) (System.currentTimeMillis() - millis) / 1000);
    logger.log(Level.FINEST, String.format(messageBundle.getMessage(Messages.CORE_PROCESSING_FINISHED_IN), String.format("%.2f", interval)));
    return processedRule;
  }

  @Override
  public void addStep(ICorePipelineStep step) {
    this.steps.add(step);
  }

  @Override
  public void addSteps(Collection<ICorePipelineStep> steps) {
    this.steps.addAll(steps);
  }

  @Override
  public void clearSteps() {
    this.steps.clear();
  }

  @Override
  public List<ICorePipelineStep> getSteps() {
    return steps;
  }

}
