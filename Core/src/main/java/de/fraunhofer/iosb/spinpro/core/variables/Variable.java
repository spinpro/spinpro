package de.fraunhofer.iosb.spinpro.core.variables;

import de.fraunhofer.iosb.spinpro.variables.IVariable;

import java.io.Serializable;
import java.util.*;

/**
 * Represents a variable that can be used in a rule.
 */
public abstract class Variable implements Serializable, IVariable {

  private static final long serialVersionUID = 3994283109938449476L;
  /**
   * The name of the variable.
   */
  protected String name;

  /**
   * All possible names that refer to this variable.
   */
  protected Collection<String> aliases;

  /**
   * Devices that support the variable.
   * If null all devices should support this variable.
   */
  protected Collection<String> devices;

  /**
   * Creates a variable with a given name.
   *
   * @param name of the variable
   */
  public Variable(String name) {
    this.name = name;
    this.aliases = null;
    this.devices = null;
  }


  /**
   * Creates a variable with a given name and aliases.
   * Copies the aliases to a {@link HashSet}.
   *
   * @param name    of the variable
   * @param aliases possible aliases for the variable
   */
  public Variable(String name, Collection<String> aliases) {
    this.name = name;
    this.aliases = new HashSet<>(aliases);
    this.devices = null;
  }

  /**
   * Creates a variable with a given name, aliases and devices.
   * Copies the aliases and the devices to a {@link HashSet}.
   *
   * @param name    of the variable
   * @param aliases possible aliases for the variable
   * @param devices devices that support the variable
   */
  public Variable(String name, Collection<String> aliases, Collection<String> devices) {
    this.name = name;
    this.aliases = new HashSet<>(aliases);
    this.devices = new HashSet<>(devices);
  }

  /**
   * Returns the name of the variable.
   *
   * @return the name
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * Returns the aliases of the variable.
   *
   * @return the aliases, can be null
   */
  @Override
  public Collection<String> getAliases() {
    return aliases;
  }

  /**
   * Returns the devices that support the variable.
   *
   * @return the devices, can be null
   */
  @Override
  public Collection<String> getDevices() {
    return devices;
  }

  @Override
  public String getRepresentation() {
    return getName();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj.getClass() == this.getClass()) {
      Variable input = (Variable) obj;
      boolean checkAliases;
      boolean checkDevices;
      if (input.aliases == null && this.aliases == null) {
        checkAliases = true;
      } else if (input.aliases != null && this.aliases != null) {
        List<String> thisAliases = new ArrayList<>(this.aliases);
        List<String> inputAliases = new ArrayList<>(input.aliases);
        Collections.sort(thisAliases);
        Collections.sort(inputAliases);
        if (thisAliases.size() != inputAliases.size()) {
          return false;
        }
        checkAliases = thisAliases.equals(inputAliases);
      } else {
        return false;
      }

      if (input.devices == null && this.devices == null) {
        checkDevices = true;
      } else if (input.devices != null && this.devices != null) {
        List<String> thisDevices = new ArrayList<>(this.devices);
        List<String> inputDevices = new ArrayList<>(input.devices);
        Collections.sort(thisDevices);
        Collections.sort(inputDevices);
        if (thisDevices.size() != inputDevices.size()) {
          return false;
        }
        checkDevices = thisDevices.equals(inputDevices);
      } else {
        return false;
      }
      return Objects.equals(this.name, input.name) && checkAliases && checkDevices;
    }
    return super.equals(obj);
  }
}
