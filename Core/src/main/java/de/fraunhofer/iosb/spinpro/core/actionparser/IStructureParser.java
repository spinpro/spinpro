package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;

import java.util.List;


/**
 * A Parser that can recognize information about the structure of a given sentence.
 */
public interface IStructureParser {

  /**
   * Finds all given sequences in the input sentence.
   * A sequence is defined as the string that ends before the first top-level concatenation phrase.
   * If a concatenation phrase is positioned inside of a begin-end structure, it does not lie in the top-level structure and it will therefore be ignored as sequence-ending.
   * Example: "If a &gt; b begin foo also bar end also b = 3;" using the phrases: (begin, end, also)
   * -&gt; splits to <b>If a &gt; b begin foo also bar end</b> and <b>bar end also b = 3</b> (Only the second <b>also</b> defines a sequence ending.
   * Note that the splitting concatenation phrase, as well as spaces at the beginning and end of each sequence will be removed.
   *
   * @param sentence        The sentence that contains multiple sections
   * @param beginningPhrase the specified start of nested sequences
   * @param endingPhrase    the specified end of nested sequences
   * @param concatPhrase    the specified phrase to concatenate sequences
   * @return the sections that are contained in the sentence
   * @throws ParsingException         if the sections cannot be parsed (it is not a well formed bracket expression)
   * @throws IllegalArgumentException if the passed arguments are not valid, i.e. the start-word equals the end-word.
   */
  List<String> findToplevelSections(String sentence, String beginningPhrase, String endingPhrase, String concatPhrase) throws ParsingException, IllegalArgumentException;

  /**
   * Splits the given strings into sequences.
   * This method assumes that the the given text input is a resulting string of
   * {@link #findToplevelSections(String, String, String, String) findToplevelSections(...)}.
   * The string, in particular, does not contain any concatenation phrase at top-level,
   * but is a concatenation of beginning-ending-nested sequences, and other (top-level) sequences between them.
   * The resulting array will also contain these kind of entries: the top-level sequences and the nested beginning-ending sequences.
   * Each array entry is one of a kind.
   *
   * @param sentence        the input sentence
   * @param beginningPhrase the specified start of nested sequences
   * @param endingPhrase    the specified end of nested sequences
   * @return the sequences contained in the string
   * @throws ParsingException         if the sections cannot be parsed (it is not a well formed bracket expression)
   * @throws IllegalArgumentException if the passed arguments are not valid, i.e. the start-word equals the end-word.
   */
  String[] splitSections(String sentence, String beginningPhrase, String endingPhrase) throws ParsingException, IllegalArgumentException;
}
