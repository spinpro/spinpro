package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;

/**
 * A class that modifies the general information stored inside of the {@link IRule}.
 */
public class GeneralInformationStep extends AbstractCorePipelineStep {

  private IPreferences preferences;

  /**
   * Creates a new GeneralInformationStep.
   */
  public GeneralInformationStep() {
    super();
    preferences = ConfigurationProvider.getMainPreferences();
  }

  /**
   * Adds general information that depend on the environment (e.g. company name, product version).
   * In order to create a valid name, the creator and machine of the {@link IRule} must be set.
   *
   * @param rule   the input rule
   * @param logger used for logging errors and status information
   * @return the rule containing the general information
   */
  @Override
  public IRule execute(IRule rule, ILogger logger) {
    rule.setCompany(preferences.getValue(PreferenceStrings.RULE_COMPANY));
    rule.setDomain(preferences.getValue(PreferenceStrings.RULE_DOMAIN));
    rule.setCopyright(preferences.getValue(PreferenceStrings.RULE_COPYRIGHT));
    if ((rule.getCreator() != null) && (rule.getMachine() != null)) {
      rule.setName(rule.getCreator() + "_" + rule.getMachine());
    }
    rule.setID(System.currentTimeMillis());
    return rule;
  }
}
