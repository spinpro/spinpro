package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector.SimpleActionServiceSelector;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

/**
 * Manages the translation of a String, containing a simple action pattern, to a Collection of {@link IAction}.
 */
public class SimpleActionParser extends AbstractCompleteActionParser<ISimpleActionService> {


  private final int maxAttemptCount;

  /**
   * Creates an ActionParser.
   *
   * @throws ConstructionException if the {@link SimpleActionServiceSelector} could not be created
   */
  public SimpleActionParser() throws ConstructionException {
    super();
    maxAttemptCount = Integer.parseInt(prefs.getValue(PreferenceStrings.MAX_ATTEMPT_COUNT_SIMPLE));
    actionServiceSelector = new SimpleActionServiceSelector();
  }

  /**
   * Translates a String to a Collection of {@link IAction}.
   * Uses a {@link SimpleActionServiceSelector} to determine which {@link IActionService} is used for the translation.
   * Can be called recursively by the selected {@link IActionService} with a callback.
   * Guarantees that the whole given String is parsed, otherwise an exception is thrown.
   *
   * @param s the String that is translated
   * @return the translated Collection of {@link IAction}
   * @throws ParsingException if the input could not be parsed
   */
  @Override
  public Collection<IAction> parse(String s) throws ParsingException {
    Iterator<IServiceWrapper<ISimpleActionService>> serviceIterator;
    IServiceWrapper<ISimpleActionService> service;
    List<IAction> actions = new ArrayList<>();
    IAction newAction = null;
    String trimmedS = s.trim();

    // Repeat to try for every single action until maximum amount of tries is reached
    serviceIterator = actionServiceSelector.getServices(new String[]{trimmedS}).iterator();
    int attempt = 1;
    boolean successful;
    do {
      // Throw if input can not be parsed within given amount of tries
      if (attempt > maxAttemptCount) {
        throw new ParsingException(messageBundle.getMessage(Messages.CORE_ERROR_MAX_PARS_ATTEMPTS_REACHED));
      }
      if (!serviceIterator.hasNext()) {
        throw new ParsingException(messageBundle.getMessage(Messages.CORE_ERROR_NO_FURTHER_SERVICES));
      }
      // Execute next service
      service = serviceIterator.next();
      try {
        newAction = service.getService().execute(trimmedS, inputVariableManager, outputEventManager, this);
        successful = true;
      } catch (Exception e) {
        if (logger != null) {
          logger.log(Level.FINE, String.format(messageBundle.getMessage(Messages.CORE_ERROR_SPEECH_CONSTR_SERVICE_EXCEPTION), e.getMessage()));
        }
        successful = false;
      }
      // Check if it was actually successful
      if (successful) {
        if ((newAction == null)) {
          if (logger != null) {
            logger.log(Level.FINE, messageBundle.getMessage(Messages.CORE_ERROR_SPEECH_CONSTR_SERVICE_FAILED));
          }
          successful = false;
        } else if (!newAction.getOriginalString().equals(trimmedS)) {
          successful = false;
        }
      }
      attempt++;
    } while (!successful);

    // Pack parsed action in List
    actions.add(newAction);
    usedServices.add(service);
    return actions;
  }

  /**
   * Parses a text to multiple {@link IAction}.
   * Gets the text and machine name from the given {@link IRule}.
   * Writes the {@link IAction actions}, the used {@link IActionService} names and their hashes to the {@link IRule}.
   *
   * @param rule   that contains the parsed text and machine name
   * @param logger the logger that is used
   * @throws ParsingException if the input could not be parsed completely
   */
  @Override
  public void parseComplete(IRule rule, ILogger logger) throws ParsingException {
    this.logger = logger;
    this.parseCompleteBody(rule, rule.getOriginalSentence());
  }

}
