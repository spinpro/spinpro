package de.fraunhofer.iosb.spinpro.core.variables;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.variables.IVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;

/**
 * A child of an {@link AbstractVariableManager} that creates {@link InputVariable InputVariables} and returns them.
 * Holding multiple instances of this class might be expensive, since all {@link Variable variables} are loaded and collected.
 */
public class InputVariableManager extends AbstractVariableManager {

  private static final String JSON_FAILED = "The file '%s' could not be parsed from JSON. The error message is '%s'.";
  private static final String FILE_NOT_FOUND = "The file '%s' does not exist or could not be read. The error message is '%s'.";

  /**
   * Gets the storage folder from the preferences and loads the variables from that path.
   */
  public InputVariableManager() {
    super(PreferenceStrings.INPUT_VARIABLE_DIR);
  }

  /**
   * Loads the variables from the given path.
   *
   * @param storageFolder folder where the {@link InputVariable variables} are saved
   */
  public InputVariableManager(String storageFolder) {
    super(storageFolder);
  }

  /**
   * Loads an {@link InputVariable} with the specified name from folder.
   *
   * @param path the path of the {@link InputVariable} that should be loaded
   * @return the {@link InputVariable} or null if it could not be loaded
   */
  @Override
  protected IVariable loadVariable(String path) {
    try {
      logger.log(Level.FINEST, String.format("%s: %s", ConfigurationProvider.getMessageBundle().getMessage(Messages.LOADING_VARIABLES_FROM_PATH), new File(path).getAbsolutePath()));
      return gson.fromJson(new FileReader(path), InputVariable.class);
    } catch (JsonSyntaxException | JsonIOException e) {
      logger.log(Level.WARNING, String.format(JSON_FAILED, path, e.getMessage()));
    } catch (FileNotFoundException e) {
      logger.log(Level.WARNING, String.format(FILE_NOT_FOUND, path, e.getMessage()));
    }
    return null;
  }

}
