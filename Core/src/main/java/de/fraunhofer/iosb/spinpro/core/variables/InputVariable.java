package de.fraunhofer.iosb.spinpro.core.variables;

import de.fraunhofer.iosb.spinpro.variables.IInputVariable;

import java.util.Collection;

/**
 * Defines a variable type that represents an input {@link Variable} in a rule.
 */
public class InputVariable extends Variable implements IInputVariable {

  private static final long serialVersionUID = 316533008350471442L;

  /**
   * Creates an InputVariable by calling the parents constructor.
   *
   * @param name of the InputVariable
   */
  public InputVariable(String name) {
    super(name);
  }

  /**
   * Creates an InputVariable by calling the parents constructor.
   *
   * @param name    of the InputVariable
   * @param aliases of the InputVariable
   */
  public InputVariable(String name, Collection<String> aliases) {
    super(name, aliases);
  }

  /**
   * Creates an InputVariable by calling the parents constructor.
   *
   * @param name    of the InputVariable
   * @param aliases of the InputVariable
   * @param devices that support the InputVariable
   */
  public InputVariable(String name, Collection<String> aliases, Collection<String> devices) {
    super(name, aliases, devices);
  }


}
