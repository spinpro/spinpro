package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;


/**
 * An abstract class that provides each AbstractCorePipelineStep an {@link IMessageBundle}.
 */
public abstract class AbstractCorePipelineStep implements ICorePipelineStep {

  protected IMessageBundle messageBundle;

  /**
   * Loads {@link IMessageBundle message bundle} from configuration.
   */
  public AbstractCorePipelineStep() {
    messageBundle = ConfigurationProvider.getMessageBundle();
  }

}
