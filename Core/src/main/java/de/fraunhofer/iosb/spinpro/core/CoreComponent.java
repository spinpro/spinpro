package de.fraunhofer.iosb.spinpro.core;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.pipeline.CorePipeline;
import de.fraunhofer.iosb.spinpro.core.pipeline.ICorePipeline;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * The implementation of a Core Component.
 * Acts as facade, delivers the input to a pipeline.
 */
public class CoreComponent implements ICoreComponent {

  private ICorePipeline pipeline;
  private IMessageBundle messageBundle;

  /**
   * Creates a CoreComponent.
   * If the construction fails, an error message is written to the main logger.
   *
   * @throws ConstructionException if the speech construction could not be initialized correctly
   */
  public CoreComponent() throws ConstructionException {
    ILogger logger = ConfigurationProvider.getMainLogger();
    messageBundle = ConfigurationProvider.getMessageBundle();
    try {
      pipeline = new CorePipeline(null);
    } catch (ConstructionException e) {
      logger.log(Level.SEVERE, messageBundle.getMessage(Messages.EXCEPTION_WHILE_CREATION), e);
      throw new ConstructionException(e);
    }
  }

  /**
   * Runs the Core to create a {@link IRule}.
   *
   * @param input   the input sentence
   * @param machine the machine this rule should belong to
   * @param creator the person that created the input sentence
   * @return a File, pointing to the created rule
   */
  public File createRule(String input, String machine, String creator, ILogger logger) {
    // Create new Rule passing the original sentence, the machine and the identifier of the person that created the rule
    IRule rule = new Rule(input, machine, creator);
    try {
      rule = pipeline.startPipeline(rule, logger);
    } catch (ParsingException | IOException e) {
      logger.log(Level.SEVERE, String.format(messageBundle.getMessage(Messages.CORE_ERROR_PIPELINE_FAILED), e.getMessage()), e);
      return null;
    }
    // Return the file in which the Rule is saved by accessing the file through the Rule
    return rule.getOutputFile();

  }

}
