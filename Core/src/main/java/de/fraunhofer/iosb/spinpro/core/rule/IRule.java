package de.fraunhofer.iosb.spinpro.core.rule;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.ICorePipelineStep;

import java.io.File;
import java.io.Serializable;

/**
 * An interface that represents a rule. Rules are a central Core Object,
 * they are passed in every {@link ICorePipelineStep Pipelinestep},
 * and every class that affects the output JSON will read from and write to these {@link IRule IRules}.
 */
public interface IRule extends Serializable {
  /**
   * Returns the name of the rule.
   *
   * @return the name
   */
  String getName();

  /**
   * Sets the name.
   *
   * @param name the name
   */
  void setName(String name);

  /**
   * Returns the original input sentence.
   *
   * @return the original input
   */
  String getOriginalSentence();

  /**
   * Sets the original input sentence.
   *
   * @param originalSentence the original input sentence
   */
  void setOriginalSentence(String originalSentence);

  /**
   * Returns the ID of the rule.
   *
   * @return the ID
   */
  long getID();

  /**
   * Sets the ID.
   *
   * @param id the ID
   */
  void setID(long id);

  /**
   * Returns the version of the Core.
   *
   * @return the version
   */
  String getVersion();

  /**
   * Sets the version of the Core.
   *
   * @param version the version
   */
  void setVersion(String version);

  /**
   * Returns the language of the rule.
   *
   * @return the language
   */
  String getLang();

  /**
   * Sets the language.
   *
   * @param lang the language
   */
  void setLang(String lang);

  /**
   * Returns the domain.
   *
   * @return the domain
   */
  String getDomain();

  /**
   * Sets the domain.
   *
   * @param domain the domain
   */
  void setDomain(String domain);

  /**
   * Returns the machine for this rule.
   *
   * @return the machine
   */
  String getMachine();

  /**
   * Sets the machine for this rule.
   *
   * @param machine the machine
   */
  void setMachine(String machine);

  /**
   * Returns the name of the company.
   *
   * @return the company
   */
  String getCompany();

  /**
   * Sets the company.
   *
   * @param company the company
   */
  void setCompany(String company);

  /**
   * Returns the copyright.
   *
   * @return the copyright
   */
  String getCopyright();

  /**
   * Sets the copyright.
   *
   * @param copyright the copyright
   */
  void setCopyright(String copyright);


  /**
   * Returns the array of {@link IAction IActions} this rule contains.
   *
   * @return array of {@link IAction IActions}
   */
  IAction[] getActions();

  /**
   * Sets the array of {@link IAction IActions} this rule should contain.
   *
   * @param actions the array of {@link IAction IActions}
   */
  void setActions(IAction[] actions);

  /**
   * Returns the array of names of the plugins that were used to create this rule.
   * These are only names, no unique identifier.
   *
   * @return array of plugin names
   */
  String[] getPluginSet();

  /**
   * Sets the array of names of the plugins that were used to create this rule.
   * These are only names, no unique identifier.
   *
   * @param names the plugin names
   */
  void setPluginSet(String[] names);

  /**
   * Returns the array of hashes of the plugins that were used to create this rule.
   * These are unique, but more cryptic to the reader than the plugin names.
   *
   * @return array of plugin hashes
   */
  String[] getPluginHash();

  /**
   * Sets the array of hashes of the plugins that were used to create this rule.
   * These are unique, but more cryptic to the reader than the plugin names.
   *
   * @param hashes the plugin hashes
   */
  void setPluginHash(String[] hashes);

  /**
   * Adds an {@link IAction} to the set of {@link IAction IActions} this IRule holds.
   *
   * @param action the added {@link IAction}
   */
  void addAction(IAction action);

  /**
   * Adds {@link IAction IActions} to the set of {@link IAction IActions} this IRule holds.
   *
   * @param actions the added {@link IAction IActions}
   */
  void addActions(IAction[] actions);

  /**
   * Returns the file in which the IRule is saved.
   *
   * @return the file in which this IRule is saved
   */
  File getOutputFile();

  /**
   * Adds the file, in which the IRule is saved, to this IRule.
   *
   * @param file the file in which this IRule is saved
   */
  void setOutputFile(File file);

  /**
   * Returns the id of the person that created the rule.
   *
   * @return the id of the person that created the rule
   */
  String getCreator();

  /**
   * Sets the id of the person that created the rule.
   *
   * @param creator the id of the person that created the rule
   */
  void setCreator(String creator);
}
