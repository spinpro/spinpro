package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.actionparser.StructureParser;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.service.GenericServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IActionService;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;


/**
 * A class that selects complex services to process a complex text input structure.
 * A complex structure is defined by using beginning, ending and concatenation phrases.
 */
public class ComplexActionServiceSelector extends AbstractActionServiceSelector<IComplexActionService> {

  private final String beginPhrase;
  private final String endPhrase;

  /**
   * Creates an new ComplexActionServiceSelector using the main preferences.
   *
   * @throws ConstructionException if the {@link GenericServiceProvider} failed to load the services
   */
  public ComplexActionServiceSelector() throws ConstructionException {
    this(ConfigurationProvider.getMainPreferences());
  }

  /**
   * Creates an new ComplexActionServiceSelector using specified preferences.
   *
   * @param prefs the specified preferences
   * @throws ConstructionException if the {@link GenericServiceProvider} failed to load the services
   */
  public ComplexActionServiceSelector(IPreferences prefs) throws ConstructionException {
    super(prefs, IComplexActionService.class);
    beginPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_BEGINNING_PHRASE);
    endPhrase = prefs.getValue(PreferenceStrings.CORE_SP_CON_ENDING_PHRASE);
  }

  /**
   * Returns an {@link List} of {@link IActionService} depending on the input text.
   * Every available {@link IActionService} is examined whether it can possibly parse the text.
   * The {@link List} is ordered in a way, that the ones, which are most likely to parse the text, are at the beginning.
   *
   * @param textInput The input text
   * @return the {@link IActionService services} inside their {@link IServiceWrapper} that can possibly parse the text
   */
  @Override
  public List<IServiceWrapper<IComplexActionService>> getServices(String[] textInput) {
    // Delete nested actions from input
    StructureParser parser = new StructureParser();
    String[] stringCopy = textInput.clone();
    String[] newArray = Arrays.stream(stringCopy).map(s -> ((parser.checkWordInString(s, beginPhrase) ? beginPhrase + endPhrase : s))).toArray(String[]::new);

    // Concatenate newArray
    StringJoiner arrayJoiner = new StringJoiner(" ");
    for (String string : newArray) {
      arrayJoiner.add(string);
    }
    final String fullInput = arrayJoiner.toString();

    // Filter services, whose regex still matches
    List<IServiceWrapper<IComplexActionService>> possibleServices;
    possibleServices = services.stream()
        .filter(s -> fullInput.matches(s.getService().getRegex()))
        .collect(Collectors.toList());

    // Sort by amount of matching hotwords
    possibleServices.sort(Comparator.comparingInt(s -> -hotwordCount(s, fullInput)));

    return possibleServices;
  }

  /**
   * Calculates amount of hotwords that are contained in the given text.
   *
   * @param service which hotwords are used
   * @param text    that is searched
   * @return amount of hotwords
   */
  private int hotwordCount(IServiceWrapper<IComplexActionService> service, String text) {
    StructureParser structureParser = new StructureParser();
    return (int) Arrays.stream(service.getService().getHotwords()).filter(s -> structureParser.checkWordInString(text, s)).count();
  }
}
