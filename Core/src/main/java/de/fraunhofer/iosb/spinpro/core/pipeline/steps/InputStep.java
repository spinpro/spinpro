package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

/**
 * A input processor that formats the text in lowercase, allowing only letters, numbers and decimal numbers.
 */
public class InputStep extends AbstractCorePipelineStep {

  private static final char POINT = 44;
  private static final char COMMA = 46;
  private static final char MINUS = '-';

  public InputStep() {
    super();
  }

  /**
   * Formats the input text of the rule into a form that is accepted by the core.
   * The output will be lowercase-only. Beside unicode letters and numbers, decimal
   * numbers (separated by either dot or comma), as well as hyphens between letters, are allowed.
   *
   * @param rule   with the input text saved inside of the rule
   * @param logger used for logging errors and status information
   * @return the rule with the processed input text
   * @throws ParsingException is thrown when the Input could not be parsed (i.e. it contains chars that are not allowed).
   */
  @Override
  public IRule execute(IRule rule, ILogger logger) throws ParsingException {
    String originSentence = rule.getOriginalSentence().toLowerCase();

    originSentence = originSentence.replaceAll("[\\s]+", " ");

    int removedCharCounter = 0;

    // Iterate using a while-loop, because chars may get deleted while iterating
    int i = 0;
    while (i < originSentence.length()) {
      char actualChar = originSentence.charAt(i);
      if (!Character.isLetterOrDigit(actualChar) && !Character.isSpaceChar(actualChar)) {
        // char is no lowercase letter, or number, or space
        if (actualChar == POINT || actualChar == COMMA) {
          // Check if char is at beginning or at the end of the sentence,
          // or if it is not surrounded by numbers
          if ((i - 1) < 0 || (i + 1) > originSentence.length() - 1 || !Character.isDigit(originSentence.charAt(i - 1)) || !Character.isDigit(originSentence.charAt(i + 1))) {
            originSentence = removeChar(originSentence, i);
            removedCharCounter++;
            // Don't increase i, because the char at pos i was just deleted,
            // so now the next char to be inspected is at pos i.
            continue;
          }
        } else if (actualChar == MINUS) {
          // Check if char is at beginning or at the end of the sentence,
          // or if it is not surrounded by letters
          if ((i - 1) < 0 || (i + 1) > originSentence.length() - 1 || !Character.isLetter(originSentence.charAt(i - 1)) || !Character.isLetter(originSentence.charAt(i + 1))) {
            originSentence = removeChar(originSentence, i);
            removedCharCounter++;
            // Don't increase i, because the char at pos i was just deleted,
            // so now the next char to be inspected is at pos i.
            continue;
          }
        } else {
          if (!Character.isLetter(actualChar)) {
            throw new ParsingException(String.format(messageBundle.getMessage(Messages.CORE_ERROR_INVALID_CHAR_INPUT), actualChar));
          }
        }
      }
      i++;

    }
    logger.log(Level.FINEST, String.format(messageBundle.getMessage(Messages.CORE_LOG_DELETED_CHARS_INPUT_SENTENCE), removedCharCounter));

    rule.setOriginalSentence(originSentence);
    return rule;
  }


  private String removeChar(String input, int pos) {
    if (pos == 0) {
      return input.substring(pos + 1);
    }
    if (pos == input.length() - 1) {
      return input.substring(0, pos);
    }
    return input.substring(0, pos) + input.substring(pos + 1);
  }

}
