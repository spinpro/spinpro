package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.services.IActionParser;

/**
 * Manages the complete translation of a String to a Collection of {@link IAction}.
 */
public interface ICompleteActionParser extends IActionParser {

  /**
   * Parses a text to multiple {@link IAction}.
   * Gets the needed information from the given{@link IRule}.
   * Outputs the {@link IAction actions} and further information that where created to the given{@link IRule}.
   *
   * @param rule   that contains the parsed text and machine name
   * @param logger used for logging errors and status information
   * @throws ParsingException if the input could not be parsed completely
   */
  void parseComplete(IRule rule, ILogger logger) throws ParsingException;

}

