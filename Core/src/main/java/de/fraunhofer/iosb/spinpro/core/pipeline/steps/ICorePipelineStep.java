package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;

import java.io.IOException;

/**
 * Provides an interface for every step in the pipeline, defining a general accessing method.
 */
public interface ICorePipelineStep {
  /**
   * Defines the method each pipeline step must provide to operate on a rule.
   *
   * @param rule   the rule that will be processed by the pipeline step
   * @param logger used for logging errors and status information
   * @return the processed rule
   * @throws ParsingException if the step could not execute successfully with the given rule
   * @throws IOException      if the step could not execute successfully with the given rule
   */
  IRule execute(IRule rule, ILogger logger) throws ParsingException, IOException;
}
