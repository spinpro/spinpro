package de.fraunhofer.iosb.spinpro.core.pipeline;

import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.ICorePipelineStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.InputStep;
import de.fraunhofer.iosb.spinpro.core.pipeline.steps.SpeechConstructionStep;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

class CorePipelineTest {

  private final static TestEnvironmentSetup ENV_SETUP = new TestEnvironmentSetup();
  private IRule rule;
  private List<ICorePipelineStep> initSteps;

  @AfterEach
  public void end() throws IOException {
    ENV_SETUP.clear();
  }

  @BeforeEach
  void setUp() throws IOException {
    ENV_SETUP.init();
    ENV_SETUP.createDefaultEnvironment();
    rule = new Rule("");
    initSteps = new ArrayList<>();
    initSteps.add(new InputStep());
  }

  @Test
  public void standardPipeline() throws ConstructionException {
    ICorePipeline pipeline = new CorePipeline(initSteps);
    List<ICorePipelineStep> steps = new ArrayList<>();

    pipeline.addSteps(steps);

    try {
      rule = pipeline.startPipeline(rule, new LoggerMock());
      assertNotEquals(rule, null);
    } catch (ParsingException | IOException e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void addStep() throws ConstructionException {
    ICorePipeline pipeline = new CorePipeline(initSteps);
    int numberBefore = pipeline.getSteps().size();

    pipeline.addStep(new InputStep());
    pipeline.addStep(new SpeechConstructionStep());

    assertSame(numberBefore + 2, pipeline.getSteps().size());

    pipeline.clearSteps();
    pipeline.addStep(new InputStep());
    pipeline.addStep(new SpeechConstructionStep());

    assertSame(2, pipeline.getSteps().size());
  }

  @Test
  public void addSteps() throws ConstructionException {
    ICorePipeline pipeline = new CorePipeline(initSteps);
    int numberBefore = pipeline.getSteps().size();

    List<ICorePipelineStep> steps = new ArrayList<>();

    steps.add(new InputStep());
    steps.add(new SpeechConstructionStep());

    pipeline.addSteps(steps);
    assertSame(numberBefore + 2, pipeline.getSteps().size());

    pipeline.clearSteps();
    pipeline.addSteps(steps);
    assertSame(2, pipeline.getSteps().size());
  }
}