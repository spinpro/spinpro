package de.fraunhofer.iosb.spinpro.core.variables;

import de.fraunhofer.iosb.spinpro.variables.IVariable;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OutputEventTest {

  @Test
  void createSimpleOutputEvent() {
    String name = "Temperatur";
    IVariable variable = new OutputEvent(name);
    assertEquals(name, variable.getName());
    assertEquals(name, variable.getRepresentation());
  }

  @Test
  void createOutputEventWithAliases() {
    String alias1 = "Hitze";
    String alias2 = "Gradzahl";
    String name = "Temperatur";
    List<String> aliases = new ArrayList<>();
    aliases.add(alias1);
    aliases.add(alias2);
    IVariable variable = new OutputEvent(name, aliases);
    assertEquals(name, variable.getName());
    assertTrue(variable.getAliases().contains(alias1));
    assertTrue(variable.getAliases().contains(alias2));
  }

  @Test
  void createOutputEventWithAliasesAndDevices() {
    String alias1 = "Hitze";
    String alias2 = "Gradzahl";
    String device1 = "Stahlschneider";
    String device2 = "Kaffeeautomat";
    String name = "Temperatur";
    List<String> aliases = new ArrayList<>();
    List<String> devices = new ArrayList<>();
    aliases.add(alias1);
    aliases.add(alias2);
    devices.add(device1);
    devices.add(device2);
    IVariable variable = new OutputEvent(name, aliases, devices);
    assertEquals(name, variable.getName());
    assertTrue(variable.getAliases().contains(alias1));
    assertTrue(variable.getAliases().contains(alias2));
    assertTrue(variable.getDevices().contains(device1));
    assertTrue(variable.getDevices().contains(device2));
  }

}