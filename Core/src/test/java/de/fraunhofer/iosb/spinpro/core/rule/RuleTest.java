package de.fraunhofer.iosb.spinpro.core.rule;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.actions.WhileAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class RuleTest {

  private IRule rule;

  @BeforeEach
  void setUp() {
    rule = new Rule("");
  }

  @Test
  void createSimpleRule() {
    String originalSentence = "Wenn a > 3, dann setze b auf 5";
    rule = new Rule(originalSentence);
    assertEquals(originalSentence, rule.getOriginalSentence());
  }

  @Test
  void createRuleWithMachine() {
    String originalSentence = "Wenn a > 3, dann setze b auf 5";
    String machine = "Hochdruckwasserstrahlglasschneider";
    rule = new Rule(originalSentence, machine);
    assertEquals(originalSentence, rule.getOriginalSentence());
    assertEquals(machine, rule.getMachine());
  }

  @Test
  void createRuleWithMachineAndCreator() {
    String originalSentence = "Wenn a > 3, dann setze b auf 5";
    String machine = "Hochdruckwasserstrahlglasschneider";
    String creator = "God himself";
    rule = new Rule(originalSentence, machine, creator);
    assertEquals(originalSentence, rule.getOriginalSentence());
    assertEquals(machine, rule.getMachine());
    assertEquals(creator, rule.getCreator());
  }

  @Test
  void setOriginSentence() {
    String originalSentence = "Wenn a > 3, dann setze b auf 5";
    rule.setOriginalSentence(originalSentence);
    assertEquals(originalSentence, rule.getOriginalSentence());
  }

  @Test
  void setName() {
    String name = "someName";
    rule.setName(name);
    assertEquals(name, rule.getName());
  }

  @Test
  void setID() {
    int id = 421337;
    rule.setID(id);
    assertEquals(id, rule.getID());
  }

  @Test
  void setVersion() {
    String version = "0.42.1337";
    rule.setVersion(version);
    assertEquals(version, rule.getVersion());
  }

  @Test
  void setLang() {
    String language = "de_DE";
    rule.setLang(language);
    assertEquals(language, rule.getLang());
  }

  @Test
  void setDomain() {
    String domain = "spinProTest.com";
    rule.setDomain(domain);
    assertEquals(domain, rule.getDomain());
  }

  @Test
  void setMachine() {
    String machine = "Hochdruckwasserstrahlglasschneider";
    rule.setMachine(machine);
    assertEquals(machine, rule.getMachine());
  }

  @Test
  void setCompany() {
    String company = "SpinPro GmbH - the best company in the world";
    rule.setCompany(company);
    assertEquals(company, rule.getCompany());
  }

  @Test
  void setCopyright() {
    String copyright = "Copyright";
    rule.setCopyright(copyright);
    assertEquals(copyright, rule.getCopyright());
  }



  @Test
  void setActions() {
    IAction action = new EventAction(null, null);
    rule.setActions(new IAction[]{action});
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 1);
    assertEquals(actions[0], action);
  }

  @Test
  void setAndAddAction() {
    IAction action = new EventAction(null, null);
    IAction actionNew = new IfAction("a > b");
    rule.setActions(new IAction[]{action});
    rule.addAction(actionNew);
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 2);
    assertEquals(actions[1], actionNew);
  }

  @Test
  void setAndAddActions() {
    IAction action = new EventAction(null, null);
    IAction actionNew = new IfAction("a > b");
    IAction actionNewer = new WhileAction("a < b", null);
    rule.setActions(new IAction[]{action});
    rule.addActions(new IAction[]{actionNew, actionNewer});
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 3);
    assertEquals(actions[1], actionNew);
    assertEquals(actions[2], actionNewer);
  }

  @Test
  void addAction() {
    IAction actionNew = new IfAction("a > b");
    rule.addAction(actionNew);
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 1);
    assertEquals(actions[0], actionNew);
  }

  @Test
  void addActions() {
    IAction actionNew = new IfAction("a > b");
    IAction actionNewer = new WhileAction("a < b", null);
    rule.addActions(new IAction[]{actionNew, actionNewer});
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 2);
    assertEquals(actions[0], actionNew);
    assertEquals(actions[1], actionNewer);
  }

  @Test
  void addNullAction() {
    assertSame(rule.getActions(), null);
    rule.addAction(null);
    assertSame(rule.getActions(), null);
    IAction actionNew = new IfAction("a > b");
    IAction actionNewer = new WhileAction("a < b", null);
    rule.setActions(new IAction[]{actionNew, actionNewer});
    assertSame(rule.getActions().length, 2);
    rule.addAction(null);
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 2);
    assertEquals(actions[0], actionNew);
  }

  @Test
  void addNullActions() {
    assertSame(rule.getActions(), null);
    rule.addActions(null);
    assertSame(rule.getActions(), null);
    IAction actionNew = new IfAction("a > b");
    IAction actionNewer = new WhileAction("a < b", null);
    rule.setActions(new IAction[]{actionNew, actionNewer});
    assertSame(rule.getActions().length, 2);
    rule.addActions(null);
    IAction[] actions = rule.getActions();
    assertSame(actions.length, 2);
    assertEquals(actions[0], actionNew);
    assertEquals(actions[1], actionNewer);
  }

  @Test
  void setPluginSet() {
    String name1 = "Simple If/Else Plugin";
    String name2 = "complex event pattern plugin";
    rule.setPluginSet(new String[]{name1, name2});
    String[] strings = rule.getPluginSet();
    assertSame(strings.length, 2);
    assertEquals(strings[0], name1);
    assertEquals(strings[1], name2);
  }

  @Test
  void setPluginHash() {
    String name1 = "2873814232348";
    String name2 = "3458534238487468346";
    rule.setPluginHash(new String[]{name1, name2});
    String[] strings = rule.getPluginHash();
    assertSame(strings.length, 2);
    assertEquals(strings[0], name1);
    assertEquals(strings[1], name2);
  }

  @Test
  void setOutputFile() {
    File output = new File("somePath");
    rule.setOutputFile(output);
    assertEquals(output, rule.getOutputFile());
  }

  @Test
  void setCreator() {
    String creator = "God himself";
    rule.setCreator(creator);
    assertEquals(creator, rule.getCreator());
  }
}