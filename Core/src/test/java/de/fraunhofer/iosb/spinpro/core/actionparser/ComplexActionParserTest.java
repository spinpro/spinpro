package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComplexActionParserTest {

  private static final TestEnvironmentSetup ENV = new TestEnvironmentSetup();
  private ComplexActionParser parser;

  @BeforeAll
  static void init() throws IOException {
    ENV.init();
    ENV.createDefaultEnvironment();
  }

  @AfterAll
  static void end() throws IOException {
    ENV.clear();
  }

  @BeforeEach
  void setUp() throws ConstructionException {
    parser = new ComplexActionParser();
  }

  @Test
  public void parseTestAttempts() throws ConstructionException {
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    String attempts = prefs.getValue(PreferenceStrings.MAX_ATTEMPT_COUNT_COMPLEX);
    prefs.setValue(PreferenceStrings.MAX_ATTEMPT_COUNT_COMPLEX, "0"); // 0 attempts
    ComplexActionParser parserAttempt = new ComplexActionParser(); // load new parser with different attempt amount
    prefs.setValue(PreferenceStrings.MAX_ATTEMPT_COUNT_COMPLEX, attempts); // reset to old value

    assertThrows(ParsingException.class, () -> parserAttempt.parse("begin do check furthermore begin do begin do begin furthermore do begin stop end end end end end"));
  }

  @Test
  public void parseTestNoServices1() {
    assertThrows(ParsingException.class, () -> parser.parse("begin do check furthermore begin do begin do begin furthermore do begin nothing end end end end end"));
  }

  @Test
  public void parseTestNoServices2() {
    assertThrows(ParsingException.class, () -> parser.parse(""));
  }

  @Test
  public void parseTest1() throws ParsingException {
    Collection<IAction> actions = parser.parse("begin do check furthermore begin do begin do begin furthermore do begin stop end end end end end");
    assertNotNull(actions);

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'furthermore do begin stop end'
    IfAction[] ifAction1 = new IfAction[1];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("furthermore do begin stop end");
    // 'do begin furthermore do begin stop end end'
    IfAction[] ifAction2 = new IfAction[1];
    ifAction2[0] = new IfAction("true");
    ifAction2[0].setThenActions(ifAction1);
    ifAction2[0].setOriginalString("do begin furthermore do begin stop end end");
    // 'do begin do begin furthermore do begin stop end end end'
    IfAction[] ifAction3 = new IfAction[1];
    ifAction3[0] = new IfAction("true");
    ifAction3[0].setThenActions(ifAction2);
    ifAction3[0].setOriginalString("do begin do begin furthermore do begin stop end end end");
    // 'do check furthermore begin do begin do begin furthermore do begin stop end end end end'
    IfAction[] ifAction4 = new IfAction[1];
    ifAction4[0] = new IfAction("true");
    ifAction4[0].setThenActions(ifAction3);
    ifAction4[0].setOriginalString("do check furthermore begin do begin do begin furthermore do begin stop end end end end");

    assertEquals(Arrays.asList(ifAction4), actions);
  }

  @Test
  public void parseTest2() throws ParsingException {
    Collection<IAction> actions = parser.parse("begin do begin stop end also furthermore do begin stop end end");
    assertNotNull(actions);

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'stop'
    EventAction[] eventAction2 = new EventAction[1];
    eventAction2[0] = new EventAction("out", "valuable");
    eventAction2[0].setOriginalString("stop");
    // 'do begin stop end' and 'furthermore do begin stop end'
    IfAction[] ifAction1 = new IfAction[2];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("do begin stop end");
    ifAction1[1] = new IfAction("true");
    ifAction1[1].setThenActions(eventAction2);
    ifAction1[1].setOriginalString("furthermore do begin stop end");

    assertEquals(Arrays.asList(ifAction1), actions);
  }

  @Test
  public void parseCompleteTest() throws ParsingException {
    IRule rule = new Rule("do begin furthermore do begin stop end end", "robot", "I");
    parser.parseComplete(rule, new LoggerMock());

    assertNotNull(rule.getActions());

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'furthermore do begin stop end'
    IfAction[] ifAction1 = new IfAction[1];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("furthermore do begin stop end");
    // 'do begin furthermore do begin stop end end'
    IfAction[] ifAction2 = new IfAction[1];
    ifAction2[0] = new IfAction("true");
    ifAction2[0].setThenActions(ifAction1);
    ifAction2[0].setOriginalString("do begin furthermore do begin stop end end");

    assertArrayEquals(ifAction2, rule.getActions());
    assertEquals("do begin furthermore do begin stop end end", rule.getOriginalSentence());
    assertEquals("robot", rule.getMachine());
    assertEquals("I", rule.getCreator());
    assertArrayEquals(new String[]{"ComplexTestService1", "ComplexTestService2", "ComplexTestService3"}, rule.getPluginSet());
    assertEquals(3, rule.getPluginHash().length);
  }

  @Test
  public void parseCompleteTestInvalid() {
    IRule rule = new Rule("do furthermore do stop", "robot", "I");
    assertThrows(IllegalArgumentException.class, () -> parser.parseComplete(rule, new LoggerMock()));
  }

  @Test
  public void isComplexTestTrue() {
    assertTrue(ComplexActionParser.isComplex("do begin stop end also do check furthermore begin stop end"));
  }

  @Test
  public void isComplexTestFalse() {
    assertFalse(ComplexActionParser.isComplex("do check furthermore stop"));
  }
}
