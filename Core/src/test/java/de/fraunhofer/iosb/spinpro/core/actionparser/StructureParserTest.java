package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StructureParserTest {


  private final static TestEnvironmentSetup ENV_SETUP = new TestEnvironmentSetup();

  @BeforeEach
  public void init() throws IOException {
    ENV_SETUP.init();
    ENV_SETUP.createDefaultEnvironment();
  }

  @AfterEach
  public void end() throws IOException {
    ENV_SETUP.clear();
  }


  @Test
  void findSectionByOuterBrackets() {
    IStructureParser parser = new StructureParser();
    String section1 = "Wenn a > b dann begin hallo123 begin a = 4 end begin while c>3 begin do nothing end c = 7 end finally end";
    String section2 = "do begin something end while lalala";
    String section3 = "set b to 3 Test Test";
    String originSection = section1 + " also " + section2 + " also " + section3;
    try {
      List<String> sections = parser.findToplevelSections(originSection, "begin", "end", "also");
      assertEquals(sections.get(0), section1);
      assertEquals(sections.get(1), section2);
      assertEquals(sections.get(2), section3);
    } catch (ParsingException e) {
      e.printStackTrace();
    }
  }

  @Test
  void noOuterConcat() {
    IStructureParser parser = new StructureParser();
    String section1 = "Wenn a > b dann begin hallo123 also a = 4 end";
    String originSection = section1;
    try {
      List<String> sections = parser.findToplevelSections(originSection, "begin", "end", "also");
      assertEquals(sections.get(0), section1);
    } catch (ParsingException e) {
      e.printStackTrace();
    }
  }


  @Test
  void findSectionEndBeforeStart() {
    IStructureParser parser = new StructureParser();
    String originSection = "begin hallo123 begin a = 4 end begin while c>3 begin do nothing end c = 7 end finally end";
    String sentence = "Wenn a > b dann end " + originSection + " Test Test";
    assertThrows(ParsingException.class, () -> {
      parser.findToplevelSections(sentence, "begin", "end", "also");
    });
  }


  @Test
  void findSectionWrongAmountOfEnds() {
    IStructureParser parser = new StructureParser();
    String originSection = "begin hallo123 begin a = 4 end begin while c>3 begin do nothing end c = 7 end finally";
    String sentence = "Wenn a > b dann " + originSection + " Test Test";
    assertThrows(ParsingException.class, () -> {
      parser.findToplevelSections(sentence, "begin", "end", "also");
    });
  }

  @Test
  void findSectionNoStartOrEndContained() {
    IStructureParser parser = new StructureParser();
    String sentenceWithBegin = "Wenn a > b dann begin Test Test";
    String sentenceWithEnd = "Wenn a > b dann Test Test end";
    assertThrows(ParsingException.class, () -> {
      parser.findToplevelSections(sentenceWithBegin, "begin", "end", "also");
    });
    assertThrows(ParsingException.class, () -> {
      parser.findToplevelSections(sentenceWithEnd, "begin", "end", "also");
    });
  }

  @Test
  void findSectionEndQualsStart() {
    IStructureParser parser = new StructureParser();
    String sentenceWithBegin = "Wenn a > b dann begin Test Test";
    assertThrows(IllegalArgumentException.class, () -> {
      parser.findToplevelSections(sentenceWithBegin, "begin", "begin", "also");
    });
  }

  @Test
  void cutOutSection() {
    IStructureParser parser = new StructureParser();
    List<String> originStrings = new ArrayList<>();
    originStrings.add("Wenn a > b dann");
    originStrings.add("begin hallo123 begin a = 4 end begin while c>3 begin do nothing end c = 7 end finally end");
    originStrings.add("also do");
    originStrings.add("begin something end");
    originStrings.add("while lalala also set b to 3 Test Test");
    StringBuilder originSection = new StringBuilder();
    for (String sentence : originStrings) {
      originSection.append(sentence).append(" ");
    }
    originSection = new StringBuilder(originSection.toString().trim());
    String[] returningList;
    try {
      returningList = parser.splitSections(originSection.toString(), "begin", "end");
      for (int i = 0; i < originStrings.size(); i++) {
        assertEquals(originStrings.get(i), returningList[i]);
      }
    } catch (ParsingException e) {
      e.printStackTrace();
    }
  }

  @Test
  void oneNestedSection() {
    IStructureParser parser = new StructureParser();
    String originSection = "begin Wenn a > b dann end";
    String[] returningList;
    try {
      returningList = parser.splitSections(originSection, "begin", "end");
      assertEquals(returningList.length, 1);
      assertEquals(originSection, returningList[0]);
    } catch (ParsingException e) {
      e.printStackTrace();
    }
  }

  @Test
  void endBeforeBegin() {
    IStructureParser parser = new StructureParser();
    String originSection = "end Wenn a > b dann begin";
    assertThrows(ParsingException.class, () -> parser.splitSections(originSection, "begin", "end"));
  }

  @Test
  void anotherEndBeforeBegin() {
    IStructureParser parser = new StructureParser();
    String originSection = "begin do 123 end end Wenn a > b dann begin";
    assertThrows(ParsingException.class, () -> parser.splitSections(originSection, "begin", "end"));
  }
}