package de.fraunhofer.iosb.spinpro.core;

import de.fraunhofer.iosb.spinpro.configuration.IPreferenceStrings;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;

import java.util.HashMap;
import java.util.Map;

public class PreferencesMock implements IPreferences {

  private Map<IPreferenceStrings, String> preferenceMap = new HashMap<>();

  @Override
  public String getValue(IPreferenceStrings key) {
    return (preferenceMap.containsKey(key)) ? preferenceMap.get(key) : key.getDefault();
  }

  @Override
  public void setValue(IPreferenceStrings key, String value) {
    preferenceMap.put(key, value);
  }

}
