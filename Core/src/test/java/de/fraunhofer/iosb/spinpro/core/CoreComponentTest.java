package de.fraunhofer.iosb.spinpro.core;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CoreComponentTest {

  private final static TestEnvironmentSetup ENV_SETUP = new TestEnvironmentSetup();

  @BeforeEach
  public void init() throws IOException {
    ENV_SETUP.init();
  }

  @AfterEach
  public void end() throws IOException {
    ENV_SETUP.clear();
  }

  @Test
  public void constructorTest() throws IOException {
    Properties props = new Properties();
    props.setProperty(PreferenceStrings.SERVICE_DIR.toString(), "src/test/resources/invalid_path_to_services");
    ENV_SETUP.createPreferences(props);
    assertThrows(ConstructionException.class, CoreComponent::new);
  }

  @Test
  public void createRuleTestParsingException() throws ConstructionException, IOException {
    ENV_SETUP.createDefaultEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "do furthermore do do nothing";
    String machine = "Bunsenbrenner";
    String creator = "Chemiker1";
    assertNull(coreComponent.createRule(input, machine, creator, logger));
  }

  @Test
  public void createRuleTestIOException() throws ConstructionException, IOException {
    ENV_SETUP.createDefaultEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "do furthermore do do stop";
    String machine = null;
    String creator = null;
    assertNull(coreComponent.createRule(input, machine, creator, logger));
  }

  @Test
  void createRuleTestSimple1() throws ConstructionException, IOException {
    ENV_SETUP.createDefaultEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "do furthermore do do stop";
    String machine = "Stahlschneider";
    String creator = "God himself";
    File output = coreComponent.createRule(input, machine, creator, logger);
    assertNotEquals(output, null);
    String content = Files.readString(output.toPath());
    content = content.replaceAll("\\n", "");
    content = content.replaceAll("\\s+", " ");
    assertTrue(content.matches("\\{" +
        "\"name\":\".*\"," +
        "\"originalSentence\":\"do furthermore do do stop\"," +
        "\"id\":[0-9]*," +
        "\"creator\":\"God himself\"," +
        "\"domain\":\"Domain\"," +
        "\"machine\":\"Stahlschneider\"," +
        "\"company\":\"Company\"," +
        "\"copyright\":\"\"," +
        "\"actions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"out\"," +
        "\"value\":\"valuable\"" +
        "}]" +
        "}]" +
        "}]" +
        "}]," +
        "\"pluginSet\":\\[\"SimpleTestService1\",\"SimpleTestService2\",\"SimpleTestService3\"]," +
        "\"pluginHash\":\\[\"8a3c58e4512bebea263ac39bc068bd217a67d95cc663e29b61eeea6f5677c176\",\"975de389c4554e02b9c206e72d4e0c83875464db7b00e671be0059f792c93405\",\"d062cc3d832cafda50ee3a78840016f4576c607eba8aab250868ac5402f59bea\"" +
        "]}"));
  }

  @Test
  void createRuleTestComplex1() throws ConstructionException, IOException {
    ENV_SETUP.createDefaultEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "do begin stop end also furthermore do begin do begin stop end end";
    String machine = "Stahlschneider";
    String creator = "God himself";
    File output = coreComponent.createRule(input, machine, creator, logger);
    assertNotEquals(output, null);
    String content = Files.readString(output.toPath());
    content = content.replaceAll("\\n", "");
    content = content.replaceAll("\\s+", " ");
    assertTrue(content.matches("\\{" +
        "\"name\":\".*\"," +
        "\"originalSentence\":\"do begin stop end also furthermore do begin do begin stop end end\"," +
        "\"id\":[0-9]*," +
        "\"creator\":\"God himself\"," +
        "\"domain\":\"Domain\"," +
        "\"machine\":\"Stahlschneider\"," +
        "\"company\":\"Company\"," +
        "\"copyright\":\"\"," +
        "\"actions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"out\"," +
        "\"value\":\"valuable\"" +
        "}]" +
        "},\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"true\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"out\"," +
        "\"value\":\"valuable\"" +
        "}]" +
        "}]" +
        "}]," +
        "\"pluginSet\":\\[\"ComplexTestService1\",\"ComplexTestService2\",\"ComplexTestService3\"]," +
        "\"pluginHash\":\\[\"1cf2c9d087433cda8399dda5480535882b6330c9a3fc12e256c3b614b2d34069\",\"87253a080ced5425ccd6c5d5a138e6753ba03b291ed608243fc43c9094a9573d\",\"dcba787f17cfd81d7ed58f0b2f7667736a9906a43f11a7c9d0476df51b18c528\"" +
        "]}"));
  }

  @Test
  void createRuleTestSimple2() throws ConstructionException, IOException {
    ENV_SETUP.createAdvancedEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "if b is greater than five do set b to five";
    String machine = "Stahlschneider";
    String creator = "God himself";
    File output = coreComponent.createRule(input, machine, creator, logger);
    assertNotEquals(output, null);
    String content = Files.readString(output.toPath());
    content = content.replaceAll("\\n", "");
    content = content.replaceAll("\\s+", " ");
    assertTrue(content.matches("\\{" +
        "\"name\":\".*\"," +
        "\"originalSentence\":\"if b is greater than five do set b to five\"," +
        "\"id\":[0-9]*," +
        "\"creator\":\"God himself\"," +
        "\"domain\":\"Domain\"," +
        "\"machine\":\"Stahlschneider\"," +
        "\"company\":\"Company\"," +
        "\"copyright\":\"\"," +
        "\"actions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"\\(B\\s\\\\u003e\\s5\\.0\\)\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"B\"," +
        "\"value\":\"5.0\"" +
        "}]" +
        "}]," +
        "\"pluginSet\":\\[\"EventActionService - EN\",\"IfServiceS - EN\"]," +
        "\"pluginHash\":\\[\"4bdcb120b7100e9107d748845ed37a8e39b6f9ed9cbf9f974bb4c9ac70d86138\",\"9df952d019acee02b11f63b2c6dc8bf3ae38ce821ff047767311d0f4695b0a58\"" +
        "]}"));
  }

  @Test
  void createRuleTestSimple3() throws ConstructionException, IOException {
    ENV_SETUP.createAdvancedEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "if setting is less than four times heat do set warmth to five modulo forty-two";
    String machine = "toaster";
    String creator = "cuisine person";
    assertNull(coreComponent.createRule(input, machine, creator, logger));
  }

  @Test
  void createRuleTestComplex2() throws ConstructionException, IOException {
    ENV_SETUP.createAdvancedEnvironment();
    ILogger logger = new LoggerMock();
    ICoreComponent coreComponent = new CoreComponent();
    String input = "if degrees equals b then begin set temperature to minus two-hundred-seventy-four end else begin set temperature to one end also print degrees";
    String machine = "grill";
    String creator = "someone";
    File output = coreComponent.createRule(input, machine, creator, logger);
    assertNotEquals(output, null);
    String content = Files.readString(output.toPath());
    content = content.replaceAll("\\n", "");
    content = content.replaceAll("\\s+", " ");
    assertTrue(content.matches("\\{" +
        "\"name\":\".*\"," +
        "\"originalSentence\":\"if degrees equals b then begin set temperature to minus two-hundred-seventy-four end else begin set temperature to one end also print degrees\"," +
        "\"id\":[0-9]*," +
        "\"creator\":\"someone\"," +
        "\"domain\":\"Domain\"," +
        "\"machine\":\"grill\"," +
        "\"company\":\"Company\"," +
        "\"copyright\":\"\"," +
        "\"actions\":\\[" +
        "\\{" +
        "\"type\":\"if\"," +
        "\"condition\":\"\\(warmth \\\\u003d\\\\u003d B\\)\"," +
        "\"thenActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"temperature\"," +
        "\"value\":\"\\(-274.0\\)\"" +
        "}]," +
        "\"elseActions\":\\[" +
        "\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"temperature\"," +
        "\"value\":\"1.0\"" +
        "}]" +
        "},\\{" +
        "\"type\":\"event\"," +
        "\"output\":\"stdout\"," +
        "\"value\":\"warmth\"" +
        "}]," +
        "\"pluginSet\":\\[\"EventActionService - EN\",\"IfElseServiceC - EN\",\"PrintService - EN\"]," +
        "\"pluginHash\":\\[\"22297a8417b220c9cfbe84ae9c18dec6da2f6d4e59a9ee84f9aaa798b22a05a5\",\"9df952d019acee02b11f63b2c6dc8bf3ae38ce821ff047767311d0f4695b0a58\",\"ad3b7850f4e545296fe0706a0dfa8f90d552bcc9d2575322a29b2509a5168eaa\"" +
        "]}"));
  }
}