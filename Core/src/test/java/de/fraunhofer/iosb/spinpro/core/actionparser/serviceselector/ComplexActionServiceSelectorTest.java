package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.PreferencesMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.IComplexActionService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComplexActionServiceSelectorTest {

  private static final TestEnvironmentSetup ENV = new TestEnvironmentSetup();
  private ComplexActionServiceSelector selector;

  @BeforeAll
  static void init() throws IOException {
    ENV.init();
    ENV.createDefaultEnvironment();
  }

  @AfterAll
  static void end() throws IOException {
    ENV.clear();
  }

  @BeforeEach
  void setUp() throws ConstructionException {
    selector = new ComplexActionServiceSelector();
  }

  @Test
  public void constructorInvalid() {
    IPreferences prefs = new PreferencesMock();
    prefs.setValue(PreferenceStrings.SERVICE_DIR, "src/test/resources/invalidServices");
    assertThrows(ConstructionException.class, () -> new SimpleActionServiceSelector(prefs));
  }

  @Test
  public void getServicesTest1() {
    List<IServiceWrapper<IComplexActionService>> services;
    services = selector.getServices(new String[]{"do", "begin end", "do check furthermore", "begin do begin do begin furthermore do begin stop end end end end"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("ComplexTestService1"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f831"::equals));
  }

  @Test
  public void getServicesTest2() {
    List<IServiceWrapper<IComplexActionService>> services;
    services = selector.getServices(new String[]{"furthermore do", "begin do begin furthermore do begin stop end end end"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("ComplexTestService2"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f832"::equals));
  }

  @Test
  public void getServicesTest3() {
    List<IServiceWrapper<IComplexActionService>> services;
    services = selector.getServices(new String[]{"stop"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("ComplexTestService3"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f835"::equals));
  }

  @Test
  public void getServicesTest4() {
    List<IServiceWrapper<IComplexActionService>> services;
    services = selector.getServices(new String[]{"do check furthermore", "begin do do furthermore do stop end", "do", "begin stop end"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("ComplexTestService4"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34faf0"::equals));
  }
}
