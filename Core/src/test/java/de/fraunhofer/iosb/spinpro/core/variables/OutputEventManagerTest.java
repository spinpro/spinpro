package de.fraunhofer.iosb.spinpro.core.variables;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.variables.IVariable;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class OutputEventManagerTest {
  private final static TestEnvironmentSetup ENV_SETUP = new TestEnvironmentSetup();
  private static OutputEventManager manager;

  private Variable a;
  private Variable b;
  private Variable c;

  @BeforeEach
  public void init() throws IOException {
    ENV_SETUP.init();
    ENV_SETUP.createDefaultEnvironment();
    manager = new OutputEventManager();

    Collection<String> devicesA = new ArrayList<>();
    Collection<String> aliasesA = new ArrayList<>();
    devicesA.add("toaster");
    devicesA.add("grill");
    aliasesA.add("heat");
    aliasesA.add("degrees");
    a = new OutputEvent("temperature", aliasesA, devicesA);

    Collection<String> aliasesB = new ArrayList<>();
    aliasesB.add("b");
    aliasesB.add("letterB");
    b = new OutputEvent("B", aliasesB);

    Collection<String> devicesC = new ArrayList<>();
    Collection<String> aliasesC = new ArrayList<>();
    devicesC.add("print");
    devicesC.add("printer");
    aliasesC.add("c");
    aliasesC.add("smallC");
    c = new OutputEvent("smallC", aliasesC, devicesC);

  }

  @AfterEach
  public void end() throws IOException {
    ENV_SETUP.clear();
  }

  @Test
  public void containsVariableTest() {
    // Check available aliases when all devices are allowed
    manager.setDevice(null);
    assertTrue(manager.containsVariable("heat"));
    assertTrue(manager.containsVariable("degrees"));
    assertTrue(manager.containsVariable("letterB"));
    assertTrue(manager.containsVariable("b"));
    assertFalse(manager.containsVariable("ABC")); // Does not exist
    // Check available aliases when only the device toaster is allowed
    manager.setDevice("toaster");
    assertTrue(manager.containsVariable("heat"));
    assertTrue(manager.containsVariable("degrees"));
    assertTrue(manager.containsVariable("letterB"));
    assertFalse(manager.containsVariable("ABC")); // Does not exist
    assertFalse(manager.containsVariable("c"));
  }

  @Test
  public void getVariableTest() {
    IVariable var;
    // Get only variables which support the device toaster
    manager.setDevice("toaster");
    // Check if variable was returned correctly
    var = manager.getVariable("degrees");
    assertEquals(var, a);

    // Get variables when all devices are allowed
    manager.setDevice(null);
    // Check if variable was returned correctly
    var = manager.getVariable("b");
    assertEquals(b, var);
    // Get only variables which support the device grill
    manager.setDevice("grill");
    var = manager.getVariable("letterB");
    assertEquals(b, var);
    // c does not support the device grill
    assertNull(manager.getVariable("c"));
  }

  @Test
  public void folderDoesntExists() {
    manager = new OutputEventManager("thisFolderDoesntExists");
    assertEquals(0, manager.getVariables().size());
  }

  @Test
  public void getVariablesTest() {
    manager.setDevice("toaster");
    Collection<IVariable> variables = manager.getVariables();
    assertTrue(variables.contains(a));
    assertTrue(variables.contains(b));
    assertEquals(variables.size(), 2);

    manager.setDevice("print");
    variables = manager.getVariables();
    assertTrue(variables.contains(c));
    assertTrue(variables.contains(b));
    assertEquals(variables.size(), 2);

    manager.setDevice("");
    variables = manager.getVariables();
    assertTrue(variables.contains(b));
    assertEquals(variables.size(), 1);

    manager.setDevice(null);
    variables = manager.getVariables();
    assertTrue(variables.contains(a));
    assertTrue(variables.contains(b));
    assertTrue(variables.contains(c));
    assertEquals(variables.size(), 3);
  }

  @Test
  public void getVariablesInStringTest() {
    Iterator<String> iterator = a.getAliases().iterator();
    iterator.next();
    String testA = iterator.next();
    String testB = b.getAliases().iterator().next();
    List<Pair<String, IVariable>> varInString = manager.getVariablesInString("This is a test with " + testA + " and " + testB);
    assertEquals(varInString.size(), 2);
    if (varInString.get(0).getLeft().equals(testA)) {
      assertTrue(varInString.get(0).getLeft().equals(testA) && varInString.get(0).getRight().equals(a));
      assertTrue(varInString.get(1).getLeft().equals(testB) && varInString.get(1).getRight().equals(b));
    } else if (varInString.get(0).getLeft().equals(testB)) {
      assertTrue(varInString.get(0).getLeft().equals(testB) && varInString.get(0).getRight().equals(b));
      assertTrue(varInString.get(1).getLeft().equals(testA) && varInString.get(1).getRight().equals(a));
    } else {
      fail();
    }
  }

}