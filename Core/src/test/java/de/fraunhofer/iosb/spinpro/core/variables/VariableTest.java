package de.fraunhofer.iosb.spinpro.core.variables;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class VariableTest {

  @Test
  public void testEqualsMethod(){
    Collection<String> devices = new ArrayList<>();
    Collection<String> aliases = new ArrayList<>();
    devices.add("Computer");
    devices.add("Toaster");
    aliases.add("Heat");
    aliases.add("Degrees");
    Collection<String> devices2 = new ArrayList<>();
    Collection<String> aliases2 = new ArrayList<>();
    devices2.add("Computer");
    devices2.add("Toaster");
    aliases2.add("Heat");
    aliases2.add("Degrees");
    Variable var = new InputVariable("Temperature", aliases, devices);
    Variable var2 = new InputVariable("Temperature", aliases2, devices2);
    assertEquals(var, var2);
  }

}