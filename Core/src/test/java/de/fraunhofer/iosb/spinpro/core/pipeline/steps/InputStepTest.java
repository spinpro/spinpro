package de.fraunhofer.iosb.spinpro.core.pipeline.steps;


import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

class InputStepTest {
  private final static TestEnvironmentSetup ENV_SETUP = new TestEnvironmentSetup();
  private ICorePipelineStep step;
  private IRule rule;

  @BeforeEach
  public void init() throws IOException {
    ENV_SETUP.init();
    ENV_SETUP.createDefaultEnvironment();
    step = new InputStep();
    rule = null;
  }

  @AfterEach
  public void end() throws IOException {
    ENV_SETUP.clear();
  }


  @Test
  void executeUpperLowerLetters() {
    String input = "Hello this is onLy a teSt";
    String formatted = "hello this is only a test";
    rule = new Rule(input);
    String output = "";
    try {
      output = step.execute(rule, new LoggerMock()).getOriginalSentence();
    } catch (ParsingException | IOException e) {
      e.printStackTrace();
    }
    assertEquals(output, formatted);
  }

  @Test
  void executeCommaSurroundedWithNumbers() {
    String input = "Sinnvolle VerWendUnG eines Kommas 3,4 bzw Punktes 7.3";
    String formatted = "sinnvolle verwendung eines kommas 3,4 bzw punktes 7.3";
    rule = new Rule(input);
    String output = "";
    try {
      output = step.execute(rule, new LoggerMock()).getOriginalSentence();
    } catch (ParsingException | IOException e) {
      e.printStackTrace();
    }
    assertEquals(output, formatted);
  }

  @Test
  void executeCommaNotSurroundedWithNumbers() {
    String input = ".,.KeI.,nE Sinnvolle ,.VerWendUnG ..eines Kommas bzw punktes..";
    String formatted = "keine sinnvolle verwendung eines kommas bzw punktes";
    String output = "";
    rule = new Rule(input);
    try {
      output = step.execute(rule, new LoggerMock()).getOriginalSentence();

    } catch (ParsingException | IOException e) {
      e.printStackTrace();
    }
    assertEquals(output, formatted);
  }

  @Test
  void executeInvalidChar() {
    String input = "/";
    rule = new Rule(input);
    assertThrows(ParsingException.class, () -> step.execute(rule, new LoggerMock()));
  }

  @Test
  void checkForUnusualLetters() {
    String input = "ü";
    rule = new Rule(input);
    assertEquals(rule.getOriginalSentence(), input);
  }

  @Test
  void removeRedundandSpaces() {
    String input = "Hallo   das ist ein     Test   123  ";
    String wanted = "hallo das ist ein test 123 ";
    rule = new Rule(input);
    try {
      rule = step.execute(rule, new LoggerMock());
      assertEquals(wanted, rule.getOriginalSentence());
    } catch (ParsingException | IOException e) {
      fail();
    }
  }


}