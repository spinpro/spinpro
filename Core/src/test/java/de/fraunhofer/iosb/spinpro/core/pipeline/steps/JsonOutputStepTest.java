package de.fraunhofer.iosb.spinpro.core.pipeline.steps;


import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class JsonOutputStepTest {

  private static final String TEST_PATH = "RuleOutputTest";
  private ICorePipelineStep step;
  private IRule rule;
  private Path outputTestDir;
  private TestEnvironmentSetup initiator;

  @AfterEach
  void tearDown() throws IOException {
    initiator.clear();
    if (outputTestDir != null) {
      outputTestDir.toFile().delete();
    }
  }

  @BeforeEach
  void setUp() throws IOException {
    initiator = new TestEnvironmentSetup();
    initiator.init();

    if (outputTestDir != null) {
      outputTestDir.toFile().delete();
    }
    outputTestDir = Files.createTempDirectory(TEST_PATH);

    rule = new Rule("");
    step = new JsonOutputStep(outputTestDir.toFile());
  }

  @Test
  void saveSimpleRule() {
    rule.setName("SimpleRule");
    rule.setID((int) System.currentTimeMillis());
    rule.addAction(new IfAction("a > b", new IAction[]{new EventAction("10", "Width")}, null));
    File finalRuleFile = new File(outputTestDir + "/" + rule.getName() + "_" + rule.getID() + ".json");
    if (finalRuleFile.exists()) {
      if (!finalRuleFile.delete()) {
        System.err.println("Couln't delete File");
        return;
      }
    }
    try {
      step.execute(rule, new LoggerMock());
    } catch (ParsingException | IOException e) {
      e.printStackTrace();
    }
    assertTrue(finalRuleFile.exists());
    assertEquals(finalRuleFile, rule.getOutputFile());
  }

  @Test
  void saveRuleWithoutName() {
    rule.addAction(new IfAction("a > b", new IAction[]{new EventAction("10", "Width")}, null));
    File finalRuleFile = new File(outputTestDir + "/" + rule.getName() + "_" + rule.getID() + ".JSON");
    if (finalRuleFile.exists()) {
      if (!finalRuleFile.delete()) {
        System.err.println("Couln't delete File");
        return;
      }
    }
    assertThrows(IOException.class, () -> step.execute(rule, new LoggerMock()));
  }

  @Test
  void saveRuleTwice() {
    rule.setID(123);
    rule.setName("someName");
    rule.addAction(new IfAction("a > b", new IAction[]{new EventAction("10", "Width")}, null));
    File finalRuleFile = new File(outputTestDir + "/" + rule.getName() + "_" + rule.getID() + ".json");
    if (finalRuleFile.exists()) {
      if (!finalRuleFile.delete()) {
        System.err.println("Couln't delete File");
        return;
      }
    }
    assertThrows(IOException.class, () -> {
      step.execute(rule, new LoggerMock());
      step.execute(rule, new LoggerMock());
    });
  }

  @Test
  void saveInvalidFolder() {
    step = new JsonOutputStep(new File("//\\/"));

    assertThrows(IOException.class, () -> step.execute(rule, new LoggerMock()));
  }

  @Test
  void saveDefaultFolder() throws IOException {

    // -- remove old initialisation and init without valid values
    Path tempDir = Files.createTempDirectory("coreComponentTest");
    File config = new File(tempDir.toFile(), "test.properties");
    if (!config.createNewFile()) {
      fail();
    }
    ConfigurationProvider.init(config.getAbsolutePath());
    // --

    step = new JsonOutputStep();

    rule.setName("SimpleRule");
    rule.setCreator("God");
    rule.setID((int) System.currentTimeMillis());

    rule.addAction(new IfAction("a > b", new IAction[]{new EventAction("10", "Width")}, null));

    try {
      step.execute(rule, new LoggerMock());
    } catch (ParsingException | IOException e) {
      e.printStackTrace();
    }
    File finalRuleFile = rule.getOutputFile();
    assertTrue(finalRuleFile.exists());
    assertEquals(PreferenceStrings.RULE_OUTPUT_DIR.getDefault().replaceAll("\\\\", "/"), rule.getOutputFile().getPath().replaceAll("(?:.(?!([\\\\/])))+$", "").replaceAll("\\\\", "/"));
  }
}