package de.fraunhofer.iosb.spinpro.core.pipeline.steps;

import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GeneralInformationStepTest {

  private static final TestEnvironmentSetup ENV = new TestEnvironmentSetup();

  @BeforeAll
  static void setUp() throws IOException {
    ENV.init();
  }

  @Test
  public void standardValuesTest() throws IOException {
    String company = "Google";
    String domain = "google.com";
    String copyright = "abc";
    Properties props = new Properties();
    props.setProperty(PreferenceStrings.RULE_COMPANY.toString(), "Google");
    props.setProperty(PreferenceStrings.RULE_DOMAIN.toString(), "google.com");
    props.setProperty(PreferenceStrings.RULE_COPYRIGHT.toString(), "abc");
    ENV.createPreferences(props);
    GeneralInformationStep step = new GeneralInformationStep();
    IRule rule = new Rule("");
    rule = step.execute(rule, new LoggerMock());
    assertEquals(rule.getCompany(), company);
    assertEquals(rule.getCopyright(), copyright);
    assertEquals(rule.getDomain(), domain);
  }
}