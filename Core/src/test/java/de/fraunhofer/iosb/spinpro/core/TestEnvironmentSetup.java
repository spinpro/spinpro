package de.fraunhofer.iosb.spinpro.core;

import com.google.gson.Gson;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.variables.InputVariable;
import de.fraunhofer.iosb.spinpro.core.variables.OutputEvent;
import de.fraunhofer.iosb.spinpro.variables.IVariable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.Properties;

public class TestEnvironmentSetup {

  private static final String TEMP_NAME = "coreComponentTest";
  private static final String RESULT_DIR = "result";
  private static final String LOGGING_DIR = "logs";
  private static final String VARIABLE_DIR = "input_variables";
  private static final String EVENT_DIR = "output_event";
  private static final String TEST_PROPERTIES = "test.properties";
  private Path tempDir;

  public void init() throws IOException {
    clear();
    tempDir = Files.createTempDirectory(TEMP_NAME);
    if (!new File(tempDir.toFile(), LOGGING_DIR).mkdirs()) {
      throw new FileAlreadyExistsException(LOGGING_DIR + " already exists");
    }
    if (!new File(tempDir.toFile(), RESULT_DIR).mkdirs()) {
      throw new FileAlreadyExistsException(RESULT_DIR + " already exists");
    }
    if (!new File(tempDir.toFile(), VARIABLE_DIR).mkdirs()) {
      throw new FileAlreadyExistsException(VARIABLE_DIR + " already exists");
    }
    if (!new File(tempDir.toFile(), EVENT_DIR).mkdirs()) {
      throw new FileAlreadyExistsException(EVENT_DIR + " already exists");
    }
  }

  public void clear() throws IOException {
    if ((tempDir != null) && (Files.exists(tempDir))) {
      Files.walk(tempDir)
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .forEach(File::delete);
    }
  }

  public void createVariables(IVariable var, String path) throws IOException {
    File baseDirectory = new File(path);
    if (!baseDirectory.exists()) {
      if (!baseDirectory.mkdirs()) {
        throw new IOException("Could not create test directory");
      }
    }

    Gson gson = new Gson();
    FileWriter writer;
    writer = new FileWriter(path + "/" + var.getName() + ".json");
    gson.toJson(var, writer);
    writer.flush();
    writer.close();
  }

  public void createPreferences(Properties properties) throws IOException {
    File config = new File(tempDir.toAbsolutePath() + "/" + TEST_PROPERTIES);
    if (!config.createNewFile()) {
      throw new FileAlreadyExistsException(tempDir.toAbsolutePath() + "/" + TEST_PROPERTIES + " already exists");
    }
    FileOutputStream outputStream = new FileOutputStream(config);
    properties.store(outputStream, null);
    outputStream.flush();
    outputStream.close();
    ConfigurationProvider.init(config.getAbsolutePath());
  }

  public void createDefaultEnvironment() throws IOException {
    Properties props = new Properties();
    props.setProperty(PreferenceStrings.LOG_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + LOGGING_DIR);
    props.setProperty(PreferenceStrings.CONSOLE_LOGGING.toString(), "true");
    props.setProperty(PreferenceStrings.RULE_DOMAIN.toString(), "Domain");
    props.setProperty(PreferenceStrings.RULE_COMPANY.toString(), "Company");
    props.setProperty(PreferenceStrings.RULE_OUTPUT_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + RESULT_DIR);
    props.setProperty(PreferenceStrings.INPUT_VARIABLE_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + VARIABLE_DIR);
    props.setProperty(PreferenceStrings.OUTPUT_EVENT_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + EVENT_DIR);
    props.setProperty(PreferenceStrings.SERVICE_DIR.toString(), "src/test/resources/services/mock_services");
    props.setProperty(PreferenceStrings.LOCALE.toString(), Locale.US.toString());
    createPreferences(props);

    IVariable aInput = new InputVariable("temperature", Arrays.asList("heat", "degrees"), Arrays.asList("toaster", "grill"));
    createVariables(aInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable aOutput = new OutputEvent("temperature", Arrays.asList("heat", "degrees"), Arrays.asList("toaster", "grill"));
    createVariables(aOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
    IVariable bInput = new InputVariable("B", Arrays.asList("letterB", "b"));
    createVariables(bInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable bOutput = new OutputEvent("B", Arrays.asList("letterB", "b"));
    createVariables(bOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
    IVariable cInput = new InputVariable("smallC", Arrays.asList("c", "smallC"), Arrays.asList("printer", "print"));
    createVariables(cInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable cOutput = new OutputEvent("smallC", Arrays.asList("c", "smallC"), Arrays.asList("printer", "print"));
    createVariables(cOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
  }

  public void createAdvancedEnvironment() throws IOException {
    Properties props = new Properties();
    props.setProperty(PreferenceStrings.LOG_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + LOGGING_DIR);
    props.setProperty(PreferenceStrings.CONSOLE_LOGGING.toString(), "true");
    props.setProperty(PreferenceStrings.RULE_DOMAIN.toString(), "Domain");
    props.setProperty(PreferenceStrings.RULE_COMPANY.toString(), "Company");
    props.setProperty(PreferenceStrings.RULE_OUTPUT_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + RESULT_DIR);
    props.setProperty(PreferenceStrings.INPUT_VARIABLE_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + VARIABLE_DIR);
    props.setProperty(PreferenceStrings.OUTPUT_EVENT_DIR.toString(), tempDir.toFile().getAbsolutePath() + "/" + EVENT_DIR);
    props.setProperty(PreferenceStrings.SERVICE_DIR.toString(), "src/test/resources/services");
    props.setProperty(PreferenceStrings.LOCALE.toString(), Locale.US.toString());
    createPreferences(props);

    IVariable aInput = new InputVariable("warmth", Arrays.asList("heat", "degrees"), Arrays.asList("toaster", "grill"));
    createVariables(aInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable aOutput = new OutputEvent("temperature", Arrays.asList("temperature", "celsius"), Arrays.asList("thermometer", "grill"));
    createVariables(aOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
    IVariable bInput = new InputVariable("B", Arrays.asList("letterB", "b"));
    createVariables(bInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable bOutput = new OutputEvent("B", Arrays.asList("letterB", "b"));
    createVariables(bOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
    IVariable cInput = new InputVariable("smallC", Arrays.asList("c", "smallC"), Arrays.asList("printer", "print"));
    createVariables(cInput, tempDir.toAbsolutePath() + "/" + VARIABLE_DIR);
    IVariable cOutput = new OutputEvent("mode", Arrays.asList("mode", "setting"), Arrays.asList("microwave", "oven"));
    createVariables(cOutput, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
    IVariable stdout = new OutputEvent("stdout", Arrays.asList("stdout", "STDOUT"));
    createVariables(stdout, tempDir.toAbsolutePath() + "/" + EVENT_DIR);
  }
}
