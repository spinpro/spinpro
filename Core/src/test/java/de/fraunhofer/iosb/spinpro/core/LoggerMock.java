package de.fraunhofer.iosb.spinpro.core;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class LoggerMock implements ILogger {

  List<String> input = new ArrayList<>();

  @Override
  public void log(Level ll, String s) {
    System.err.println(s);
    input.add(s);
  }

  @Override
  public void log(Level ll, Messages s) {
    System.err.println(s);
    input.add(s.toString());
  }

  @Override
  public void log(Level ll, String message, Exception e) {
    System.err.println(message);
    input.add(message);
  }

  @Override
  public void log(Level ll, Messages message, Exception e) {
    System.err.println(message);
    input.add(message.toString());
  }

  public String[] getInput() {
    return input.toArray(new String[0]);
  }
}