package de.fraunhofer.iosb.spinpro.core.actionparser;

import de.fraunhofer.iosb.spinpro.actions.EventAction;
import de.fraunhofer.iosb.spinpro.actions.IAction;
import de.fraunhofer.iosb.spinpro.actions.IfAction;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.LoggerMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.core.rule.IRule;
import de.fraunhofer.iosb.spinpro.core.rule.Rule;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleActionParserTest {

  private static final TestEnvironmentSetup ENV = new TestEnvironmentSetup();
  private SimpleActionParser parser;

  @BeforeAll
  static void init() throws IOException {
    ENV.init();
    ENV.createDefaultEnvironment();
  }

  @AfterAll
  static void end() throws IOException {
    ENV.clear();
  }

  @BeforeEach
  void setUp() throws ConstructionException {
    parser = new SimpleActionParser();
  }

  @Test
  public void parseTestAttempts() throws ConstructionException {
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    String attempts = prefs.getValue(PreferenceStrings.MAX_ATTEMPT_COUNT_SIMPLE);
    prefs.setValue(PreferenceStrings.MAX_ATTEMPT_COUNT_SIMPLE, "1"); // only 1 attempt
    SimpleActionParser parserAttempt = new SimpleActionParser(); // load new parser with different attempt amount
    prefs.setValue(PreferenceStrings.MAX_ATTEMPT_COUNT_SIMPLE, attempts); // reset to old value

    assertThrows(ParsingException.class, () -> parserAttempt.parse("do check furthermore do do furthermore do stop"));
  }

  @Test
  public void parseTestNoServices1() {
    assertThrows(ParsingException.class, () -> parser.parse("do check furthermore do do furthermore do nothing"));
  }

  @Test
  public void parseTestNoServices2() {
    assertThrows(ParsingException.class, () -> parser.parse(""));
  }

  @Test
  public void parseTest1() throws ParsingException {
    Collection<IAction> actions = parser.parse("do check furthermore do do furthermore do stop");
    assertNotNull(actions);

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'furthermore do stop'
    IfAction[] ifAction1 = new IfAction[1];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("furthermore do stop");
    // 'do furthermore do stop'
    IfAction[] ifAction2 = new IfAction[1];
    ifAction2[0] = new IfAction("true");
    ifAction2[0].setThenActions(ifAction1);
    ifAction2[0].setOriginalString("do furthermore do stop");
    // 'do do furthermore do stop'
    IfAction[] ifAction3 = new IfAction[1];
    ifAction3[0] = new IfAction("true");
    ifAction3[0].setThenActions(ifAction2);
    ifAction3[0].setOriginalString("do do furthermore do stop");
    // 'do check furthermore do do furthermore do stop'
    IfAction[] ifAction4 = new IfAction[1];
    ifAction4[0] = new IfAction("true");
    ifAction4[0].setThenActions(ifAction3);
    ifAction4[0].setOriginalString("do check furthermore do do furthermore do stop");

    assertEquals(Arrays.asList(ifAction4), actions);
  }

  @Test
  public void parseTest2() throws ParsingException {
    Collection<IAction> actions = parser.parse("do furthermore do do stop");
    assertNotNull(actions);

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'furthermore do stop'
    IfAction[] ifAction1 = new IfAction[1];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("do stop");
    // 'do furthermore do stop'
    IfAction[] ifAction2 = new IfAction[1];
    ifAction2[0] = new IfAction("true");
    ifAction2[0].setThenActions(ifAction1);
    ifAction2[0].setOriginalString("furthermore do do stop");
    // 'do do furthermore do stop'
    IfAction[] ifAction3 = new IfAction[1];
    ifAction3[0] = new IfAction("true");
    ifAction3[0].setThenActions(ifAction2);
    ifAction3[0].setOriginalString("do furthermore do do stop");

    assertEquals(Arrays.asList(ifAction3), actions);
  }

  @Test
  public void parseCompleteTest() throws ParsingException {
    IRule rule = new Rule("do furthermore do stop", "robot", "I");
    parser.parseComplete(rule, new LoggerMock());

    assertNotNull(rule.getActions());

    // 'stop'
    EventAction[] eventAction1 = new EventAction[1];
    eventAction1[0] = new EventAction("out", "valuable");
    eventAction1[0].setOriginalString("stop");
    // 'furthermore do stop'
    IfAction[] ifAction1 = new IfAction[1];
    ifAction1[0] = new IfAction("true");
    ifAction1[0].setThenActions(eventAction1);
    ifAction1[0].setOriginalString("furthermore do stop");
    // 'do furthermore do stop'
    IfAction[] ifAction2 = new IfAction[1];
    ifAction2[0] = new IfAction("true");
    ifAction2[0].setThenActions(ifAction1);
    ifAction2[0].setOriginalString("do furthermore do stop");

    assertArrayEquals(ifAction2, rule.getActions());
    assertEquals("do furthermore do stop", rule.getOriginalSentence());
    assertEquals("robot", rule.getMachine());
    assertEquals("I", rule.getCreator());
    assertArrayEquals(new String[]{"SimpleTestService1", "SimpleTestService2", "SimpleTestService3"}, rule.getPluginSet());
    assertEquals(3, rule.getPluginHash().length);
  }
}
