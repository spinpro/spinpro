package de.fraunhofer.iosb.spinpro.core.actionparser.serviceselector;

import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.core.PreferencesMock;
import de.fraunhofer.iosb.spinpro.core.TestEnvironmentSetup;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.services.ISimpleActionService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimpleActionServiceSelectorTest {

  private static final TestEnvironmentSetup ENV = new TestEnvironmentSetup();
  private SimpleActionServiceSelector selector;

  @BeforeAll
  static void init() throws IOException {
    ENV.init();
    ENV.createDefaultEnvironment();
  }

  @AfterAll
  static void end() throws IOException {
    ENV.clear();
  }

  @BeforeEach
  void setUp() throws ConstructionException {
    selector = new SimpleActionServiceSelector();
  }

  @Test
  public void constructorTestInvalid() {
    IPreferences prefs = new PreferencesMock();
    prefs.setValue(PreferenceStrings.SERVICE_DIR, "src/test/resources/invalidServices");
    assertThrows(ConstructionException.class, () -> new SimpleActionServiceSelector(prefs));
  }

  @Test
  public void getServicesTest1() {
    List<IServiceWrapper<ISimpleActionService>> services;
    services = selector.getServices(new String[]{"do do check furthermore do do furthermore do stop"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("SimpleTestService1"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f833"::equals));
  }

  @Test
  public void getServicesTest2() {
    List<IServiceWrapper<ISimpleActionService>> services;
    services = selector.getServices(new String[]{"furthermore do do furthermore do stop"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("SimpleTestService2"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f834"::equals));
  }

  @Test
  public void getServicesTest3() {
    List<IServiceWrapper<ISimpleActionService>> services;
    services = selector.getServices(new String[]{"stop"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("SimpleTestService3"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f836"::equals));
  }

  @Test
  public void getServicesTest4() {
    List<IServiceWrapper<ISimpleActionService>> services;
    services = selector.getServices(new String[]{"do check furthermore do do furthermore do stop"});
    assertTrue(services.stream().map(IServiceWrapper::getName).anyMatch("SimpleTestService4"::equals));
    assertTrue(services.stream().map(IServiceWrapper::getCrypticId).anyMatch("b6653151f1aaa60bc4fbd7b202c0965e0ed002fc243aa4a5c520d4338b34f842"::equals));
  }
}
