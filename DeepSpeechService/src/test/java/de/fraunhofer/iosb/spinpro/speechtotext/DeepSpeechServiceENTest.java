package de.fraunhofer.iosb.spinpro.speechtotext;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.junit.jupiter.api.Test;

class DeepSpeechServiceENTest {
  static DeepSpeechServiceEN deepSpeech;

  @Test
  void speechToText() throws ServiceInitializingException {
    deepSpeech = new DeepSpeechServiceEN();
    String out = null;
    try {
      AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel.wav"));
      out = deepSpeech.speechToText(testWav);

    } catch (UnsupportedAudioFormat | IOException | UnsupportedAudioFileException e) {
      e.printStackTrace();
      fail();
    }

    String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
                  "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";

    assertEquals("it almost looks as though the wheel is weightless how does this word well instead of pulling a wheel down to the ground as you expect the weight of the wheel creates a torch which pushes it around in a circle you may recognize " +
                 "this as gyroscopic procession for a more detailed explanation clicked the annotation or the lincoln the description to see my video on the topic here", out);

  }

  @Test
  void speechToTextMultithreaded() throws ServiceInitializingException, InterruptedException {
    deepSpeech = new DeepSpeechServiceEN();


    Runnable a = new Runnable() {
      @Override
      public void run() {
        String out = null;
        try {
          AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel.wav"));
          out = deepSpeech.speechToText(testWav);

        } catch (UnsupportedAudioFormat | IOException | UnsupportedAudioFileException | ServiceInitializingException e) {
          e.printStackTrace();
          fail();
        }
        String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
                      "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";

        assertEquals("it almost looks as though the wheel is weightless how does this word well instead of pulling a wheel down to the ground as you expect the weight of the wheel creates a torch which pushes it around in a circle you may recognize " +
                     "this as gyroscopic procession for a more detailed explanation clicked the annotation or the lincoln the description to see my video on the topic here", out);
      }
    };

    Thread[] b = new Thread[8];
    for (Thread c:b) {
      c = new Thread(a);
      c.start();
    }

    for (Thread c:b) {
      c.join();
    }

  }
}