package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 * Plugin Class for a speech-to-text plugin. ConfigurationProvider has to be in
 */
@Service(crypticId = "3cd645636fbffb53a28f4ade2b0a5dd1e8a53dfc2a1781fbb21c05ba1a219133", name = "DeepSpeechServiceEN", author = "Jochen Mueller", organisation = "Fraunhofer IOSB", version = "0.1")
public class DeepSpeechServiceEN implements ISpeechToTextService {
  private static final String CONFIGPATH = "DeepSpeechService.properties";
  private final String scriptPath;
  private final Path tempPath;
  private final String modelPath;
  private final String virtualEnvironmentPath;
  private final File basePath;
  private Properties prop;

  public DeepSpeechServiceEN() throws ServiceInitializingException {
    try {
      basePath = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
    } catch (URISyntaxException e) {
      throw new ServiceInitializingException("Failed to load base dir.");
    }
    prop = new Properties();
    try {
      InputStream fis = this.getClass().getResourceAsStream(CONFIGPATH);
      prop.load(fis);
    } catch (IOException e) {
      e.printStackTrace();
      throw new ServiceInitializingException("Failed to load speech-to-text plugin.", e);
    }

    //making Paths absolut to load them as usual cause they are inside an external jar file

    File tempModelPath = new File(prop.getProperty("modelPath", "deepspeech/model"));
    File tempVirtualEnvironmentPath = new File(prop.getProperty("virtualEnvironmentPath", "deepspeech/virtual"));
    File tempScriptPath = new File(prop.getProperty("scriptPath", "deepspeech/shell.sh"));

    tempModelPath = tempScriptPath.isAbsolute() ? tempScriptPath : new File(basePath, prop.getProperty("modelPath", "deepspeech/model"));
    tempVirtualEnvironmentPath = tempVirtualEnvironmentPath.isAbsolute() ? tempVirtualEnvironmentPath : new File(basePath, prop.getProperty("virtualEnvironmentPath", "deepspeech/virtual"));
    tempScriptPath = tempScriptPath.isAbsolute() ? tempScriptPath : new File(basePath, prop.getProperty("scriptPath", "deepspeech/shell.sh"));

    if (!tempModelPath.exists()) {
      if (!tempModelPath.mkdir()) {
        throw new ServiceInitializingException("Could not find model dir (" + tempModelPath.getAbsolutePath() + ").");
      }
    }
    if (!tempVirtualEnvironmentPath.exists()) {
      if (!tempVirtualEnvironmentPath.mkdir()) {
        throw new ServiceInitializingException("Could not find virtual environment dir.");
      }
    }
    if (!tempScriptPath.exists()) {
      if (!tempScriptPath.mkdir()) {
        throw new ServiceInitializingException("Could not find script.");
      }
    }

    modelPath = tempModelPath.getAbsolutePath();
    virtualEnvironmentPath = tempVirtualEnvironmentPath.getAbsolutePath();
    scriptPath = tempScriptPath.getAbsolutePath();
    String environmentCount = prop.getProperty("environmentCount", "1");


    try {
      tempPath = Files.createTempDirectory("deepspeechTemp");
    } catch (IOException e) {
      throw new ServiceInitializingException("Could not initialize temporary directory.");
    }
    if (prop.getProperty("init", "false").equals("true")) {
      List<String> commands = new LinkedList<>();
      commands.add(scriptPath);
      commands.add("-sv");
      commands.add(virtualEnvironmentPath);
      commands.add("-t");
      commands.add(tempPath.toAbsolutePath().toString());
      commands.add("-m");
      commands.add(modelPath);
      commands.add("-n");
      commands.add(environmentCount);
      if (prop.getProperty("downloadENModel", "false").equals("true")) {
        commands.add("-d");
      }

      ProcessBuilder pb = new ProcessBuilder(commands);
      Process p = null;
      try {
        p = pb.start();
      } catch (IOException e) {
        throw new ServiceInitializingException("Could not start process (" + Arrays.toString(pb.command().toArray()) + ").");
      }
      try {
        p.waitFor(10, TimeUnit.MINUTES);
      } catch (InterruptedException e) {
      }
      p.destroy();
      if (p.exitValue() != 0) {
        throw new ServiceInitializingException("Process returned exit value " + p.exitValue() + ".");
      }
    }
  }

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat, ServiceInitializingException {
    File audio;
    try {
      audio = File.createTempFile("audio" + Thread.currentThread().getId(), "wav");
    } catch (IOException e) {
      audio = new File(tempPath.toAbsolutePath().toString() + "/audio" + Thread.currentThread().getId() + ".wav");
    }
    try {
      AudioSystem.write(ais, AudioFileFormat.Type.WAVE, audio);
    } catch (IOException e) {
      throw new ServiceInitializingException("Could not write audio file.");
    }
    ProcessBuilder pb = new ProcessBuilder(scriptPath, "-rv", virtualEnvironmentPath, "-m", modelPath, "-i", "0", "-a", audio.getAbsolutePath());
    final Process p;
    try {
      p = pb.start();
    } catch (IOException e) {
      throw new ServiceInitializingException("Could not start process.");
    }
    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
    StringBuilder out = new StringBuilder();
    try {
      p.waitFor(10, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      p.destroy();
    }
    String s;
    while (true) {
      try {
        if ((s = br.readLine()) == null) {
          break;
        }
      } catch (IOException e) {
        throw new ServiceInitializingException("Could not read input of process.");
      }
      out.append(s);
    }
    p.destroy();
    return out.toString();
  }

  @Override
  public AudioFormat[] getSupportedAudioFormats() {
    return new AudioFormat[] {new AudioFormat(16000, 16, 1, false, false)};
  }
}
