package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;
import de.fraunhofer.iosb.spinpro.main.IJobProvider;
import de.fraunhofer.iosb.spinpro.main.MockCommunicator;
import de.fraunhofer.iosb.spinpro.main.TimeOutMaker;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class JobProviderTest {
    static private String path = "./tmpPrefPipelineStepsTest";
    static private File pref;
    private Thread t;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
    }

    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testFailure() {
        IClientCommunicator cc = new MockCommunicator();
        IJobProvider jp = new JobProvider(cc);
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "v5h57j4");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE, "sddddfs");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "asdfgrthzjuikl");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "7867i565zthhj");
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        AudioInputStream ais = null;
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        assertFalse(jp.generateAudioJob(ais, machine, clientId));
        assertFalse(jp.generateTextJob(input, machine, clientId));
    }

    @Test
    public void testAudioSuccess() {
        IClientCommunicator cc = new MockCommunicator();
        IJobProvider jp = new JobProvider(cc);
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE, "1");
        try {
            PipelineSteps.getAudioStep().terminate();
        } catch (InstantiationException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        AudioInputStream ais = null;
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        assertTrue(jp.generateAudioJob(ais, machine, clientId));
        assertFalse(jp.generateAudioJob(ais, machine, clientId));
        AudioJob j = null;
        cancelTimeouts();
        t = new Thread(new TimeOutMaker(Thread.currentThread()));
        t.start();
        try {
            j = PipelineSteps.getAudioStep().getNextJob();
        } catch (InterruptedException | InstantiationException e) {
            fail();
        }
        cancelTimeouts();
        assertNotNull(j);
        assertEquals(j.getAudio(), ais);
        assertEquals(j.getClientCommunicator(), cc);
        assertEquals(j.getClientID(), clientId);
        assertNotNull(j.getLogger());
        assertEquals(j.getMachine(), machine);
    }

    @Test
    public void testTextSuccess() {
        IClientCommunicator cc = new MockCommunicator();
        IJobProvider jp = new JobProvider(cc);
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        try {
            PipelineSteps.getTextStep().terminate();
        } catch (InstantiationException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        assertTrue(jp.generateTextJob(input, machine, clientId));
        assertFalse(jp.generateTextJob(input, machine, clientId));
        TextJob j = null;
        cancelTimeouts();
        t = new Thread(new TimeOutMaker(Thread.currentThread()));
        t.start();
        try {
            j = PipelineSteps.getTextStep().getNextJob();
        } catch (InterruptedException | InstantiationException e) {
            fail();
        }
        cancelTimeouts();
        assertNotNull(j);
        assertEquals(j.getText(), input);
        assertEquals(j.getClientCommunicator(), cc);
        assertEquals(j.getClientID(), clientId);
        assertNotNull(j.getLogger());
        assertEquals(j.getMachine(), machine);
    }

    @AfterEach
    public void cancelTimeouts() {
        if (t != null) {
            t.interrupt();
        }
    }
}
