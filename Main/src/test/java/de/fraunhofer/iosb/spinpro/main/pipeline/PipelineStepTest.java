package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.main.TimeOutMaker;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockFunctionBlock;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockFunctionError;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockFunctionNull;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockFunctionSelf;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockFunctionSlowInterrupt;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockJob;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class PipelineStepTest {

    static private String path = "./tmpPrefPipelineStepTest";
    static private File pref;
    private TimeOutMaker t;
    private PipelineStep<MockJob> ps = null;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
    }

    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testInitializationError() {
        try {
            ps = new PipelineStep<MockJob>(0, true, 10, new MockFunctionSelf());
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.PIPELINE_STEP_ERROR_ZERO_CAPACITY));
        }
        try {
            ps = new PipelineStep<MockJob>(10, true, 0, new MockFunctionSelf());
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.PIPELINE_STEP_ERROR_ZERO_THREADS));
        }
    }

    @Test
    public void testQueue() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(2, true, 1, new MockFunctionBlock());
        } catch (InstantiationException e) {
            fail();
        }

        // stopping the pipeline worker
        ps.terminate();

        // checking queue empty
        makeTimeout();
        try {
            ps.getNextJob();
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }

        // adding jobs
        MockJob j1 = new MockJob();
        assertTrue(ps.tryAddJob(j1));
        MockJob j2 = new MockJob();
        makeTimeout();
        try {
            ps.addJob(j2);
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }

        // failing to add jobs
        MockJob j3 = new MockJob();
        assertFalse(ps.tryAddJob(j3));
        MockJob j4 = new MockJob();
        makeTimeout();
        try {
            ps.addJob(j4);
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }

        // getting jobs
        IPipableJob o1 = null;
        IPipableJob o2 = null;
        makeTimeout();
        try {
            o1 = ps.getNextJob();
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }
        makeTimeout();
        try {
            o2 = ps.getNextJob();
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }
        assertTrue((o1 == j1 && o2 == j2) || (o1 == j2 && o2 == j1));

        // failing to get job
        makeTimeout();
        try {
            ps.getNextJob();
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testProgress() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(10, true, 1, new MockFunctionSelf());
        } catch (InstantiationException e) {
            fail();
        }

        // adding jobs
        makeTimeout();
        MockJob j = new MockJob();
        try {
            for (int i = 0; i < 10; i++) {
                ps.addJob(j);
            }
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }

        // waiting for jobs to complete
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            fail();
        }

        // checking that jobs have been completed
        makeTimeout();
        try {
            ps.getNextJob();
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }
        assertEquals(10, j.getEnqueueCalls());
    }


    @Test
    public void testFunctionError() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(10, true, 1, new MockFunctionError());
        } catch (InstantiationException e) {
            fail();
        }

        // adding job
        makeTimeout();
        MockJob j = new MockJob();
        try {
            ps.addJob(j);
            ps.addJob(j);
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }

        // waiting for job to complete
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            fail();
        }

        // checking that function has made an error (jobs didn't get reenqueued), and that it was ignored (both jobs completed)
        makeTimeout();
        try {
            ps.getNextJob();
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }
        assertEquals(0, j.getEnqueueCalls());
    }

    @Test
    public void testInterruptStep() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(10, true, 1, new MockFunctionSlowInterrupt());
        } catch (InstantiationException e) {
            fail();
        }

        // adding job
        makeTimeout();
        try {
            ps.addJob(new MockJob());
        } catch (InterruptedException e) {
            fail();
        }

        // interrupting step
        makeTimeout(3);
        ps.terminate();
    }

    @Test
    public void testInterruptWorker() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(10, true, 1, new MockFunctionSlowInterrupt());
        } catch (InstantiationException e) {
            fail();
        }

        // adding job
        makeTimeout();
        try {
            ps.addJob(new MockJob());
        } catch (InterruptedException e) {
            fail();
        }

        // interrupting worker
        cancelTimeout();
        ps.terminate();
    }

    @Test
    public void testNulloutput() {
        // initializing step
        try {
            ps = new PipelineStep<MockJob>(10, true, 1, new MockFunctionNull());
        } catch (InstantiationException e) {
            fail();
        }


        // adding job
        makeTimeout();
        try {
            ps.addJob(new MockJob());
        } catch (InterruptedException e) {
            fail();
        }
        cancelTimeout();

        // waiting for job to complete
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            fail();
        }
    }

    @AfterEach
    public void cancelTimeout() {
        if (t != null) {
            t.cancel();
        }
    }

    private void makeTimeout(int seconds) {
        cancelTimeout();
        t = new TimeOutMaker(Thread.currentThread(), seconds);
    }

    private void makeTimeout() {
        makeTimeout(1);
    }
}

