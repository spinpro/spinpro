package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.main.pipeline.IPipableJob;
import de.fraunhofer.iosb.spinpro.main.pipeline.IPipelineFunction;

public class MockFunctionSelf implements IPipelineFunction<MockJob, IPipableJob> {
    @Override
    public IPipableJob apply(MockJob mockJob) throws InterruptedException {
        return mockJob;
    }
}
