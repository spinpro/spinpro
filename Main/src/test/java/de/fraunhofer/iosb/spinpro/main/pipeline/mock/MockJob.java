package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.main.pipeline.IPipableJob;

public class MockJob implements IPipableJob {

    private int enqueueCalls = 0;
    private int tryEnqueueCalls = 0;

    @Override
    public void enqueue() {
        enqueueCalls++;
    }

    @Override
    public boolean tryEnqueue() {
        tryEnqueueCalls++;
        return false;
    }

    public int getEnqueueCalls() {
        return enqueueCalls;
    }

    public int getTryEnqueueCalls() {
        return tryEnqueueCalls;
    }
}
