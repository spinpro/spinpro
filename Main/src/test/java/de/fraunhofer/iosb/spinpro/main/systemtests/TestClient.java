package de.fraunhofer.iosb.spinpro.main.systemtests;

import de.fraunhofer.iosb.spinpro.main.communication.MessageHandler;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.IPart;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class TestClient extends WebSocketClient {
  private Lock lock = new ReentrantLock();
  private boolean blocked = false;
  private Condition available  = lock.newCondition();
  private Condition possibleToSend  = lock.newCondition();
  private DataObject data = new DataObject();
  private MessageHandler messageHandler = new TestMessageHandler(lock, available, data);

  public TestClient(URI serverUri) {
    super(serverUri);
  }


  @Override
  public abstract void onOpen(ServerHandshake handshakedata);
  @Override
  public void onMessage(String message) {
    //IGNORE
  }
  @Override
  public abstract void onClose(int code, String reason, boolean remote);
  @Override
  public abstract void onError(Exception ex);

  @Override
  public void onMessage(ByteBuffer bytes) {
    messageHandler.onMessage(bytes);
  }

  public IPart send(IPart part) {
    lock.lock();
    try {
      while (blocked) {
        possibleToSend.await();
      }
    } catch (InterruptedException e) {
      lock.unlock();
      return null;
    }
    blocked = true;
    send(part.getBytes());
    try {
      available.await();
    } catch (InterruptedException e) {
      lock.unlock();
      return null;
    }
    blocked = false;
    possibleToSend.signal();
    lock.unlock();
    return data.destroyData();
  }

  public void sendVoid(IPart part) {
    send(part.getBytes());
  }

  class DataObject {
    private IPart data;

    public IPart destroyData() {
      IPart d = data;
      data = null;
      return d;
    }

    public void setData(IPart data) {
      this.data = data;
    }
  }
}
