package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockCommunicator;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockLogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class ApproveFunctionTest {
    static private ApproveFunction apvf;
    static private String path = "./tmpPrefApproveFunctionTest";
    static private File pref;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
        apvf = new ApproveFunction();
    }


    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }


    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testSuccess() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        ApproveJob inJob = null;
        try {
            inJob = new ApproveJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = apvf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNotNull(outJob);
        assertSame(outJob.getClass(), TextJob.class);
        TextJob outJob2 = (TextJob) outJob;
        assertSame(outJob2.getClientCommunicator(), cc);
        assertEquals(outJob2.getClientID(), clientId);
        assertSame(outJob2.getLogger(), logger);
        assertEquals(outJob2.getMachine(), machine);
        assertEquals(outJob2.getText(), input);
    }

    @Test
    public void testErrorNulljob() {
        ApproveJob inJob = null;
        IPipableJob outJob = null;
        try {
            outJob = apvf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
    }

    @Test
    public void testErrorDisapproval() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(false);
        ApproveJob inJob = null;
        try {
            inJob = new ApproveJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = apvf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
    }

    @Test
    public void testErrorCreateJob() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        ApproveJob inJob = null;
        try {
            inJob = new ApproveJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        PipelineSteps.reset();
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "assdfge45zhsd");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "fbs8do39jj,.");
        IPipableJob outJob = null;
        try {
            outJob = apvf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertNull(logger.getLastException());
        assertSame(logger.getLastLevel(), Level.SEVERE);
        assertEquals(logger.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        assertEquals("fail", cc.getLastType());
        assertEquals(cc.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        assertEquals(cc.getLastCode(), -1);
    }
}
