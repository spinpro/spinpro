package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.main.pipeline.IPipableJob;
import de.fraunhofer.iosb.spinpro.main.pipeline.IPipelineFunction;

public class MockFunctionSlowInterrupt implements IPipelineFunction<MockJob, IPipableJob> {
    @Override
    public IPipableJob apply(MockJob mockJob) throws InterruptedException {
        while (!Thread.interrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }
        throw new InterruptedException();
    }
}
