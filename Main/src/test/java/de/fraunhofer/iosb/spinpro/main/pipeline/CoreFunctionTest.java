package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockCommunicator;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockLogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class CoreFunctionTest {
    static private CoreFunction cf;
    static private String path = "./tmpPrefCoreFunctionTest";
    static private File pref;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
        cf = new CoreFunction();
    }


    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testSuccess() {
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "do furthermore do do stop";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        TextJob inJob = null;
        try {
            inJob = new TextJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = cf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertSame(logger.getLastLevel(), Level.INFO);
        assertNull(logger.getLastException());
        assertNotNull(logger.getLastString());
        assertEquals("success", cc.getLastType());
        assertEquals(cc.getLastCode(), Long.MIN_VALUE);
        assertNotNull(cc.getLastString());
    }

    @Test
    public void testErrorNulljob() {
        TextJob inJob = null;
        IPipableJob outJob = null;
        try {
            outJob = cf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
    }

    @Test
    public void testErrorFailure() {
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        TextJob inJob = null;
        try {
            inJob = new TextJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = cf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertSame(logger.getLastLevel(), Level.WARNING);
        assertNull(logger.getLastException());
        assertEquals(logger.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_GETTING_RESULT));
        assertSame("fail", cc.getLastType());
        assertTrue(cc.getLastCode()<0);
        assertEquals("Error while getting result.", cc.getLastString());
    }
}
