package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.main.communication.enums.BitDepth;
import de.fraunhofer.iosb.spinpro.main.communication.enums.SampleRate;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Signed;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.ByteBuffer;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AtomicPartTest {

  @ParameterizedTest
  @CsvFileSource(resources = "/atomicPartTestInt.csv", numLinesToSkip = 1)
  public void integer(String expectedBytes, int input) throws ParsingException {
    IntAtomicPart a = new IntAtomicPart(input);
    StringBuilder s = new StringBuilder();
    byte[] bytes = a.getBytes();
    for (byte b : bytes) {
      s.append(String.format("%02X ", b));
    }
    assertEquals(expectedBytes, s.substring(0, s.length() - 1));
    a = new IntAtomicPart(ByteBuffer.wrap(bytes));
    assertEquals(input, a.getT());
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/atomicPartTestByte.csv", numLinesToSkip = 1)
  public void bytes(String expectedBytes, byte input) throws ParsingException {
    ByteAtomicPart a = new ByteAtomicPart(input);
    StringBuilder s = new StringBuilder();
    byte[] bytes = a.getBytes();
    for (byte b : bytes) {
      s.append(String.format("%02X ", b));
    }
    assertEquals(expectedBytes, s.substring(0, s.length() - 1));
    a = new ByteAtomicPart(ByteBuffer.wrap(bytes));
    assertEquals(input, a.getT());
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/atomicPartTestString.csv", numLinesToSkip = 1)
  public void string(String expectedBytes, String input) throws ParsingException {
    StringAtomicPart a = new StringAtomicPart(input);
    StringBuilder s = new StringBuilder();
    byte[] bytes = a.getBytes();
    for (byte b : bytes) {
      s.append(String.format("%02X ", b));
    }
    assertEquals(expectedBytes, s.substring(0, s.length() - 1));
    a = new StringAtomicPart(ByteBuffer.wrap(bytes));
    assertEquals(input, a.getT().trim());
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/atomicPartTestFormat.csv", numLinesToSkip = 1)
  public void format(String expectedBytes, boolean signed, int sampleRate, int bitDepth) throws ParsingException, ParseException {
    FormatAtomicPart a = new FormatAtomicPart(signed ? Signed.SIGNED : Signed.UNSIGNED, SampleRate.getFromSampleRate(sampleRate), BitDepth.getFromBitDepth(bitDepth));
    StringBuilder s = new StringBuilder();
    byte[] bytes = a.getBytes();
    for (byte b : bytes) {
      s.append(String.format("%02X ", b));
    }
    assertEquals(expectedBytes, s.substring(0, s.length() - 1));
    a = new FormatAtomicPart(ByteBuffer.wrap(bytes));
    assertEquals(signed, a.isSigned());
    assertEquals(sampleRate, a.getHz());
    assertEquals(bitDepth, a.getBitDepth());
    assertEquals(a, a.getT());
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/atomicPartTestError.csv", numLinesToSkip = 1)
  public void error(String expectedBytes, byte code, String message) throws ParsingException, ParseException {
    ErrorAtomicPart a = new ErrorAtomicPart(code, message);
    StringBuilder s = new StringBuilder();
    byte[] bytes = a.getBytes();
    for (byte b : bytes) {
      s.append(String.format("%02X ", b));
    }
    assertEquals(expectedBytes, s.substring(0, s.length() - 1));
    ByteBuffer b = ByteBuffer.wrap(bytes);
    b.get(); //remove error code.
    a = new ErrorAtomicPart(code, b);
    assertEquals(code, a.getCode());
    assertEquals(message, a.getMessage().trim());
    assertEquals(a, a.getT());
  }

  @ParameterizedTest
  @ValueSource(bytes = {0x00, 0x17})
  public void errorFormat(byte b) {
    assertThrows(ParseException.class, () -> {
      new FormatAtomicPart(ByteBuffer.allocate(1).put(b).position(0));
    });
  }

}