package de.fraunhofer.iosb.spinpro.main;

public class MockCommunicator implements IClientCommunicator {

  @Override
  public boolean approve(String sentence) {
    return false;
  }

  @Override
  public void sendLog(String message, int number) {

  }

  @Override
  public void sendStatus(String message, int number) {

  }

  @Override
  public void fail(byte errorNumber, String message) {

  }

  @Override
  public void success(String rule) {

  }
}
