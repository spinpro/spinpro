package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;

public class MockCommunicator implements IClientCommunicator {
    private boolean approval;
    private String lastType;
    private long lastCode = Long.MAX_VALUE;
    private String lastString;

    public MockCommunicator(boolean approval) {
        this.approval = approval;
    }

    @Override
    public boolean approve(String sentence) {
        return approval;
    }

    @Override
    public void sendLog(String message, int number) {
        lastType = "log";
        lastString = message;
        lastCode = number;
    }

    @Override
    public void sendStatus(String message, int number) {
        lastType = "status";
        lastString = message;
        lastCode = number;
    }

    @Override
    public void fail(byte errorNumber, String message) {
        lastType = "fail";
        lastString = message;
        lastCode = errorNumber;
    }

    @Override
    public void success(String rule) {
        lastType = "success";
        lastString = rule;
        lastCode = Long.MIN_VALUE;
    }

    public String getLastType() {
        return lastType;
    }

    public long getLastCode() {
        return lastCode;
    }

    public String getLastString() {
        return lastString;
    }
}
