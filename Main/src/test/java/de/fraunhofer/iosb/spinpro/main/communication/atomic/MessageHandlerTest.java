package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import de.fraunhofer.iosb.spinpro.exceptions.ParsingException;
import de.fraunhofer.iosb.spinpro.main.communication.MessageHandler;
import de.fraunhofer.iosb.spinpro.main.communication.enums.BitDepth;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import de.fraunhofer.iosb.spinpro.main.communication.enums.SampleRate;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Approve;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.ApproveRequest;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.AudioFrame;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.AudioRequest;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Deny;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.FailResponse;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.IPart;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.LogMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.StatusMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.SuccessResponse;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.TextRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class MessageHandlerTest {

  private static Stream<Arguments> provideForInteger() {
    return Stream.of(Arguments.of(new byte[] {0x20, 0x02, 0x00, 0x00, 0x00, 0x04, 0x03, 0x61, 0x61, 0x61, 0x61, 0x00, 0x01, 0x62, 0x62, 0x62, 0x62, 0x00, 0x09, 0x23, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00},
                                  new AudioRequest(4, "aaaa", "bbbb", FormatAtomicPart.get16bitU16khz(), 0, new byte[] {0x00})),
                     Arguments.of(new byte[] {0x41, 0x02, 0x00, 0x00, 0x00, 0x07, -128, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x00}, new FailResponse(7,
                                                                                                                                                                                                                                               (byte) 0x80,
                                                                                                                                                                                                                                   "error")),
                     Arguments.of(new byte[] {0x40, 0x02, 0x00, 0x00, 0x00, (byte) 0xea, 0x10, 0x61, 0x61, 0x61, 0x00}, new SuccessResponse(234, "aaa")),
                     Arguments.of(new byte[] {0x21, 0x02, 0x00, 0x00, 0x00, 0x04, 0x03, 0x61, 0x61, 0x61, 0x61, 0x00, 0x01, 0x62, 0x62, 0x62, 0x62, 0x00, 0x0a, 0x63, 0x63, 0x63, 0x63, 0x00}, new TextRequest(4, "aaaa", "bbbb", "cccc")),
                     Arguments.of(new byte[] {0x22, 0x02, 0x00, 0x00, 0x00, (byte) 0xea, 0x11, 0x61, 0x61, 0x61, 0x00}, new ApproveRequest(234, "aaa")),
                     Arguments.of(new byte[] {0x61, 0x02, 0x00, 0x00, 0x00, 0x04, 0x05, 0x61, 0x61, 0x61, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03}, new StatusMessage(4, "aaa", 3)),
                     Arguments.of(new byte[] {0x60, 0x05, 0x61, 0x61, 0x61, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03}, new LogMessage("aaa", 3)), Arguments.of(new byte[] {0x63, 0x02, 0x00, 0x00, 0x00, 0x00}, new Approve(0)),
                     Arguments.of(new byte[] {0x64, 0x02, 0x00, 0x00, 0x00, 0x00}, new Deny(0)));
  }

  private static Stream<Arguments> provideForEquals() {
    return Stream.of(Arguments.of(new byte[] {0x20, 0x02, 0x00, 0x00, 0x00, 0x04, 0x03, 0x61, 0x61, 0x61, 0x61, 0x00, 0x01, 0x62, 0x62, 0x62, 0x62, 0x00, 0x09, 0x23, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00},
                                  new AudioRequest(4, "aaaa", "bbbb", FormatAtomicPart.get16bitU16khz(), 0, new byte[] {0x00})),
                     Arguments.of(new byte[] {0x41, 0x02, 0x00, 0x00, 0x00, 0x07, -128, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x00}, new FailResponse(7, (byte) 0x80, "error")),
                     Arguments.of(new byte[] {0x40, 0x02, 0x00, 0x00, 0x00, (byte) 0xea, 0x10, 0x61, 0x61, 0x61, 0x00}, new SuccessResponse(234, "aaa")),
                     Arguments.of(new byte[] {0x21, 0x02, 0x00, 0x00, 0x00, 0x04, 0x03, 0x61, 0x61, 0x61, 0x61, 0x00, 0x01, 0x62, 0x62, 0x62, 0x62, 0x00, 0x0a, 0x63, 0x63, 0x63, 0x63, 0x00}, new TextRequest(4, "aaaa", "bbbb", "cccc")),
                     Arguments.of(new byte[] {0x03}, new ByteAtomicPart((byte) 3)), Arguments.of(new byte[] {0x07}, new IntAtomicPart(7)), Arguments.of(new byte[] {0x62, 0x00}, new StringAtomicPart("a")),
                     Arguments.of(new byte[] {0x23}, FormatAtomicPart.get16bitU16khz()), Arguments.of(new byte[] {(byte) 0x80, 0x62, 0x00}, new ErrorAtomicPart((byte) 0x80, "a")),
                     Arguments.of(new byte[] {0x22, 0x02, 0x00, 0x00, 0x00, (byte) 0xea, 0x11, 0x61, 0x61, 0x61, 0x00}, new ApproveRequest(234, "aaa")),
                     Arguments.of(new byte[] {0x61, 0x02, 0x00, 0x00, 0x00, 0x04, 0x05, 0x61, 0x61, 0x61, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03}, new StatusMessage(4, "aaa", 3)),
                     Arguments.of(new byte[] {0x60, 0x05, 0x61, 0x61, 0x61, 0x00, 0x06, 0x00, 0x00, 0x00, 0x03}, new LogMessage("aaa", 3)), Arguments.of(new byte[] {0x63, 0x02, 0x00, 0x00, 0x00, 0x00}, new Approve(0)),
                     Arguments.of(new byte[] {0x64, 0x02, 0x00, 0x00, 0x00, 0x00}, new Deny(0)));
  }

  @ParameterizedTest
  @MethodSource("provideForInteger")
  public void integer(byte[] expectedBytes, IPart input) throws ParsingException {
    byte[] a = input.getBytes();
    assertArrayEquals(expectedBytes, a);
    MessageHandlerMock m = new MessageHandlerMock(input);
    m.testHandle(ByteBuffer.wrap(a).position(0));
  }

  @Test
  public void nullTest() {
    assertNull(BitDepth.getFromBitDepth(35));
    assertNull(SampleRate.getFromSampleRate(34));
    assertNull(SampleRate.getFromValue((byte) 0x18));
    assertNull(Codec.getFromValue((byte) 223));
  }

  @Test
  public void assertNoError() {
    new MessageHandlerMock().onMessage(ByteBuffer.allocate(0));
    new MessageHandlerMock().onMessage(ByteBuffer.allocate(1).put((byte) 0x80));
  }

  @Test
  public void equals() {
    assertEquals(new AudioFrame(0, 0, 3, new byte[] {0x00, 0x21, 0x34}), new AudioFrame(0, 0, 3, new byte[] {0x00, 0x21, 0x34}));
    assertEquals(new ByteAtomicPart((byte) 0), new ByteAtomicPart((byte) 0));
    ByteAtomicPart b = new ByteAtomicPart((byte) 0);
    assertEquals(b, b);
  }


  @ParameterizedTest
  @MethodSource("provideForEquals")
  public void equals1(byte[] expectedBytes, IPart input) throws ParsingException {
    assertEquals(input, input);
    assertNotEquals(input, null);
    assertNotEquals(input, new MessageHandlerMock());

  }


  private class MessageHandlerMock extends MessageHandler {
    boolean tookBranch = false;
    private IPart p;

    private MessageHandlerMock() {
    }

    private MessageHandlerMock(IPart p) {
      this.p = p;
    }

    public void testHandle(ByteBuffer b) {
      onMessage(b);
      assertTrue(tookBranch);
    }

    @Override
    public void onAudioRequest(ByteBuffer b) {
      try {
        assertEquals(p, new AudioRequest(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onTextRequest(ByteBuffer b) {
      try {
        assertEquals(p, new TextRequest(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onSuccess(ByteBuffer b) {
      try {
        assertEquals(p, new SuccessResponse(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onFail(ByteBuffer b) {
      try {
        assertEquals(p, new FailResponse(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onApprove(ByteBuffer b) {
      try {
        assertEquals(p, new Approve(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onApproveRequest(ByteBuffer b) {
      try {
        assertEquals(p, new ApproveRequest(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onDeny(ByteBuffer b) {
      try {
        assertEquals(p, new Deny(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onLogMessage(ByteBuffer b) {
      try {
        assertEquals(p, new LogMessage(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }

    @Override
    public void onStatusMessage(ByteBuffer b) {
      try {
        assertEquals(p, new StatusMessage(b));
        tookBranch = true;
      } catch (ParseException e) {
        fail();
      }
    }
  }
}