package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.TimeOutMaker;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockCommunicator;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockLogger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class TextJobTest {
    static private String path = "./tmpPrefTextJobTest";
    static private File pref;
    private TimeOutMaker t;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
    }

    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testInitialization() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        TextJob job = null;
        try {
            job = new TextJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        assertEquals(job.getText(), input);
        assertEquals(job.getMachine(), machine);
        assertSame(job.getLogger(), logger);
        assertEquals(job.getClientID(), clientId);
        assertSame(job.getClientCommunicator(), cc);

        try {
            job = new TextJob(job, input);
        } catch (InstantiationException e) {
            fail();
        }
        assertEquals(job.getText(), input);
        assertEquals(job.getMachine(), machine);
        assertSame(job.getLogger(), logger);
        assertEquals(job.getClientID(), clientId);
        assertSame(job.getClientCommunicator(), cc);
    }

    @Test
    public void testEnqueue() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        try {
            PipelineSteps.getTextStep().terminate();
        } catch (InstantiationException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        String input = "zxcvbn";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        TextJob j1 = null;
        TextJob j2 = null;
        try {
            j1 = new TextJob(cc, logger, input, machine, clientId);
            j2 = new TextJob(cc, logger, input, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }

        makeTimeout();
        try {
            j1.enqueue();
            cancelTimeout();
        } catch (InterruptedException e) {
            fail();
        }

        makeTimeout();
        try {
            j2.enqueue();
            fail();
        } catch (InterruptedException e) {
            assertTrue(true);
        }
        makeTimeout();
        try {
            assertSame(PipelineSteps.getTextStep().getNextJob(), j1);
        } catch (InterruptedException | InstantiationException e) {
            fail();
        }
        cancelTimeout();

        assertTrue(j1.tryEnqueue());
        assertFalse(j2.tryEnqueue());
        makeTimeout();
        try {
            assertSame(PipelineSteps.getTextStep().getNextJob(), j1);
        } catch (InterruptedException | InstantiationException e) {
            fail();
        }
        cancelTimeout();
    }

    @AfterEach
    public void cancelTimeout() {
        if (t != null) {
            t.cancel();
        }
    }

    private void makeTimeout(int seconds) {
        cancelTimeout();
        t = new TimeOutMaker(Thread.currentThread(), seconds);
    }

    private void makeTimeout() {
        makeTimeout(1);
    }
}
