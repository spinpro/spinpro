package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.main.pipeline.IPipableJob;
import de.fraunhofer.iosb.spinpro.main.pipeline.IPipelineFunction;

public class MockFunctionError implements IPipelineFunction<MockJob, IPipableJob> {
    @Override
    public IPipableJob apply(MockJob mockJob) throws InterruptedException {
        int x = 1 / 0;
        if (x != 0 || x == 0) {
            return mockJob;
        }
        else {
            return null;
        }
    }
}
