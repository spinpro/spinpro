package de.fraunhofer.iosb.spinpro.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestTest {
    @Test
    public void testTest() {
        int a = 1;
        int b = 2;
        int c = a + b;
        assertTrue(c == 3);
    }
}
