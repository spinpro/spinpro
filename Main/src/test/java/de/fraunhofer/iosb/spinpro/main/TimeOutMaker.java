package de.fraunhofer.iosb.spinpro.main;

public class TimeOutMaker implements Runnable {
  private Thread toInterrupt;
  private long timeout;
  private int nanos;
  private Thread t;

  public TimeOutMaker(Thread toInterrupt, long timeout, int nanos) {
    this.toInterrupt = toInterrupt;
    this.timeout = timeout;
    this.nanos = nanos;
    t = new Thread(this);
    t.start();
  }

  public TimeOutMaker(Thread toInterrupt, long timeout) {
    this(toInterrupt, timeout, 0);
  }

  public TimeOutMaker(Thread toInterrupt) {
    this(toInterrupt, 1);
  }

  public void run() {
    try {
      Thread.sleep(1000 * timeout, nanos);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    synchronized (toInterrupt) {
      if (!Thread.interrupted()) {
        toInterrupt.interrupt();
      }
    }
  }

  public void cancel() {
    synchronized (toInterrupt) {
      if (!Thread.interrupted()) {
        t.interrupt();
      }
    }
  }
}
