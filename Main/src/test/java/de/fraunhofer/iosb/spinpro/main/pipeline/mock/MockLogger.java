package de.fraunhofer.iosb.spinpro.main.pipeline.mock;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

public class MockLogger implements ILogger {
    private Level lastLevel;
    private String lastString;
    private Exception lastException;


    @Override
    public void log(Level ll, String s) {
        lastLevel = ll;
        lastString = s;
        lastException = null;
    }

    @Override
    public void log(Level ll, Messages s) {
        lastLevel = ll;
        lastString = ConfigurationProvider.getMessageBundle().getMessage(s);
        lastException = null;
    }

    @Override
    public void log(Level ll, String message, Exception e) {
        lastLevel = ll;
        lastString = message;
        lastException = e;
    }

    @Override
    public void log(Level ll, Messages message, Exception e) {
        lastLevel = ll;
        lastString = ConfigurationProvider.getMessageBundle().getMessage(message);
        lastException = e;
    }

    public Level getLastLevel() {
        return lastLevel;
    }

    public String getLastString() {
        return lastString;
    }

    public Exception getLastException() {
        return lastException;
    }
}
