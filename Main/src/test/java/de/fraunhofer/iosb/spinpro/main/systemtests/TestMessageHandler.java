package de.fraunhofer.iosb.spinpro.main.systemtests;

import de.fraunhofer.iosb.spinpro.main.communication.MessageHandler;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Approve;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.ApproveRequest;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Deny;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.FailResponse;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.LogMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.StatusMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.SuccessResponse;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.TextRequest;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import static org.junit.jupiter.api.Assertions.fail;

public class TestMessageHandler extends MessageHandler {

  private final Condition condition;
  private final Lock lock;
  private final TestClient.DataObject data;

  public TestMessageHandler(Lock lock, Condition condition, TestClient.DataObject data) {
    this.lock = lock;
    this.condition = condition;
    this.data = data;
  }


  @Override
  protected void onApprove(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new Approve(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onAudioRequest(ByteBuffer b) {
    //IGNORE
  }

  @Override
  public void onTextRequest(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new TextRequest(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onSuccess(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new SuccessResponse(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onFail(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new FailResponse(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onApproveRequest(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new ApproveRequest(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onDeny(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new Deny(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onLogMessage(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new LogMessage(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }

  @Override
  public void onStatusMessage(ByteBuffer b) {
    lock.lock();
    try {
      data.setData(new StatusMessage(b));
    } catch (ParseException e) {
      fail();
    }
    condition.signal();
    lock.unlock();
  }
}
