package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class PipelineStepsTest {
    static private String path = "./tmpPrefPipelineStepsTest";
    static private File pref;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
    }

    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testParsingErrors() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "assdfge45zhsd");
        try {
            PipelineSteps.getAudioStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT));
        }
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE, "fbs8do39jj,.");
        try {
            PipelineSteps.getAudioStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE));
        }
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "sdgb7s8h");
        try {
            PipelineSteps.getApproveStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT));
        }
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "jls,d.büehglwjh");
        try {
            PipelineSteps.getApproveStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE));
        }
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "jjhhgiujsduihfjhmsgdjflg");
        try {
            PipelineSteps.getTextStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT));
        }
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "2345678iujhgf+#+#");
        try {
            PipelineSteps.getTextStep();
            fail();
        } catch (InstantiationException e) {
            assertEquals(e.getMessage(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE));
        }

    }

    @Test
    public void testIdempotence() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        IPipelineStep<AudioJob> ps1 = null;
        IPipelineStep<AudioJob> ps2 = null;
        try {
            ps1 = PipelineSteps.getAudioStep();
        } catch (InstantiationException e) {
            fail();
        }
        try {
            ps2 = PipelineSteps.getAudioStep();
        } catch (InstantiationException e) {
            fail();
        }
        assertNotNull(ps1);
        assertNotNull(ps2);
        assertSame(ps1, ps2);
        IPipelineStep<ApproveJob> ps3 = null;
        IPipelineStep<ApproveJob> ps4 = null;
        try {
            ps3 = PipelineSteps.getApproveStep();
        } catch (InstantiationException e) {
            fail();
        }
        try {
            ps4 = PipelineSteps.getApproveStep();
        } catch (InstantiationException e) {
            fail();
        }
        assertNotNull(ps3);
        assertNotNull(ps4);
        assertSame(ps3, ps4);
        IPipelineStep<TextJob> ps5 = null;
        IPipelineStep<TextJob> ps6 = null;
        try {
            ps5 = PipelineSteps.getTextStep();
        } catch (InstantiationException e) {
            fail();
        }
        try {
            ps6 = PipelineSteps.getTextStep();
        } catch (InstantiationException e) {
            fail();
        }
        assertNotNull(ps5);
        assertNotNull(ps6);
        assertSame(ps5, ps6);
    }

    @Test
    public void testReload() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        IPipelineStep<AudioJob> ps1 = null;
        IPipelineStep<ApproveJob> ps2 = null;
        IPipelineStep<TextJob> ps3 = null;
        try {
            ps1 = PipelineSteps.getAudioStep();
            ps2 = PipelineSteps.getApproveStep();
            ps3 = PipelineSteps.getTextStep();
        } catch (InstantiationException e) {
            fail();
        }
        PipelineSteps.reset();
        IPipelineStep<AudioJob> ps4 = null;
        IPipelineStep<ApproveJob> ps5 = null;
        IPipelineStep<TextJob> ps6 = null;
        try {
            ps4 = PipelineSteps.getAudioStep();
            ps5 = PipelineSteps.getApproveStep();
            ps6 = PipelineSteps.getTextStep();
        } catch (InstantiationException e) {
            fail();
        }
        assertNotSame(ps1, ps4);
        assertNotSame(ps2, ps5);
        assertNotSame(ps3, ps6);
    }
}
