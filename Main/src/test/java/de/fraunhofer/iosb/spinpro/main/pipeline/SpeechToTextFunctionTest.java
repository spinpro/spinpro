package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockCommunicator;
import de.fraunhofer.iosb.spinpro.main.pipeline.mock.MockLogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class SpeechToTextFunctionTest {
    static private SpeechToTextFunction s2tf;
    static private AudioInputStream ais;
    static private String path = "./tmpPrefSpeechToTextFunctionTest";
    static private File pref;

    @BeforeAll
    public static void init() {
        pref = new File(path);
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
        try {
            assertTrue(pref.createNewFile());
        } catch (IOException e) {
            fail();
        }
        ConfigurationProvider.init(path);
        s2tf = new SpeechToTextFunction();
    }


    @AfterAll
    public static void cleanup() {
        if(pref.exists()) {
            assertTrue(pref.delete());
        }
    }

    @BeforeEach
    public void resetSteps() {
        PipelineSteps.reset();
    }

    @Test
    public void testSuccess() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH, "../SpeechToText/src/test/resources/jars/CMUSphinxService-0.1-all.jar");
        s2tf.reload();
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        AudioJob inJob = null;
        try {
            inJob = new AudioJob(cc, logger, ais, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = s2tf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNotNull(outJob);
        assertSame(outJob.getClass(), ApproveJob.class);
        ApproveJob outJob2 = (ApproveJob) outJob;
        assertSame(outJob2.getClientCommunicator(), cc);
        assertEquals(outJob2.getClientID(), clientId);
        assertSame(outJob2.getLogger(), logger);
        assertEquals(outJob2.getMachine(), machine);
        assertNotNull(outJob2.getText());
    }

    @Test
    public void testErrorNulljob() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH, "../SpeechToText/src/test/resources/jars/CMUSphinxService-0.1-all.jar");
        s2tf.reload();
        AudioJob inJob = null;
        IPipableJob outJob = null;
        try {
            outJob = s2tf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
    }

    @Test
    public void testErrorS2TInit() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH, "zxcvbnm");
        s2tf.reload();
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        AudioJob inJob = null;
        try {
            inJob = new AudioJob(cc, logger, ais, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = s2tf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertNull(logger.getLastException());
        assertSame(logger.getLastLevel(), Level.SEVERE);
        assertEquals(logger.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT));
        assertEquals("fail", cc.getLastType());
        assertEquals(cc.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT));
        assertEquals(cc.getLastCode(), -1);
    }

    @Test
    public void testErrorS2TProcess() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH, "../SpeechToText/src/test/resources/jars/CMUSphinxService-0.1-all.jar");
        s2tf.reload();
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
            ais = AudioSystem.getAudioInputStream(AudioFormat.Encoding.ALAW, ais);
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        AudioJob inJob = null;
        try {
            inJob = new AudioJob(cc, logger, ais, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        IPipableJob outJob = null;
        try {
            outJob = s2tf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertNull(logger.getLastException());
        assertSame(logger.getLastLevel(), Level.SEVERE);
        assertEquals(logger.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PROCESSING_AUDIO));
        assertEquals("fail", cc.getLastType());
        assertEquals(cc.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PROCESSING_AUDIO));
        assertEquals(cc.getLastCode(), -1);
    }

    @Test
    public void testErrorCreateJob() {
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "1");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH, "../SpeechToText/src/test/resources/jars/CMUSphinxService-0.1-all.jar");
        s2tf.reload();
        try {
            ais = AudioSystem.getAudioInputStream(new File("src/test/resources/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
            fail();
        }
        String clientId = "qwerty";
        String machine = "asdfgh";
        MockLogger logger = new MockLogger();
        MockCommunicator cc = new MockCommunicator(true);
        AudioJob inJob = null;
        try {
            inJob = new AudioJob(cc, logger, ais, machine, clientId);
        } catch (InstantiationException e) {
            fail();
        }
        PipelineSteps.reset();
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_COUNT, "assdfge45zhsd");
        ConfigurationProvider.getMainPreferences().setValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE, "fbs8do39jj,.");
        IPipableJob outJob = null;
        try {
            outJob = s2tf.apply(inJob);
        } catch (InterruptedException e) {
            fail();
        }
        assertNull(outJob);
        assertNull(logger.getLastException());
        assertSame(logger.getLastLevel(), Level.SEVERE);
        assertEquals(logger.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        assertEquals("fail", cc.getLastType());
        assertEquals(cc.getLastString(), ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        assertEquals(cc.getLastCode(), -1);
    }
}
