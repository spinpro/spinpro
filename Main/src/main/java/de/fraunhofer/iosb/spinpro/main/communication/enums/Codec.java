package de.fraunhofer.iosb.spinpro.main.communication.enums;

public enum Codec {
  RESERVED1((byte) 0x00), //End of string
  RESERVED2((byte) 0xff), //  none directional
  CLIENT_ID((byte) 0x01), REQUEST_ID((byte) 0x02), MACHINE((byte) 0x03), FRAME_NUMBER((byte) 0x04), MESSAGE((byte) 0x05), NUMBER((byte) 0x06),

  AUDIO_DATA((byte) 0x08), AUDIO_FORMAT((byte) 0x09), SENTENCE_ORIG((byte) 0x0a), AUDIO_LENGTH((byte) 0x0b), SAMPLE_COUNT((byte) 0x0c),

  RULE((byte) 0x10), SENTENCE_REC((byte) 0x11), //  Client-to-Server
  AUDIO_REQUEST((byte) 0x20), TEXT_REQUEST((byte) 0x21), APPROVE_REQUEST((byte) 0x22), //  Server-to-Client
  SUCCESS_REQUEST((byte) 0x40), FAIL_REQUEST((byte) 0x41), //  Unidirectional
  LOG_MESSAGE((byte) 0x60), STATUS_MESSAGE((byte) 0x61), AUDIO_FRAME((byte) 0x62), APPROVE((byte) 0x63), DENY((byte) 0x64);

  private final byte value;

  Codec(byte b) {
    this.value = b;
  }

  /**
   * Gets the codec from a binary codec.
   *
   * @param a The binary codec.
   * @return The codec.
   */
  public static Codec getFromValue(byte a) {
    for (Codec s : Codec.values()) {
      if (s.getCodec() == a) {
        return s;
      }
    }
    return null;
  }

  /**
   * Gets the Code of this value.
   *
   * @return The code.
   */
  public byte getCodec() {
    return value;
  }

}
