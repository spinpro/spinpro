package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import de.fraunhofer.iosb.spinpro.main.communication.enums.BitDepth;
import de.fraunhofer.iosb.spinpro.main.communication.enums.SampleRate;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Signed;

import java.nio.ByteBuffer;
import java.text.ParseException;

/**
 * A audio format part of a message.
 */
public class FormatAtomicPart implements IAtomicPath<FormatAtomicPart> {
  private Signed signed;
  private SampleRate hz;
  private BitDepth bitDepth;

  /**
   * Creates a new instance.
   *
   * @param bitDepth The bit depth.
   * @param hz       The sample rate.
   * @param signed   If it is signed or not.
   */
  public FormatAtomicPart(Signed signed, SampleRate hz, BitDepth bitDepth) {
    this.signed = signed;
    this.hz = hz;
    this.bitDepth = bitDepth;
  }

  /**
   * Creates a new instance.
   *
   * @param data The bytes of the message received.
   * @throws ParseException If it could not get parsed.
   */
  public FormatAtomicPart(ByteBuffer data) throws ParseException {
    byte codec = data.get();
    Signed a = Signed.getFromValue(codec);
    SampleRate b = SampleRate.getFromValue(codec);
    BitDepth c = BitDepth.getFromValue(codec);
    if (a == null || b == null || c == null) {
      throw new ParseException("Could not get audio format.", 0);
    }
    this.signed = a;
    this.hz = b;
    this.bitDepth = c;
  }

  /**
   * Gets the standard format.
   *
   * @return The format.
   */
  public static FormatAtomicPart get16bitU16khz() {
    return new FormatAtomicPart(Signed.UNSIGNED, SampleRate.HZ16000, BitDepth.Bit16);
  }

  /**
   * If format is signed.
   *
   * @return if format is signed.
   */
  public boolean isSigned() {
    return signed.equals(Signed.SIGNED);
  }

  /**
   * Gets the sample rate.
   *
   * @return The sample rate.
   */
  public int getHz() {
    return hz.getHz();
  }

  /**
   * Gets the bit depth.
   *
   * @return The bit depth.
   */
  public int getBitDepth() {
    return bitDepth.getDepth();
  }

  @Override
  public byte[] getBytes() {
    return new byte[] {(byte) (signed.getCodec() | bitDepth.getCodec() | hz.getCodec())};
  }

  @Override
  public FormatAtomicPart getT() {
    return this;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    FormatAtomicPart b = (FormatAtomicPart) obj;

    return hz.equals(b.hz) && bitDepth.equals(b.bitDepth) && signed.equals(b.signed);
  }
}
