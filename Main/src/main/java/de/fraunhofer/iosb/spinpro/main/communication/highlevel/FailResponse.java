package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.ErrorAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * A fail message.
 */
public class FailResponse implements IPart {

  private IAtomicPath<Integer> requestID;
  private IAtomicPath<ErrorAtomicPart> error;

  /**
   * Creates a new fail message.
   *
   * @param requestID The request id.
   * @param error     The error.
   */
  public FailResponse(IAtomicPath<Integer> requestID, IAtomicPath<ErrorAtomicPart> error) {
    this.requestID = requestID;
    this.error = error;
  }

  /**
   * Creates a new fail message.
   *
   * @param requestID    The request id.
   * @param errorCode    The error code.
   * @param errorMessage The error message.
   */
  public FailResponse(int requestID, byte errorCode, String errorMessage) {
    this(new IntAtomicPart(requestID), new ErrorAtomicPart(errorCode, errorMessage));
  }

  /**
   * Creates a new fail message.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public FailResponse(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      byte sb = b.get();
      if ((sb & 0x80) != 0) {
        error = new ErrorAtomicPart(sb, b);
        continue;
      }
      Codec c = Codec.getFromValue(sb);
      if (c == null) {
        throw new ParseException("Could not find enum.", 0);
      }
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Gets the request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the error.
   *
   * @return The error.
   */
  public ErrorAtomicPart getError() {
    return error.getT();
  }


  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.FAIL_REQUEST.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    addAll(bytes, error);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    for (byte b : c.getBytes()) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    FailResponse b = (FailResponse) obj;

    return this.error.equals(b.error) && requestID.equals(b.requestID);
  }

}
