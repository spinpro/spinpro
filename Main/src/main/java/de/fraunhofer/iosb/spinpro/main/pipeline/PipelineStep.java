package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;

/**
 * A step in the pipeline using a blocking queue to synchronise multiple threads.
 *
 * @param <T> The type of the objects in the pipeline.
 */
public class PipelineStep<T extends IPipableJob> implements IPipelineStep<T> {
  /**
   * The logger to use for this class.
   */
  private static ILogger logger;

  private static final String ERROR_ZERO_CAPACITY = ConfigurationProvider.getMessageBundle().getMessage(Messages.PIPELINE_STEP_ERROR_ZERO_CAPACITY);
  private static final String ERROR_ZERO_THREADS = ConfigurationProvider.getMessageBundle().getMessage(Messages.PIPELINE_STEP_ERROR_ZERO_THREADS);
  private static final String INTERRUPT_WHILE_CLOSING = ConfigurationProvider.getMessageBundle().getMessage(Messages.PIPELINE_STEP_INTERRUPT_WHILE_CLOSING);

  /**
   * Queue holding jobs to be processed.
   */
  private BlockingQueue<T> blockingQueue;
  /**
   * Array of all worker threads.
   */
  private Thread[] threads;

  /**
   * Construct a new pipeline step and initiates a thread pool to work with.
   *
   * @param capacity    The capacity of the buffer.
   * @param fair        If the buffer should be fair.
   * @param threadCount The thread count.
   * @param function    The function to perform in this step.
   * @throws InstantiationException If an error occurred during initialization.
   */
  public PipelineStep(int capacity, boolean fair, int threadCount, IPipelineFunction<T, IPipableJob> function) throws InstantiationException {
    logger = ConfigurationProvider.getMainLogger();
    if (capacity < 1) {
      logger.log(Level.SEVERE, ERROR_ZERO_CAPACITY);
      throw new InstantiationException(ERROR_ZERO_CAPACITY);
    }
    if (threadCount < 1) {
      logger.log(Level.SEVERE, ERROR_ZERO_THREADS);
      throw new InstantiationException(ERROR_ZERO_THREADS);
    }
    blockingQueue = new ArrayBlockingQueue<>(capacity, fair);

    threads = new Thread[threadCount];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new Thread(new PipelineRunnable<T>(this, function), String.format("%s-%s-%d", "Pipeline", function.getClass().getSimpleName(), i));
    }

    for (Thread thread : threads) {
      thread.start();
    }
  }

  /**
   * Tries adding a job to the queue.
   *
   * @param t The job to add.
   * @return If the job could be added.
   */
  @Override
  public boolean tryAddJob(T t) {
    return blockingQueue.offer(t);
  }

  /**
   * Adds a job, blocking if queue is full.
   *
   * @param t The job to add.
   * @throws InterruptedException If thread gets interrupted while waiting.
   */
  @Override
  public void addJob(T t) throws InterruptedException {
    blockingQueue.put(t);
  }

  /**
   * Gets the next job.
   *
   * @return The job.
   * @throws InterruptedException If thread gets interrupted while waiting.
   */
  @Override
  public T getNextJob() throws InterruptedException {
    return blockingQueue.take();
  }

  /**
   * Terminates the pipeline step by interrupting all worker threads and waiting for them to finish.
   */
  @Override
  public void terminate() {
    for (Thread t : threads) {
      t.interrupt();
    }
    for (Thread t : threads) {
      try {
        t.join();
      } catch (InterruptedException e) {
        logger.log(Level.WARNING, INTERRUPT_WHILE_CLOSING);
      }
    }
  }

  /**
   * Worker class for the pipeline step.
   *
   * @param <T> The type of the worker.
   */
  private static class PipelineRunnable<T extends IPipableJob> implements Runnable {
    /**
     * The input buffer for the worker.
     */
    private IPipelineStep<T> in;
    /**
     * The function to be applied.
     */
    private IPipelineFunction<T, IPipableJob> function;

    /**
     * Initializes the worker.
     *
     * @param in       The input buffer for the worker.
     * @param function The function to be applied.
     */
    private PipelineRunnable(IPipelineStep<T> in, IPipelineFunction<T, IPipableJob> function) {
      this.in = in;
      this.function = function;
    }

    /**
     * Continuously tries to take jobs form in and apply function to them, giving the result back to pipeline, until interrupted.
     */
    @Override
    public void run() {
      while (!Thread.interrupted()) {
        try {
          IPipableJob a = function.apply(in.getNextJob());
          if (a == null) {
            continue;
          }
          a.enqueue();
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          break;
        } catch (Exception e) {
          logger.log(Level.WARNING, Messages.EXCEPTION_WHILE_EXECUTION, e);
          //Just go on.
        }
      }
    }
  }
}
