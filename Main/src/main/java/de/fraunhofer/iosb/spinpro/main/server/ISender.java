package de.fraunhofer.iosb.spinpro.main.server;

import java.nio.ByteBuffer;

public interface ISender {

  /**
   * Initiates the asynchronous transmission of a binary message. This method returns before the message is
   * transmitted. Developers use the returned Future object to track progress of the transmission. The Future's
   * get() method returns {@code null} upon successful completion. Errors in transmission are wrapped in the
   * {@link java.util.concurrent.ExecutionException} thrown when querying the Future object.
   *
   * @param data the data being sent.
   * @throws IllegalArgumentException if the data is {@code null}.
   */
  void sendBinary(ByteBuffer data);

  /**
   * Closes the connection.
   */
  void close();

}
