package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.Logger;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;
import de.fraunhofer.iosb.spinpro.main.IJobProvider;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.sound.sampled.AudioInputStream;

/**
 * An implementation of a {@link IJobProvider}.
 */
public class JobProvider implements IJobProvider {
  private static IJobProvider jobProvider;
  private IClientCommunicator cc;

  /**
   * Creates a new {@link JobProvider}.
   * @param cc The {@link IClientCommunicator} to use.
   */
  public JobProvider(IClientCommunicator cc) {
    this.cc = cc;
  }

  @Override
  public boolean generateAudioJob(AudioInputStream inputStream, String machine, String clientID) {
    try {
      AudioJob job = new AudioJob(cc, new Logger("AudioJob-" + clientID + "-" + new SimpleDateFormat("yyyyMMddHHmmssSS").format(new Date())), inputStream, machine, clientID);
      job.getLogger().log(Level.FINE, ConfigurationProvider.getMessageBundle().getMessage(Messages.JOB_GENERATED));
      job.getLogger().log(Level.FINE,ConfigurationProvider.getMessageBundle().getMessage(Messages.MACHINE) + ": " +  machine);
      job.getLogger().log(Level.FINE, ConfigurationProvider.getMessageBundle().getMessage(Messages.CLIENT_ID) + ": " + clientID);
      return job.tryEnqueue();
    } catch (InstantiationException e) {
      return false;
    }
  }

  @Override
  public boolean generateTextJob(String sentence, String machine, String clientID) {
    try {
      TextJob job = new TextJob(cc, new Logger("TextJob-" + clientID + "-" + new SimpleDateFormat("yyyyMMddHHmmssSS").format(new Date())), sentence, machine, clientID);
      job.getLogger().log(Level.FINE, ConfigurationProvider.getMessageBundle().getMessage(Messages.JOB_GENERATED));
      job.getLogger().log(Level.FINE,ConfigurationProvider.getMessageBundle().getMessage(Messages.MACHINE) + ": " +  machine);
      job.getLogger().log(Level.FINE, ConfigurationProvider.getMessageBundle().getMessage(Messages.CLIENT_ID) + ": " + clientID);
      return job.tryEnqueue();
    } catch (InstantiationException e) {
      return false;
    }
  }

}