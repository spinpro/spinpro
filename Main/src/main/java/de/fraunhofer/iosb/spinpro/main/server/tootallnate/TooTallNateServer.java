package de.fraunhofer.iosb.spinpro.main.server.tootallnate;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.IServer;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import javax.websocket.DeploymentException;

/**
 * Implementation of a {@link IServer} using TooTallNate's Java-Websocket.
 */
public class TooTallNateServer implements IServer {
  WebSocketServer server;

  /**
   * Constructor of the Server class. Creates the connection listener and worker
   * threads.
   *
   * @throws DeploymentException If not possible to start server.
   */
  public TooTallNateServer() throws DeploymentException {
    InetSocketAddress inetSocketAddress = new InetSocketAddress(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.HOSTNAME), Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.PORT)));
    inetSocketAddress = new InetSocketAddress(8085);
    server = new CustomWebsocketServer(inetSocketAddress);
    server.start();

  }

  /**
   * Terminates all listener and worker threads.
   */
  public void stop() {
    try {
      server.stop();
    } catch (IOException e) {
      ConfigurationProvider.getMainLogger().log(Level.SEVERE, Messages.ERROR_STOPPING_CLIENT_COMMUNICATOR);
    } catch (InterruptedException e) {
      // Just return
    }
  }
}

