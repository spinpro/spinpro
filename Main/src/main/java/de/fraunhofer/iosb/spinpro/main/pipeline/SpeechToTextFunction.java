package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceLoadingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.speechtotext.SpeechToTextComponent;

import java.util.logging.Level;

/**
 * Function for Speech-to-Text component interaction.
 */
public class SpeechToTextFunction implements IPipelineFunction<AudioJob, IPipableJob> {

  private static String ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT);
  private static String ERROR_PROCESSING_AUDIO = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PROCESSING_AUDIO);
  private static String ERROR_CREATING_JOB = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB);
  private static SpeechToTextComponent component;

  /**
   * Initializes speech to text component to use by function if necessary.
   */
  private void init() {
    if (component == null) {
      reload();
    }
  }

  /**
   * Forcibly reinitializes speech to text component.
   */
  public void reload() {
    component = null;
    try {
      component = new SpeechToTextComponent();
    } catch (ServiceLoadingException | ConfigurationException e) {
      ConfigurationProvider.getMainLogger().log(Level.SEVERE, ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT);
    }
  }

  /**
   * Passes a job to the speech-to-text component.
   *
   * @param job The job to process.
   * @return The processed job.
   * @throws InterruptedException If interrupted during execution.
   */
  @Override
  public IPipableJob apply(AudioJob job) throws InterruptedException {
    if (job == null) {
      ConfigurationProvider.getMainLogger().log(Level.WARNING, Messages.PIPELINE_STEP_RETURNED_NULL);
      return null;
    }
    init();
    job.getLogger().log(Level.FINE, Messages.ENTERED_SPEECH_TO_TEXT_STEP);
    if (component == null) {
      job.getLogger().log(Level.SEVERE, ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT);;
      job.getClientCommunicator().fail((byte) -1, ERROR_INITIALIZING_SPEECH_TO_TEXT_COMPONENT);
      return null;
    }
    try {
      String result = component.speechToText(job.getAudio(), job.getLogger());
      return new ApproveJob(job, result);
    } catch (UnsupportedAudioFormat | ServiceInitializingException e) {
      job.getLogger().log(Level.SEVERE, ERROR_PROCESSING_AUDIO);
      job.getClientCommunicator().fail((byte) -1, ERROR_PROCESSING_AUDIO);
      return null;
    } catch (InstantiationException e) {
      job.getLogger().log(Level.SEVERE, Messages.ERROR_CREATING_JOB);
      job.getClientCommunicator().fail((byte) -1, ERROR_CREATING_JOB);
      return null;
    }
  }
}
