package de.fraunhofer.iosb.spinpro.main;

import javax.sound.sampled.AudioInputStream;

/**
 * Provides method for interacting with the pipeline.
 */
public interface IJobProvider {
  /**
   * Starts the pipeline with a new audio job.
   * @param inputStream The audio data.
   * @param machine The machine.
   * @param clientID The clientID.
   * @return If the job could be created.
   */
  public boolean generateAudioJob(AudioInputStream inputStream, String machine, String clientID);

  /**
   * Starts the pipeline with a new text job.
   * @param sentence The text data.
   * @param machine The machine.
   * @param clientID The clientID.
   * @return If the job could be created.
   */
  public boolean generateTextJob(String sentence, String machine, String clientID);
}
