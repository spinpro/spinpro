package de.fraunhofer.iosb.spinpro.main.server;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.ApproveRequest;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.FailResponse;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.LogMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.StatusMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.SuccessResponse;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A communication object to communicate with a client.
 */
public class ClientCommunicator implements IClientCommunicator {
  private static final long TIMEOUT = Long.parseLong(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.APPROVE_TIMEOUT));
  private ISender session;
  private int requestID;
  private ServerWsHandler handler;

  /**
   * Creates a new {@link ClientCommunicator}.
   * @param session The sender to use.
   * @param requestID The request id.
   * @param handler The handler.
   */
  public ClientCommunicator(ISender session, int requestID, ServerWsHandler handler) {
    this.session = session;
    this.requestID = requestID;
    this.handler = handler;
  }

  @Override
  public boolean approve(String sentence) {
    Lock lock = new ReentrantLock();
    Condition changed = lock.newCondition();
    lock.lock();
    AtomicInteger sync = new AtomicInteger(-1);
    ApproveHandler messageHandler = new ApproveHandler(sync, lock, changed, session, requestID);
    handler.addApproveHandler(messageHandler);
    session.sendBinary(ByteBuffer.wrap(new ApproveRequest(requestID, sentence).getBytes()));
    try {
      long time = System.currentTimeMillis();
      while (!(sync.get() == 0 || sync.get() == 1)) {
        changed.await(TIMEOUT / 100, TimeUnit.MILLISECONDS);
        if (System.currentTimeMillis() - time > TIMEOUT) {
          break;
        }
      }
    } catch (InterruptedException e) {
      // Just return false will work here.
    } finally {
      handler.removeApproveHandler(messageHandler);
      lock.unlock();
    }
    return sync.get() == 1;
  }

  @Override
  public void sendLog(String message, int number) {
    session.sendBinary(ByteBuffer.wrap(new LogMessage(message, number).getBytes()));
  }

  @Override
  public void sendStatus(String message, int number) {
    session.sendBinary(ByteBuffer.wrap(new StatusMessage(requestID, message, number).getBytes()));
  }

  @Override
  public void fail(byte errorNumber, String message) {
    session.sendBinary(ByteBuffer.wrap(new FailResponse(requestID, errorNumber, message).getBytes()));
    session.close();
  }

  @Override
  public void success(String rule) {
    session.sendBinary(ByteBuffer.wrap(new SuccessResponse(requestID, rule).getBytes()));
    session.close();
  }
}
