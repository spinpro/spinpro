package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;

/**
 * A implementation of {@link AbstractJob} using a text.
 */
public class TextJob extends AbstractJob {

  /**
   * The pipeline step this job will use.
   */
  private IPipelineStep<TextJob> currentStep;

  /**
   * The input text data of this job.
   */
  private String text;

  /**
   * Creates a new job.
   *
   * @param clientCommunicator The client communicator object.
   * @param logger             The logger to be used for this job.
   * @param inputText          The input text data.
   * @param machine            The machine.
   * @param clientID           The client id.
   * @throws InstantiationException If the job could not get it's pipeline step.
   */
  public TextJob(IClientCommunicator clientCommunicator, ILogger logger, String inputText, String machine, String clientID) throws InstantiationException {
    super(clientCommunicator, logger, machine, clientID);
    currentStep = PipelineSteps.getTextStep();
    text = inputText;
  }

  /**
   * Creates a new job from a given abstract job.
   *
   * @param job       The input job.
   * @param inputText The input text data.
   * @throws InstantiationException If the job could not get it's pipeline step.
   */
  public TextJob(AbstractJob job, String inputText) throws InstantiationException {
    this(job.getClientCommunicator(), job.getLogger(), inputText, job.getMachine(), job.getClientID());
  }

  /**
   * Getter for this job's input text.
   *
   * @return The text attribute
   */
  public String getText() {
    return text;
  }

  /**
   * Puts this job into the respective pipeline step, blocking until possible.
   *
   * @throws InterruptedException if Thread got interrupted while waiting.
   */
  @Override
  public void enqueue() throws InterruptedException {
    enqueueGen(currentStep, this);
  }

  /**
   * Tries putting this job into the respective pipeline step.
   *
   * @return If it was possible to put this job into the pipeline step.
   */
  @Override
  public boolean tryEnqueue() {
    return tryEnqueueGen(currentStep, this);
  }
}
