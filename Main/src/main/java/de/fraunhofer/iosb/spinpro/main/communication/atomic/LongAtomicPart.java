package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import java.nio.ByteBuffer;

/**
 * An integer part of a message.
 */
public class LongAtomicPart implements IAtomicPath<Long> {

  private long value;

  /**
   * Creates a new instance.
   *
   * @param value The value to assign.
   */
  public LongAtomicPart(long value) {
    this.value = value;
  }

  /**
   * Creates a new instance.
   *
   * @param buffer The bytes of the message received.
   */
  public LongAtomicPart(ByteBuffer buffer) {
    value = buffer.getLong();
  }

  @Override
  public byte[] getBytes() {
    return ByteBuffer.allocate(8).putLong(value).array();
  }

  @Override
  public Long getT() {
    return value;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    LongAtomicPart b = (LongAtomicPart) obj;

    return b.value == value;
  }
}
