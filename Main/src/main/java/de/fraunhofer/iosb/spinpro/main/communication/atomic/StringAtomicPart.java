package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * A string part of a message.
 */
public class StringAtomicPart implements IAtomicPath<String> {
  private String data;

  /**
   * Creates a new instance.
   *
   * @param data The value to assign.
   */
  public StringAtomicPart(String data) {
    this.data = data;
  }

  /**
   * Creates a new instance.
   *
   * @param data The bytes of the message received.
   */
  public StringAtomicPart(ByteBuffer data) {
    ByteBuffer alloc = ByteBuffer.allocate(data.remaining());
    byte read;
    while (data.remaining() > 0 && (read = data.get()) != 0x00) {
      alloc.put(read);
    }
    this.data = new String(alloc.array(), StandardCharsets.UTF_8).trim();
  }

  @Override
  public byte[] getBytes() {
    byte[] temp = data.getBytes(StandardCharsets.UTF_8);
    byte[] out = new byte[temp.length + 1];
    System.arraycopy(temp, 0, out, 0, temp.length);
    out[temp.length] = 0x00;
    return out;
  }

  @Override
  public String getT() {
    return data;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    StringAtomicPart b = (StringAtomicPart) obj;

    return data.equals(b.data);
  }
}
