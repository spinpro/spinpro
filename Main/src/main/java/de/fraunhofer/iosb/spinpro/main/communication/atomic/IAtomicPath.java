package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import de.fraunhofer.iosb.spinpro.main.communication.highlevel.IPart;

/**
 * A part of a message.
 *
 * @param <T> The type of the part.
 */
public interface IAtomicPath<T> extends IPart {
  /**
   * Gets the containing data.
   *
   * @return The data.
   */
  T getT();
}
