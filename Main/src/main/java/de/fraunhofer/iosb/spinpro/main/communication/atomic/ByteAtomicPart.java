package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import java.nio.ByteBuffer;

/**
 * A byte part of a message.
 */
public class ByteAtomicPart implements IAtomicPath<Byte> {

  private byte value;

  /**
   * Creates a new instance.
   *
   * @param value The value to assign.
   */
  public ByteAtomicPart(byte value) {
    this.value = value;
  }

  /**
   * Creates a new instance.
   *
   * @param buffer The bytes of the message received.
   */
  public ByteAtomicPart(ByteBuffer buffer) {
    value = buffer.get();
  }

  @Override
  public byte[] getBytes() {
    return ByteBuffer.allocate(1).put(value).array();
  }

  @Override
  public Byte getT() {
    return value;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    ByteAtomicPart b = (ByteAtomicPart) obj;

    return b.value == value;
  }
}
