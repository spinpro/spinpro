package de.fraunhofer.iosb.spinpro.main.pipeline;

/**
 * A step in the pipeline.
 *
 * @param <T> The type of the objects in the pipeline.
 */
public interface IPipelineStep<T> {

  /**
   * Adds a job if possible.
   *
   * @param t The job to add.
   * @return If it was possible to add.
   */
  boolean tryAddJob(T t);

  /**
   * Adds a job blocking.
   *
   * @param t The job to add.
   * @throws InterruptedException If thread gets interrupted while waiting.
   */
  void addJob(T t) throws InterruptedException;

  /**
   * Gets the next job.
   *
   * @return The job.
   * @throws InterruptedException If thread gets interrupted while waiting.
   */
  T getNextJob() throws InterruptedException;

  /**
   * Terminates the pipeline step.
   */
  void terminate();
}
