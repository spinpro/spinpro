package de.fraunhofer.iosb.spinpro.main.pipeline;

/**
 * Interface for a job that can be put into a pipeline.
 */
public interface IPipableJob {
  /**
   * Puts the job into a pipeline.
   *
   * @throws InterruptedException if Thread got interrupted while waiting.
   */
  void enqueue() throws InterruptedException;

  /**
   * Tries putting the job into a pipeline.
   *
   * @return If it was possible to put the job into a pipeline.
   */
  boolean tryEnqueue();
}
