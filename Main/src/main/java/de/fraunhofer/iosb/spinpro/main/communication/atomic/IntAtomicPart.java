package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import java.nio.ByteBuffer;

/**
 * An integer part of a message.
 */
public class IntAtomicPart implements IAtomicPath<Integer> {

  private int value;

  /**
   * Creates a new instance.
   *
   * @param value The value to assign.
   */
  public IntAtomicPart(int value) {
    this.value = value;
  }

  /**
   * Creates a new instance.
   *
   * @param buffer The bytes of the message received.
   */
  public IntAtomicPart(ByteBuffer buffer) {
    value = buffer.getInt();
  }

  @Override
  public byte[] getBytes() {
    return ByteBuffer.allocate(4).putInt(value).array();
  }

  @Override
  public Integer getT() {
    return value;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    IntAtomicPart b = (IntAtomicPart) obj;

    return b.value == value;
  }
}
