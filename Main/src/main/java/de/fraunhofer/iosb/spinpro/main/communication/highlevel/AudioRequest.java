package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.FormatAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.LongAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.StringAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * An audio request message.
 */
public class AudioRequest implements IPart {
  private IAtomicPath<Integer> requestID;
  private IAtomicPath<String> machine;
  private IAtomicPath<String> clientID;
  private IAtomicPath<FormatAtomicPart> audio;
  private IAtomicPath<Long> length;
  private byte[] data;

  /**
   * Creates a new text request.
   *
   * @param requestID The request id.
   * @param machine   The machine.
   * @param clientID  The client id.
   * @param audio     The original sentence.
   * @param length  The length.
   * @param data The data.
   */
  public AudioRequest(IAtomicPath<Integer> requestID, IAtomicPath<String> machine, IAtomicPath<String> clientID, IAtomicPath<FormatAtomicPart> audio, IAtomicPath<Long> length, byte[] data) {
    this.requestID = requestID;
    this.machine = machine;
    this.clientID = clientID;
    this.audio = audio;
    this.length = length;
    this.data = data;
  }

  /**
   * Creates a new audio request.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public AudioRequest(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      Codec c = Codec.getFromValue(b.get());
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        case MACHINE:
          machine = new StringAtomicPart(b);
          break;
        case CLIENT_ID:
          clientID = new StringAtomicPart(b);
          break;
        case AUDIO_FORMAT:
          audio = new FormatAtomicPart(b);
          break;
        case SAMPLE_COUNT:
          length = new LongAtomicPart(b);
          break;
        case AUDIO_DATA:
          this.data = new byte[b.remaining()];
          b.get(this.data);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Creates a new text request.
   *
   * @param requestID The request id.
   * @param machine   The machine.
   * @param clientID  The client id.
   * @param audio     The original sentence.
   * @param count  The length.
   * @param data The data.
   */
  public AudioRequest(int requestID, String machine, String clientID, FormatAtomicPart audio, long count, byte[] data) {
    this(new IntAtomicPart(requestID), new StringAtomicPart(machine), new StringAtomicPart(clientID), audio, new LongAtomicPart(count), data);
  }

  /**
   * Gets request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the client id.
   *
   * @return The client id.
   */
  public String getClientID() {
    return clientID.getT();
  }

  /**
   * Gets the machine.
   *
   * @return The machine.
   */
  public String getMachine() {
    return machine.getT();
  }

  /**
   * Gets the sample count.
   *
   * @return The machine.
   */
  public Long getSampleCount() {
    return length.getT();
  }

  /**
   * Gets the audio format.
   *
   * @return The audio format.
   */
  public FormatAtomicPart getAudioFormat() {
    return audio.getT();
  }

  /**
   * Gets the audio data.
   *
   * @return The audio format.
   */
  public byte[] getData() {
    return data;
  }

  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.AUDIO_REQUEST.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    bytes.add(Codec.MACHINE.getCodec());
    addAll(bytes, machine);
    bytes.add(Codec.CLIENT_ID.getCodec());
    addAll(bytes, clientID);
    bytes.add(Codec.AUDIO_FORMAT.getCodec());
    addAll(bytes, audio);
    bytes.add(Codec.SAMPLE_COUNT.getCodec());
    addAll(bytes, length);
    bytes.add(Codec.AUDIO_DATA.getCodec());
    addAll(bytes, this.data);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    addAll(bytes, c.getBytes());
  }

  private void addAll(List<Byte> bytes, byte[] c) {
    for (byte b : c) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    AudioRequest b = (AudioRequest) obj;

    return audio.equals(b.audio) && machine.equals(b.machine) && requestID.equals(b.requestID) && clientID.equals(b.clientID) && length.equals(b.length) && Arrays.equals(data, ((AudioRequest) obj).getData());
  }
}
