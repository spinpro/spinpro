package de.fraunhofer.iosb.spinpro.main.communication;

import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;

import java.nio.ByteBuffer;

/**
 * A handler for incoming messages.
 */
public abstract class MessageHandler implements javax.websocket.MessageHandler.Whole<ByteBuffer> {

  @Override
  public void onMessage(ByteBuffer message) {
    if (message.remaining() <= 0) {
      return;
    }
    Codec c = Codec.getFromValue(message.get());
    if (c == null) {
      return;
    }
    switch (c) {
      case TEXT_REQUEST:
        onTextRequest(message);
        break;
      case AUDIO_REQUEST:
        onAudioRequest(message);
        break;
      case SUCCESS_REQUEST:
        onSuccess(message);
        break;
      case FAIL_REQUEST:
        onFail(message);
        break;
      case LOG_MESSAGE:
        onLogMessage(message);
        break;
      case STATUS_MESSAGE:
        onStatusMessage(message);
        break;
      case APPROVE:
        onApprove(message);
        break;
      case DENY:
        onDeny(message);
        break;
      case APPROVE_REQUEST:
        onApproveRequest(message);
        break;
      default:
        return;
    }
  }

  /**
   * Handles a incoming message.
   *
   * <p>
   * DEPRECATED use onMessage instead.
   * </p>
   *
   * @param message The message.
   */
  @Deprecated(forRemoval = true)
  public void handle(ByteBuffer message) {
    onMessage(message);
  }

  /**
   * Gets called if an approve message is handled.
   *
   * @param b The buffer with the message in it.
   */
  protected abstract void onApprove(ByteBuffer b);

  /**
   * Gets called if an audio request is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onAudioRequest(ByteBuffer b);

  /**
   * Gets called if a text request is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onTextRequest(ByteBuffer b);

  /**
   * Gets called if a success message is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onSuccess(ByteBuffer b);

  /**
   * Gets called if a fail message is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onFail(ByteBuffer b);

  /**
   * Gets called if an approve request is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onApproveRequest(ByteBuffer b);

  /**
   * Gets called if an deny is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onDeny(ByteBuffer b);

  /**
   * Gets called if a log message is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onLogMessage(ByteBuffer b);

  /**
   * Gets called if a status message is handled.
   *
   * @param b The buffer with the message in it.
   */
  public abstract void onStatusMessage(ByteBuffer b);
}
