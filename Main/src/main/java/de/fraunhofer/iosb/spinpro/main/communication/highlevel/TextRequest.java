package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.StringAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * A text request message.
 */
public class TextRequest implements IPart {
  private IAtomicPath<Integer> requestID;
  private IAtomicPath<String> machine;
  private IAtomicPath<String> clientID;
  private IAtomicPath<String> origSentence;

  /**
   * Creates a new text request.
   *
   * @param requestID    The request id.
   * @param machine      The machine.
   * @param clientID     The client id.
   * @param origSentence The original sentence.
   */
  public TextRequest(IAtomicPath<Integer> requestID, IAtomicPath<String> machine, IAtomicPath<String> clientID, IAtomicPath<String> origSentence) {
    this.requestID = requestID;
    this.machine = machine;
    this.clientID = clientID;
    this.origSentence = origSentence;
  }

  /**
   * Creates a new text request.
   *
   * @param requestID    The request id.
   * @param machine      The machine.
   * @param clientID     The client id.
   * @param origSentence The original sentence.
   */
  public TextRequest(int requestID, String machine, String clientID, String origSentence) {
    this(new IntAtomicPart(requestID), new StringAtomicPart(machine), new StringAtomicPart(clientID), new StringAtomicPart(origSentence));
  }

  /**
   * Creates a new text request.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public TextRequest(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      Codec c = Codec.getFromValue(b.get());
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        case MACHINE:
          machine = new StringAtomicPart(b);
          break;
        case CLIENT_ID:
          clientID = new StringAtomicPart(b);
          break;
        case SENTENCE_ORIG:
          origSentence = new StringAtomicPart(b);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Gets request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the client id.
   *
   * @return The client id.
   */
  public String getClientID() {
    return clientID.getT();
  }

  /**
   * Gets the machine.
   *
   * @return The machine.
   */
  public String getMachine() {
    return machine.getT();
  }

  /**
   * Gets the original sentence.
   *
   * @return The sentence.
   */
  public String getOrigSentence() {
    return origSentence.getT();
  }

  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.TEXT_REQUEST.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    bytes.add(Codec.MACHINE.getCodec());
    addAll(bytes, machine);
    bytes.add(Codec.CLIENT_ID.getCodec());
    addAll(bytes, clientID);
    bytes.add(Codec.SENTENCE_ORIG.getCodec());
    addAll(bytes, origSentence);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    for (byte b : c.getBytes()) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    TextRequest b = (TextRequest) obj;

    return this.clientID.equals(b.clientID) && requestID.equals(b.requestID) && this.machine.equals(b.machine) && this.origSentence.equals(b.origSentence);
  }
}
