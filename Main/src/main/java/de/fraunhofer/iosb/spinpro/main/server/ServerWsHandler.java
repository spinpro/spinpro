package de.fraunhofer.iosb.spinpro.main.server;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.main.communication.MessageHandler;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.FormatAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.AudioRequest;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.LogMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.StatusMessage;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.TextRequest;
import de.fraunhofer.iosb.spinpro.main.pipeline.JobProvider;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 * Implementation of {@link MessageHandler} for a server.
 */
public class ServerWsHandler extends MessageHandler {
  private static final int CHANNEL_COUNT = 1;
  private static final boolean BIG_ENDIAN = false;
  private static final Lock lock = new ReentrantLock();
  private ISender session;
  private List<ApproveHandler> approveHandlerList = new LinkedList<>();
  private ILogger logger;

  /**
   * Creates a new {@link ServerWsHandler}.
   * @param logger The logger to use.
   */
  public ServerWsHandler(ILogger logger) {
    this.logger = logger;
  }

  /**
   * Adds an ApproveHandler.
   *
   * @param a The ApproveHandler.
   */
  public void addApproveHandler(ApproveHandler a) {
    approveHandlerList.add(a);
  }

  /**
   * Removes an ApproveHandler.
   *
   * @param a The ApproveHandler.
   */
  public void removeApproveHandler(ApproveHandler a) {
    approveHandlerList.remove(a);
  }

  /**
   * Handles a message with a specified session.
   *
   * @param message The message.
   * @param session The session.
   */
  public void onMessage(ByteBuffer message, ISender session) {
    lock.lock();
    this.session = session;
    super.onMessage(message);
    lock.unlock();
  }

  @Override
  protected void onApprove(ByteBuffer b) {
    for (ApproveHandler a : approveHandlerList) {
      if (a.getSender().equals(session)) {
        a.onApprove(b.duplicate());
      }
    }
  }

  @Override
  public void onAudioRequest(ByteBuffer b) {
    AudioRequest request;
    try {
      request = new AudioRequest(b);
      FormatAtomicPart fat = request.getAudioFormat();
      AudioFormat af = new AudioFormat(fat.getHz(), fat.getBitDepth(), CHANNEL_COUNT, fat.isSigned(), BIG_ENDIAN);
      InputStream stream = new ByteArrayInputStream(request.getData());
      AudioInputStream ais = new AudioInputStream(stream, af, request.getSampleCount());
      logger.log(Level.FINE, Messages.GOT_AUDIO_REQUEST);
      ClientCommunicator c = new ClientCommunicator(session, request.getRequestID(), this);
      if (!new JobProvider(c).generateAudioJob(ais, request.getMachine(), request.getClientID())) {
        c.fail((byte) -1, ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        logger.log(Level.WARNING, Messages.ERROR_CREATING_JOB);
      }
    } catch (ParseException e) {
      logger.log(Level.WARNING, Messages.ERROR_PARSING_AUDIO_REQUEST, e);
    }
  }

  @Override
  public void onTextRequest(ByteBuffer b) {
    TextRequest request;
    try {
      request = new TextRequest(b);
      logger.log(Level.FINE, Messages.GOT_TEXT_REQUEST);
      ClientCommunicator c = new ClientCommunicator(session, request.getRequestID(), this);
      if (!new JobProvider(c).generateTextJob(request.getOrigSentence(), request.getMachine(), request.getClientID())) {
        c.fail((byte) -1, ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB));
        logger.log(Level.WARNING, Messages.ERROR_CREATING_JOB);
      }
    } catch (ParseException e) {
      logger.log(Level.WARNING, Messages.ERROR_PARSING_TEXT_REQUEST, e);
    }
  }

  @Override
  public void onSuccess(ByteBuffer b) {
    // Ignoring invalid sending from client.
  }

  @Override
  public void onFail(ByteBuffer b) {
    // Ignoring invalid sending from client.
  }

  @Override
  public void onApproveRequest(ByteBuffer b) {
    // Nothing to do here. Client cannot send approve request.
  }

  @Override
  public void onDeny(ByteBuffer b) {
    for (ApproveHandler a : approveHandlerList) {
      if (a.getSender().equals(session)) {
        a.onDeny(b.duplicate());
      }
    }
  }

  @Override
  public void onLogMessage(ByteBuffer b) {
    LogMessage request;
    try {
      request = new LogMessage(b);
      logger.log(Level.FINE, String.format("LOG: (%d) %S", request.getNumber(), request.getMessage()));
    } catch (ParseException e) {
      logger.log(Level.WARNING, Messages.ERROR_PARSING_MESSAGE, e);
    }
  }

  @Override
  public void onStatusMessage(ByteBuffer b) {
    StatusMessage request;
    try {
      request = new StatusMessage(b);
      logger.log(Level.FINE, String.format("STATUS: (%d) %S", request.getNumber(), request.getMessage()));
    } catch (ParseException e) {
      logger.log(Level.WARNING, Messages.ERROR_PARSING_MESSAGE, e);
    }
  }
}