package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

/**
 * A part of a message.
 */
public interface IPart {
  /**
   * Returns the byte representation of the part.
   *
   * @return The byte representation of the part.
   */
  byte[] getBytes();
}


