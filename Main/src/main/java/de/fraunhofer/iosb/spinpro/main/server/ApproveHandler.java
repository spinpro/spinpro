package de.fraunhofer.iosb.spinpro.main.server;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Approve;
import de.fraunhofer.iosb.spinpro.main.communication.highlevel.Deny;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * An handler for approvals. Can be added to a {@link ServerWsHandler}.
 */
public class ApproveHandler extends ServerWsHandler {
  private AtomicInteger sync;
  private Condition condition;
  private Lock lock;
  private ISender session;
  private int requestID;

  protected ApproveHandler(AtomicInteger sync, Lock lock, Condition condition, ISender session, int requestID) {
    super(ConfigurationProvider.getMainLogger());
    this.sync = sync;
    this.condition = condition;
    this.lock = lock;
    this.session = session;
    this.requestID = requestID;
  }

  @Override
  protected void onApprove(ByteBuffer b) {
    try {
      if (new Approve(b).getRequestID() != requestID) {
        return;
      }
    } catch (ParseException e) {
      return;
    }
    lock.lock();
    sync.set(1);
    condition.signalAll();
    lock.unlock();
  }

  @Override
  public void onDeny(ByteBuffer b) {
    try {
      if (new Deny(b).getRequestID() != requestID) {
        return;
      }
    } catch (ParseException e) {
      return;
    }
    lock.lock();
    sync.set(0);
    condition.signalAll();
    lock.unlock();
  }

  public ISender getSender() {
    return session;
  }
}
