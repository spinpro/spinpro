package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.core.CoreComponent;
import de.fraunhofer.iosb.spinpro.core.ICoreComponent;
import de.fraunhofer.iosb.spinpro.exceptions.ConstructionException;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;

/**
 * Function for Core interaction.
 */
public class CoreFunction implements IPipelineFunction<TextJob, IPipableJob> {
  private static ICoreComponent component;

  /**
   * Initializes core component to use by function if necessary.
   */
  private void init() {
    if (component == null) {
      reload();
    }
  }

  /**
   * Forcibly reinitializes core component.
   */
  public void reload() {
    component = null;
    try {
      component = new CoreComponent();
    } catch (ConstructionException e) {
      ConfigurationProvider.getMainLogger().log(Level.SEVERE, Messages.ERROR_GETTING_RESULT, e);
    }
  }

  /**
   * Passes a job to the core.
   *
   * @param job The job to process.
   * @return The processed job.
   * @throws InterruptedException If interrupted during execution.
   */
  @Override
  public IPipableJob apply(TextJob job) throws InterruptedException {
    if (job == null) {
      ConfigurationProvider.getMainLogger().log(Level.WARNING, Messages.PIPELINE_STEP_RETURNED_NULL);
      return null;
    }
    init();
    job.getLogger().log(Level.FINE, Messages.ENTERED_RULE_CREATION_STEP);

    File rule = component.createRule(job.getText(), job.getMachine(), job.getClientID(), job.getLogger());
    if (rule == null) {
      job.getClientCommunicator().fail((byte) -1, ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_GETTING_RESULT));
      job.getLogger().log(Level.WARNING, Messages.ERROR_GETTING_RESULT);
      return null;
    }
    String str;
    try {
      str = Files.readString(rule.toPath());
    } catch (IOException e) {
      job.getClientCommunicator().fail((byte) -1, e.getMessage());
      job.getLogger().log(Level.WARNING, Messages.ERROR_GETTING_RESULT, e);
      return null;
    }

    job.getClientCommunicator().success(str);
    job.getLogger().log(Level.INFO, str);
    return null;
  }
}
