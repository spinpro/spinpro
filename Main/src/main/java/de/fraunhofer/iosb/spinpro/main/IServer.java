package de.fraunhofer.iosb.spinpro.main;

/**
 * Interface for initialization of the ClientInterface module.
 */
public interface IServer {
  /**
   * Stops the system.
   */
  void stop();
}
