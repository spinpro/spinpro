package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

/**
 * Function to approve a job.
 */
public class ApproveFunction implements IPipelineFunction<ApproveJob, IPipableJob> {
  private static final String ERROR_CREATING_JOB = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_CREATING_JOB);
  private static final String JOB_GOT_NOT_APPROVED = ConfigurationProvider.getMessageBundle().getMessage(Messages.JOB_GOT_NOT_APPROVED);

  @Override
  public IPipableJob apply(ApproveJob job) throws InterruptedException {
    if (job == null) {
      ConfigurationProvider.getMainLogger().log(Level.WARNING, Messages.PIPELINE_STEP_RETURNED_NULL);
      return null;
    }
    job.getLogger().log(Level.FINE, Messages.ENTERED_APPROVE_STEP);
    if (job.getClientCommunicator().approve(job.getText())) {
      try {
        TextJob a = new TextJob(job, job.getText());
        job.getLogger().log(Level.FINE, Messages.JOB_GOT_APPROVED);
        return a;
      } catch (InstantiationException e) {
        job.getLogger().log(Level.SEVERE, Messages.ERROR_CREATING_JOB);
        job.getClientCommunicator().fail((byte) -1, ERROR_CREATING_JOB);
        return null;
      }
    }
    job.getLogger().log(Level.WARNING, Messages.JOB_GOT_NOT_APPROVED);
    job.getClientCommunicator().fail((byte) -1, JOB_GOT_NOT_APPROVED);
    return null;
  }
}
