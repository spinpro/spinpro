package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;

import javax.sound.sampled.AudioInputStream;

/**
 * Class representing a sound based client request.
 */
public class AudioJob extends AbstractJob {

  /**
   * The pipeline step this job will use.
   */
  private IPipelineStep<AudioJob> pipeline;

  /**
   * The input audio data of this job.
   */
  private AudioInputStream audio;

  /**
   * Creates a new job.
   *
   * @param clientCommunicator The client communicator object.
   * @param logger             The logger to be used for this job.
   * @param audioInput         The input audio data.
   * @param machine The machine.
   * @param clientID The client id.
   * @throws InstantiationException If the job could not get it's pipeline step.
   */
  public AudioJob(IClientCommunicator clientCommunicator, ILogger logger, AudioInputStream audioInput, String machine, String clientID) throws InstantiationException {
    super(clientCommunicator, logger, machine, clientID);
    audio = audioInput;
    pipeline = PipelineSteps.getAudioStep();
  }

  /**
   * Getter for this job's input audio.
   *
   * @return The audio attribute
   */
  public AudioInputStream getAudio() {
    return audio;
  }

  /**
   * Puts this job into the respective pipeline step, blocking until possible.
   *
   * @throws InterruptedException if Thread got interrupted while waiting.
   */
  @Override
  public void enqueue() throws InterruptedException {
    enqueueGen(pipeline, this);
  }

  /**
   * Tries putting this job into the respective pipeline step.
   *
   * @return If it was possible to put this job into the pipeline step.
   */
  @Override
  public boolean tryEnqueue() {
    return tryEnqueueGen(pipeline, this);
  }
}
