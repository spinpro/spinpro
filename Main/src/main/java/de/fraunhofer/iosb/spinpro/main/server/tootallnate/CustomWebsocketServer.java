package de.fraunhofer.iosb.spinpro.main.server.tootallnate;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.Logger;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.server.ServerWsHandler;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.logging.Level;

/**
 * Decoration of a {@link WebSocketServer} to use as server.
 */
public class CustomWebsocketServer extends WebSocketServer {
  private static final Logger logger = new Logger("tootallnate.server");
  private ServerWsHandler handler = new ServerWsHandler(logger);
  private int maxCon = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.MAX_CONNECTIONS));

  /**
   * Creates a new {@link CustomWebsocketServer}.
   * @param inetSocketAddress The socket address to use.
   */
  public CustomWebsocketServer(InetSocketAddress inetSocketAddress) {
    super(inetSocketAddress);
    //setConnectionLostTimeout(Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.CLIENT_TIMEOUT)));
  }

  @Override
  protected boolean onConnect(SelectionKey key) {
    return getConnections().size() <= maxCon;
  }

  @Override
  public void onOpen(WebSocket conn, ClientHandshake handshake) {
    logger.log(Level.INFO, Messages.CLIENT_CONNECTED);
    logger.log(Level.INFO, ConfigurationProvider.getMessageBundle().getMessage(Messages.CLIENT_COUNT) + ": " + getConnections().size());
  }

  @Override
  public void onClose(WebSocket conn, int code, String reason, boolean remote) {
    logger.log(Level.INFO, Messages.CLIENT_DISCONNECTED);
    logger.log(Level.INFO, ConfigurationProvider.getMessageBundle().getMessage(Messages.CLIENT_COUNT) + ": " + getConnections().size());
  }

  @Override
  public void onMessage(WebSocket conn, String message) {
    // IGNORING
  }

  @Override
  public void onMessage(WebSocket conn, ByteBuffer message) {
    handler.onMessage(message, new TooTallNateSender(conn));
  }

  @Override
  public void onError(WebSocket conn, Exception ex) {
    logger.log(Level.SEVERE, Messages.EXCEPTION_WHILE_CREATION, ex);
    ex.printStackTrace();
  }

  @Override
  public void onStart() {
    logger.log(Level.INFO, Messages.SERVER_STARTED);
    setConnectionLostTimeout(0);
    setConnectionLostTimeout(Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.CLIENT_TIMEOUT)));
  }
}
