package de.fraunhofer.iosb.spinpro.main.communication.enums;

public enum SampleRate {
  HZ8000((byte) 0x01, 8000), HZ11025((byte) 0x02, 11025), HZ16000((byte) 0x03, 16000), HZ22050((byte) 0x04, 22050), HZ32000((byte) 0x05, 32000), HZ37800((byte) 0x06, 37800), HZ44056((byte) 0x07, 44056), HZ44100((byte) 0x08, 44100),
  HZ47250((byte) 0x09, 47250), HZ48000((byte) 0x0a, 48000), HZ50000((byte) 0x0b, 50000), HZ50400((byte) 0x0c, 50400), HZ64000((byte) 0x0d, 64000), HZ88200((byte) 0x0e, 88200), HZ96000((byte) 0x0f, 96000), HZ176400((byte) 0x10, 176400),
  HZ192000((byte) 0x11, 192000), HZ352800((byte) 0x12, 352800), HZ2822400((byte) 0x13, 2822400), HZ5644800((byte) 0x14, 5644800), HZ11289600((byte) 0x15, 11289600), HZ22579200((byte) 0x16, 22579200),
  ;

  private final byte codec;
  private final int hz;

  SampleRate(byte b, int i) {
    this.codec = b;
    this.hz = i;
  }

  /**
   * Gets the sample rate from a code.
   *
   * @param a The code.
   * @return The sample rate.
   */
  public static SampleRate getFromValue(byte a) {
    byte b = (byte) (a & 0x1f);
    for (SampleRate s : SampleRate.values()) {
      if (s.getCodec() == b) {
        return s;
      }
    }
    return null;
  }

  /**
   * Gets the sample rate from the value.
   *
   * @param a The value.
   * @return The sample rate.
   */
  public static SampleRate getFromSampleRate(int a) {
    for (SampleRate s : SampleRate.values()) {
      if (s.getHz() == a) {
        return s;
      }
    }
    return null;
  }

  /**
   * Gets the Code of this value.
   *
   * @return The code.
   */
  public byte getCodec() {
    return codec;
  }

  /**
   * Gets the sample rate.
   *
   * @return The sample rate.
   */
  public int getHz() {
    return hz;
  }
}
