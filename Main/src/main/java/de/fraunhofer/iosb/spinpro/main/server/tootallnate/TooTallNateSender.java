package de.fraunhofer.iosb.spinpro.main.server.tootallnate;

import de.fraunhofer.iosb.spinpro.main.server.ISender;
import org.java_websocket.WebSocket;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Implementation of a {@link ISender} for {@link TooTallNateServer}.
 */
public class TooTallNateSender implements ISender {
  WebSocket conn;

  /**
   * Creates a new {@link TooTallNateSender}.
   *
   * @param conn The {@link WebSocket} to use.
   */
  public TooTallNateSender(WebSocket conn) {
    this.conn = conn;
  }

  @Override
  public void sendBinary(ByteBuffer data) {
    conn.send(data.array());
  }

  @Override
  public void close() {
    conn.close();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    TooTallNateSender o = (TooTallNateSender) obj;
    return Objects.equals(o.conn, this.conn);
  }
}
