package de.fraunhofer.iosb.spinpro.main.communication.enums;

public enum Signed {
  UNSIGNED((byte) 0x00), SIGNED((byte) 0x80),
  ;
  private final byte codec;

  Signed(byte codec) {
    this.codec = codec;
  }

  /**
   * Gets the signed from a code.
   *
   * @param a The code.
   * @return The signed value.
   */
  public static Signed getFromValue(byte a) {
    byte b = (byte) (a & 0x80);
    for (Signed s : Signed.values()) {
      if (s.getCodec() == b) {
        return s;
      }
    }
    return null;
  }

  /**
   * Gets the Code of this value.
   *
   * @return The code.
   */
  public byte getCodec() {
    return codec;
  }
}
