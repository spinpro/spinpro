package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * An audio frame message.
 */
@Deprecated
public class AudioFrame implements IPart {
  private IAtomicPath<Integer> requestID;
  private IAtomicPath<Integer> frameNumber;
  private IAtomicPath<Integer> frameLength;
  private byte[] data;

  /**
   * Creates a new audio frame.
   *
   * @param requestID   The request id.
   * @param frameNumber The frame number.
   * @param frameLength The frame length.
   * @param data        The data.
   */
  public AudioFrame(IAtomicPath<Integer> requestID, IAtomicPath<Integer> frameNumber, IAtomicPath<Integer> frameLength, byte[] data) {
    this.requestID = requestID;
    this.frameNumber = frameNumber;
    this.frameLength = frameLength;
    this.data = data;
  }

  /**
   * Creates a new audio frame.
   *
   * @param requestID   The request id.
   * @param frameNumber The frame number.
   * @param frameLength The frame length.
   * @param data        The data.
   */
  public AudioFrame(int requestID, int frameNumber, int frameLength, byte[] data) {
    this(new IntAtomicPart(requestID), new IntAtomicPart(frameNumber), new IntAtomicPart(frameLength), data);
  }

  /**
   * Creates a new audio frame.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public AudioFrame(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      Codec c = Codec.getFromValue(b.get());
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        case FRAME_NUMBER:
          frameNumber = new IntAtomicPart(b);
          break;
        case AUDIO_LENGTH:
          frameLength = new IntAtomicPart(b);
          break;
        case AUDIO_DATA:
          if (frameLength == null || frameLength.getT() <= 0) {
            throw new ParseException("Wrong length.", 0);
          }
          data = new byte[frameLength.getT()];
          b.get(data);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Gets the request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the frame number.
   *
   * @return The frame number.
   */
  public int getFrameNumber() {
    return frameNumber.getT();
  }

  /**
   * Gets the frame length.
   *
   * @return The frame number.
   */
  public int getFrameLength() {
    return frameLength.getT();
  }

  /**
   * Gets the audio data length.
   *
   * @return The data length.
   */
  public int getAudioDataLength() {
    return frameLength.getT();
  }

  public byte[] getData() {
    return data;
  }

  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.AUDIO_FRAME.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    bytes.add(Codec.FRAME_NUMBER.getCodec());
    addAll(bytes, frameNumber);
    bytes.add(Codec.AUDIO_LENGTH.getCodec());
    addAll(bytes, frameLength);
    bytes.add(Codec.AUDIO_DATA.getCodec());
    addAll(bytes, data);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    addAll(bytes, c.getBytes());
  }

  private void addAll(List<Byte> bytes, byte[] c) {
    for (byte b : c) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    AudioFrame b = (AudioFrame) obj;

    return frameLength.equals(b.frameLength) && frameNumber.equals(b.frameNumber) && requestID.equals(b.requestID) && Arrays.equals(b.data, data);
  }

}
