package de.fraunhofer.iosb.spinpro.main.communication.atomic;

import java.nio.ByteBuffer;

/**
 * A error part of a message.
 */
public class ErrorAtomicPart implements IAtomicPath<ErrorAtomicPart> {

  private byte code;
  private IAtomicPath<String> message;

  /**
   * Creates a new instance.
   *
   * @param code   The error code.
   * @param string The value to assign.
   */
  public ErrorAtomicPart(byte code, String string) {
    this.code = code;
    message = new StringAtomicPart(string);
  }

  /**
   * Creates a new instance.
   *
   * @param code   The error code.
   * @param buffer The bytes of the message received.
   */
  public ErrorAtomicPart(byte code, ByteBuffer buffer) {
    this.code = code;
    message = new StringAtomicPart(buffer);
  }

  /**
   * Gets the error code.
   *
   * @return The error code.
   */
  public byte getCode() {
    return code;
  }

  /**
   * Gets the message.
   *
   * @return The message.
   */
  public String getMessage() {
    return message.getT();
  }

  @Override
  public byte[] getBytes() {
    byte[] a = message.getBytes();
    byte[] b = new byte[a.length + 1];
    b[0] = code;
    System.arraycopy(a, 0, b, 1, a.length);
    return b;
  }

  @Override
  public ErrorAtomicPart getT() {
    return this;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    ErrorAtomicPart b = (ErrorAtomicPart) obj;

    return b.code == code && message.equals(b.message);
  }
}
