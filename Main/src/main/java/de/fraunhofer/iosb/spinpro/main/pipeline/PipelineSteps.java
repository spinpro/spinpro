package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

/**
 * Class for storing the static pipeline step constants.
 */
public class PipelineSteps {

  private static IPipelineStep<AudioJob> AUDIO_STEP;
  private static IPipelineStep<TextJob> TEXT_STEP;
  private static IPipelineStep<ApproveJob> APPROVE_STEP;

  private static IPipelineFunction<AudioJob, IPipableJob> speechToTextFunction = new SpeechToTextFunction();
  private static IPipelineFunction<ApproveJob, IPipableJob> approveFunction = new ApproveFunction();
  private static IPipelineFunction<TextJob, IPipableJob> coreFunction = new CoreFunction();

  private static ILogger logger = ConfigurationProvider.getMainLogger();
  private static final String ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT);
  private static final String ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT);
  private static final String ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE);
  private static final String ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE);
  private static final String ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE);
  private static final String ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT = ConfigurationProvider.getMessageBundle().getMessage(Messages.ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT);

  /**
   * Terminates all pipeline steps and replaces them with new ones.
   */
  public static void reset() {
    logger = ConfigurationProvider.getMainLogger();
    if (AUDIO_STEP != null) {
      AUDIO_STEP.terminate();
      AUDIO_STEP = null;
    }
    if (APPROVE_STEP != null) {
      APPROVE_STEP.terminate();
      APPROVE_STEP = null;
    }
    if (TEXT_STEP != null) {
      TEXT_STEP.terminate();
      TEXT_STEP = null;
    }
  }

  /**
   * Initializes the audio pipeline step.
   *
   * @return The pipeline step.
   * @throws InstantiationException If initializing the pipeline step was impossible.
   */
  private static IPipelineStep<AudioJob> initializeAudioStep() throws InstantiationException {
    int audioWorkers = 0;
    int audioBuffer = 0;
    try {
      audioWorkers = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_COUNT));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_COUNT);
    }
    try {
      audioBuffer = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.SPEECH_TO_TEXT_WORKER_BUFFER_SIZE));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_SPEECH_TO_TEXT_WORKER_BUFFER_SIZE);
    }
    return new PipelineStep<AudioJob>(audioBuffer, true, audioWorkers, speechToTextFunction);
  }

  /**
   * Initializes the text pipeline step.
   *
   * @return The pipeline step.
   * @throws InstantiationException If initializing the pipeline step was impossible.
   */
  private static IPipelineStep<TextJob> initializeTextStep() throws InstantiationException {
    int textWorkers = 0;
    int textBuffer = 0;
    try {
      textWorkers = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.CORE_WORKER_COUNT));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_CORE_WORKER_COUNT);
    }
    try {
      textBuffer = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.CORE_WORKER_BUFFER_SIZE));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_CORE_WORKER_BUFFER_SIZE);
    }
    return new PipelineStep<TextJob>(textBuffer, true, textWorkers, coreFunction);
  }

  /**
   * Initializes the approve pipeline step.
   *
   * @return The pipeline step.
   * @throws InstantiationException If initializing the pipeline step was impossible.
   */
  private static IPipelineStep<ApproveJob> initializeApproveStep() throws InstantiationException {
    int approveWorkers = 0;
    int textBuffer = 0;
    try {
      approveWorkers = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.APPROVE_WORKER_COUNT));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_APPROVE_WORKER_COUNT);
    }
    try {
      textBuffer = Integer.parseInt(ConfigurationProvider.getMainPreferences().getValue(PreferenceStrings.APPROVE_WORKER_BUFFER_SIZE));
    } catch (NumberFormatException e) {
      logger.log(Level.SEVERE, ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE);
      throw new InstantiationException(ERROR_PARSING_CONFIGURED_APPROVE_WORKER_BUFFER_SIZE);
    }
    return new PipelineStep<ApproveJob>(textBuffer, true, approveWorkers, approveFunction);
  }

  /**
   * Gets the audio pipeline Step.
   *
   * @return The audio pipeline step.
   * @throws InstantiationException If the pipeline step could not be created.
   */
  public static IPipelineStep<AudioJob> getAudioStep() throws InstantiationException {
    if (AUDIO_STEP == null) {
      AUDIO_STEP = initializeAudioStep();
    }
    return AUDIO_STEP;
  }

  /**
   * Gets the text pipeline Step.
   *
   * @return The text pipeline step.
   * @throws InstantiationException If the pipeline step could not be created.
   */
  public static IPipelineStep<TextJob> getTextStep() throws InstantiationException {
    if (TEXT_STEP == null) {
      TEXT_STEP = initializeTextStep();
    }
    return TEXT_STEP;
  }

  /**
   * Gets the approve pipeline Step.
   *
   * @return The approve pipeline step.
   * @throws InstantiationException If the pipeline step could not be created.
   */
  public static IPipelineStep<ApproveJob> getApproveStep() throws InstantiationException {
    if (APPROVE_STEP == null) {
      APPROVE_STEP = initializeApproveStep();
    }
    return APPROVE_STEP;
  }
}
