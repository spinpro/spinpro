package de.fraunhofer.iosb.spinpro.main.pipeline;

/**
 * The function to apply in an pipeline step.
 *
 * @param <T> The Object to use on.
 * @param <R> The Object to be returned.
 */
@FunctionalInterface
public interface IPipelineFunction<T, R> {

  /**
   * Applies function.
   *
   * @param t The function argument.
   * @return The function result.
   * @throws InterruptedException If thread got interrupted during execution.
   */
  R apply(T t) throws InterruptedException;
}
