package de.fraunhofer.iosb.spinpro.main.pipeline;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.main.IClientCommunicator;
import de.fraunhofer.iosb.spinpro.messages.Messages;

/**
 * Class that represents a client request.
 */
public abstract class AbstractJob implements IPipableJob {

  protected static final String SERVER_STOPPED = ConfigurationProvider.getMessageBundle().getMessage(Messages.SERVER_STOPPED);

  /**
   * The logger to use for this job.
   */
  private ILogger logger;
  /**
   * The client communicator object for client communication.
   */
  private IClientCommunicator clientCommunicator;
  private String machine;
  private String clientID;

  /**
   * Creates a new job with a given client communicator object.
   *
   * @param clientCommunicator The client communicator object.
   * @param logger             The logger to be used for this job.
   * @param machine            The machine.
   * @param clientID           The client id.
   */
  public AbstractJob(IClientCommunicator clientCommunicator, ILogger logger, String machine, String clientID) {
    this.machine = machine;
    this.clientID = clientID;
    this.clientCommunicator = clientCommunicator;
    this.logger = logger;
  }

  /**
   * Getter for this job's logger.
   *
   * @return The logger attribute
   */
  public ILogger getLogger() {
    return logger;
  }

  /**
   * Getter for this job's client communicator.
   *
   * @return The clientCommunicator attribute
   */
  public IClientCommunicator getClientCommunicator() {
    return clientCommunicator;
  }

  protected final <T> void enqueueGen(IPipelineStep<T> step, T input) throws InterruptedException {
    step.addJob(input);
  }

  protected final <T> boolean tryEnqueueGen(IPipelineStep<T> step, T input) {
    return step.tryAddJob(input);
  }

  public String getMachine() {
    return this.machine;
  }

  public String getClientID() {
    return this.clientID;
  }
}
