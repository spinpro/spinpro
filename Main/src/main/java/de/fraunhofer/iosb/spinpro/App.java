package de.fraunhofer.iosb.spinpro;

import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.IPreferences;
import de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings;
import de.fraunhofer.iosb.spinpro.main.IServer;
import de.fraunhofer.iosb.spinpro.main.pipeline.PipelineSteps;
import de.fraunhofer.iosb.spinpro.main.server.tootallnate.TooTallNateServer;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.MessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;

/**
 * The class for the start of an new instance of the program.
 */
public class App {
  private static IServer server;

  /**
   * Testing only!.
   */
  public static void close() {
    if (server != null) {
      try {
        server.stop();
      } catch (Exception e) {
        ConfigurationProvider.getMainLogger().log(Level.SEVERE, Messages.EXCEPTION_WHILE_TERMINATION, e);
      }
    }
    ConfigurationProvider.getMainLogger().log(Level.SEVERE, ConfigurationProvider.getMessageBundle().getMessage(Messages.SHUTDOWN_THROUGH_USER_INTERRUPT));
    PipelineSteps.reset();
  }

  /**
   * The main entrypoint of the program.
   *
   * @param args Console arguments.
   */
  public static void main(String[] args) {
    // Clear terminal
    try {
      final String os = System.getProperty("os.name");

      if (os.contains("Windows")) {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
      } else {
        new ProcessBuilder("clear").inheritIO().start().waitFor();
      }
    } catch (final Exception e) {
      //haven't cleared the screen, so no worries.
    }
    printLogo();


    if (args.length != 1) {
      System.err.println(new MessageBundle(PreferenceStrings.MESSAGES_BASE_PATH.getDefault(), Locale.getDefault()).getMessage(Messages.WRONG_ARGUMENT_COUNT) + (args.length));
      System.exit(-1);
    }
    File path = new File(args[0]);
    if (!path.exists() || path.isDirectory()) {
      System.err.println(new MessageBundle(PreferenceStrings.MESSAGES_BASE_PATH.getDefault(), Locale.getDefault()).getMessage(Messages.CONFIG_FILE_NOT_FOUND));
      System.exit(-1);
    }
    ConfigurationProvider.init(path.getAbsolutePath());
    IMessageBundle mb = ConfigurationProvider.getMessageBundle();

    ConfigurationProvider.getMainLogger().log(Level.INFO, mb.getMessage(Messages.INITIALISATION_COMPLETE));
    try {
      server = new TooTallNateServer();
    } catch (Exception e) {
      ConfigurationProvider.getMainLogger().log(Level.SEVERE, Messages.EXCEPTION_WHILE_CREATION, e);
    }
    Runtime.getRuntime().addShutdownHook(new ShutdownHock(server));
    Object waiting = new Object();
    IPreferences prefs = ConfigurationProvider.getMainPreferences();
    while (true) {
      try {
        synchronized (waiting) {
          long expiration = Long.parseLong(prefs.getValue(PreferenceStrings.LOG_EXPIRATION_TIME));
          waiting.wait(Long.parseLong(prefs.getValue(PreferenceStrings.MAINTENANCE_INTERVAL)));
          File logDir = new File(prefs.getValue(PreferenceStrings.LOG_DIR));
          if (logDir.exists() && logDir.isDirectory()) {
            boolean removed = false;
            for (File f : Objects.requireNonNull(logDir.listFiles())) {
              if (f.isFile() && f.getName().contains("Job") && (new Date().getTime() - f.lastModified()) > expiration) {
                try {
                  removed |= Files.deleteIfExists(f.toPath());
                } catch (IOException e) {
                  // Just ignoring, it's just a log.
                }
              }
            }
            if (removed) {
              ConfigurationProvider.getMainLogger().log(Level.FINEST, Messages.PERFORMED_MAINTENANCE);
            }
          }
        }
      } catch (InterruptedException e) {
        break;
      }
    }
  }

  private static void printLogo() {
    System.err.println("                                 -.                                                                           \n"
                       + "  .odNmmmdy                     .Mm                                    ``.....``                              \n"
                       + " -NN:`   .+                      ``                              ./oydNNMMMMMMMMNmho:`                        \n"
                       + " +Mh           /dssddmdo`    -hhhdy       +doohdNms`             ohNMMMMMMMMMMMMMMMMMNys`     /-              \n"
                       + " `hMds+/-`     oMN/```/Mm`    ``-Mm       oMN:```sMs               `-omMMMMMMMMMMMMMMMMMm:    dNh:            \n"
                       + "   -+syhmNy.   oMy     hM+      .Mm       oMs    -Mm         `.--smmmNMMMMMMMMMMMMMMMMMMMNos. yMMMh-          \n"
                       + "       ``oMd   oMs     sMo      .Mm       oMo    .Mm      .+yNNNNMMMMMMMMMMMNNmmNNMMMMMMMMMMm-NMMMMN+         \n"
                       + " ``      :Mm   oMm`   `mM:      .Mm       oMo    .Mm    -yNMMMMMMMMMMMNhs/-.``````.:+ydNMMMMMNMMMMMMMy`       \n"
                       + " -mho+++yNd:   oMmd+/+dN+   `+++sMN+++/   oMo    .Mm  `sNMMMMMMMMMMNy:`               `.+hMMMMMMMMMMMMy       \n"
                       + "  .:+oo+/-`    oMs./oo/.    `/////////:   ./.    `/: -mMMMMMMMMMMmo.    `:+shddddhs+:`   `-yNMMMMMMMMMMo      \n"
                       + "               oMs                                  /NMMMMMMMMMMy.    :ymNMMNdhyhNMMMmy/`   :dMMMMMMMMMN.     \n"
                       + "               :y/                                 /NMMMMMMMMMM+    :hMMMMMh-/ss+-oMMMMMd:   `yMMMMMMMMMo     \n"
                       + "                                                  .NMMMMMMMMMM+    +NMMMMMM./MMMMs mMMMMMNo   `hMMMMMMMMs     \n"
                       + "            +++++++++/-.                          yMMMmMMMMMMd    /MMMMMMMM`+MMMMh mMMMMMMM+   .NMMMMMMM:     \n"
                       + "           `MMMdddddmNNmy-                       `Nmy:/MMMMMM/    mMMMMMNMM`+MMMMh mMNMMMMMN`   yMMMMMMy      \n"
                       + "           `MMM.````.-sMMN:                      `:.  yMMMMMM.   .MMMMMM/oM./MMMMy my-NMMMMM-   oMMMMMM-   `  \n"
                       + "           `MMM`       hMMh      `oo+ ./shhhs+`       yMMMMMM-   .MMMMMMo`ds.ohhs-+m-:MMMMMM-   oMMMMMd``-sh  \n"
                       + "           `MMM`       yMMh      `MMm+mmhyyydm/      /NMMMMMMo    hMMMMMNo-oysoooys-/mMMMMMd    dMMMMMyydNMy  \n"
                       + "           `MMM.    `.oMMN/      `MMMm/`     .`      hMMMMMMMm`   .mMMMMMMms//:://odMMMMMMN-   :MMMMMMMMMMM-  \n"
                       + "           `MMMhhhhhdNMNd:       `MMM-               yMMMMMMMMy`   .hMMMMMMMMN/.NNMMMMMMMd-   .mMMMMMMMMMMs   \n"
                       + "           `MMMysssss+/-         `MMm                oMMMMMMMMMy.   `+dNMMMo//-.///hMMMm+`   -mMMMMMMMMMMy`   \n"
                       + "           `MMM`                 `MMd                .NMMMMMMMMMm/`   `:sdNmmNNNNmmmds:`   `oNMMMMMMMMMMy`    \n"
                       + "           `MMM`                 `MMd                 /NMMMMMMMMMMh/`     .-//++//-.     .omMMMMMMMMMMN+      \n"
                       + "           `MMM`                 `MMd                  /NMMMMMMMMMMMms/.`            `.+hNMMMMMMMMMMNh-       \n"
                       + "           `MMM`                 `MMd                   :mMMMMMMMMMMMMMNdys+/::::/oshmMMMMMMMMMMMMNy-         \n"
                       + "            ///                   //:                    `yMMMMNyNMMMMMMMMMMMMMMMMMMMMMMMMMMNNNms:`           \n"
                       + "                                                           :dMMN``ommMMMMMMMMMMMMMMMMMNhyyyo``                \n"
                       + "                                                             /dM-   `yMMMMMMMMMMMMMMMMNh+-`                   \n"
                       + "                                                               -`     :smMMMMMMMMMMMMMMMMMNh`                 \n"
                       + "                                                                         .:+syhhddhhyso+/-.                   \n"
                       + "                                                                                                              ");
  }

  private static class ShutdownHock extends Thread {
    private IServer server;

    private ShutdownHock(IServer server) {
      this.server = server;
    }

    public void run() {
      if (server != null) {
        try {
          server.stop();
        } catch (Exception e) {
          ConfigurationProvider.getMainLogger().log(Level.SEVERE, Messages.EXCEPTION_WHILE_TERMINATION, e);
        }
      }
      ConfigurationProvider.getMainLogger().log(Level.SEVERE, ConfigurationProvider.getMessageBundle().getMessage(Messages.SHUTDOWN_THROUGH_USER_INTERRUPT));
    }
  }
}