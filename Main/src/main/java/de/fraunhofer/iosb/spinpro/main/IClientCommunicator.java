package de.fraunhofer.iosb.spinpro.main;

/**
 * Interface for the protocol for client connection.
 */
public interface IClientCommunicator {

  /**
   * Approves a job blocking.
   * @param sentence The sentence recognized.
   * @return If a job could be approved.
   */
  boolean approve(String sentence);

  /**
   * Sends a log message.
   * @param message The message to send.
   * @param number The number to send.
   */
  void sendLog(String message, int number);

  /**
   * Sends a status message.
   * @param message The message to send.
   * @param number The number to send.
   */
  void sendStatus(String message, int number);

  /**
   * Sends a fail response.
   * @param errorNumber The error number to send.
   * @param message The message to send.
   */
  void fail(byte errorNumber, String message);

  /**
   * Sends a result.
   * @param rule The result.
   */
  void success(String rule);
}
