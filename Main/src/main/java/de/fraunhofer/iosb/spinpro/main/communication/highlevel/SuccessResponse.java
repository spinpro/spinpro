package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.StringAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * A success message.
 */
public class SuccessResponse implements IPart {
  private IAtomicPath<Integer> requestID;
  private IAtomicPath<String> rule;

  /**
   * Creates a new success message.
   *
   * @param requestID The request id.
   * @param rule      The rule.
   */
  public SuccessResponse(IAtomicPath<Integer> requestID, IAtomicPath<String> rule) {
    this.requestID = requestID;
    this.rule = rule;
  }

  /**
   * Creates a new success message.
   *
   * @param requestID The request id.
   * @param rule      The rule.
   */
  public SuccessResponse(int requestID, String rule) {
    this(new IntAtomicPart(requestID), new StringAtomicPart(rule));
  }

  /**
   * Creates a new success message.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public SuccessResponse(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      Codec c = Codec.getFromValue(b.get());
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        case RULE:
          rule = new StringAtomicPart(b);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Gets the request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the rule.
   *
   * @return The rule.
   */
  public String getRule() {
    return rule.getT();
  }

  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.SUCCESS_REQUEST.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    bytes.add(Codec.RULE.getCodec());
    addAll(bytes, rule);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    for (byte b : c.getBytes()) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    SuccessResponse b = (SuccessResponse) obj;

    return this.rule.equals(b.rule) && requestID.equals(b.requestID);
  }
}
