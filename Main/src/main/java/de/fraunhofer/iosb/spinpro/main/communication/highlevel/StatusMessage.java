package de.fraunhofer.iosb.spinpro.main.communication.highlevel;

import de.fraunhofer.iosb.spinpro.main.communication.atomic.IAtomicPath;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.IntAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.atomic.StringAtomicPart;
import de.fraunhofer.iosb.spinpro.main.communication.enums.Codec;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

/**
 * A approve message.
 */
public class StatusMessage implements IPart {
  private IAtomicPath<Integer> requestID;
  private IAtomicPath<String> message;
  private IAtomicPath<Integer> number;

  /**
   * Creates a new status message.
   *
   * @param requestID The request id.
   * @param message   The recSentence.
   * @param number    The number to set.
   */
  public StatusMessage(IAtomicPath<Integer> requestID, IAtomicPath<String> message, IAtomicPath<Integer> number) {
    this.requestID = requestID;
    this.message = message;
    this.number = number;
  }

  /**
   * Creates a new status message.
   *
   * @param requestID The request id.
   * @param message   The rule.
   * @param number    The number.
   */
  public StatusMessage(int requestID, String message, int number) {
    this(new IntAtomicPart(requestID), new StringAtomicPart(message), new IntAtomicPart(number));
  }

  /**
   * Creates a new status message.
   *
   * @param b The byte representation.
   * @throws ParseException If not parsable.
   */
  public StatusMessage(ByteBuffer b) throws ParseException {
    while (b.remaining() > 0) {
      Codec c = Codec.getFromValue(b.get());
      switch (c) {
        case REQUEST_ID:
          requestID = new IntAtomicPart(b);
          break;
        case MESSAGE:
          message = new StringAtomicPart(b);
          break;
        case NUMBER:
          number = new IntAtomicPart(b);
          break;
        default:
          throw new ParseException("Could not find this opcode.", 0);
      }
    }
  }

  /**
   * Gets the request id.
   *
   * @return The request id.
   */
  public int getRequestID() {
    return requestID.getT();
  }

  /**
   * Gets the rule.
   *
   * @return The rule.
   */
  public String getMessage() {
    return message.getT();
  }

  /**
   * Gets the rule.
   *
   * @return The rule.
   */
  public int getNumber() {
    return number.getT();
  }

  @Override
  public byte[] getBytes() {
    List<Byte> bytes = new LinkedList<>();
    bytes.add(Codec.STATUS_MESSAGE.getCodec());
    bytes.add(Codec.REQUEST_ID.getCodec());
    addAll(bytes, requestID);
    bytes.add(Codec.MESSAGE.getCodec());
    addAll(bytes, message);
    bytes.add(Codec.NUMBER.getCodec());
    addAll(bytes, number);
    return ArrayUtils.toPrimitive(bytes.toArray(new Byte[0]));
  }

  private void addAll(List<Byte> bytes, IPart c) {
    for (byte b : c.getBytes()) {
      bytes.add(b);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }

    if (obj == this) {
      return true;
    }

    if (!obj.getClass().equals(getClass())) {
      return false;
    }

    StatusMessage b = (StatusMessage) obj;

    return this.message.equals(b.message) && requestID.equals(b.requestID) && number.equals(b.number);
  }
}
