package de.fraunhofer.iosb.spinpro.main.communication.enums;

/**
 * An enum for the bit depth.
 */
public enum BitDepth {
  Bit8((byte) 0x00, 8), Bit16((byte) 0x20, 16), Bit32((byte) 0x40, 32), Bit64((byte) 0x60, 64),
  ;

  private final byte codec;
  private final int depth;

  BitDepth(byte b, int i) {
    this.codec = b;
    this.depth = i;
  }

  /**
   * Gets the bit depth from a code.
   *
   * @param a The code.
   * @return The bit depth.
   */
  public static BitDepth getFromValue(byte a) {
    byte b = (byte) (a & 0x60);
    for (BitDepth s : BitDepth.values()) {
      if (s.getCodec() == b) {
        return s;
      }
    }
    return null;
  }

  /**
   * Gets the bit depth from a value.
   *
   * @param temp The value.
   * @return The bit depth.
   */
  public static BitDepth getFromBitDepth(int temp) {
    for (BitDepth a : BitDepth.values()) {
      if (a.getDepth() == temp) {
        return a;
      }
    }
    return null;
  }

  /**
   * Gets the Code of this value.
   *
   * @return The code.
   */
  public byte getCodec() {
    return codec;
  }

  /**
   * Gets the depth of this value.
   *
   * @return The depth.
   */
  public int getDepth() {
    return depth;
  }
}
