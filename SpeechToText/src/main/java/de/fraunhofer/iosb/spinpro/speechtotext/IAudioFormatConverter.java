package de.fraunhofer.iosb.spinpro.speechtotext;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 * Interface to convert the {@link AudioFormat} of an {@link AudioInputStream} to another {@link AudioFormat}. This
 * interface is part of a strategy.
 */
interface IAudioFormatConverter {
  /**
   * Checks whether you can convert from {@code in} to {@code out}.
   *
   * @param in  The {@link AudioFormat} to convert from.
   * @param out The {@link AudioFormat} to convert to.
   * @return {@code true} if you can convert from {@code in} to {@code out}.
   */
  boolean isConvertable(AudioFormat in, AudioFormat out);

  /**
   * Converts the {@link AudioInputStream} {@code in} to the {@link AudioFormat} {@code format}.
   *
   * @param in     The {@link AudioInputStream} to convert.
   * @param format The {@link AudioFormat} to convert to.
   * @return A copy of the {@link AudioInputStream} {@code in} with the {@link AudioFormat} {@code format}.
   */
  AudioInputStream convert(AudioInputStream in, AudioFormat format);
}
