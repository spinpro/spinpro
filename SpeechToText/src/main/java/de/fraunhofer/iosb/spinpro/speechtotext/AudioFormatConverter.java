package de.fraunhofer.iosb.spinpro.speechtotext;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 * Implementation of {@link IAudioFormatConverter} using {@link AudioSystem}.
 */
class AudioFormatConverter implements IAudioFormatConverter {

  @Override
  public boolean isConvertable(AudioFormat in, AudioFormat out) {
    return AudioSystem.isConversionSupported(in, out);
  }

  @Override
  public AudioInputStream convert(AudioInputStream in, AudioFormat format) {
    return AudioSystem.getAudioInputStream(format, in);
  }
}
