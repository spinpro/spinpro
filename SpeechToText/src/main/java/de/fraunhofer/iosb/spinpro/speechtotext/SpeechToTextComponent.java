package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.annotations.Service;
import de.fraunhofer.iosb.spinpro.configuration.ConfigurationProvider;
import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceLoadingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;
import de.fraunhofer.iosb.spinpro.service.GenericServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.logging.Level;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

import static de.fraunhofer.iosb.spinpro.configuration.PreferenceStrings.SPEECH_TO_TEXT_JAR_PATH;

/**
 * The implementation of an speech-to-text-component.
 */
public class SpeechToTextComponent implements ISpeechToTextComponent {
  private final IMessageBundle bundle;
  private URL classpath;
  private IServiceProvider<ISpeechToTextService> sp;
  private ISpeechToTextService service;
  private AudioFormat[] supportedAudioFormats;
  private IAudioFormatConverter audioFormatConverter;
  private ILogger logger = null;

  /**
   * Constructs a new {@link SpeechToTextComponent} with the given {@link ILogger}. Uses always the first
   * class that is annotated with {@link Service}. You may have only
   * one class annotated with {@link Service}.
   *
   * @param logger The logger to use.
   * @throws ServiceLoadingException If the plugin could not be loaded properly.
   * @throws ConfigurationException  If the configuration could not be loaded properly.
   */
  public SpeechToTextComponent(ILogger logger) throws ServiceLoadingException, ConfigurationException {
    this.logger = logger;
    this.bundle = ConfigurationProvider.getMessageBundle();
    try {
      classpath = new File(ConfigurationProvider.getMainPreferences().getValue(SPEECH_TO_TEXT_JAR_PATH)).toURI().toURL();
    } catch (MalformedURLException | NullPointerException e) {
      logger.log(Level.SEVERE, Messages.CONFIG_FILE_NOT_FOUND);
      throw new ConfigurationException(bundle.getMessage(Messages.CONFIG_FILE_NOT_FOUND), e);
    }
    sp = new GenericServiceProvider<>(classpath, ISpeechToTextService.class, logger, bundle);
    Collection<IServiceWrapper<ISpeechToTextService>> coll = sp.getServices();
    if (coll.size() > 1) {
      logger.log(Level.WARNING, Messages.MORE_THAN_ONE_SERVICE_FOUND);
    }
    service = coll.iterator().next().getService();
  }

  /**
   * Constructs a new {@link SpeechToTextComponent} with the main {@link ILogger}.
   *
   * @throws ServiceLoadingException If the plugin could not be loaded properly.
   * @throws ConfigurationException  If the configuration could not be loaded properly.
   */
  public SpeechToTextComponent() throws ServiceLoadingException, ConfigurationException {
    this(ConfigurationProvider.getMainLogger());
  }

  /**
   * Testing only!.
   *
   * @param sp     The provider to use.
   * @param logger The logger to use.
   * @param bundle The bundle to use.
   */
  SpeechToTextComponent(IServiceProvider<ISpeechToTextService> sp, ILogger logger, IMessageBundle bundle) {
    this.logger = logger;
    this.sp = sp;
    service = sp.getServices().iterator().next().getService();
    this.bundle = bundle;
  }

  @Override
  public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat, ServiceInitializingException {
    return speechToText(ais, logger);
  }

  /**
   * Recognizes speech in an audio file and returns a {@code String} containing only valid characters.
   *
   * @param ais    The audio data to recognize speech in.
   * @param logger The logger to use.
   * @return The recognized speech as {@code String} with only valid characters defined in IPreferences.
   * @throws UnsupportedAudioFormat       Whenever an {@link AudioFormat} is not supported.
   * @throws ServiceInitializingException Whenever parsing fails.
   */
  public String speechToText(AudioInputStream ais, ILogger logger) throws UnsupportedAudioFormat, ServiceInitializingException {
    supportedAudioFormats = service.getSupportedAudioFormats();
    audioFormatConverter = new AudioFormatConverter();
    AudioInputStream out = ais;
    if (Arrays.stream(supportedAudioFormats).noneMatch(audioFormat -> ais.getFormat().matches(audioFormat))) {
      Optional<AudioFormat> af = Arrays.stream(supportedAudioFormats).filter(audioFormat -> audioFormatConverter.isConvertable(ais.getFormat(), audioFormat)).findFirst();
      if (af.isEmpty()) {
        logger.log(Level.WARNING, Messages.UNSUPPORTED_AUDIO_FORMAT);
        throw new UnsupportedAudioFormat(bundle.getMessage(Messages.UNSUPPORTED_AUDIO_FORMAT));
      }
      out = audioFormatConverter.convert(ais, af.get());
      logger.log(Level.INFO, Messages.AUDIO_FORMAT_CONVERTED);
    }
    String output = service.speechToText(out);
    logger.log(Level.FINEST, bundle.getMessage(Messages.SPEECH_TO_TEXT_RECOGNIZED_SENTENCE) + ": " + output);
    return output;
  }
}
