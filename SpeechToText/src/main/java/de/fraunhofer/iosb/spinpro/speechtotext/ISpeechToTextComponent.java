package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 * The interface for the Client-Interface-Component to communicate with the Speech-to-Text-Component.
 */
public interface ISpeechToTextComponent {
  /**
   * Recognizes speech in an audio file and returns a {@code String} containing only valid characters.
   *
   * @param ais The audio data to recognize speech in.
   * @return The recognized speech as {@code String} with only valid characters defined in IPreferences.
   * @throws UnsupportedAudioFormat       Whenever an {@link AudioFormat} is not supported.
   * @throws ServiceInitializingException Whenever parsing fails.
   */
  String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat, ServiceInitializingException;
}
