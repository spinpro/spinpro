package de.fraunhofer.iosb.spinpro;

import de.fraunhofer.iosb.spinpro.messages.IMessageBundle;
import de.fraunhofer.iosb.spinpro.messages.Messages;

public class MessageMock implements IMessageBundle {
  @Override
  public String getMessage(Messages msg) {
    return "LOGMOCK";
  }
}
