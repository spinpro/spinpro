package de.fraunhofer.iosb.spinpro;

import de.fraunhofer.iosb.spinpro.configuration.ILogger;
import de.fraunhofer.iosb.spinpro.messages.Messages;

import java.util.logging.Level;

public class LoggerMock implements ILogger {

  @Override
  public void log(Level ll, String s) {
    System.err.println(s);
  }

  @Override
  public void log(Level ll, Messages s) {
    System.err.println(s);
  }

  @Override
  public void log(Level ll, String message, Exception e) {
    System.err.println(message);
  }

  @Override
  public void log(Level ll, Messages message, Exception e) {
    System.err.println(message);
  }
}