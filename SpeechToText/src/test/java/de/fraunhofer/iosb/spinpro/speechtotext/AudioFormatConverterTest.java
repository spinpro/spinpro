package de.fraunhofer.iosb.spinpro.speechtotext;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class AudioFormatConverterTest {
  @Test
  public void convertableAudioformatCheck() {
    AudioFormatConverter afc = new AudioFormatConverter();
    AudioFormat in = new AudioFormat(44000, 32, 2, false, true);
    AudioFormat out = new AudioFormat(16000, 16, 1, false, true);
    assertTrue(afc.isConvertable(in, out));
  }

  @Test
  public void convertAudioFormatWAV44HzTO16Hz() {
    AudioFormatConverter afc = new AudioFormatConverter();
    AudioFormat target = new AudioFormat(16000, 16, 1, false, false);
    String a = new File("").getAbsoluteFile().getPath();
    File testfile = new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel.wav");
    AudioInputStream ais = null;
    try {
      ais = AudioSystem.getAudioInputStream(testfile);
    } catch (UnsupportedAudioFileException | IOException e) {
      e.printStackTrace();
      fail();
    }
    AudioInputStream out = afc.convert(ais, target);

    assertTrue(out.getFormat().matches(target));
    try {
      assertTrue(out.readAllBytes().length > 0);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }

  }
}