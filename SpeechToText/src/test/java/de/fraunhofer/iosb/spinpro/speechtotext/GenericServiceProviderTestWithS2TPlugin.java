package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.LoggerMock;
import de.fraunhofer.iosb.spinpro.MessageMock;
import de.fraunhofer.iosb.spinpro.exceptions.ConfigurationException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceLoadingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import de.fraunhofer.iosb.spinpro.service.GenericServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class GenericServiceProviderTestWithS2TPlugin {
  @Test
  public void genericTestWithS2TMock() throws ServiceLoadingException, MalformedURLException, UnsupportedAudioFormat, ConfigurationException, ServiceInitializingException {
    URL url = null;
    File file = new File("src/test/resources/jars/MockSpeechToTextService-0.1.jar");
    url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    assertTrue("if this teststring is right then pass the test.".equals(wrapper.getService().speechToText(null)));
    assertEquals("MockPlugin", wrapper.getName());
    assertEquals("9bc9e34875ddde9e66b0803016f99d538b7c6e1472b12f5d74d405e9953f779a", wrapper.getCrypticId());
    assertEquals("Jochen Mueller", wrapper.getAuthor());
    assertEquals("Fraunhofer IOSB", wrapper.getOrganisation());
    assertEquals("0.1", wrapper.getVersion());
    assertEquals("MockPlugin:0.1:Jochen Mueller:Fraunhofer IOSB:9bc9e34875ddde9e66b0803016f99d538b7c6e1472b12f5d74d405e9953f779a", wrapper.toString());
    assertEquals("a8bbb584b2e5b6c58c3b4cb79d1700503e7d38496613fa31d00869e9028c01d9", wrapper.getHash());
  }

  @Test
  public void loadCMUSphinx() throws IOException, UnsupportedAudioFileException, UnsupportedAudioFormat, ServiceLoadingException, ConfigurationException, ServiceInitializingException {
    AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel2.wav"));
    String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
        "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";
    File file = new File("src/test/resources/jars/CMUSphinxService-0.1-all.jar");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    ISpeechToTextService service = wrapper.getService();

    String out = service.speechToText(testWav);
    System.out.println("Output of CMU: " + out);

    double distance = StringUtils.getLevenshteinDistance(test, out);
    System.out.println("Failrate: " + distance / test.length());
    assertTrue(distance / test.length() < 0.41d);
    assertEquals("CMUSphinxEN", wrapper.getName());
    assertEquals("3cd645636fbffb53a28f4ade2b0a5dd1e8a53dfc2a1781fbb21c05ba1a219135", wrapper.getCrypticId());
    assertEquals("Jochen Mueller", wrapper.getAuthor());
    assertEquals("Fraunhofer IOSB", wrapper.getOrganisation());
    assertEquals("0.1", wrapper.getVersion());
    assertEquals("CMUSphinxEN:0.1:Jochen Mueller:Fraunhofer IOSB:3cd645636fbffb53a28f4ade2b0a5dd1e8a53dfc2a1781fbb21c05ba1a219135", wrapper.toString());
    assertEquals("9a3d5bfd4d07378115a21f5ba2fadc0fa42f39068c5458a3849994815104d448", wrapper.getHash());
    WrapperMock a = new WrapperMock(wrapper.getName(), wrapper.getCrypticId(), wrapper.getAuthor(), wrapper.getOrganisation(), wrapper.getVersion());
  }

  @Test
  @Disabled
  public void loadDeepSpeech() throws IOException, UnsupportedAudioFileException, UnsupportedAudioFormat, ServiceLoadingException, ConfigurationException, ServiceInitializingException {
    AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel2.wav"));
    String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
        "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";
    File file = new File("src/test/resources/jars/DeepSpeechService-0.1-all.jar");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    ISpeechToTextService service = wrapper.getService();

    String out = service.speechToText(testWav);
    System.out.println("Output of DeepSpeech: " + out);

    double distance = StringUtils.getLevenshteinDistance(test, out);
    System.out.println("Failrate: " + distance / test.length());
    assertTrue(distance / test.length() < 0.07d);
  }

  @Test
  @Disabled
  public void loadDeepSpeechMultiThread() throws IOException, UnsupportedAudioFileException, UnsupportedAudioFormat, ServiceLoadingException, ConfigurationException, ServiceInitializingException, InterruptedException {
    File file = new File("src/test/resources/jars/DeepSpeechService-0.1-all.jar");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    ISpeechToTextService service = wrapper.getService();
    Runnable runnable = new Runnable() {
      @Override
      public void run() {
        AudioInputStream testWav = null;
        try {
          testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel2.wav"));
        } catch (UnsupportedAudioFileException | IOException e) {
          e.printStackTrace();
          fail();
        }
        String test = "it almost looks as though the wheel is weightless how does this work well instead of pulling the wheel\n" + "\tdown to the ground as you d expect the weight of the wheel creates a torque which pushes it around as a circle\n" +
            "\tyou may recognise this as gyroscopic precession for a more detailed explanation click the annotation or the\n" + "\tlink in the description to see my video on the topic here\n";
        String out = null;
        try {
          out = service.speechToText(testWav);
        } catch (UnsupportedAudioFormat | ServiceInitializingException unsupportedAudioFormat) {
          unsupportedAudioFormat.printStackTrace();
          fail();
        }
        System.out.println("Output of DeepSpeech: " + out);

        double distance = StringUtils.getLevenshteinDistance(test, out);
        System.out.println("Failrate: " + distance / test.length());
        assertTrue(distance / test.length() < 0.07d);
      }
    };

    Thread[] threads = new Thread[8];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new Thread(runnable);
      threads[i].start();
    }

    for (Thread t : threads) {
      t.join();
    }


  }


  public void test1point4() throws IOException, UnsupportedAudioFileException, UnsupportedAudioFormat, ServiceLoadingException, ConfigurationException, ServiceInitializingException {

    AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/1.4 test.wav"));
    File file = new File("src/test/resources/jars/CMUSphinxService-0.1-all.jar");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    ISpeechToTextService service = wrapper.getService();

    String out = service.speechToText(testWav);
    System.out.println("Output of CMU: " + out);
  }


  public void test1point4DeepSpeech() throws IOException, UnsupportedAudioFileException, UnsupportedAudioFormat, ServiceLoadingException, ConfigurationException, ServiceInitializingException {
    AudioInputStream testWav = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/1.4 test.wav"));
    File file = new File("src/test/resources/jars/DeepSpeechService-0.1-all.jar");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    IServiceWrapper<ISpeechToTextService> wrapper = s2tp.getServices().iterator().next();
    ISpeechToTextService service = wrapper.getService();

    String out = service.speechToText(testWav);
    System.out.println("Output of DeepSpeech: " + out);
  }

  @Test
  public void loadFolder() throws MalformedURLException, ServiceLoadingException {
    File file = new File("src/test/resources/jars");
    URL url = file.toURI().toURL();
    IServiceProvider<ISpeechToTextService> s2tp = new GenericServiceProvider<ISpeechToTextService>(url, ISpeechToTextService.class, new LoggerMock(), new MessageMock());
    Collection<IServiceWrapper<ISpeechToTextService>> services = s2tp.getServices();
    assertTrue(services.stream().anyMatch(service -> {
      try {
        return service.getHash().equals("9a3d5bfd4d07378115a21f5ba2fadc0fa42f39068c5458a3849994815104d448");
      } catch (ConfigurationException e) {
        e.printStackTrace();
        return false;
      }
    }));
    assertTrue(services.stream().anyMatch(service -> {
      try {
        return service.getHash().equals("a8bbb584b2e5b6c58c3b4cb79d1700503e7d38496613fa31d00869e9028c01d9");
      } catch (ConfigurationException e) {
        e.printStackTrace();
        return false;
      }
    }));
  }

  @Test
  public void wrongURL() throws MalformedURLException, ServiceLoadingException {
    assertThrows(ServiceLoadingException.class, () -> new GenericServiceProvider<ISpeechToTextService>(new URL("file://"), ISpeechToTextService.class, new LoggerMock(), new MessageMock()));
  }

  @Test
  public void noJars() throws MalformedURLException, ServiceLoadingException {
    File file = new File("src/test/resources/de");
    URL url = file.toURI().toURL();
    assertThrows(ServiceLoadingException.class, () -> new GenericServiceProvider<ISpeechToTextService>(file.toURI().toURL(), ISpeechToTextService.class, new LoggerMock(), new MessageMock()));
  }

  @Test
  public void failingJars() throws MalformedURLException, ServiceLoadingException {
    File file = new File("src/test/resources/failingJars");
    URL url = file.toURI().toURL();
    assertThrows(ServiceLoadingException.class, () -> new GenericServiceProvider<ISpeechToTextService>(file.toURI().toURL(), ISpeechToTextService.class, new LoggerMock(), new MessageMock()));
  }

  static class WrapperMock implements IServiceWrapper<ISpeechToTextService> {

    private String name;
    private String crypticId;
    private String author;
    private String organisation;
    private String version;

    public WrapperMock(String name, String crypticId, String author, String organisation, String version) {
      this.name = name;
      this.crypticId = crypticId;
      this.author = author;
      this.organisation = organisation;
      this.version = version;
    }

    @Override
    public ISpeechToTextService getService() {
      return null;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public String getCrypticId() {
      return crypticId;
    }

    @Override
    public String getAuthor() {
      return author;
    }

    @Override
    public String getOrganisation() {
      return organisation;
    }

    @Override
    public String getVersion() {
      return version;
    }

    @Override
    public String getHash() throws ConfigurationException {
      return null;
    }
  }
}