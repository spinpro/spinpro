package de.fraunhofer.iosb.spinpro.speechtotext;

import de.fraunhofer.iosb.spinpro.LoggerMock;
import de.fraunhofer.iosb.spinpro.MessageMock;
import de.fraunhofer.iosb.spinpro.exceptions.ServiceInitializingException;
import de.fraunhofer.iosb.spinpro.exceptions.UnsupportedAudioFormat;
import de.fraunhofer.iosb.spinpro.service.IServiceProvider;
import de.fraunhofer.iosb.spinpro.service.IServiceWrapper;
import de.fraunhofer.iosb.spinpro.service.ServiceWrapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class SpeechToTextComponentTest {
  private ISpeechToTextComponent component;
  private AudioInputStream ais = AudioSystem.getAudioInputStream(new File("src/test/resources/de/fraunhofer/iosb/spinpro/speechtotext/Anti-GravityWheel.wav"));

  public SpeechToTextComponentTest() throws IOException, UnsupportedAudioFileException {
  }

  @Test
  public void testWithLocalMock() throws ServiceInitializingException {
    component = new SpeechToTextComponent(new LocalMockProvider(), new LoggerMock(), new MessageMock());
    try {
      assertEquals("if this test fails this is wrong", component.speechToText(ais));
    } catch (UnsupportedAudioFormat unsupportedAudioFormat) {
      unsupportedAudioFormat.printStackTrace();
      fail();
    }
  }

  class LocalMockProvider implements IServiceProvider<ISpeechToTextService> {

    @Override
    public Collection<IServiceWrapper<ISpeechToTextService>> getServices() {
      List<IServiceWrapper<ISpeechToTextService>> temp = new ArrayList<>();
      temp.add(new ServiceWrapper<ISpeechToTextService>(new LocalMock(), null, null, null, null, null));
      return temp;
    }
  }

  class LocalMock implements ISpeechToTextService {

    @Override
    public String speechToText(AudioInputStream ais) throws UnsupportedAudioFormat {
      return "if this test fails this is wrong";
    }

    @Override
    public AudioFormat[] getSupportedAudioFormats() {
      return new AudioFormat[]{ais.getFormat()};
    }
  }
}